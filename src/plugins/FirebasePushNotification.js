import { NativeModules, DeviceEventEmitter, Platform } from 'react-native';

const module = NativeModules.FirebasePushNotificationModule;

class FirebasePushNotificacion {

    constructor() {
        if (Platform.OS === 'android') {

            DeviceEventEmitter.addListener("firebaseprushnotificacionrefreshtoken", token =>{
                if (this.onTokenRefreshCallback) this.onTokenRefreshCallback( token );
            });

            DeviceEventEmitter.addListener("FirebasePushNotificationPayload", payload => {
                const getParams = payload => eval("("+ payload +")");
                const params = getParams(payload);
                if (this.onPayloadReceiverCallback) this.onPayloadReceiverCallback( params );
            });

        }   
    }

    /**
     * Función que devuelve el token actual.
     */
    getToken(callback) {
        if (Platform.OS === 'android') {
            module.getToken( token => callback(token) );
        }        
    }

    /**
     * Evento que se ejecuta al actualizar el token
     */
    onTokenRefresh(callback) {
        if (Platform.OS === 'android') {
            this.onTokenRefreshCallback = callback;
        }        
    }

    onPayloadReceiver(callback) {
        if (Platform.OS === 'android') {
            this.onPayloadReceiverCallback = callback;
        }        
    }

    /**
     * Función que le indica al módulo java. que ReactNative se encuentra dentro
     * del screen del chat.
     */
    setIntoChats(intoChats: boolean) {
        if (Platform.OS === 'ios') return;
        module.setIntoChats(intoChats);
    }

    /**
     * devuelve el payload almacenado en el dispositivo.
     */
    getPayload(callback) {
        if (Platform.OS === 'android') {
            module.getPayload( payload => callback( eval("("+ payload +")") ) );
        }        
    }

    clearPayload() {
        if (Platform.OS === 'android') {
            module.clearPayload();
        }        
    }

    setLogin(login: boolean) {
        if (Platform.OS === 'ios') return;
        module.setLogin(login);
    }

}

export default new FirebasePushNotificacion();