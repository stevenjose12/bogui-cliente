import PushNotification from "react-native-push-notification";
import { Platform, NativeModules, PushNotificationIOS } from 'react-native';

const showPayload = (title,message,payload = {}) => {
	if (Platform.OS == 'ios') {
		PushNotificationIOS.requestPermissions();
		PushNotification.localNotification({
	      message,
	      userInfo: payload
	    });
	}
	else {
		const { Notification } = NativeModules;
		Notification.showPayload(title,message,payload);
	}	
}

export default {
	showPayload
}