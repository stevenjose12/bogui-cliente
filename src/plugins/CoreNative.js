import {NativeModules} from 'react-native';

const module = NativeModules.CoreNative;

export default {
    start: (userId: number, baseUrlSocket: string) => module.start(userId, baseUrlSocket),
    stop: () => module.stop()
};
