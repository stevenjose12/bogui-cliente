function shownWelcomeModal(state = false, action) {
    if (action.type === 'SET_SHOWN_WELCOME_MODAL') {
        return { ...action.payload }
    }
    return state
}

export default shownWelcomeModal
