function night(state = false, action) {

    if (action.type === 'SET_NIGHT') return { ...action.payload };
    return state;

}

export default night;