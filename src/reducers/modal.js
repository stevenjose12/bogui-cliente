function modal(state = false, action) {

    if (action.type === 'SET_MODAL') return {...action.payload,};
    return state;

}

export default modal;