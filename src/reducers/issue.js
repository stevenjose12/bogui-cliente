function issue(state = false, action) {

    if (action.type === 'SET_ISSUE') return { ...action.payload };
    return state;

}

export default issue;