function chronometer(state = false, action) {

    if (action.type === 'SET_TIME') return { ...action.payload };
    return state;

}

export default chronometer;