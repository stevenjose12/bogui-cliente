function route(state = null, action) {

    if (action.type === 'SET_LAST_ROUTE') return action.payload;
    return state;

}

export default route;