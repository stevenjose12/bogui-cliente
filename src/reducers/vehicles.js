function auth(state = false, action) {
    switch (action.type) {
        case 'SET_VEHICLES':
            return {
                ...action.payload,
                isStorageVehicles: !!action.payload
            };
        default:
            return state;
    }
}
export default auth;