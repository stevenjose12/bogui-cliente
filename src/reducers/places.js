function places(state = false, action) {

    if (action.type === 'SET_PLACES') return { ...action.payload };
    return state;

}

export default places;