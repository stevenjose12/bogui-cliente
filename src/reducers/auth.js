function auth(state = false, action) {

    switch (action.type) {

        case 'SET_USER':
            return {
                ...action.payload,
                isAuth: !!action.payload
            };

        case 'UPDATE_USER':
            state.user = action.payload.user;
            return state;

        default:
            return state;

    }

}

export default auth;