import { combineReducers } from 'redux';
import navigation from "./NavigationReducer"
import phonecodes from './PhoneCodes';
import vehicles from './vehicles';
import auth from "./auth";
import modal from "./modal";
import places from "./places";
import request from "./request";
import route from "./route";
import chronometer from "./chronometer";
import showModalDriver from "./showModalDriver";
import night from "./night";
import issue from "./issue";
import shownWelcomeModal from "./shownWelcomeModal";
import panic from './panic';
import lastRoute from "./lastRoute";

const reducer = combineReducers({
    lastRoute: lastRoute,
    nav: navigation,
    auth: auth,
    phonecodes: phonecodes,
    modal: modal,
    places: places,
    vehicles: vehicles,
    request: request,
    route: route,
    chronometer: chronometer,
    showModalDriver: showModalDriver,
    night: night,
    issue: issue,
    shownWelcomeModal: shownWelcomeModal,
    panic: panic,
});

export default reducer;