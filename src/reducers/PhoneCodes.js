function auth(state = false, action) {
    switch (action.type) {
        case 'SET_PHONECODE':
            return {
                ...action.payload,
                isStoragePhoneCodes: !!action.payload
            };

        default:
            return state;
    }
}
export default auth;