function panic(state = false, action) {

    if (action.type === 'SET_PANIC') return { ...action.payload };
    return state;

}

export default panic;