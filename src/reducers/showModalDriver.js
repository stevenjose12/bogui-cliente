function showModalDriver(state = false, action) {

    if (action.type === 'SET_SHOW_MODAL_DRIVER') return { ...action.payload };
    return state;

}

export default showModalDriver;