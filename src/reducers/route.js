function route(state = false, action) {

    if (action.type === 'SET_ROUTE') return { ...action.payload };
    return state;

}

export default route;