function request(state = false, action) {

    if (action.type === 'SET_REQUEST') return { ...action.payload };
    return state;

}

export default request;