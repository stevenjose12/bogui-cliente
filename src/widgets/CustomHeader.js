import React, { Component } from 'react';
import {Image, StyleSheet, TouchableOpacity, Text, View, TouchableWithoutFeedback} from 'react-native';
import Hbox from "./Hbox";
import BackButton from "../assets/icons/back_button.png";
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from "../assets/Color";
import {Button} from "react-native-elements";

class CustomHeader extends Component {

    render() {
        return (
            <Hbox style={ styles.container }>

                {
                    this.props.backType
                        ?
                        <TouchableWithoutFeedback onPress={ () => this.props.onPress ? this.props.onPress() : null }>

                            <View style={ styles.backButtonContainer }>
                                <Image
                                    source={ BackButton }
                                    style={ styles.image } />
                            </View>

                        </TouchableWithoutFeedback>
                        :
                        <Button
                            buttonStyle={ styles.button }
                            type="clear"
                            icon={
                                <Icon
                                    color={ Colors.accent }
                                    size={ 20 }
                                    name={ 'bars' } />
                            }
                            onPress={ () => this.props.onPress ? this.props.onPress() : null } />

                }

                <Text
                    numberOfLines={ 1 }
                    style={ styles.title } >
                    { this.props.title ? this.props.title : "" }
                </Text>

                <View style={ styles.backButtonContainer } >
                    { this.props.rightButton }
                </View>

            </Hbox>
        );

    };

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: 40,
        paddingHorizontal: 5,
        borderBottomColor: Colors.accent,
        borderBottomWidth: 1,
        backgroundColor: Colors.white
    },
    button: {
        marginLeft: 0,
        width: 40,
        height: 40,
        borderRadius: 0
    },
    image: {
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginLeft: 5
    },
    title: {
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center',
        color: Colors.primary,
        fontFamily: 'Poppins-Bold',
        flex: 1,
        height: '100%'
    },
    backButtonContainer: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default CustomHeader;