import React from 'react';
import { Button } from "react-native-elements";

const ButtonBogui = props => (

    <Button
        onPress={ props.onPress }
        title={ props.title }
        titleStyle={ { fontFamily: 'Poppins', width: '100%', ...props.titleStyle } }
        containerStyle={ props.containerStyle }
        buttonStyle={ { height: 40, paddingVertical: 3, ...props.buttonStyle } }
        type={ props.type }
        loading={ props.loading }
        disabled={ props.disabled } />

);

export default ButtonBogui;