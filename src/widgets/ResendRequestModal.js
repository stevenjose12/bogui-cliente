import React from 'react';
import {Image, Modal, Text, View } from "react-native";
import Colors from "../assets/Color";
import Hbox from './Hbox';
import Button from './Button';

const ResendRequestModal = props => (
    <Modal
        animationType="slide"
        transparent
        visible={ props.visible }
        useNativeDriver>
        <View style={styles.container}>
            <View style={styles.bodyContainer}>
                <View style={styles.header}>
                    <Image
                        source={require('../assets/icons/warning.png')}
                        style={{height: '35%'}}
                        resizeMode="contain" />
                    <Text style={styles.title}>Su Solicitud no ha sido aceptada</Text>
                </View>
                <Text style={styles.textItem}>¿ Desea enviar nuevamente ?</Text>
                <Hbox style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Button
                        title="Sí"
                        containerStyle={styles.button.containerStyle}
                        titleStyle={styles.button.titleStyle}
                        buttonStyle={styles.button.buttonStyle}
                        onPress={props.onPositiveButton} />
                    <Button
                        title="No"
                        containerStyle={styles.button.containerStyle}
                        titleStyle={styles.button.titleStyle}
                        buttonStyle={styles.button.buttonStyle}
                        onPress={props.onNegativeButton} />
                </Hbox>
            </View>
        </View>
    </Modal>
);

const styles = {
    container: {
        width: "100%",
        height: '100%',
        backgroundColor: 'rgba(0, 0, 0, .80)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    header:{
        justifyContent: 'center',
        alignItems: 'center',
        width: '80%',
    },
    button:{
        titleStyle:{
            color: Colors.primary
        },
        buttonStyle:{
            backgroundColor: 'transparent',
            borderRadius: 20 ,
            borderColor: Colors.primary,
            borderWidth: 1
        },
        containerStyle:{
            padding: 5,
            width: '40%'
        },
    },
    bodyContainer: {
        width: '90%',
        height: '50%',
        borderRadius: 20,
        backgroundColor: 'rgba(255, 255, 255, .90)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textItem: {
        textAlign: 'center',
        fontSize: 15,
        paddingHorizontal: 20,
        color: Colors.primary,
        fontFamily: 'Poppins-SemiBold',
    },
    title:{
        fontFamily: 'Poppins',
        color: Colors.primary,
        fontSize: 21,
        textAlign: 'center',
        borderBottomColor: Colors.accent,
        borderBottomWidth: 1,
    }
};

export default ResendRequestModal;