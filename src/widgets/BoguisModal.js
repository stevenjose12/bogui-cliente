import React, { Component } from 'react';
import {Modal, StyleSheet, Text, View, Image, TouchableWithoutFeedback} from "react-native";
import { Button } from 'react-native-elements';
import Colors from '../assets/Color';
import Input from './InputBogui';
import Api from '../utils/Api';
import Hbox from './Hbox';
import CustomButton from './Button';
import CurstomModal from '../utils/Modal';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { connect } from "react-redux";
import G from '../utils/G';
import ENV from '../utils/env';
import Color from '../assets/Color';

class BoguisModal extends Component{

    state={
        vehicles:[],
        visible: false
    };

    componentDidMount() {
        this.getVehicles();
    }

    getVehicles = async () => {
        try {
            
            const r = await Api.vehiclesAvailable();
            if (r) this.setState(state => state.vehicles = r);
            else CurstomModal.showGenericError();

        } catch (e) {
            CurstomModal.showGenericError(e);
        }

    };

    show(show: boolean) {
        this.setState( { visible: show } );
    }

    render() {
        return (

            <Modal
                animationType="slide"
                transparent={ true }
                visible={ this.state.visible }
                useNativeDriver={ true }>

                <View style={styles.container} >

                    <Hbox>
                        <View style={{flex: 1, width: '100%'}}>
                        </View>
                            <Button
                                buttonStyle={ styles.button }
                                type="clear"
                                icon={
                                    <Icon
                                        color={ Colors.secondary }
                                        size={ 32 }
                                        outline={true}
                                        name={ 'times-circle' } />
                                }
                                onPress={ () => this.show(false) } />
                    </Hbox>

                    <View style={{width: '80%',flex: 1, alignItems: 'center',justifyContent: "center"}}>

                    <Image
                        source={require('../assets/icons/isotipo.png')}
                        style={{width: 80, height: 80, marginBottom: 20}}
                        resizeMode="contain" />

                    <Text style={ styles.title}>Selecciona tu Bogui</Text>

                    {

                        this.state.vehicles.map( (element, i) =>
                            (

                                <View styles={ { width: '100%', height: 40, borderWidth: 1, borderColor: 'red' } }>

                                        {/*<View style={{ width: '100%',alignItems: 'center', justifyContent: 'center'}}>*/}
                                            <View style={{margin: 10, width: '100%', borderColor: 'black', borderWidth: 1, borderRadius: 5, height: 40, alignItems: 'center',justifyContent: 'center', flexDirection: 'row'}}>
                                                <Image
                                                    source={{uri: ENV.BASE_PUBLIC+element.photo}}
                                                    style={{width: 54, height: 22}}
                                                    resizeMode="contain" />
                                                <Text style={{textAlign: 'right', fontSize: 15, color: Color.primary,}}> {element.name} </Text>
                                            </View>
                                        {/*</View>*/}

                                    {/*</TouchableWithoutFeedback>*/}

                                    <Text style={{color: Color.primary, textAlign: 'center', fontSize: 15}}> {element.description} </Text>

                                </View>
                            )
                        )

                    }
                    </View>
                </View>
            </Modal>

        )
    }
} 

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(255, 255, 255, .80)',
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        height: '100%',
        padding: 5
    },
    buttonStyleWhite:{
        backgroundColor: 'transparent',
        borderColor: Colors.primary,
        borderWidth: 1,
        borderRadius: 7
    },
    titleBlack:{
        color: Colors.primary,
        padding: 25
    },
    titleStyle:{
        color: Colors.white,
        padding: 10
    },
    bottomButtons:{
        width: '48%',
        paddingVertical: 12
    },
    buttonStyleBlack:{
        backgroundColor: Colors.primary,
        borderColor: Colors.primary,
        borderWidth: 1,
        borderRadius: 7
    },
    title:{
        fontFamily: 'Poppins',
        color: Colors.primary,
        fontSize: 24,
        fontWeight: 'bold',
    },
    iconBanner: {
        width: "100%",
        height: "100%",
    },
    back: {
        width: 55,
        height: 55,
        position: "absolute",
        alignSelf: "flex-start",
        padding: 12,
        zIndex: 1
    }
});
const redux = state => { return { user: state.auth.user } };
export default connect(redux)(BoguisModal);