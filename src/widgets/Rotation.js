import React from 'react';
import { Modal, StyleSheet, View, Text, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Button } from 'react-native-elements';

class Rotation extends React.Component {

	state = {
		show: false,
		degrees: 0,
		image: null
	};

	show = image => this.setState( {
		degrees: 0,
		image,
		show: true
	} );

	rotate = () => this.setState({
		degrees: this.state.degrees + 90
	});

	render() {
		return (

			<Modal
				visible={ this.state.show }
				animationType="slide"
				useNativeDriver >

				{/* HEADER */}

                <View style={ styles.buttonContainer }>
                    <Text style={ styles.title }>
						Rotar Imagen
                    </Text>
					{/* CLOSE MODAL */}
                    <Button
                        buttonStyle={ styles.close }
                        type="clear"
                        onPress={ () => this.setState( { show: false } ) }
                        icon={
                            <Icon
								color={ 'gray' }
								size={ 25 }
								name="close" />
                        } />
                </View>

				<View style={{alignItems: 'center', justifyContent: 'center'}}>
					<Image
						source={ { uri: this.state.image } }
						style={ [styles.image,{
							transform: [{
								rotate: this.state.degrees + 'deg'
							}]
						}] } />
					<View style={ styles.containerButtons }>

						<Button
							onPress={ () => {
								this.props.onSave?.(this.state.degrees);
								this.setState( { show: false } );
							} }
							title="Guardar"
							buttonStyle={ styles.buttonStyle }
							titleStyle={ styles.titleStyle } />

						<Button
							onPress={ this.rotate }
							title="Rotar"
							buttonStyle={ styles.buttonStyle }
							titleStyle={ styles.titleStyle } />

					</View>
                </View>
			</Modal>
		)
	}
}

const styles = StyleSheet.create({
	buttonContainer: {
		flexDirection: 'row',
		width: '100%',
		borderBottomWidth: 2,
		borderBottomColor: 'red',
		paddingBottom: 5
	},
	close: {
		width: 50,
		position: 'absolute',
		right: 5
	},
	title: {
		fontSize: 16,
		textAlign: 'center',
		width: '100%',
		marginTop: 10,
		color: '#000'
	},
	containerButtons: {
		flexDirection: 'row'
	},
	buttonStyle: {
		width: 100,
		borderWidth: 2,
		borderColor: '#000',
		backgroundColor: 'transparent',
		paddingVertical: 5,
		margin: 10
	},
	titleStyle: {
		color: '#000'
	},
	image: {
		width: 200,
		height: 200,
		resizeMode: 'cover',
		borderRadius: 5,
		marginTop: 20,
		marginBottom: 20
	}
});

export default Rotation;
