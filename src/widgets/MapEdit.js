import React, { Component } from "react";
import {ActivityIndicator, Image, StyleSheet, View} from "react-native";
import MapView from "react-native-maps";
import Color from "../assets/Color";
import G from "../utils/G";
import Night from "../assets/mapanocturno";
import Day from "../assets/mapadiurno";
import {connect} from "react-redux";

class MapEdit extends Component {

    render() {
        return (
            <View style={ { ...styles.container, ...this.props.style } }>

                {/* HEADER */}
                <View style={{ ...styles.header, ... this.props.styleHeader}}>
                    { this.props.header }
                </View>

                {/* MARKER CENTER */}
                {
                    this.props.marker
                        ?
                        <Image
                            source={ this.props.marker }
                            style={ styles.marker } />
                        : null
                }

                {
                    this.props.center
                        ?
                        <MapView
                            ref={ map => this.map = map }
                            style={ styles.map }
                            customMapStyle={ G.isNight(this.props.time) ? Night : Day }
                            camera={
                                {
                                    center: this.props.center,
                                    pitch: 1,
                                    heading: 1,
                                    zoom: 15,
                                    altitude: 15,
                                }
                            }
                            provider="google"
                            zoomControlEnabled
                            zoomEnabled
                            toolbarEnabled
                            loadingEnabled
                            onPress={ e => this.props.onPress ? this.props.onPress(e.nativeEvent) : null }
                            onRegionChangeComplete={ e => this.props.onDragEnd ? this.props.onDragEnd(e) : null }>

                        </MapView>
                        :
                        <View style={ { width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' } }>
                            <ActivityIndicator />
                        </View>

                }

            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        position: "relative",
        width: '100%',
        backgroundColor: Color.white,
        flex: 1
    },
    map: {
        width: "100%",
        height: "100%",
    },
    header: {
        width: '100%',
        zIndex: 2,
        position: "absolute",
        top: 0,
    },
    marker: {
        zIndex: 1,
        position: "absolute"
    },
});

const redux = state => {
    return {
        time: state.night.night
    }
};

export default connect(redux, null, null, { forwardRef: true })(MapEdit);

// export default MapEdit;