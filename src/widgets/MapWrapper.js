import React from 'react';
import MapView from "react-native-maps";
import Day from '../assets/mapadiurno.json';
import Night from '../assets/mapanocturno.json';
import G from '../utils/G';
import {connect} from "react-redux";

const MapWrapper = React.forwardRef((props, ref) => (
    <MapView
        ref={ ref }
        { ...props }
        customMapStyle={ G.isNight(props.night) ? Night : Day  }>
        { props.children }
    </MapView>
));

const redux = state => {
    return {
        time: state.night.night
    }
};

export default connect(redux, null, null, { forwardRef: true })(MapWrapper);
// export default MapWrapper;