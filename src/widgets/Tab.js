import React from 'react'
import {Text, TouchableHighlight, StyleSheet} from "react-native";
import Color from "../assets/Color";

const Tab = props => (
    <TouchableHighlight
        onPress={ props.onPress }
        style={ [styles.container, props.selected ? styles.selected : {} ] }
        underlayColor="transparent">
        <Text style={ props.selected ? styles.textSelected : {} }>
            { props.text }
        </Text>
    </TouchableHighlight>
);

const styles = StyleSheet.create( {
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        borderBottomColor: Color.secondary,
        borderBottomWidth: 0.5,
        flex: 1,
        backgroundColor: Color.white
    },
    selected: {
        backgroundColor: Color.secondary
    },
    textSelected: {
        fontFamily: 'Poppins-SemiBold',
        color: Color.white
    }
} );

export default Tab;