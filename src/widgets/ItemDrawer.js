import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import Color from "../assets/Color";

const ItemDrawer = props => (
    <TouchableOpacity onPress={ () => props.onPress ? props.onPress() : null }>
        <View style={ styles.container }>
            <View style={ styles.containerIcon }>
                <Image style={ styles.icon } source={ props.icon } resizeMode="contain" />
            </View>
            <Text style={styles.text}>
                { props.text }
            </Text>
        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        width: "100%",
        height: 50,
        paddingVertical: 5,
    },
    containerIcon: {
        width: 40,
        height: 40,
        padding: 5,
    },
    icon: {
        width: "100%",
        height: "100%",
        tintColor: '#fff'
    },
    text: {
        width: '100%',
        color: Color.white,
        textAlignVertical: 'center',
        paddingLeft: 10,
        paddingTop: Platform.OS == 'ios' ? 10 : null
    }
});

export default ItemDrawer;