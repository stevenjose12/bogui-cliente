import React, {Component} from 'react';
import {Modal, StyleSheet, Text, View, TouchableOpacity, ActivityIndicator} from "react-native";
import Colors from '../assets/Color';

class ModalAlertSmart extends Component {

    state = {
        title: null,
        visible: false,
        message: "",
        textCancelButton: null,
        textPositiveButton: null,
        cancelable: true,
        time: 0
    };

    show = (body=null) => {
        this.setState(state => state.visible = true);
        if (body) {
            this.message(body.message);
            this.title(body.title);
            this.cancelable(body.cancelable ? body.cancelable : true);
            this.setState( { time: body.time || 0 });
        }
        this.setState( { visible: true } );

        if (this.state.time) setTimeout(this.dismiss, this.state.time);

        if (this.props.loading) this.cancelable(false);

    };

    dismiss = () => {
        this.setState( { visible: false } );
        this.setState( { textCancelButton: null } );
        this.setState( { textPositiveButton: null } );
    };

    title = title => this.setState( { title } )

    message = message => this.setState( { message } )

    cancelable = (cancelable: boolean) => this.setState( { cancelable } );

    onCancelButton = (textCancelButton, cancelButtonCallback) => {
        this.setState( { textCancelButton } );
        this.cancelButtonCallback = cancelButtonCallback;
    };

    onPositiveButton = (textPositiveButton, positiveButtonCallback) => {
        this.setState( { textPositiveButton } );
        this.positiveButtonCallback = positiveButtonCallback;
    };

    render() {
        return (
            <Modal
                animationType={this.props.loading ? 'fade' : "slide"}
                transparent={ true }
                visible={ this.state.visible }
                hideModalContentWhileAnimating={ true }
                useNativeDriver={ true }>

                <TouchableOpacity
                    style={ {...styles.container, ...this.props.style} }
                    onPress={ () => {
                        if (this.state.cancelable) this.dismiss();
                    } }>

                    <View style={[styles.card, this.props.loading ? null : {borderRadius: 15}]}>

                        {/* TITLE */}
                        {
                            this.state.title && (
                                <Text style={styles.title}>
                                    { this.state.title }
                                </Text>
                            )
                        }

                        {/* BODY */}
                        {

                            this.props.loading
                                ?
                                <View style={ { flexDirection: 'row', padding: 10 } }>
                                    <ActivityIndicator />
                                    <Text style={ { paddingLeft: 10 } }>
                                        { this.props.loading }
                                    </Text>
                                </View>
                                :
                                this.props.children
                                    ?
                                    this.props.children
                                    :
                                    <Text style={ styles.message }>
                                        { this.state.message }
                                    </Text>

                        }

                        {/* BUTTONS */}
                        <View style={styles.buttons}>

                            {
                                this.state.textCancelButton && (
                                    <TouchableOpacity
                                        style={ styles.button }
                                        onPress={ () => {
                                            if (this.cancelButtonCallback) this.cancelButtonCallback();
                                            this.dismiss();
                                        } } >
                                        <Text style={styles.textButton}> { this.state.textCancelButton } </Text>
                                    </TouchableOpacity>
                                )

                            }

                            {
                                this.state.textPositiveButton && (
                                    <TouchableOpacity
                                        style={ styles.button }
                                        onPress={ () => {
                                            if (this.positiveButtonCallback) this.positiveButtonCallback();
                                            this.dismiss();
                                        } } >
                                        <Text style={styles.textButton}> { this.state.textPositiveButton } </Text>
                                    </TouchableOpacity>
                                )
                            }

                        </View>

                    </View>

                </TouchableOpacity>

            </Modal>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.17)',
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        backgroundColor: "white",
        width: "80%"
    },
    textButton:{
        color: Colors.primary,
        fontSize: 15,
        fontWeight: 'bold',
        paddingVertical: 5
    },
    title: {
        paddingBottom: 7,
        fontSize: 18,
        paddingLeft: 10
    },
    message: {
        paddingVertical: 14,
        margin: "auto",
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        padding: 14,
    },
    buttons: {
        flexDirection: "row",
        borderTopColor: Colors.gray,
        borderTopWidth: .5,
        fontWeight: 'bold',
    },
    button: {
        flex: 1,
        shadowColor: "#0000",
        backgroundColor: "#0000",
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 7
    }
});

export default ModalAlertSmart;