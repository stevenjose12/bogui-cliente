import FastImage from 'react-native-fast-image';
import React, { Component } from 'react';
import { StyleSheet, View, } from 'react-native';

class ImageBackground extends Component {

    render() {
        const {children, style, imageStyle, imageRef, ...props} = this.props;
        return (
            <View
                accessibilityIgnoresInvertColors={true}
                style={style}>
                <FastImage
                    {...props}
                    style={[
                        StyleSheet.absoluteFill,
                        {
                            width: style.width,
                            height: style.height,
                        },
                        imageStyle,
                    ]}/>
                {children}
            </View>
        );
    }
}

export default ImageBackground;