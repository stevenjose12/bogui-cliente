import React, { Component } from "react"
import {Modal, StyleSheet, View, Text, TouchableWithoutFeedback, ImageBackground, Dimensions, Platform} from "react-native"
import Colors from "../assets/Color";
import {Avatar} from "react-native-elements";
import Button from "../widgets/Button";
import Ranting from "./Ranting";
import Api from "../utils/Api";
import Toast from "./Toast";
import env from "../utils/env";
// import ModalAlert from "../utils/Modal";

const large = Dimensions.get('window').width*0.9;

class QualifyModal extends Component {

    state = {
        visible: false,
        route: null,
        name: ""
    };

    ranting = 5;
    name = "";
    isExecuteOnCloseModal = true;

    show = (show: boolean, route = null) => {
        if (route) this.routeId = route.id;
        if (show && route) this.isExecuteOnCloseModal = true;
        if (show) this.ranting = 5;

        this.name = route ? route.driver.person.name : "Bogui";
        this.state.name = this.name;
        this.state.route = route;
        this.setState( state => state.visible = show, () => this.onCloseModal(show) );
    };

    onCloseModal = show => {
        if ( this.isExecuteOnCloseModal && !show && this.props.onCloseModal ) {
            this.props.onCloseModal();
            this.isExecuteOnCloseModal = false;
        }
    };

    submit = async () => {

        const params = {
            reviewed_id: this.state.route ? this.state.route.driver.id : null,
            reviewer_id: this.props.user.id,
            route: this.routeId,
            points: this.ranting,
            bogui: this.state.route ? "1" : "0"
        };

        try {
            await Api.qualify(params);
        } catch (e) {
            // ModalAlert.loading.dismiss();
        }

        Toast.show("Gracias por usar Bogui");
        this.show(false);

        if (!this.state.route) return;
        this.ranting = 0;
        this.setState( { route: null} );
        this.props?.onSuccess();

    };

    render() {
        return (
            <Modal
                animationType="none"
                transparent={ true }
                visible={ this.state.visible }
                hardwareAccelerated={ true } >

                <TouchableWithoutFeedback>

                    <View style={ styles.container }>

                        <ImageBackground
                            source={ require('../assets/icons/white.png') }
                            style={ styles.card }
                            imageStyle={ {
                                resizeMode: 'cover',
                                borderRadius: 20
                            } }>

                                <Avatar
                                    rounded
                                    source={ this.state.route ? { uri: env.BASE_PUBLIC+this.state.route.driver.person.photo  } : require("../assets/icons/bogui.png") }
                                    size={ 110 } />

                                <Text style={ styles.text }>
                                    {"¿Cómo te parecio el servicio de "+this.state.name+" ?"}
                                </Text>

                                <View style={ { marginVertical: 20, width: '100%', alignItems: 'center', justifyContent: 'center' } }>
                                    <Ranting
                                        ranting={ this.ranting }
                                        touchable
                                        onPress={ star => this.ranting = star } />
                                </View>

                                <Button
                                    title="Calificar"
                                    buttonStyle={ { backgroundColor: Colors.primary, borderRadius: 20, width: '80%', alignSelf: Platform.OS == 'ios' ? 'center' : null } }
                                    onPress={ this.submit } />

                        </ImageBackground>

                    </View>

                </TouchableWithoutFeedback>

            </Modal>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.17)',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        width: large,
        height: large,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
    text: {
        color: Colors.primary,
        fontSize: 20,
        width: '80%',
        textAlign: 'center'
    }
});

export default QualifyModal;