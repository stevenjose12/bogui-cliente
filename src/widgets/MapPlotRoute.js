import React, { Component } from 'react';
import {ActivityIndicator, Image, StyleSheet, View} from "react-native";
import MapView from "react-native-maps";
import Color from "../assets/Color";
import MapViewDirections from 'react-native-maps-directions';
import ENV from '../utils/env';
import Night from "../assets/mapanocturno";
import Day from "../assets/mapadiurno";
import {connect} from "react-redux";
import G from "../utils/G";

class MapPlotRoute extends Component {

    markers = []; // <-------------------------------------------------------------------------------------------------- Markers para los diferentes puntos
    places = []; // <--------------------------------------------------------------------------------------------------- Array de coords

    setPlaces(places) {

        this.places = places;

        this.origin = this.places[0].location;
        this.waypoints = [];
        for (let i=1; i<this.places.length-1; i++) {
            const item = this.places[i];
            if (item.location) this.waypoints[i-1] = item.location;
        }
        this.destination = this.places[this.places.length-1].location;

        this.forceUpdate();

    }

    render() {
        return(

            <View style={ { ...styles.container, ...this.props.style } }>

                {
                    this.origin
                        ?
                        <MapView
                            ref={ map => this.map = map }
                            style={ styles.map }
                            camera={
                                {
                                    center: this.origin,
                                    pitch: 1,
                                    heading: 1,
                                    zoom: 15,
                                    altitude: 15,
                                }
                            }
                            provider="google"
                            showsCompass={false}
                            rotateEnabled={false}
                            zoomControlEnabled={true}
                            zoomEnabled={true}
                            pitchEnabled={false}
                            toolbarEnabled={true}
                            liteMode={ false }
                            loadingEnabled={true}
                            customMapStyle={ G.isNight(this.props.time) ? Night : Day } >

                            {/* PLACE MARKERS */}
                            {

                                this.places.map( (place, i)  => {

                                    if (place.location) return(
                                        <MapView.Marker key={i} coordinate={ place.location } >
                                            <Image source={ this.props.markers[i] } />
                                        </MapView.Marker>
                                    )}
                                )

                            }

                            <MapViewDirections
                                origin={ this.origin }
                                destination={ this.destination }
                                waypoints={ this.waypoints.length ? this.waypoints : null }
                                strokeWidth={4}
                                strokeColor={ G.isNight(this.props.time) ? Color.chat : Color.primary }
                                apikey={ ENV.apiKey }
                                onReady={ result => this.map.fitToCoordinates(result.coordinates) } />

                        </MapView>
                        :
                        <View style={ { width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' } }>
                            <ActivityIndicator />
                        </View>

                }

            </View>

        );
    }

}

const styles = StyleSheet.create({
    container: {
        position: "relative",
        width: '100%',
        backgroundColor: Color.white,
        flex: 1,
    },
    map: {
        width: "100%",
        height: "100%"
    }
});

const redux = state => ({
    time: state.night.night
});

export default connect(redux, null, null, { forwardRef: true })(MapPlotRoute);
// export default MapPlotRoute;