import React from 'react'
import Hbox from "./Hbox";
import { TouchableOpacity, View } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome5';
import Color from "../assets/Color";

class Ranting extends React.Component {

    state = {
        stars: []
    };

    componentDidMount() {
        const stars = [];
        for (let i= 0; i < 5; i++) stars.push((i + 1) <= Math.round(this.props.ranting));
        this.setState( state => state.stars = stars );
    }

    onPress = _stars => {
        const stars = [];
        for (let i= 0; i < 5; i++) stars.push((i + 1) <= Math.round(_stars));
        this.setState( state => state.stars = stars );
        this.props.onPress(_stars);
    };

    icon = star => {
        return (
            <Icon
                size={ this.props.size ? this.props.size : 20 }
                name="star"
                solid={ star }
                outline={ !star }
                color={ this.props.color ? this.props.color :Color.primary } />
        )

    };

    render() {
        return (
            <Hbox style={ { justifyContent: 'center', alignItems: 'center', width: this.props.wrapContent ? null : '100%', ...this.props.style } }>
                {
                    this.state.stars.map( (star, i) => {
                        return this.props.touchable
                            ?
                            <TouchableOpacity key={ i } onPress={ () => this.onPress(i + 1)}>
                                { this.icon(star) }
                            </TouchableOpacity>
                            :
                            <View key={ i }>{ this.icon(star) }</View>

                    })
                }
            </Hbox>
        )
    }
}

export default Ranting;