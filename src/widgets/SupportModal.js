import React, { Component } from 'react';
import {Modal, StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, Image} from "react-native";
import Colors from '../assets/Color';
import Input from './InputBogui';
import Api from '../utils/Api';
import Hbox from './Hbox';
import Button from './Button';
import CurstomModal from '../utils/Modal';
import { connect } from "react-redux";
import G from '../utils/G';

class SupportModal extends Component{
    state={
        code: '',
        props:''
    }
    render(){
        return(
            <Modal
                animationType="slide"
                transparent={ true }
                visible={ this.props.visible }
                useNativeDriver={ true }>
                <View style={styles.container} >
                    <View style={{width: '80%',flex: 1, alignItems: 'center',justifyContent: "center"}}>
                        <Text style={styles.title}>Support</Text>
                        <Button
                            title="Cerrar"
                            onPress={this.props.close}
                        />
                    </View>
                </View>
            </Modal>
        )
    }
} 

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(255, 255, 255, .80)',
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        height: '100%',
        padding: 5,
    },
    buttonStyleWhite:{backgroundColor: 'transparent', borderColor: Colors.primary, borderWidth: 1, borderRadius: 7},
    titleBlack:{color: Colors.primary, padding: 25},
    titleStyle:{color: Colors.white, padding: 10},
    bottomButtons:{width: '48%', paddingVertical: 12},
    buttonStyleBlack:{backgroundColor: Colors.primary, borderColor: Colors.primary, borderWidth: 1, borderRadius: 7},
    title:{
        color: Colors.primary,
        fontSize: 24,
        fontWeight: 'bold'
    },
    iconBanner: {
        width: "100%",
        height: "100%",
    },
    back: {
        width: 55,
        height: 55,
        position: "absolute",
        alignSelf: "flex-start",
        padding: 12,
        zIndex: 1
    }
});
const redux = state => { return { user: state.auth.user } };
export default connect(redux)(SupportModal);