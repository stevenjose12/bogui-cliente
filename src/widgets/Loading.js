import React from 'react';
import {View, Text, ActivityIndicator, StyleSheet} from 'react-native';

const Loading = props => (
    <View style={ styles.container } >
        <Text>Cargando...</Text>
        <ActivityIndicator />
    </View>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        marginBottom: 10,
        width: 200,
        height: 80,
        resizeMode: 'contain'
    }
});

export default Loading;