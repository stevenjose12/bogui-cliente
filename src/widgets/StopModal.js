import React, { Component } from 'react';
import {Modal, StyleSheet, Text, View, Image} from "react-native";
import Colors from '../assets/Color';
import Button from './Button';
class StopModal extends Component{

    state={
        visible: false
    };

    show(show){
        this.setState(state => state.visible = show)
    }

    onPositiveButton(callback) {
        this.onPositiveButtonCallback = callback;
    }

    onNegativeButton(callback) {
        this.onNegativeButtonCallback = callback;
    }

    render() {
        return(
            <Modal
                animationType="slide"
                transparent={ true }
                visible={ this.state.visible }
                useNativeDriver={ true }>
                <View style={styles.container} >
                    <View style={styles.modal}>
                        <View style={styles.modalContainer}>
                            <Image
                                source={require('../assets/icons/isotipo.png')}
                                style={styles.logo}
                                resizeMode="contain"
                            />
                            <View style={styles.viewText}>
                                <Text style={styles.title} >¿Desea agregar una parada adicional?</Text>
                            </View>
                            <View style={styles.viewButtons}>
                                <View style={styles.buttonsRow}>
                                    <Button
                                        title="NO"
                                        containerStyle={styles.noButton}
                                        buttonStyle={styles.buttonStyleWhite}
                                        titleStyle={styles.titleBlack}
                                        onPress={ () => {
                                            if (this.onNegativeButtonCallback) this.onNegativeButtonCallback();
                                            this.show(false);
                                        } }
                                    />
                                    <Button
                                        title="SI"
                                        containerStyle={styles.yesButton}
                                        buttonStyle={styles.buttonStyleWhite}
                                        titleStyle={styles.titleWhite}
                                        onPress={ () => {
                                            if (this.onPositiveButtonCallback) this.onPositiveButtonCallback();
                                            this.show(false);
                                        } }
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
} 
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, .5)',
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        height: '100%',
        padding: 5,
    },
    yesButton:{
        width: '20%',
        borderColor: Colors.primary,
        backgroundColor: Colors.primary,
        borderRadius: 8
    },
    noButton:{
        width: '20%',
        padding: 0,
        margin: 0
    },
    modal:{
        width: '90%',
        flex: 1,
        alignItems: 'center',
        justifyContent: "center"
    },
    buttonsRow:{
        flexDirection: 'row',
        paddingVertical: 5
    },
    viewButtons:{
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    viewText:{
        width: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 5
    },
    logo:{
        width: '23%',
        height: '21.5%',
        borderTopRightRadius: 40,
        borderTopLeftRadius: 40
    },
    modalContainer:{
        width: '85%',
        height: '50%',
        alignItems: 'center',
        backgroundColor: 'white',
        paddingVertical: 9,
        borderRadius: 40,
        bottom: '3%',
        borderColor: Colors.primary,
        borderWidth: 1
    },
    buttonStyleWhite:{
        backgroundColor: 'transparent',
        borderRadius: 7,
        right: 0,
        left: 0,
        top: 0,
        width: '100%', 
        bottom: 0,
        padding: 0,
        margin: 0,
    },
    titleBlack:{
        color: Colors.primary,
        fontSize: 13,
    },
    titleWhite:{
        color: Colors.white,
        fontSize: 13,
    },
    title:{
        color: Colors.primary,
        fontSize: 23,
        fontFamily: 'Poppins',
        textAlign: 'center'
    },
});
export default StopModal;