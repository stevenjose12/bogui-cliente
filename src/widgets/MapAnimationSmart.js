import React, { Component  } from 'react';
import {StyleSheet, View, ActivityIndicator, Image } from 'react-native';
import Animation from "../utils/Animation";
import MapView from "react-native-maps";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome"
import Geolocation from "../utils/Geolocation";
import Day from '../assets/mapadiurno.json';
import Night from '../assets/mapanocturno.json';
import moment from 'moment';
import G from "../utils/G";

class MapAnimationSmart extends Component {

    animation: Animation;
    static startTime: number = 0;
    static finishTime: number = 0;
    static deltaTime: number = 0;
    cameraCenter = {};

    state = {
        markers: [],
        zoom: 15
    };

    componentDidMount() {
        this.animation = new Animation(this.props.timeAnimation);
    }

    getCameraCenter = () => this.cameraCenter;

    anim = (oldMarkers, newMarkers) => {

        try {

            if (oldMarkers.length===0) {
                this.setState({markers: []});
                return;
            }

            this.setState({markers: oldMarkers});

            const deltas = [];

            for (let i=0; i<this.state.markers.length; i++) {
                const lat = oldMarkers[i].latitude;
                const lng = oldMarkers[i].longitude;
                const dlat = newMarkers[i].latitude - oldMarkers[i].latitude;
                const dlng = newMarkers[i].longitude - oldMarkers[i].longitude;
                const rotation = oldMarkers[i].rotation;
                let dRot = newMarkers[i].rotation - oldMarkers[i].rotation;
                if (Math.abs(dRot)>180) dRot = (dRot<0 ? 1 : -1 )*360+dRot;
                deltas[i] = { lat: lat, lng: lng, dlat: dlat, dlng: dlng, rotation: rotation, dRot: dRot };
                this.state.markers[i].rotation = oldMarkers[i].rotation;
            }

            this.animation.onUpdate( (anim: Animation) => {

                for (let i=0; i<this.state.markers.length; i++) {

                    if (oldMarkers[i] && newMarkers[i] && oldMarkers[i].id===newMarkers[i].id) {

                        const lat = deltas[i].lat + anim.delta( deltas[i].dlat );
                        const lng = deltas[i].lng + anim.delta( deltas[i].dlng );

                        if ( !isNaN(lat) && !isNaN(lng) ) {
                            this.state.markers[i].latitude = lat;
                            this.state.markers[i].longitude = lng;
                            const rot = deltas[i].rotation + anim.delta( deltas[i].dRot );
                            this.state.markers[i].rotation = isNaN(rot) ? oldMarkers[i].rotation : rot;
                        }

                    }

                }
                this.forceUpdate();
            });

            this.animation.start();

        } catch (e) {
            console.log(">>: MapAnimationSmart > anim > error: ", e);
        }

    };

    getCamera = () => this.map.getCamera();

    update = () => this.forceUpdate();

    location = () => {
        const center = Geolocation.getLatLng(Geolocation.getCurrentLocation());
        this.map.animateCamera({ center }, 1000);
    };

    isNight = () => {
        if (!this.props.nightMode) return false;
        const now   = moment();
        const start = moment(this.props.nightMode.start,'HH:mm:ss');
        const end   = moment(this.props.nightMode.end,'HH:mm:ss');
        return ( now>=start && ( now<=end || end<start ) && ( now>=start || now<=end ) );
    };

    applyZoom = level => this.setState({ zoom: this.state.zoom + level }, () => {
        this.map.setCamera({ center: this.getCameraCenter() })
    });

    render() {
        return (
            <View style={styles.container}>

                {/* HEADER */}
                <View style={{ ...styles.header, ... this.props.styleHeader}}>
                    { this.props.header }
                </View>

                {/* MARKER CENTER */}
                {
                    !!this.props.marker && (
                        <Image
                            source={ this.props.marker }
                            style={ styles.marker } />
                    )
                }

                {/* FOOTER */}
                <View style={{ ...styles.footer, ... this.props.styleFooter }}>
                    { this.props.footer }
                </View>

                {
                    this.props.center
                        ?
                        <React.Fragment>

                            {
                                this.props.children
                            }

                            <View style={ stylesZoom.container }>

                                <Button
                                    byttonStyle={ { width: 40, height: 40 } }
                                    type="clear"
                                    onPress={ this.location }
                                    icon={
                                        <Image
                                            style={ { width: 30, height: 30 } }
                                            source={ require('../assets/icons/current.png') } resizeMode="contain" />
                                    } />

                                <View style={ stylesZoom.separator } />

                                <Button
                                    buttonStyle={ stylesZoom.button }
                                    onPress={ () => { if (this.state.zoom < 30) this.applyZoom(1) } }
                                    type="clear"
                                    icon={ <Icon name="plus" color={ "rgba(102,102,102,0.8)" } size={ 20 } /> } />

                                <View style={ stylesZoom.separator } />

                                <Button
                                    buttonStyle={ stylesZoom.button }
                                    onPress={ () => {if (this.state.zoom > 5) this.applyZoom(- 1) } }
                                    type="clear"
                                    icon={ <Icon name="minus" color={ "rgba(102,102,102,0.8)" } size={ 20 } /> } />

                            </View>

                            <MapView
                                ref={ map => this.map = map }
                                style={styles.map}
                                customMapStyle={ G.isNight(this.props.nightMode) ? Night : Day }
                                camera={
                                    {
                                        center: this.props.center,
                                        pitch: 1,
                                        heading: 1,
                                        zoom: this.state.zoom,
                                        altitude: 15,
                                    }
                                }
                                provider="google"
                                showsCompass={false}
                                rotateEnabled={false}
                                zoomControlEnabled={false}
                                pitchEnabled={false}
                                toolbarEnabled
                                zoomEnabled
                                onPress={ this.props.onPress }
                                onRegionChangeComplete={event => {
                                    if (this.props.onDragEnd) this.props.onDragEnd(event.nativeEvent);
                                    const { latitude, longitude } = (event || {});
                                    this.cameraCenter = {
                                        latitude: latitude || this.props.center.latitude,
                                        longitude: longitude || this.props.center.longitude,
                                    }
                                }}
                                onRegionChange={ event => {
                                    if (this.props.onDrag) this.props.onDrag(event.nativeEvent)
                                }} > 

                                {
                                    this.state.markers.map( (m: MarkerData, i) =>
                                        <MapView.Marker
                                            key={m.id}
                                            coordinate={m.getLocation()}
                                            onPress={ this.props.onPressMarker }
                                            rotation={ parseFloat(m.rotation) }
                                            anchor={{x: 0.5, y: 0.5}} >

                                            <Image
                                                key={m.id}
                                                source={ this.props.markerIcon }
                                                renderToHardwareTextureAndroid={true} />

                                        </MapView.Marker>
                                    )
                                }

                                {/* PLACE MARKERS */}
                                {
                                    this.props.placeMarkers.map( (place, i)  => {
                                        if (place.location) return(
                                            <MapView.Marker key={i} coordinate={ place.location } >
                                                <Image source={ place.icon } />
                                            </MapView.Marker>
                                        )}
                                    )
                                }

                            </MapView>

                        </React.Fragment>
                        :
                        <View style={ { width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' } }>
                            <ActivityIndicator />
                        </View>

                }

            </View>
        );
    }

}

class MarkerData {

    id: number;
    latitude: number;
    longitude: number;
    rotation: number = 0;
    time: number = 0;
    category: number = 0;

    constructor(id: number, latitude: number, longitude: number, category: number) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.time = new Date().getTime();
        this.category = category;
    }

    getLocation() {
        return {
            latitude: this.latitude,
            longitude: this.longitude
        }
    }

    setParams(latitude, longitude, rotation, time) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.rotation = rotation;
        this.time = time;
    }

    clone() : MarkerData {
        const marker: MarkerData = new MarkerData(this.id, this.latitude, this.longitude, this.category);
        marker.rotation = this.rotation;
        marker.time = this.time;
        return marker;
    }

}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        position: "relative"
    },
    map: {
        width: "100%",
        height: "100%",
        alignItems: "center",
        justifyContent: "center",
    },
    marker: {
        zIndex: 1,
        position: "absolute"
    },
    header: {
        width: '100%',
        zIndex: 2,
        position: "absolute",
        top: 40,
    },
    footer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1,
        position: "absolute",
        bottom: 0,
    }
});

const stylesZoom = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        width: 40,
        position: 'absolute',
        zIndex: 9999,
        bottom: 90,
        right: 10,
        borderRadius: 2
    },
    separator: {
        width: '100%',
        height: 1,
        backgroundColor: '#e1e1e1'
    },
    button: {
        paddingVertical: 10
    }
});

export { MapAnimationSmart, MarkerData };