import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Hbox from "./Hbox";

const LabelForm = props => (

    props.vertical
        ?
        <View style={ { ...styles.container, ...props.style } }>

            <Text style={ { ...styles.valueVertical, fontSize: props.fontSize ? props.fontSize : 17, ...props.styleValue } }>
                <Text style={ { ...styles.key, fontSize: props.fontSize ? props.fontSize : 17 } }>
                    { props.label+": " }
                </Text>
                { props.value }
            </Text>
        </View>
        :
        <Hbox style={ { ...styles.container, ...props.style } }>

            <Text style={ { ...styles.key, fontSize: props.fontSize ? props.fontSize : 17 } }>
                { props.label+":" }
            </Text>

            <Text style={ { ...styles.value, fontSize: props.fontSize ? props.fontSize : 17, ...props.styleValue } }>
                { props.value }
            </Text>

        </Hbox>
);

const styles = StyleSheet.create({
    container: {
        width: '100%',
    },
    key: {
        fontFamily: "Poppins-SemiBold",
        color: 'black',
        marginRight: 5,
    },
    value: {
        color: 'black',
        flex: 1
    },
    valueVertical: {
        color: 'black',
    }
});

export default LabelForm;