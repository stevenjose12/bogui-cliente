import React, { Component } from 'react';
import {Image, Modal, StyleSheet, TouchableWithoutFeedback, View, Text, Platform} from "react-native";
import Colors from "../assets/Color";
import Button from "./Button";

class ModalNoBogui extends Component {

    state = {
        show: false,
        vehicles: null
    };

    show(show: boolean) {
        this.setState( state => state.show = show );
    }

    setVehicles(vehicles) {
        this.setState( state => state.vehicles = vehicles );
        this.forceUpdate();
    }

    getCategory(id): string {
        if (!this.state.vehicles) return "";
        for (let i=0; i<this.state.vehicles.length; i++) {
            const item = this.state.vehicles[i];
            if (item.id===id) return item.name;
        }

    }

    render() {
        return (
            <Modal
                animationType="slide"
                visible={ this.state.show }
                transparent
                useNativeDriver>

                <TouchableWithoutFeedback
                    onPress={ () => this.show(false) } >

                    <View style={ styles.container }>

                        <View style={ styles.card }>

                            <Image
                                source={ require('../assets/icons/isotipo.png')}
                                style={{width: 70, height: 70, marginBottom: 30, marginTop: 10}}
                                resizeMode="contain" />

                            <Text style={ styles.text }>
                                {
                                    "No se encuentran Boguis "+this.getCategory(this.props.vehicleCategory)+" cercano."
                                }
                            </Text>

                            <Text style={ styles.text }>
                                Por favor intente seleccionando otra Categoría.
                            </Text>

                            <View style={ styles.buttonContainer }>
                                <Button
                                    title="Cerrar"
                                    buttonStyle={ styles.closeBtn }
                                    titleStyle={ { color: Colors.primary, width: "100%" } }
                                    onPress={ () => this.show(false) } />
                            </View>

                        </View>

                    </View>

                </TouchableWithoutFeedback>

            </Modal>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.17)',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        backgroundColor: "white",
        width: "90%",
        justifyContent: 'center',
        alignItems: 'center',
        padding: 7,
        borderRadius: 10
    },
    closeBtn: {
        width: "70%",
        borderColor: Colors.primary,
        borderWidth: 1,
        borderRadius: 20,
        backgroundColor: Colors.white,
        alignSelf: Platform.OS == 'ios' ? 'center' : null
    },
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        borderTopColor: Colors.gray,
        borderTopWidth: 0.5,
        paddingTop: 10,
        paddingBottom: 5,
        marginTop: 10
    },
    text: {
        textAlign: 'center',
        paddingVertical: 5
    }
});

export default ModalNoBogui;