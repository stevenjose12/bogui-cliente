import React, { Component } from 'react';
import {Modal, StyleSheet, Text, View, Image, Platform} from "react-native";
import Colors from '../assets/Color';
import Api from '../utils/Api';
import Button from './Button';
import CustomModal from '../utils/Modal';
import { connect } from "react-redux";
import ENV from '../utils/env';

class WelcomeModal extends Component{

    state={
        data: {},
        support: false,
    };

    componentDidMount() {
        this.submit();
    }

    submit = async () =>{
        try {
            const data = await Api.welcomeModal();
            if (data) this.setState({ data });
        } catch (e) {
            CustomModal.showGenericError(e);
        }
    };

    render() {
        return(
            <Modal
                animationType="slide"
                transparent
                visible={ this.props.visible }
                useNativeDriver>
                <View style={styles.container} >
                    <View style={styles.card}>
                        <Image
                            source={{uri: ENV.BASE_PUBLIC+this.state.data.logo}}
                            style={{width: '100%', height: '30%', borderTopRightRadius: 40, borderTopLeftRadius: 40, backgroundColor: 'white'}}
                            resizeMode="contain" />
                        <View style={{width: '100%', height: '25%',alignItems: 'center', backgroundColor: 'white', borderBottomLeftRadius: 40, bottom: '3%',borderBottomRightRadius: 40}}>
                            <View style={{width: '100%', flex: 1, justifyContent: 'center', paddingHorizontal: 5}}>
                                <Text style={styles.title} >
                                    {this.state.data.title}
                                </Text>
                                <Text style={styles.description} >
                                    {this.state.data.description}
                                </Text>
                            </View>
                            <Button
                                title="Continuar"
                                containerStyle={{padding: 10}}
                                buttonStyle={styles.buttonStyleWhite}
                                titleStyle={styles.titleBlack}
                                onPress={this.props.close} />
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(255, 255, 255, .5)',
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        height: '100%',
        padding: 5,
    },
    card: {
        width: '80%',
        flex: 1,
        alignItems: 'center',
        justifyContent: "center"
    },
    buttonStyleWhite: {
        backgroundColor: 'transparent',
        width: Platform.OS === 'ios' ? '80%' : '70%',
        borderColor: Colors.primary,
        borderWidth: 1,
        borderRadius: 7,
        alignSelf: Platform.OS === 'ios' ? 'center' : null
    },
    titleBlack:{
        color: Colors.primary,
        padding: 25,
        width: '100%',
        height: '100%',
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 15
    },
    title:{
        color: Colors.accent,
        fontSize: 21,
        fontWeight: 'bold',
        fontFamily: 'Poppins',
        textAlign: 'center'
    },
    description:{
        fontSize: 14,
        textAlign: 'center',
    },  
    iconBanner: {
        width: "100%",
        height: "100%",
    },
    back: {
        width: 55,
        height: 55,
        position: "absolute",
        alignSelf: "flex-start",
        padding: 12,
        zIndex: 1
    }
});
const redux = state => ({ user: state.auth.user });
export default connect(redux)(WelcomeModal);