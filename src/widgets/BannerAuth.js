import React from 'react';
import {Image, StyleSheet, TouchableWithoutFeedback, View} from "react-native";

const BannerAuth = props => (
    <View style={styles.banner} >

        <Image
            style={ styles.iconBanner }
            source={ props.iconBanner }
            resizeMode="contain"/>

        {
            props.backIcon
                ?
                <TouchableWithoutFeedback onPress={ props.onBackPress }>
                    <View style={ styles.back }>
                        <Image
                            style={ { width: "100%", height: "100%" } }
                            source={ props.backIcon }
                            resizeMode="contain" />
                    </View>
                </TouchableWithoutFeedback>
                : null

        }


    </View>
);

const styles = StyleSheet.create({
    banner: {
        width: "100%",
        height: 55,
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        position: "relative"
    },
    iconBanner: {
        width: "100%",
        height: "100%",
    },
    back: {
        width: 55,
        height: 55,
        position: "absolute",
        alignSelf: "flex-start",
        padding: 12,
        zIndex: 1
    }
});

export default BannerAuth;