import React from 'react';
import { ToastAndroid, Platform } from 'react-native';
import _Toast from 'react-native-root-toast';

class Toast {

    static show(message: string) {
    	if (Platform.OS == 'android') {
    		ToastAndroid.showWithGravityAndOffset(
	            message,
	            ToastAndroid.LONG,
	            ToastAndroid.BOTTOM,
	            0,
	            50,
	        );
    	}
    	else {
    		_Toast.show(message, {
		      duration: _Toast.durations.SHORT,
		      position: _Toast.positions.BOTTOM
		    });
    	}
    }

}

export default Toast;