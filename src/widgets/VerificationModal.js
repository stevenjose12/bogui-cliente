import React, { Component } from 'react';
import {Modal, StyleSheet, Text, View} from "react-native";
import Colors from '../assets/Color';
import Input from './InputBogui';
import Api from '../utils/Api';
import Hbox from './Hbox';
import Button from './Button';
import CurstomModal from '../utils/Modal';
import { connect } from "react-redux";
import G from '../utils/G';
import { socket } from '../utils/Socket';
import Toast from "./Toast";

class VerificationModal extends Component{

    state={
        code: '',
        props:'',
    };

    componentDidMount(): void {

        socket.on('usuario-validated', async data => {
            if (data.user_id == this.props.user.id) {
                Toast.show('Verificación exitosa');
                const data = {
                    id: this.props.user.id,
                    email: this.props.user.email,
                    phone: this.props.user.phone
                };
                const response = await Api.updateProfile( data );
                if (response.result) await this.props.dispatch( { type: 'SET_USER', payload: { user: response.user } } );
                socket.removeListener('usuario-validated');
            }
        });

    }

    submit = async () =>{
        try {
            const data = {
                id: this.props.user.id,
                code: this.state.code
            };
            const r = await Api.verifyUser(data);
            if (r.success) {
                const data = {
                    id: this.props.user.id,
                    email: this.props.user.email,
                    phone: this.props.user.phone
                };
                const response = await Api.updateProfile( data );
                if (response.result) await this.props.dispatch( { type: 'SET_USER', payload: { user: response.user } } );
                Toast.show(r.message);
            } else {
                CurstomModal.alert.show({message: r.message})
            }
        } catch (e) {
            CurstomModal.showGenericError(e);
        }
    };

    _resendCode = async () => {
        const hash = G.getHash();
        try {
            const r = await Api.resendCode({id: this.props.user.id, hash: hash});
            if (r.success) {
                G.sendMessage(this.props.user.phone, 'Bienvenido a Bogui, su nuevo código de verificación es: '+hash);
                CurstomModal.alert.show({message: r.message, time: 5000})
            } else {
                CurstomModal.alert.show({message: r.message});
            }
        } catch (e) {
            CurstomModal.showGenericError(e);
        }
    };

    upperCase = () => this.setState(state => state.code = state.code.toUpperCase());

    render() {
        return(
            <View>
                <Modal
                    animationType="slide"
                    transparent
                    visible={ this.props.visible }
                    useNativeDriver>
                    <View style={styles.container} >
                        <View style={{width: '80%',flex: 1, alignItems: 'center',justifyContent: "center"}}>
                        <Text style={styles.title}>Ingresa el Código</Text>
                            <Input
                                onChangeText={ (text) => {
                                    this.setState( state => state.code = text);
                                }}
                                inputStyle={{borderColor: Colors.primary, borderWidth: 2, borderRadius: 10,}}
                                text={this.text}
                                autoCapitalize="characters" />

                            <View style={{width: '92%'}}>
                                <Button
                                    title="Reenviar Código"
                                    containerStyle={{width: '100%', paddingVertical: 12}}
                                    buttonStyle={styles.buttonStyleWhite}
                                    titleStyle={styles.titleBlack}
                                    onPress={this._resendCode} />
                                <Button
                                    title="Aceptar"
                                    buttonStyle={styles.buttonStyleBlack}
                                    titleStyle={styles.titleStyle}
                                    onPress={this.submit} />
                            </View>
                        </View>
                    </View>
                    <Hbox style={{justifyContent: 'center', width: '100%', paddingHorizontal: 5, backgroundColor: 'rgba(255, 255, 255, .80)',}}>
                        <Button
                            containerStyle={styles.bottomButtons}
                            buttonStyle={styles.buttonStyleBlack}
                            titleStyle={styles.titleStyle}
                            onPress={this.props.onSupportPress}
                            title='Soporte'/>
                        <View style={{width: '5%'}}></View>
                        <Button
                            containerStyle={styles.bottomButtons}
                            buttonStyle={styles.buttonStyleBlack}
                            titleStyle={styles.titleStyle}
                            onPress={this.props.logout}
                            title='Cerrar Sesión'/>
                    </Hbox>
                </Modal>
            </View>
        )
    }
} 

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(255, 255, 255, .80)',
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        height: '90%',
        padding: 5,
    },
    buttonStyleWhite:{backgroundColor: 'transparent', borderColor: Colors.primary, borderWidth: 1, borderRadius: 7},
    titleBlack:{color: Colors.primary, padding: 25},
    titleStyle:{color: Colors.white, padding: 10},
    bottomButtons:{width: '48%', paddingVertical: 12},
    buttonStyleBlack:{backgroundColor: Colors.primary, borderColor: Colors.primary, borderWidth: 1, borderRadius: 7},
    title:{
        color: Colors.primary,
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'Poppins'
    },
    iconBanner: {
        width: "100%",
        height: "100%",
    },
    back: {
        width: 55,
        height: 55,
        position: "absolute",
        alignSelf: "flex-start",
        padding: 12,
        zIndex: 1
    }
});

const redux = state => ({ user: state.auth.user });
export default connect(redux)(VerificationModal);