import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Colors from '../assets/Color';

const Header = props => (
	<View style={ [
			styles.container,
			props.style,
			props.containerDefault ? null : styles.containerRed,
			props.transparent ? styles.transparent : styles.bgWhite] }>
		<View style={ [styles.containerLeft] }>
			{ props.headerLeft }
		</View>
		<Text numberOfLines={ 1 } style={ [styles.title,props.titleStyle] }>{ props.scene.descriptor.options.title }</Text>
		<View style={ styles.containerRight }>
			{ props.headerRight }
		</View>
	</View>
);

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		paddingVertical: 10,
		alignItems: 'center',
		justifyContent: 'center',
		position: 'absolute',
		width: '100%',
		top: 0
	},
	containerRed: {
		borderBottomWidth: 2,
		borderBottomColor: Colors.accent
	},
	transparent: {
		backgroundColor: 'transparent',
	},
	bgWhite: {
		backgroundColor: '#fff'
	},
	title: {
		fontSize: 18,
		textAlign: 'center',
		color: Colors.primary,
		fontFamily: 'Poppins-Bold',
		width: '80%'
	},
	containerRight: {
		position: 'absolute',
		right: 5
	},
	containerLeft: {
		position: 'absolute',
		left: 5
	}
});

export default Header;