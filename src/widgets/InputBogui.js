import React from 'react';
import {Image, StyleSheet, Text, View  } from "react-native";
import { Input } from 'react-native-elements';
import Color from "../assets/Color";

const InputBogui = props => (

    <View style={ [ { width: '100%' }, props.style ? props.style : null] }>

        {
            props.label && (
                <Text style={{...styles.label, ...props.labelStyle}}>
                    { props.label }
                </Text>
            )
        }

        <Input
            keyboardType={props.keyboardType ? props.keyboardType : "default"}
            inputContainerStyle={{...styles.input,...props.inputContainerStyle}}
            containerStyle={{...props.containerStyle}}
            placeholder={ props.hint }
            inputStyle={{...styles.inputStyle, ...props.inputStyle}}
            leftIconContainerStyle={styles.inputsLeftIcon}
            secureTextEntry={ !!props.password }
            onChangeText={ text => props.onChangeText ? props.onChangeText(text) : null }
            onFocus={ () => props.onFocus ? props.onFocus() : null }
            onBlur={ () => props.onBlur ? props.onBlur() : null }
            leftIcon={
                props.leftIcon && (
                    <Image
                        style={ styles.inputIconSize }
                        source={ props.leftIcon }
                        resizeMode="contain"/>
                )
            }
            errorMessage={ props.errorMessage }
            errorStyle={ { fontFamily: 'Poppins' } }
            autoCapitalize={props.autoCapitalize ? props.autoCapitalize : 'none'}
            value={ props.text ? props.text : null }/>

    </View>

);

const styles = StyleSheet.create({
    input: {
        borderBottomWidth: 0,
        backgroundColor: Color.white,
        height: 40,
        marginVertical: 5,
        padding: 0,
        margin: 0,
    },
    inputStyle: {
        backgroundColor: 'white',
        paddingLeft: 20,
        paddingVertical: 5,
        fontFamily: 'Poppins'
    },
    inputsLeftIcon: {
        height: 40,
        alignItems: 'center',
        paddingRight: 5
    },
    inputIconSize: {
        width: 15,
        height: 15,
    },
    label: {
        color: Color.primary,
        width: "100%",
        paddingHorizontal: 15,
    },
});

export default InputBogui;