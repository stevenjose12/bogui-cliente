import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
} from "react-native";
import Colors from "../assets/Color";
import LabelForm from "./LabelForm";
import Button from "./Button";
import Hbox from "./Hbox";
import Api from "../utils/Api";
import Toast from "./Toast";
import ModalAlert from "../utils/Modal";
import QualifyModal from "./QualifyModal";
import G from "../utils/G";
import { connect } from "react-redux";
import env from "../utils/env";
import { socket } from "../utils/Socket";
import WebPay from "../screens/webpay/WebPay";

const fontSize = 16;
const labels = ["Punto adicional", "Destino"];

class Bill extends Component {

  static navigationOptions = {
    header: null
    // Usando el modo de bloqueo del cajón aquí ...
    // drawerLockMode: "locked-closed"
  };

  places = [];

  state = {
    visible: false,
    route: null,
    vehicle: "",
    address: []
  };

  componentDidMount() {
    const route = this.props.navigation.getParam("route");
    this.show(route);
    this.setState({ route });
    socket.on("webpay", data => {
      console.log(">>: Bill > componentDidMount > webpay > data: ", data);
      if (data.id !== this.props.user.id) return;
      if (this.modal) this.modal.dismiss();
      if (data.status === 1) {
        this.payment();
      } else {
        Toast.show("Lo sentimos, no se pudo procesar su pago");
      }
    });
  }

  show = async (route): void => {
    this.defaultValues();

    this.setState({
      visible: true,
      route
    });

    G.getVehicleNameByCategory(route.vehicle_type_id).then(vehicle =>
      this.setState({ vehicle })
    );

    const address = G.getPlacesFromRoute(route).address;
    address.shift();
    this.setState({ address });

    this.total = route.cost;

    try {
      const promotion = await Api.promotionByRoute(route.id);
      this.commission = promotion.comision;
      if (promotion.success) {
        this.code = promotion.promocion;
        this.total = !this.code ? route.cost : route.cost - this.code.amount;
        if (this.total < 0) this.total = 0;
        this.forceUpdate();
      }
    } catch (e) {}
  };

  calcTariff = () => {

    const data = {
      id: this.state.route.id,
      user_id: this.props.user.id,
      total_user: this.total,
      payMethod: this.props.user.default_payment,
      total_code: 0,
      p_code: 0
    };

    data.code = this.code // <----------------------------------------------------------------------------------------- número del código promocional
      ? this.code.id
      : 0;

    //<editor-fold desc="Calculando el monto a pagar con código promocional">
    if (this.code) {
      data.total_code = (this.code.amount > this.state.route.cost)
        ? this.state.route.cost
        : this.code.amount;
    }//</editor-fold>

    //<editor-fold desc="Porcentaje del código promocional">
    if (this.code) {
      data.p_code = data.total_code * this.commission;
      if (data.p_code % 50 !== 0) data.p_code = Math.floor(data.p_code / 50) * 50;
    }
    //</editor-fold>

    //<editor-fold desc="Porcentaje total">
    let p_total = this.state.route.cost * this.commission;
    if (p_total % 50 !== 0) p_total = Math.floor(p_total / 50) * 50;
    //</editor-fold>

    data.p_user = p_total - data.p_code; // <------------------------------------------------------------------------- Porcentaje del pago del usuario
    return data;
  };

  payment = async (): void => {
    try {
      await Api.payRoute(this.calcTariff());
      Toast.show("Pago exitoso");
      this.setState({ visible: false });
      this.props.navigation.navigate("Home");
      if (this.props.success) this.props.success(this.state.route);
    } catch (e) {
      ModalAlert.showGenericError(e);
    }
  };

  modify = () => {
    this.props.navigation.navigate("AddCard");
    this.setState({ visible: false });
  };

  pay = async () => {

    // PAGO EFECTIVO
    if (this.props.user.default_payment === 2) {
      this.payment();
      return;
    }

    const params = this.calcTariff();
    console.log(">>: Bill > pay > params: ", params);

    // PAGO CON TARJETA PERO EL MONTO DEL USUARIO ES 0
    if (params.total_user === 0) {
      this.payment();
      return;
    }

    try {
      const r = await Api.paymentUpdate(params);
      console.log(">>: Bill > pay > paymentUpdate > r: ", r);
      const webpay = await Api.webpayCheck(this.props.user.id);
      if (!webpay.result) {
        Toast.show("Tarjeta de credito no autorizada");
        return;
      }

      console.log(">>: Bill > pay > webpay: ", webpay);
      const url = webpay.result
        ? env.BASE_PUBLIC + "api/webpay/response?id=" + this.state.route.id
        : env.BASE_PUBLIC + "api/webpay?id=" + this.state.route.id;
      console.log(">>: Bill > pay > url: ", url);
      this.modal.show(url);
    } catch (e) {
      ModalAlert.showGenericError(e);
    }

  };

  defaultValues = () => this.code = null;

  render() {
    return (
      <View style={{ width: "100%", height: "100%" }}>
        {this.state.route && (
          <View style={styles.container}>

            <View style={styles.banner}>
              <Text style={styles.title}>
                Factura
              </Text>
            </View>

            <ScrollView>
              <View style={styles.body}>
                <View style={{ paddingBottom: 20 }}>

                  <LabelForm
                    vertical
                    style={styles.label}
                    fontSize={fontSize}
                    label="Vehículo"
                    value={this.state.vehicle}/>

                  <LabelForm
                    vertical
                    style={styles.label}
                    fontSize={fontSize}
                    label="Conductor"
                    value={
                      this.state.route.driver.person.name +
                      " " +
                      this.state.route.driver.person.lastname
                    }/>
                </View>

                <View style={{ paddingBottom: 20 }}>
                  <LabelForm
                    vertical
                    style={styles.label}
                    fontSize={fontSize}
                    label="Punto de partida"
                    value={this.state.route.description}
                  />

                  {!!this.state.address.length &&
                    this.state.address.map((address, i) => (
                      <LabelForm
                        vertical
                        key={i.toString()}
                        style={styles.label}
                        fontSize={fontSize}
                        label={labels[i]}
                        value={address}
                      />
                    ))}
                </View>

                <View style={{ paddingBottom: 20 }}>
                  <LabelForm
                    style={styles.label}
                    fontSize={fontSize}
                    styleValue={{
                      color: Colors.accent,
                      fontFamily: "Poppins-SemiBold"
                    }}
                    label="Método de Pago"
                    value={
                      this.props.user.default_payment === 1
                        ? "Tarjeta"
                        : "Efectivo"
                    }
                  />
                </View>

                {this.code && (
                  <View style={{ paddingBottom: 20 }}>
                    <LabelForm
                      style={styles.label}
                      fontSize={fontSize}
                      styleValue={{
                        color: Colors.accent,
                        fontFamily: "Poppins-SemiBold"
                      }}
                      label="Monto de la Promoción"
                      value={
                        " CLP " +
                        G.formatMoney(this.code ? this.code.amount : 0)
                      }
                    />

                    <LabelForm
                      style={styles.label}
                      fontSize={fontSize}
                      styleValue={{
                        color: Colors.accent,
                        fontFamily: "Poppins-SemiBold"
                      }}
                      label={
                        "Total a pagar " +
                        (this.props.user.default_payment == "2"
                          ? " en Efectivo"
                          : "con Tarjeta")
                      }
                      value={" CLP " + G.formatMoney(this.total)}
                    />
                  </View>
                )}

                <Hbox style={{ paddingVertical: 40 }}>
                  <View style={{ flex: 1 }} />

                  <Text style={{ fontSize: fontSize, color: Colors.primary }}>
                    Costo de viaje:
                  </Text>

                  <Text
                    style={{
                      fontSize: fontSize,
                      fontFamily: "Poppins-SemiBold",
                      color: Colors.accent
                    }}
                  >
                    {" CLP " + G.formatMoney(this.state.route.cost)}
                  </Text>
                </Hbox>
                {this.props.user.default_payment === 1 ? (
                  <View
                    style={{
                      width: "100%",
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Text style={{ fontStyle: "italic", textAlign: "center" }}>
                      {" "}
                      "Si desea editar sus datos de tarjeta, presione Modificar"
                    </Text>
                  </View>
                ) : null}
              </View>
            </ScrollView>

            <View style={[styles.banner, { flexDirection: "row" }]}>
              {this.props.user.default_payment === 1 ? (
                <View style={{ width: "50%", height: 50, padding: 5 }}>
                  <Button
                    buttonStyle={{
                      backgroundColor: Colors.primary,
                      width: "100%",
                      height: 40,
                      alignSelf: Platform.OS === "ios" ? "center" : null
                    }}
                    titleStyle={{ width: "100%" }}
                    title="Modificar"
                    onPress={this.modify}
                  />
                </View>
              ) : null}

              <View style={{ width: "50%", height: 50, padding: 5 }}>
                <Button
                  buttonStyle={{
                    backgroundColor: Colors.primary,
                    width: "100%",
                    height: 40,
                    alignSelf: Platform.OS === "ios" ? "center" : null
                  }}
                  titleStyle={{ width: "100%" }}
                  title="Continuar"
                  onPress={this.pay}
                />
              </View>
            </View>

            <QualifyModal
              type={1}
              user={this.props.user} />

            <WebPay ref={ modal => this.modal = modal } />

          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    backgroundColor: Colors.white
  },
  banner: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  body: {
    padding: 15
  },
  title: {
    color: Colors.accent,
    fontFamily: "Poppins-SemiBold",
    fontSize: 40
  },
  label: {
    width: "100%"
  }
});

const redux = state => ({
  user: state.auth.user
});

export default connect(redux, null, null, { forwardRef: true })(Bill);
