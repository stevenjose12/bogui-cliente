import React, { Component } from 'react';
import { Dimensions, ImageBackground, Modal, StyleSheet, Text, View } from "react-native";
import Colors from "../assets/Color";
import { Avatar } from 'react-native-elements';
import {Button} from "react-native-elements";
import Api from "../utils/Api";
import LabelForm from './LabelForm';
import Ranting from './Ranting';
import {connect} from "react-redux";
import ENV from "../utils/env";

const large = Dimensions.get('window').width*0.95;

class ModalBoguiHere extends Component {

    state = {
        visible: false,
        data:{},
    };

    show = (show: boolean) => {
        if (show) this.getRoute();
        this.setState( { visible: show } );
    };

    getRoute = async () => {
        try {
            const r = await Api.getRoutesByUser(this.props.user.id);
            this.setState({ data: r.ruta });
        } catch(e) {
            console.log('>>: BoguiHere > gerRoute > error: ', e);
        }
    };

    render() {
        return (
            <Modal
                animationType="none"
                useNativeDriver
                transparent
                visible={ this.state.visible } >

                <View style={ styles.container }>

                    <ImageBackground
                        source={ require('../assets/icons/white.png') }
                        style={ styles.containerText }
                        imageStyle={ { resizeMode: 'cover' } }>

                        <View style={ styles.closeButtonContainer }>
                            <Text style={{fontSize: 18, color: 'black', paddingVertical: 10}}>Llegó tu Bogui</Text>
                            <Avatar
                                source={ this.state.data && this.state.data.driver ? {uri: ENV.BASE_PUBLIC+this.state.data.driver.person.photo} : require('../assets/icons/user.png') }
                                size={90}
                                rounded />
                                {
                                    this.state.data && this.state.data.driver
                                        ? <Ranting style={ { paddingTop: 3 } } ranting={ parseFloat(this.state.data.driver.average)}/>
                                        : null
                                }
                        </View>

                        <View style={styles.bodyContainer}>

                            <LabelForm
                                label="Nombre"
                                value={ this.state.data && this.state.data.driver ? this.state.data.driver.person.name+' '+this.state.data.driver.person.lastname : 'Sin nombre'}
                                fontSize={14}
                                style={{width: 'auto'}}
                                styleValue={{flex: 0}} />

                            <LabelForm
                                label="Vehículo"
                                value={ this.state.data && this.state.data.driver ? this.state.data.driver.vehicle.brand+' '+this.state.data.driver.vehicle.model : 'Sin vehiculo'}
                                fontSize={14}
                                style={{width: 'auto'}}
                                styleValue={{flex: 0}} />

                            <LabelForm
                                label="Placa"
                                value={this.state.data && this.state.data.driver ? this.state.data.driver.vehicle.plate : 'Sin placa'}
                                fontSize={14}
                                style={{width: 'auto'}}
                                styleValue={{flex: 0}} />

                        </View>

                        <View style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
                            <Button
                                title="Cerrar"
                                onPress={() => this.show(false)}
                                containerStyle={{ width: '50%', padding: 10}}
                                buttonStyle={{backgroundColor: 'transparent', borderColor: Colors.primary, borderWidth: 1,  borderRadius: 20, height: 35}}
                                titleStyle={{color: Colors.primary}} />
                        </View>

                    </ImageBackground>

                </View>

            </Modal>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: '100%',
        padding: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    containerText:{
        backgroundColor: 'white',
        width: large,
        height: large
    },
    closeButtonContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    closeBtn: {
        width: 40,
        height: 40,
        padding: 0,
        position: 'absolute',
        right: 0,
    },
    bodyContainer: {
        width: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rowContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    touchableView: {
        margin: 10,
        width: '70%',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 5,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    textItem: {
        textAlign: 'right',
        fontSize: 14,
        color: Colors.primary
    },
    title:{
        fontFamily: 'Poppins-Bold',
        color: Colors.primary,
        fontSize: 20
    },
    description: {
        color: Colors.primary,
        textAlign: 'center',
        fontSize: 14
    }
});

const redux = state => {
    return {
        user: state.auth.user,
        route: state.route.route,
    }
};

export default connect(redux, null, null, { forwardRef: true })(ModalBoguiHere);