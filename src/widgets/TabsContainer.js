import React, { Component  } from 'react';
import { StyleSheet, View } from "react-native";
import Tab from "./Tab";

class TabsContainer extends Component {

    render() {
        return (
            <View style={ styles.container }>

                {
                    this.props.tabs.map( (tab, i)  =>

                        <Tab
                            key={ i }
                            text={ tab }
                            selected={ i === this.props.tabSelected }
                            onPress={ () => {
                                if (i!==this.props.tabSelected) {
                                    this.props.tabSelected = i;
                                    if (this.props.onChangeTab) this.props.onChangeTab(i);
                                    this.forceUpdate();
                                }
                            } }/>

                    )
                }

            </View>
        )
    }

}

const styles = StyleSheet.create( {
    container: {
        width: '100%',
        flexDirection: 'row'
    },
    tab: {
        flex: 1,
        height: 40
    }
} );

export default TabsContainer;