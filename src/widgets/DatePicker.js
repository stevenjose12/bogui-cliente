import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import Colors from '../assets/Color';
const _DatePicker = props => {
	return (
		<View style={ [styles.container,props.style] }>
            <View style={ styles.viewContainer }>
            	{ props.value === null ?
                    <Text numberOfLines={ 1 } style={ styles.placeholderCustom }>{ props.placeholder }</Text>
                    :
                    <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', top: 12}}>
                        <Text numberOfLines={ 1 } style={{width: '60%'}}>{props.placeholder}</Text>
                        <Text style={{width: '30%'}}>{props.value}</Text>
                    </View>
                }
	            <DatePicker
	            	androidMode="spinner"
            		locale="es"
            		placeholder={ props.placeholder ? props.placeholder : null }
            		format={ props.format ? props.format : "DD/MM/YYYY" }
            		date={ props.value ? props.value : null }
            		onDateChange={ props.onChange }
            		confirmBtnText="Aceptar"
            		cancelBtnText="Cancelar"
            		showIcon={ false }
            		minDate={ props.minDate }
            	    maxDate={ props.maxDate }
            		customStyles={{
			          dateInput: styles.input,
			          placeholderText: styles.placeholder,
			          dateText: styles.text,
			          dateTouchBody: styles.touchBody
			        }}
			        style={ styles.inputContainer } />
            </View>
        </View>
	)
}

const styles = StyleSheet.create({
	container: {
		width: '100%'
	},
	input: {
		alignItems: 'flex-start',
		paddingHorizontal: 5,
		borderColor: 'transparent',
		fontFamily: 'Poppins'
	},
	touchBody: {
		paddingTop: 7.5,
		fontFamily: 'Poppins'
	},
	text: {
		fontSize: 16,		
        fontFamily: 'Poppins',
        color: 'transparent'
	},
	placeholder: {
		fontSize: 16,
		color: Colors.gray,
		fontFamily: 'Poppins',
		opacity: 0
	},
	inputContainer: {
		width: '100%',
		height: 50,
		position: 'absolute',
		zIndex: 2
	},
	viewContainer: {
		backgroundColor: '#fff',
		borderWidth: 1,
		borderColor: '#000',
		width: '100%',
		height: 50
	},
	label: {
		fontWeight: 'bold',
		color: '#000',
		fontSize: 16,
		paddingBottom: 3,
		marginTop: 10,
		textAlign: 'left',
		width: '94%'
	},
	placeholderCustom: {
		fontSize: 16,
		color: Colors.gray,
		fontFamily: 'Poppins',
        width: '95%',
        textAlign: 'center',
		marginLeft: '2.5%',
		paddingTop: 10,
		position: 'absolute',
		zIndex: 1
	}
});

export default _DatePicker;