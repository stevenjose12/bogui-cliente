import React, { Component } from 'react';
import {Image, Modal, StyleSheet, Text, View, TouchableWithoutFeedback, Platform } from "react-native";
import Colors from "../assets/Color";
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Button} from "react-native-elements";
import Api from "../utils/Api";
import CustomModal from "../utils/Modal";
import ENV from "../utils/env";

class ModalCategoryBogui extends Component {

    state = {
        visible: false,
        vehicles: [],
        outVehicles: false,
    };

    componentDidMount() {
        this.getVehicles();
    }

    getVehicles = async () => {
        try {
            const r = await Api.vehiclesAvailable();
            if (r) {
                this.setState(state => state.vehicles = r);
                const setVehiclesRedux = {
                    condition: true,
                    vehicles: r,
                };
                this.props.onOutVehicles(setVehiclesRedux)
            }
            else CustomModal.showGenericError();
        } catch (e) {
            const vehiclesRedux= this.props.onOutVehicles({condition: false});
            if (vehiclesRedux){
                this.setState(state => {
                    state.vehicles = vehiclesRedux;
                    state.outVehicles = true;
                });
            }
        }

    };

    show(show: boolean) {
        this.setState( state => state.visible = show );
    }

    render() {
        return (
            <Modal
                animationType="slide"
                transparent
                visible={ this.state.visible }
                useNativeDriver={ true } >

                <View style={styles.container}>

                    {/* HEADER */}
                    <View style={ styles.closeButtonContainer }>
                        <Button
                            buttonStyle={ styles.closeBtn }
                            type="clear"
                            icon={
                                <Icon
                                    color={ Colors.secondary }
                                    size={ 30 }
                                    outline={true}
                                    name={ 'times-circle' } />
                            }
                            onPress={ () => this.show(false) } />
                    </View>

                    <View style={styles.bodyContainer}>

                        <Image
                            source={require('../assets/icons/isotipo.png')}
                            style={{width: 80, height: 80, marginBottom: 20}}
                            resizeMode="contain" />

                        <Text style={ styles.title}>Selecciona tu Bogui</Text>

                        {
                            this.state.vehicles.map( (element, i) =>
                                (
                                <View  key={i} style={ styles.rowContainer }>

                                    <TouchableWithoutFeedback onPress={ () => {
                                        this.props.onSelect ? this.props.onSelect(element) : null;
                                        this.show(false);
                                    } }>

                                        <View style={ styles.touchableView }>

                                            <Image
                                                source={ this.state.outVehicles ? require('../assets/img/bogui1.png') : {uri: ENV.BASE_PUBLIC+element.photo} }
                                                style={{width: 68, height: 42, marginRight: 5}}
                                                resizeMode="contain" />

                                            <Text style={ styles.textItem }> {element.name} </Text>

                                        </View>

                                    </TouchableWithoutFeedback>

                                    <Text style={ styles.description }> {element.description} </Text>

                                </View>
                                ) )
                        }

                    </View>

                </View>

            </Modal>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: '100%',
        backgroundColor: 'rgba(255, 255, 255, .80)',
        padding: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    closeButtonContainer: {
        width: '100%',
        position: 'relative',
        height: 40,
    },
    closeBtn: {
        width: 40,
        height: 40,
        padding: 0,
        position: 'absolute',
        right: 0,
    },
    bodyContainer: {
        width: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rowContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    touchableView: {
        margin: 10,
        paddingVertical: 25,
        width: '70%',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 5,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    textItem: {
        textAlign: 'right',
        fontSize: 20,
        color: Colors.primary,
        height: Platform.OS == 'ios' ? 40 : null,
        paddingTop: Platform.OS == 'ios' ? 5 : null
    },
    title:{
        fontFamily: 'Poppins-SemiBold',
        color: Colors.primary,
        fontSize: 20
    },
    description: {
        color: Colors.primary,
        textAlign: 'center',
        fontSize: 20
    }
});

export default ModalCategoryBogui;