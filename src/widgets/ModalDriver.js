import React, { Component } from 'react';
import {ActivityIndicator, Modal, StyleSheet, View, Platform} from "react-native";
import Colors from "../assets/Color";
import Hbox from "./Hbox";
import { Avatar } from 'react-native-elements';
import Button from './Button';
import env from "../utils/env";
import Ranting from "./Ranting";
import LabelForm_ from "./LabelForm";
import MapPlotRoute from "./MapPlotRoute";
import Api from "../utils/Api";
import Geo from "../utils/Geo";
import G from "../utils/G";

const markers =  [
    require('../assets/icons/point_start.png'),
    require('../assets/icons/point_stop.png'),
    require('../assets/icons/point_goal.png')
];

class ModalDriver extends Component {

    state = {
        show: false,
        driver: null,
        address: "",
        time: null
    };

    show = async (show: boolean, driverId = null) => {

        if (!show) {
            this.setState( { show });
            return;
        }

        try {
            const origin = this.props?.places[0]?.location;
            const r = await Api.getDriver(driverId);
            if (!r.success) return;
            await this.setState({ driver: r.driver});
            const driverLocation = {
                latitude: parseFloat(this.state.driver.lat),
                longitude: parseFloat(this.state.driver.lng)
            };
            this.setState( { show });
            const address = await Geo.getAddress( driverLocation );
            this.setState( { address });
            const time = await G.getTimeBetweenCoords(origin, driverLocation );
            this.setState( { time });
            this.map.setPlaces( this.props.places );
        } catch (e) {
            this.setState( { show: false });
        }

    };

    dismiss = callback => {
        if (!this.state.show) {
            callback();
        } else {
            this.setState( { show: false }, () => setTimeout(callback, 500) );
        }
    };

    render() {

        const LabelForm = props => ( <LabelForm_ {...props}  fontSize={15} /> );

        return (
            <Modal
                animationType="none"
                visible={ this.state.show }
                transparent
                useNativeDriver>

                <View style={ styles.container }>

                    <View style={ styles.card }>

                        {
                            this.state.driver
                                ?
                                <View style={ { padding: 10 } }>

                                    <Hbox style={ { justifyContent: 'center' } }>

                                        <View style={ { width: '50%', alignItems: 'center' } }>
                                            <Avatar
                                                rounded
                                                source={{ uri: env.BASE_PUBLIC+this.state.driver.photo }}
                                                size={ 110 }
                                                containerStyle={ styles.avatar } />
                                        </View>

                                        <View style={ { width: '50%', alignItems: 'center' } }>
                                            <Avatar
                                                rounded
                                                source={{ uri: env.BASE_PUBLIC+this.state.driver.file }}
                                                size={ 110 }
                                                containerStyle={ styles.avatar } />
                                        </View>

                                    </Hbox>

                                    <Ranting style={ { paddingVertical: 5 } }  ranting={ this.state.driver.rantng }/>

                                    <LabelForm
                                        label="Nombre"
                                        value={ this.state.driver.name+" "+this.state.driver.lastname }/>

                                    <LabelForm
                                        label="Teléfono"
                                        value={ G.hideLastFourDigits(this.state.driver.phone) }/>

                                    <LabelForm
                                        label="Vehículo"
                                        value={ this.state.driver.brand+" "+this.state.driver.model }/>

                                    <LabelForm
                                        label="Patente"
                                        value={ this.state.driver.plate }/>
                                    {
                                        !!this.state.time && (
                                            <LabelForm
                                                label="Tiempo de recogida"
                                                value={ this.state.time }/>
                                        )
                                    }

                                    {
                                        !!this.state.address && (
                                            <LabelForm
                                                vertical
                                                label="Ubicación"
                                                value={ this.state.address }/>
                                        )
                                    }

                                </View>
                                :
                                <View>
                                    <ActivityIndicator />
                                </View>
                        }

                        <View style={ {width: '100%', height: 100} }>
                            <MapPlotRoute
                                ref={ map => this.map = map }
                                markers={ markers }/>
                        </View>

                        <View style={ styles.buttonContainer }>
                            <Button
                                title="Cerrar"
                                buttonStyle={ styles.closeBtn }
                                titleStyle={ { color: Colors.primary, width: "100%" } }
                                onPress={ () => this.show(false) } />
                        </View>

                    </View>

                </View>

            </Modal>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.17)',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        backgroundColor: "white",
        width: "80%",
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
        borderRadius: 10
    },
    closeBtn: {
        width: "70%",
        borderColor: Colors.primary,
        borderWidth: 1,
        borderRadius: 20,
        backgroundColor: Colors.white,
        alignSelf: Platform.OS == 'ios' ? 'center' : null
    },
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        paddingVertical: 10,
    },
    avatar: {
        marginBottom: 10
    }
});

export default ModalDriver;