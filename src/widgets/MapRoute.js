import React, { Component } from 'react';
import {ActivityIndicator, Image, StyleSheet, View} from 'react-native';
import MapView from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import Color from "../assets/Color";
import ENV from "../utils/env";
import Animation from "../utils/Animation";
import Night from "../assets/mapanocturno";
import Day from "../assets/mapadiurno";

class MapRoute extends Component {

    places = [];
    waypoints = [];
    markers = []; // <-------------------------------------------------------------------------------------------------- Markers para los diferentes puntos
    driver: MarkerData = null;
    initRoute: null;

    componentDidMount() {
        this.animation = new Animation(this.props.timeAnimation);
    }

    anim = (oldDriver, driver) => {

        this.driver = oldDriver;

        try {

            //<editor-fold desc="PERSISTIENDO DATOS">
            const delta = {};
            delta.lat = oldDriver.latitude;
            delta.lng = oldDriver.longitude;
            delta.dlat = driver.latitude - oldDriver.latitude;
            delta.dlng = driver.longitude - oldDriver.longitude;
            delta.rotation = oldDriver.rotation;
            delta.dRot = driver.rotation - oldDriver.rotation;
            if (Math.abs(delta.dRot)>180) delta.dRot = (delta.dRot<0 ? 1 : -1 )*360+delta.dRot;
            //</editor-fold>

            this.animation.onUpdate( (anim: Animation) => {

                if (oldDriver && driver) {

                    const lat = delta.lat + anim.delta( delta.dlat );
                    const lng = delta.lng + anim.delta( delta.dlng );

                    if ( !isNaN(lat) && !isNaN(lng) ) {
                        this.driver.latitude = lat;
                        this.driver.longitude = lng;
                        const rot = delta.rotation + anim.delta( delta.dRot );
                        this.driver.rotation = isNaN(rot) ? oldDriver.rotation : rot;
                    }

                }

                if (anim.i%10 === 0 && this.driver) this.initRoute = this.driver.getLocation();

                this.forceUpdate();
            });

            this.animation.start();

        } catch (e) {
            // console.log(">>: MapRoute > anim > error: ", e);
        }

    };

    getCameraCenter = () => this.cameraCenter;

    setPlaces = (places, markers) => {

        try {
            this.markers = markers;
            this.places = places;
            this.origin = this.places[0];
            this.waypoints = [];
            for (let i = 1; i < this.places.length - 1; i++) {
                const item = this.places[i];
                if (item) this.waypoints[i - 1] = item;
            }
            this.destination = this.places[this.places.length - 1];
            this.forceUpdate();
        } catch (e) {
            console.log(">>: MapRoute > setPlaces > error: ", e);
        }

    };

    location = location => this.map.animateCamera( { center: location }, 1000 );

    render() {
        return (
            <View style={styles.container}>

                {/* HEADER */}
                <View style={{ ...styles.header, ... this.props.styleHeader}}>
                    { this.props.header }
                </View>

                {/* FOOTER */}
                <View style={{ ...styles.footer, ... this.props.styleFooter }}>
                    { this.props.footer }
                </View>

                {
                    this.props.center
                        ?
                        <MapView
                            ref={ map => this.map = map }
                            style={ styles.map }
                            customMapStyle={ this.props.nightMode ? Night : Day }
                            camera={ {
                                center: this.props.center,
                                pitch: 1,
                                heading: 1,
                                zoom: 15,
                                altitude: 15,
                            } }
                            provider="google"
                            onRegionChangeComplete={ e => {
                                this.cameraCenter = { latitude: e.latitude, longitude: e.longitude };
                                this.props.onDragEnd ? this.props.onDragEnd(e.nativeEvent) : null
                            } }
                            showsCompass={false}
                            rotateEnabled={false}
                            pitchEnabled={false}
                            zoomControlEnabled
                            zoomEnabled
                            toolbarEnabled
                            loadingEnabled
                            onRegionChange={ e => this.props.onDrag ? this.props.onDrag(e.nativeEvent) : null } >

                            {/*  */}
                            {
                                this.driver
                                    ?
                                    <MapView.Marker
                                        coordinate={ this.driver.getLocation() }
                                        rotation={ parseFloat(this.driver.rotation) }
                                        anchor={{x: 0.5, y: 0.5}} >

                                        <Image
                                            source={ this.props.markerIcon }
                                            renderToHardwareTextureAndroid={true} />

                                    </MapView.Marker>
                                    : null
                            }

                            {/* PLACE MARKERS */}
                            {
                                this.places.length>=0
                                    ?
                                    this.places.map( (place, i)  => place && this.markers[i]
                                        ?
                                        <MapView.Marker key={i} coordinate={ place } >
                                            <Image source={ this.markers[i] } />
                                        </MapView.Marker>
                                        : null
                                    )
                                    : null

                            }

                            {/* GRAFICACIÓN DE LA RUTA */}
                            <MapViewDirections
                                resetOnChange={false}
                                origin={ this.initRoute || this.origin }
                                destination={ this.destination }
                                waypoints={ this.waypoints.length ? this.waypoints : null }
                                strokeWidth={4}
                                strokeColor={ this.props.nightMode ? Color.chat : Color.primary }
                                apikey={ ENV.apiKey }
                                onReady={ result => { /* this.map.fitToCoordinates(result.coordinates) */} }/>

                        </MapView>
                        :
                        <View style={ styles.loading }>
                            <ActivityIndicator />
                        </View>

                }

            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        position: "relative",
        flex: 1
    },
    map: {
        width: "100%",
        height: "100%",
        alignItems: "center",
        justifyContent: "center",
    },
    loading: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.white
    },
    header: {
        width: '100%',
        zIndex: 2,
        position: "absolute",
        top: 0,
    },
    footer: {
        position: "absolute",
        bottom: 0,
        left: 0,
        zIndex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

class MarkerData {

    id: number;
    latitude: number;
    longitude: number;
    rotation: number = 0;
    time: number = 0;

    constructor(id: number, latitude: number, longitude: number) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.time = new Date().getTime();
    }

    getLocation() {
        return {
            latitude: this.latitude,
            longitude: this.longitude
        }
    }

    setParams(latitude, longitude, rotation, time) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.rotation = rotation;
        this.time = time;
    }

    clone() : MarkerData {
        const marker: MarkerData = new MarkerData(this.id, this.latitude, this.longitude);
        marker.rotation = this.rotation;
        marker.time = this.time;
        return marker;
    }

}


export { MapRoute, MarkerData };