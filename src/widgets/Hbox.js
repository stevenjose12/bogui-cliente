import React from 'react';
import { View } from "react-native";

const Hbox = props => (

    <View style={ { flexDirection: 'row', width: "100%", ...props.style } }>
        { props.children }
    </View>

);

export default Hbox;