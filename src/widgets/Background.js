import React, { Fragment } from "react";
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View
} from "react-native";
import { Panic } from "../utils/Panic";
import ImageBackground from '../widgets/ImageBackground';

const withPanicEvent = Component => props => (
  <Component delayLongPress={Panic.DELAY} onLongPress={Panic.emit} {...props} />
);

const TouchableWithoutFeedbackWithPanicEvent = withPanicEvent(TouchableWithoutFeedback);

const Background = props => {

  const Touchable = props.noPanicEvent
    ? Fragment
    : TouchableWithoutFeedbackWithPanicEvent;

  return (
    <Touchable>
      <View
        style={{
          paddingTop: props.header ? 40 : 0,
          ...styles.container,
          ...props.style
        }}
      >
        {props.noBackground ? (
          <View style={{ ...styles.container, ...props.styleContainer }}>
            {props.children}
          </View>
        ) : (
          <ImageBackground
            source={require("../assets/img/background.jpg")}
            style={styles.container}
          >
            {props.children}
          </ImageBackground>
        )}
      </View>
    </Touchable>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  }
});

export default Background;
