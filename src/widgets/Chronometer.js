import React, { Component } from "react";
import {StyleSheet, Text, View, Platform} from "react-native";
import Color from "../assets/Color";
import moment from 'moment';
import numeral from "numeral";
import G from "../utils/G";
import {connect} from "react-redux";
import Task from "../utils/Task";

class Chronometer extends Component {

    state = {
        time: 0,
        visible: false,
    };

    getMinutes = () => {
        return numeral(moment.duration(this.state.time,'seconds').minutes()).format('00') + ':' + numeral(moment.duration(this.state.time,'seconds').seconds()).format('00');
    };

    componentDidMount() {
        if (this.props.time) this.start();
    }

    start = async () => {

        await this.setState( state => state.visible = true );
        await this.setState( state => state.time = 0 );
        const start = this.props.time ? this.props.time : new Date().getTime();

        await this.props.dispatch({type: 'SET_TIME', payload: { time: start} } );

        Task.add("counter", 125, async () =>
            await this.setState(state => state.time = Math.round((new Date().getTime() - start)) / 1000 )
        );
    };

    stop = async () => {
        await this.props.dispatch({type: 'SET_TIME', payload: { time: null} } );
        Task.remove("counter");
        this.setState( state => state.visible = false );
    };

    render() {
        return(
            <View style={ styles.container }>

                {
                    this.state.visible
                        ?
                        <View style={ styles.chronometer }>

                            <Text style={ styles.label }> Tiempo en parada adicional </Text>

                            <Text style={ styles.time }> { this.getMinutes() } </Text>

                        </View>
                        : null
                }

            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    chronometer: {
        marginTop: 10,
        paddingHorizontal: 30,
        paddingTop: 15,
        backgroundColor: Color.blue,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        color: Color.white,
        fontSize: 10
    },
    time: {
        fontSize: 40,
        textAlign: 'center',
        fontFamily: "Poppins-Bold",
        color: Color.white,
    }
});

const redux = state => {
    return {
        time: state.chronometer.time
    }
};

export default connect(redux, null, null, { forwardRef: true })(Chronometer);