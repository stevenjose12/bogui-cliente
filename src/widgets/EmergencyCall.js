import React from 'react';
import {View, TouchableWithoutFeedback, Image, Linking, Platform} from "react-native";

const EmergencyCall = props => (

    <TouchableWithoutFeedback onPress={ () => Linking.openURL(Platform.OS === "ios" ? `telprompt:${133}` : `tel:${133}`) }>

        <View style={ { width: 40, height: 40, borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: props.night ? 'white' : 'black', ...props.style } }>

            <Image
                style={ { width: 30, height: 30 } }
                source={ require('../assets/icons/call133.png') }
                resizeMode="contain"
                tintColor={ props.night ? 'black' : 'white' }/>

        </View>

    </TouchableWithoutFeedback>

);

export default EmergencyCall;