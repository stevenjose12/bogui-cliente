import React, {Component} from 'react';
import {Image, Modal, StyleSheet, Text, View, TouchableWithoutFeedback } from "react-native";
import Background from '../../widgets/Background';
import {connect} from "react-redux";

const items = [
    { label: 'Partida', icon: require('../../assets/icons/start_marker.png') },
    { label: 'Adicional', icon: require('../../assets/icons/stop_marker.png') },
    { label: 'Destino', icon: require('../../assets/icons/goal_marker.png') }
];

const HeaderDetail = props => (

    <View style={ { backgroundColor: 'white' } }>
        {
            !!props.places && props.places.map( (element, i) => {
                if (element.location) {
                    return(
                        <View key={i} style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%'}}>
                            <View style={{width: '20%'}}>
                                <Text numberOfLines={1} style={styles.text}>{items[i]['label']}</Text>
                            </View>
                            <Image
                                source={items[i]['icon']}
                                style={{width: '5%', height: '30%'}}
                                resizeMode="contain" />
                            <Text style={[styles.text, {flex: 1}]} numberOfLines={1}>{element.address}</Text>
                        </View>
                    )
                }
            })
        }
    </View>

);

const styles = StyleSheet.create({
    text: {
        padding: 4,
        fontSize: 12,
        textAlign: 'center'
    }
});

export default HeaderDetail;