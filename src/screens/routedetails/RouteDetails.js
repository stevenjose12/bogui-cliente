import React, { Component } from 'react';
import { View, Dimensions } from "react-native";
import Background from '../../widgets/Background';
import Header from './Details';
import Cost from './Cost';
import Footer from './Footer';
import {connect} from "react-redux";
import Modal from "../../utils/Modal";
import Api from "../../utils/Api";
import { socket } from "../../utils/Socket";
import ResendRequestModal from '../../widgets/ResendRequestModal';
import MapPlotRoute from "../../widgets/MapPlotRoute";
import CustomHeader from "../../widgets/CustomHeader";
import G from "../../utils/G";
import {ScrollView} from "react-native-gesture-handler";
import Color from "../../assets/Color";
import SocketEvents from "../../utils/SocketEvents";

const WIDTH = Dimensions.get('window').width;
const HEIGHT = WIDTH/Math.sqrt(2);

const WAIT_TIME = 75000; // tiempo de espera para mostrar la modal de ruta no aceptada.

class ContainerDetailRoute extends Component {

    static navigationOptions = { header: null };

    state = {
        driverId: null,
        reSendRequest: false
    };

    componentDidMount() {
        this.addSocketEvents();
        this.map.setPlaces( this.props.places );
        this.delay = setTimeout(this.openModalResend, WAIT_TIME);
    }

    addSocketEvents = () => {

        SocketEvents.onCareerAccepted( async data => {
            if (data.user_id !== this.props.user.id) return;
            try {
                const r = await Api.routeAccepted(this.props.route.id);
                if (!r.driver_id) throw 'not driver_id';
                this.props.dispatch( { type: 'SET_ROUTE', payload: { route: { driverId: r.driver_id, ...this.props.route} } } );
                const chatExist = await Api.chatExist(r.id);
                if (!chatExist.exist) await Api.chatCreate({ id: r.id });
            } catch (e) {
                console.log(">>: RouteDetails > addSocketEvent[carrera-aceptada] > error: ", e);
            }
            this.props.dispatch( { type: 'SET_SHOW_MODAL_DRIVER', payload: { showModalDriver: true} } );
            this.props.navigation.navigate("Home", { test: false });
        });

    };

    openModalResend = () => this.setState(
        { reSendRequest: !!this.props.user && !!this.props.route},
        () => { if (this.delay) clearTimeout(this.delay); }
    );

    reSendRequest = async () => {
        try {
            const r = await Api.reSendRequest({id: this.props.route.id});
            this.setState({reSendRequest: false });
            if (r.result) {

                socket.emit('cancelada-cliente', { route: this.props.route.id });

                if (!r.cercano) {
                    const params = {
                        id: r.new.id,
                        motivo: "Cancelación por el Cliente, desde el modal por no tener Boguis cercanos",
                        driver_id: this.props.user.id
                    };
                    await Api.cancelRoute(params);
                    await this.clearRedux();
                    Modal.alert.show({ message: 'No hay conductores Cercanos', time: 3000});
                } else {
                    this.props.dispatch( { type: 'SET_ROUTE', payload: { route: { driverId: r.driver_id, ...r.new  }} } );
                    const data = {
                        ...r.new,
                        carrera: { id: r.cercano.id },
                        partida_descripcion: this.props.places[0].address,
                        adicional_descripcion: this.props.places[1].address ? this.props.places[1].address : null,
                        destino_descripcion: this.props.places[2].address,
                    };
                    socket.emit('solicitude', data);
                    this.delay = setTimeout(this.openModalResend, WAIT_TIME);
                }
            }
        } catch (e) {
            this.setState({reSendRequest: false });
            Modal.showGenericError(e);
        }

    };

    // Cancela la ruta por la modal de "Solicitud no aceptada"
    cancelRequest = async () => {
        try {
            const r = await Api.cancelRouteByModal({route_id: this.props.route.id});
            this.setState({ reSendRequest: false});
            if (r.result) {
                socket.emit('cancelada-cliente', { route: this.props.route.id });
                Modal.alert.show({ message: 'Su viaje fue cancelado', time: 3000});
                await this.clearRedux();
                this.props.navigation.navigate("Home", { test: true });
            }
        } catch (e) {
            this.setState({ reSendRequest: false});
            Modal.showGenericError(e);
        }
    };

    // Cancela la ruta por el boton de "Cancelar Viaje"
    cancelRoute = () => {

        const onPositiveButton = async () => {

            if (this.delay) clearTimeout(this.delay);

            const params = {
                id: this.props.route.id,
                motivo: "Cancelación por el Cliente",
                driver_id: this.props.user.id
            };

            try {
                const r = await Api.cancelRoute(params);
                if (!r.result) return;
                socket.emit('cancelada-cliente', { route: this.props.route.id });
                Modal.alert.show({ message: "Se ha cancelado el viaje", time: 2000 });
                this.clearRedux();
                this.props.navigation.navigate("Home", { test: true });
            } catch (e) {
                Modal.showGenericError(e);
            }

        };
        Modal.alert.onPositiveButton("Si", onPositiveButton );
        Modal.alert.onCancelButton("No");
        Modal.alert.show({ message: '¿Desea cancelar la solicitud?'});
    };

    clearRedux = () => {
        this.props.dispatch( { type: 'SET_REQUEST', payload: { request:  null } } );
        this.props.dispatch( { type: 'SET_ROUTE', payload: { route: null } } );
        this.props.dispatch( { type: 'SET_PLACES', payload: { places: null } } );
    };

    render() {
        return(
            <Background noBackground styleContainer={ { backgroundColor: Color.primary } }>

                <CustomHeader
                    title="Confirmar Viaje"
                    onPress={ () => this.props.navigation.toggleDrawer() } />

                <ScrollView style={ { zIndex: 1000 } }>

                    <Header places={this.props.places} />

                    <View style={ { width: WIDTH, height: HEIGHT } }>
                        <MapPlotRoute
                            ref={ map => this.map = map }
                            markers={ G.markers } />
                    </View>

                    <Cost
                        tariff={ this.props.route ? this.props.route.cost : 0 }
                        payment={ this.props.user.default_payment } />

                    <Footer
                        onPress={ this.cancelRoute }
                        route={ this.props.route }/>

                    <ResendRequestModal
                        visible={ this.state.reSendRequest }
                        onPositiveButton={ this.reSendRequest }
                        onNegativeButton={ this.cancelRequest } />

                </ScrollView>

            </Background>
        )
    }

}

const redux = state => ({
    places: state.places.places,
    user: state.auth.user,
    route: state.route.route
});

export default connect(redux)(ContainerDetailRoute);