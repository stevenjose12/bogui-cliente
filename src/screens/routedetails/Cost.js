import React, {Component} from 'react';
import { StyleSheet, Text, View } from "react-native";
import G from "../../utils/G";

class CostDetail extends Component {
    render() {
        return(
            <View style={styles.container}>
                <Text style={{color: 'black'}}>Tu viaje tendrá un costo de</Text>
                <Text style={{fontWeight: 'bold', fontSize: 20, color: 'black'}}>
                    { "CLP "+ G.formatMoney(this.props.tariff)  }
                </Text>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{color: 'black'}}>Método de pago:</Text>
                    <Text style={{fontWeight: 'bold', paddingHorizontal: 3, color: 'black'}}>
                        {  ( this.props.payment===1 ? "Tarjeta de Crédito" : "Efectivo" ) }
                    </Text>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        width: '100%',
        height: '15%',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'black',
        paddingVertical: 20,
        backgroundColor: 'white'
    }
});
export default CostDetail;