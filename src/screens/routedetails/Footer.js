import React, {Component} from 'react';
import { StyleSheet, Text, View, Animated } from "react-native";
import Colors from '../../assets/Color';
import Button from '../../widgets/Button';
import G from "../../utils/G";

class FooterDetail extends Component {

    state = {
        fadeAnim: new Animated.Value(0)
    };

    componentDidMount() {
        this._calcOpacity();
    }

    _calcOpacity = () =>{
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.fadeAnim,{
                    toValue: 1,
                    duration: 1000,
                }),
                Animated.timing(this.state.fadeAnim,{
                    toValue: 0,
                    duration: 1000,
                })
            ])
        ).start();
    };

    getVehicle() : string {
        switch (this.props.route.vehicle_type_id) {
            case 1: return require('../../assets/icons/bogui1.png');
            case 2: return require('../../assets/icons/bogui2.png');
            case 3: return require('../../assets/icons/bogui3.png');
        }
        return require('../../assets/icons/bogui1.png');
    }

    render() {
        return(

            <View style={styles.container}>

                <View style={{flexDirection: 'row', paddingVertical: 15}}>

                    {
                        !!this.props.route && (
                            <Animated.Image
                                style={{width: '17%', height: 30, opacity: this.state.fadeAnim}}
                                resizeMode="contain"
                                tintColor="#fff"
                                source={ G.getVehicleImageByCategory(this.props.route.vehicle_type_id) } >
                            </Animated.Image>
                        )
                    }
                    <Text
                        style={{color: 'white', paddingHorizontal: 8}}
                        numberOfLines={1}>
                        Buscando Bogui
                    </Text>
                </View>

                <Text
                    style={ styles.text }
                    numberOfLines={1}>
                    Su solicitud está siendo procesada
                </Text>

                <Button
                    title="Cancelar Viaje"
                    containerStyle={{width: '65%'}}
                    buttonStyle={{backgroundColor:'white', borderRadius: 20}}
                    titleStyle={{color: 'black', fontFamily: 'Poppins-Bold'}}
                    onPress={ () => this.props.onPress ? this.props.onPress() : null } />

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 7,
        backgroundColor: Colors.primary,
    },
    text: {
        color: 'white',
        paddingHorizontal: 8,
        fontFamily: 'Poppins-Italic'
    }
});

export default FooterDetail;