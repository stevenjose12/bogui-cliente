import React, { Component } from "react";
import {Image, ImageBackground, Modal, StyleSheet, Text, View, Animated, ActivityIndicator, Platform} from "react-native";
import {Button} from "react-native-elements";
import Color from "../../assets/Color";
import Hbox from "../../widgets/Hbox";
import Api from "../../utils/Api";
import Toast from "../../widgets/Toast";
import {connect} from "react-redux";
import Sound from '../../utils/Sound';
import AudioToggle from '../../utils/AudioToggle';
import { OTSession, OTPublisher, OTSubscriber } from 'opentok-react-native';
import InCallManager from 'react-native-incall-manager';
import ModalAlert from "../../utils/Modal";
import { socket } from "../../utils/Socket";
import SocketEvents from "../../utils/SocketEvents";
import G from '../../utils/G';
import Permission from '../../utils/Permissions';

const apiKey = '46261832';

class Call extends Component {

    static navigationOptions = { header: null };

    state = {
        visible: false,
        speaker: false,
        mute: false,
        fade: [new Animated.Value(0), new Animated.Value(0), new Animated.Value(0)],
        opentok: null,
        streamId: null
    };

    calling = false;

    constructor(props) {
        super(props);
        this.sessionEventHandlers =  {
            streamCreated: event => {
                if (this.timeout) {
                    clearTimeout(this.timeout);
                    this.timeout = null;
                }
                Sound.Call.stop();
                this.setState({ streamId: event.streamId });
            },
            streamDestroyed: event => {
                this.endCall();
            }
        };
    }

    publisherProperties = {
        publishAudio: true,
        publishVideo: false
    };

    subscriberProperties = {
        subscribeToAudio: true,
        subscribeToVideo: false,
    };

    show = async (calling=false) => {
        this.calling = calling;
        await this.setState({ visible: true });
        await this.setState({ streamId: null });
        await this.setState({ opentok: null });
        this.loadingAnimation();
        this.call();
    };

    dismiss = () => {
        Sound.Call.stop();
        G.inCallPage = false;
        this.timeout = null;
        this.setState({ visible: false });
    };

    speaker = async () => {
        await this.setState( { speaker: !this.state.speaker } );
        if (this.state.speaker) {
            AudioToggle.Speaker();
        } else {
            AudioToggle.Earpiece();
        }
    };

    mute = async () => {
        await this.setState( { mute: !this.state.mute } );
        if (Platform.OS == 'android') {
            InCallManager.setMicrophoneMute(this.state.mute);
        }
        else {
            // Se realiza mediante props, no mediante nativo
        }
    };

    endCall = () => {
        if (!this.state.visible) return false;
        if (this.state.opentok) socket.emit('llamada-colgada', { id: this.props.user.id });
        if (Platform.OS == 'android')
            InCallManager.stop();
        AudioToggle.Speaker();
        clearTimeout(this.timeout);
        this.dismiss();
    };

    call = async () => {

        try {
            const r = await Api.getOpentok(this.props.user.id, 1);
            if (r.result) {
                G.inCallPage = true;
                await this.setState({opentok: r});
                if (Platform.OS == 'android')
                    InCallManager.start();
                if (this.calling) Sound.Call.play();
                AudioToggle.Earpiece();
                this.timeout = setTimeout(() => {
                    this.endCall();
                }, 60000);

                if (this.calling) {
                    socket.emit('llamada', {
                        id: this.state.opentok.conductor.id,
                        user_id: this.props.user.id,
                        name: this.props.user.name + ' ' + this.props.user.lastname
                    });
                }

                SocketEvents.onSocketEndCall(data => {
                    if (data.id === this.state.opentok.conductor.id) this.endCall();
                } );

            } else {
                Toast.show("Lo sentimos, no se pudo realizar la llamada");
                this.endCall();
            }
        } catch (e) {
            ModalAlert.showGenericError(e);
            this.endCall();
        }
    };

    loadingAnimation = () => {

        Animated.loop(
            Animated.sequence([
                Animated.parallel([
                    Animated.timing( this.state.fade[0], {
                        toValue: 1,
                        duration: 1000,
                    }),
                    Animated.timing( this.state.fade[2], {
                        toValue: 0,
                        duration: 1000,
                    }),
                ]),
                Animated.parallel([
                    Animated.timing( this.state.fade[1], {
                        toValue: 1,
                        duration: 1000,
                    }),
                    Animated.timing( this.state.fade[0], {
                        toValue: 0,
                        duration: 1000,
                    }),
                ]),
                Animated.parallel([
                    Animated.timing( this.state.fade[2], {
                        toValue: 1,
                        duration: 1000,
                    }),
                    Animated.timing( this.state.fade[1], {
                        toValue: 0,
                        duration: 1000,
                    }),
                ]),
            ])
        ).start();

    };

    render() {

        const colors = [];

        for (let i = 0; i<this.state.fade.length; i++) {
            colors[i] = this.state.fade[i].interpolate({
                inputRange: [0, 1],
                outputRange: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)']
            });
        }

        return (
            <React.Fragment>

                <Modal
                    hideModalContentWhileAnimating
                    useNativeDriver
                    animationType="slide"
                    visible={ this.state.visible }>

                    <ImageBackground
                        source={ require('../../assets/icons/fondo.jpg') }
                        style={ styles.container } >

                        {/* HEADER */}
                        <Text style={ styles.header }> CONDUCTOR </Text>

                        {
                            this.state.opentok
                                ?
                                <View style={ { flex: 1 } }>

                                    <OTSession
                                        apiKey={ apiKey }
                                        sessionId={ this.state.opentok.sessionId }
                                        token={ this.state.opentok.token }
                                        style={ styles.publisher }
                                        eventHandlers={ this.sessionEventHandlers }>
                                        <OTPublisher
                                            properties={ {
                                                publishAudio: Platform.OS == 'android' ? true : !this.state.mute,
                                                publishVideo: false
                                            } }
                                            style={ styles.publisher } />
                                        <OTSubscriber
                                            properties={ this.subscriberProperties }
                                            style={ styles.publisher } />
                                    </OTSession>

                                    {/* BODY */}
                                    <View style={ styles.body }>

                                        <Image
                                            source={ require("../../assets/icons/conductor.png") }
                                            resizeMode="contain"
                                            style={{width: 150, height: 150 }} />

                                        <Text style={ styles.name }>
                                            { this.state.opentok.conductor.person.name+" "+this.state.opentok.conductor.person.lastname }
                                        </Text>

                                        {
                                            this.state.streamId==null
                                                ?
                                                <Hbox style={ { justifyContent: 'center', alignItems: 'center' } }>

                                                    <Animated.View style={ { ...styles.circle, backgroundColor: colors[0] } } />

                                                    <Animated.View style={ { ...styles.circle, backgroundColor: colors[1] } } />

                                                    <Animated.View style={ { ...styles.circle, backgroundColor: colors[2] } } />

                                                </Hbox>
                                                :
                                                null
                                        }

                                    </View>

                                    {/* FOOTER */}
                                    <Hbox style={ styles.footer }>

                                        {/* SPEAKER */}
                                        { Platform.OS == 'android' && <View style={ styles.col }>
                                            <Button
                                                buttonStyle={ styles.btn }
                                                onPress={ this.speaker }
                                                type="clear"
                                                icon={
                                                    <Image
                                                        source={ require('../../assets/icons/mute.png')}
                                                        style={{width: '100%', height: 25, tintColor: this.state.speaker ? Color.accent : Color.gray }}
                                                        tintColor={ this.state.speaker ? Color.accent : Color.gray }
                                                        resizeMode="contain" />
                                                } />
                                        </View> }

                                        {/* MUTE */}
                                        <View style={ styles.col }>
                                            <Button
                                                buttonStyle={ [styles.btn,Platform.OS == 'ios' ? {
                                                    alignSelf: 'flex-start'
                                                } : null] }
                                                onPress={ this.mute }
                                                type="clear"
                                                icon={
                                                    <Image
                                                        source={require('../../assets/icons/altavoz.png')}
                                                        style={{width: '100%', height: 25, tintColor: this.state.mute ? Color.accent : Color.gray }}
                                                        tintColor={ this.state.mute ? Color.accent : Color.gray }
                                                        resizeMode="contain" />
                                                } />
                                        </View>

                                        {/* END CALL */}
                                        <View style={ styles.col }>
                                            <Button
                                                buttonStyle={ [styles.btn,Platform.OS == 'ios' ? {
                                                   alignSelf: 'flex-end'
                                                } : null] }
                                                onPress={ this.endCall }
                                                type="clear"
                                                icon={
                                                    <Image
                                                        source={require('../../assets/icons/phone.png')}
                                                        style={{width: '100%', height: 25}}
                                                        tintColor={ Color.accent }
                                                        resizeMode="contain" />
                                                } />
                                        </View>

                                    </Hbox>

                                </View>
                                :
                                <View style={ { flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center' } }>
                                    <ActivityIndicator/>
                                </View>
                        }

                    </ImageBackground>

                </Modal>
            </React.Fragment>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%"
    },
    header: {
        width: "100%",
        backgroundColor: Color.blue,
        padding: 5,
        color: Color.white,
        fontSize: 17,
        fontFamily: 'Poppins-SemiBold'
    },
    body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    footer: {
        padding: 5
    },
    circle: {
        width: 20,
        height: 20,
        margin: 4,
        borderRadius: 10,
        borderColor: Color.white,
        borderWidth: 1,
        backgroundColor: Color.white
    },
    col: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '80%',
        borderRadius: 20,
        backgroundColor: Color.white
    },
    name: {
        paddingVertical: 15,
        color: Color.white,
        fontSize: 18
    },
    publisher: {
        height: 0,
        width: 0
    }
});

const redux = state => {
    return { user: state.auth.user }
};

export default connect(redux, null, null, { forwardRef: true })(Call);