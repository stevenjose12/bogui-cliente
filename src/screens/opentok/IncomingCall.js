import React, { Component } from 'react';
import {View, Modal, Text, StyleSheet, Image, Platform} from 'react-native';
import {Button} from "react-native-elements";
import Color from "../../assets/Color";
import Hbox from "../../widgets/Hbox";
import SocketEvents from "../../utils/SocketEvents";
import {connect} from "react-redux";
import { socket } from "../../utils/Socket";
import Sound from '../../utils/Sound';

class IncomingCall extends Component {

    state = {
        visible: false,
        data: null
    };

    show = async data => {
        try {
            this.setState({visible: true});
            this.setState({data: data});
            SocketEvents.onSocketEndCall(data => {
                if (this.state.data.user_id == data.id) this.dismiss();
            });
            this.soundPlay();
        } catch (e) {
            console.log(">>: IncomingCall > show > error: ", e);
        }
    };

    soundPlay = () => Sound.InCall.play(this.soundPlay);

    dismiss = () => {
        Sound.InCall.stop();
        this.setState({ visible: false });
    };

    reply = () => {
        if (this.props.reply) this.props.reply();
        this.dismiss();
    };

    endCall = () => {
        socket.emit('llamada-colgada', { id: this.props.user.id });
        this.dismiss();
    };

    render() {
        return(
            <Modal
                hideModalContentWhileAnimating
                useNativeDriver
                animationType="slide"
                transparent
                visible={ this.state.visible }>

                <View style={ styles.container }>

                    <View style={ styles.card }>

                        <Text style={ { ...styles.title, ...styles.textBold, ...styles.textColor} }> LLAMADA ENTRANTE </Text>

                        <Image
                            source={ require("../../assets/icons/conductor.png") }
                            resizeMode="contain"
                            style={{width: 150, height: 150 }} />

                        <Text style={ { ...styles.textBold, ...styles.textColor, ...styles.name } } > Conductor </Text>

                        {
                            this.state.data
                                ? <Text> { this.state.data.name } </Text>
                                : null
                        }

                        <Hbox style={ styles.footer }>

                            {/* SPEAKER */}
                            <View style={ styles.col }>
                                <Button
                                    buttonStyle={ { ...styles.btn, backgroundColor: Color.green } }
                                    onPress={ this.reply }
                                    type="clear"
                                    icon={
                                        <Image
                                            source={ require('../../assets/icons/phone.png')}
                                            style={{width: '100%', height: 25, tintColor: this.state.speaker ? Color.accent : Color.white }}
                                            tintColor={ this.state.speaker ? Color.accent : Color.white }
                                            resizeMode="contain" />
                                    } />
                            </View>

                            {/* MUTE */}
                            <View style={ styles.col }>
                                <Button
                                    buttonStyle={ { ...styles.btn, backgroundColor: Color.accent, alignSelf: Platform.OS == 'ios' ? 'flex-end' : null } }
                                    onPress={ this.endCall }
                                    type="clear"
                                    icon={
                                        <Image
                                            source={require('../../assets/icons/colgar.png')}
                                            style={{width: '100%', height: 25, tintColor: this.state.mute ? Color.accent : Color.white}}
                                            tintColor={ this.state.mute ? Color.accent : Color.white }
                                            resizeMode="contain" />
                                    } />
                            </View>

                        </Hbox>

                    </View>

                </View>

            </Modal>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.17)',
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        backgroundColor: "white",
        width: "80%",
        borderRadius: 10,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    footer: {
        paddingTop: 20
    },
    col: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '80%',
        borderRadius: 20,
        backgroundColor: Color.white
    },
    textBold: {
        fontFamily: 'Poppins-SemiBold'
    },
    textColor: {
        color: Color.primary
    },
    title: {
        fontSize: 18,
        paddingVertical: 10
    },
    name: {
        paddingVertical: 10
    }
});

const redux = state => {
    return { user: state.auth.user }
};

export default connect(redux, null, null, { forwardRef: true })(IncomingCall);

// export default IncomingCall;