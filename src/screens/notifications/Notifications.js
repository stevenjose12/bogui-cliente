import React, { Component } from 'react';
import { StyleSheet, View, FlatList, Image, Text, Platform } from "react-native";
import { connect } from "react-redux";
import Background from '../../widgets/Background';
import Api from "../../utils/Api";
import Modal from "../../utils/Modal";
import Item from "./Item";
import Button from "../../widgets/Button";
import Color from "../../assets/Color";

class Notifications extends Component{

    static navigationOptions = { title: "Notificaciones" };

    state = {
        notifications: [],
    };

    currentPage = 1;
    documents = [];
    lastPage = null;

    componentDidMount(){
        this.props.navigation.addListener('didFocus', this.didFocus );
    }

    didFocus = async () => {

        try {
            const r = await Api.getNotifications(this.currentPage, this.props.user.id);
            if (r.result) {
                this.lastPage = r.notificaciones.last_page;
                let notifications = r.notificaciones.data;
                notifications = notifications.sort( (a, b) => a.id < b.id );
                this.setState( state => state.notifications = notifications );
            }
        } catch (e) {
            Modal.showGenericError(e);
        }

    };

    loadMoreItems = async () => {
        ++this.currentPage;
        try {
            const r = await Api.getNotifications(this.currentPage, this.props.user.id);
            if (r.result) {
                let notifications = [ ...r.notificaciones.data, ...this.state.notifications ];
                notifications = notifications.sort( (a, b) => a.id < b.id );
                await this.setState( state => state.notifications = notifications );
            }
        } catch (e) {
            Modal.showGenericError(e);
        }
    };

    toMyTrip = async item => {

        try {
            const r = await Api.getRouteById(item.route.id);
            this.props.navigation.navigate("HistoryDetails", { item: r.route } );
        } catch (e) {
            console.log(">>: Notifications > toMyTrip > error: ", e);
        }

    };

    toPendingDocuments = () => this.props.navigation.navigate("PendingDocuments");

    render() {
        return(
            <Background header>

                {
                    this.state.notifications.length>1
                        ?
                        <View>

                            <FlatList
                                keyExtractor={ item => item.id+"" }
                                data={ this.state.notifications }
                                renderItem={ ({item}) =>
                                    <Item
                                        item={ item }
                                        toMyTrip={ this.toMyTrip }
                                        toPendingDocuments={ this.toPendingDocuments } />
                                }
                                ListFooterComponent={
                                    this.currentPage<=this.lastPage
                                        ?
                                        <View style={ { justifyContent: 'center', alignItems: 'center', padding: 7 } }>
                                            <Button
                                                buttonStyle={ styles.showMoreButton }
                                                titleStyle={ { color: Color.primary } }
                                                title="Ver mas"
                                                onPress={ this.loadMoreItems }/>
                                        </View>
                                        : null
                                } />

                        </View>
                        :
                        <View style={ { justifyContent: 'center', alignItems: 'center', height: '100%' } }>
                            <Image
                                source={ require("../../assets/icons/notification.png") }
                                style={ { width: 100, height: 100 } }
                                resizeMode="contain" />
                            <Text style={ { fontFamily: 'Poppins-Italic', color: Color.primary, fontSize: 17, marginVertical: 10 } }>
                                No tienes notificaciones
                            </Text>
                        </View>
                }

            </Background>
        )
    }

} 

const styles = StyleSheet.create({
   showMoreButton: {
       width: Platform.OS == 'ios' ? '70%' : '50%',
       borderColor: Color.primary,
       borderWidth: 2,
       backgroundColor: 'transparent',
       borderRadius: 20,
       alignSelf: Platform.OS == 'ios' ? 'center' : null,
   }
});

const redux = state => {
    return { user: state.auth.user }
};

export default connect(redux)(Notifications);