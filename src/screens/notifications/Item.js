import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableWithoutFeedback, Image } from "react-native";
import Color from "../../assets/Color";
import Hbox from "../../widgets/Hbox";
import moment from "moment";

const actions = {
    toMyTrip: 1,
    toPendingDocuments: 2
};

class Item extends Component {

    state = {
        item: null
    };

    componentDidMount() {
        this.setState( state => state.item = this.getIcon() );
    }

    getIcon(): string {

        switch (this.props.item.type) {

            case 1: // Carrera Aceptada
                return  {
                    description: "El viaje ha sido aceptado",
                    text: "Ver Viaje",
                    icon: require("../../assets/icons/notification1.png"),
                    color: Color.blue,
                    action: actions.toMyTrip
                };

            case 2: // Carrera Finalizada
                return  {
                    description: "El viaje ha sido finalizado",
                    text: "Ver Viaje",
                    icon: require("../../assets/icons/notification2.png"),
                    color: Color.green,
                    action: actions.toMyTrip
                };

            case 3: // Carrera Cancelada
                return {
                    description: "El viaje ha sido cancelado",
                    text: "Ver Viaje",
                    icon: require("../../assets/icons/notification3.png"),
                    color: Color.accent,
                    action: actions.toMyTrip
                };

            case 4: // Destino Modificado
                return {
                    description: "El destino del viaje ha sido modificado",
                    text: "Ver Viaje",
                    icon: require("../../assets/icons/notification4.png"),
                    color: Color.blue,
                    action: actions.toMyTrip
                };

            case 5: // Solicitud de Conductor Aceptada
                return {
                    icon: require("../../assets/icons/notification5.png"),
                    color: Color.accent
                };

            case 6: // Documentos Requeridos
                return {
                    description: "Su cuenta requiere atención",
                    text: "Ver Detalles",
                    icon: require("../../assets/icons/notification6.png"),
                    color: Color.accent,
                    action: actions.toPendingDocuments
                };

            case 7: // Documentos Aceptados
                return {
                    description: "Sus documentos fueron aceptados",
                    text: "Ver Detalles",
                    icon: require("../../assets/icons/notification6.png"),
                    color: Color.green
                };

            case 8: // Documentos Rechazads
                return {
                    description: "Uno o mas de sus documentos fueron rechazados",
                    text: "Ver Detalles",
                    icon: require("../../assets/icons/notification6.png"),
                    color: Color.accent,
                    action: actions.toPendingDocuments
                };

        }

    }

    onPress = () => {
        if (this.state.item.action) {
            if (this.state.item.action === actions.toMyTrip) {
                this.props.toMyTrip ? this.props.toMyTrip(this.props.item) : null;
            } else {
                this.props.toPendingDocuments ? this.props.toPendingDocuments() : null;
            }
        }
    };

    render() {
        return (
            this.state.item
                ?
                <TouchableWithoutFeedback onPress={ this.onPress }>
                    <View style={ styles.container }>
                        <Hbox style={ { justifyContent: 'center', alignItems: 'center' } }>

                            <Image
                                style={ { width: 35, height: 35 } }
                                source={ this.state.item.icon }
                                tintColor={ this.state.item.color }
                                resizeMode="contain" />

                            <View style={ { flex: 1 } }>

                                <Text style={ { fontSize: 13, textAlign: 'right' } }>
                                    {moment(this.props.item.created_at, 'YYYY-MM-DD HH:mm:ii').format('DD-MM-YYYY')}
                                </Text>

                                <Text style={ { flex: 1, textAlignVertical: 'center', textAlign: 'left', color: Color.primary, fontSize: 15, paddingLeft: 10, paddingVertical: 4 } } >
                                    { this.props.item.description ? this.props.item.description : this.state.item.description }
                                </Text>

                                <Text style={ { fontSize: 13, textAlign: 'right', color: Color.accent } }>
                                    { this.state.item.text }
                                </Text>

                            </View>

                        </Hbox>
                    </View>
                </TouchableWithoutFeedback>
                :
                null
        );
    }

}

const styles = StyleSheet.create({
    container: {
        margin: 5,
        padding: 5,
        backgroundColor: Color.white
    },

});

export default Item;