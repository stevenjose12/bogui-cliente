import React, { Component } from "react";
import Api from "../../utils/Api";
import Modal from "../../utils/Modal";
import Background from "../../widgets/Background";
import {ActivityIndicator, Image, TouchableWithoutFeedback, View} from "react-native";
import CurrentTravel from "../travels/Current";
import {connect} from "react-redux";
import CustomHeader from "../../widgets/CustomHeader";

class MyTravel extends Component {

    static navigationOptions = { header: null };

    state = {
        travels: null
    };

    componentDidMount() {
        this.props.navigation.addListener('didFocus', this.didFocus);
    }

    didFocus = async () => {
        if (this.currentTravel) this.currentTravel.defaultValues();
        await this.setState(state => state.travels = null);
        try {
            const travels = await Api.getRoutesByUser(this.props.user.id);
            if (travels.result) {
                await this.setState(state => state.travels = travels);
                if (this.currentTravel) this.currentTravel.update(this.state.travels.ruta);
            } else {
                Modal.showGenericError();
            }
        } catch (e) {
            Modal.showGenericError(e);
        }

    };

    toChat = async () => {
        try {
            const r = await Api.chatCreate({ id: this.state.travels.ruta.id });
            if (r.result) this.props.navigation.navigate('Messages', { chat_id: r.chat })
        } catch (e) {
            console.log(">>: MyTravels > toChat > e: ", e);
        }
    };

    render() {
        return (
            <Background noBackground>

                <CustomHeader
                    title="Mi Viaje"
                    onPress={ () => this.props.navigation.navigate("HomeDrawer") }
                    backType
                    rightButton={
                        <TouchableWithoutFeedback onPress={ this.toChat }>

                            <View style={ { width: 40, height: 40, justifyContent: 'center', alignItems: 'center' } }>
                                <Image
                                    source={ require("../../assets/icons/ico_chat.png") }
                                    style={ { width: 30, height: 30 } }
                                    resizeMode="contain"/>
                            </View>

                        </TouchableWithoutFeedback>
                    }/>

                {
                    this.state.travels==null
                        ?
                        <View style={ { width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' } }>
                            <ActivityIndicator />
                        </View>
                        :
                        <CurrentTravel
                            ref={ component => this.currentTravel = component }
                            navigation={ this.props.navigation }
                            route={ this.state.travels.ruta }
                            toHome={ () => this.props.navigation.navigate("HomeDrawer") }
                            toEdit={ () => this.props.navigation.navigate("EditRouteDrawer") } />
                }

            </Background>
        )
    }

}

const redux = state => {
    return {
        user: state.auth.user,
    }
};

export default connect(redux)(MyTravel);