import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableWithoutFeedback,
    Image,
    ActivityIndicator
} from 'react-native';
import Background from "../../widgets/Background";
import Footer from "./Footer";
import{ MapRoute, MarkerData } from "../../widgets/MapRoute";
import Geolocation from "../../utils/Geolocation";
import {connect} from "react-redux";
import Modal from "../../utils/Modal";
import Api from "../../utils/Api";
import { socket } from "../../utils/Socket";
import SocketEvents from "../../utils/SocketEvents";
import G from "../../utils/G";
import env from "../../utils/env";
import Share from "react-native-share";
import Header from "./Header";
import Color from "../../assets/Color";
import Task from "../../utils/Task";
import EmergencyCall from "../../widgets/EmergencyCall";
import Permissions from "../../utils/Permissions";

// CRITERIOS PARA TOMAR EN CUENTA EL SEGMENTO ACTUAL DE LA RUTA ////////////////////////////////////////////////////////
//                                                                                                                    //
// SEARCH:                                                                                                            //
//     descripción: Momento en el cual el conductor va en busca del pasajero en el punto de inicio de la ruta.        //
//     criterios: cuando el campo "status_route" de la ruta este en 0.                                                //
//     acción: se debe graficar desde el punto actual del conductor al punto de inicio de la ruta.                    //
//                                                                                                                    //
// CASO DE UN VIAJE SIN PARADA ADICIONAL                                                                              //
//                                                                                                                    //
// IF (Inicio Final):                                                                                                 //
//     descripción: Ya la ruta se encuentra iniciada y el conductor esta en camino de llevar al pasajero al punto de  //
//                  destino.                                                                                          //
//     criterios: La ruta no debe de tener parada adicional, y "status_route" debe se igual a 2.                      //
//     accion: se debe graficar la ruta completa desde el punto de inicio al punto de destino.                        //
//                                                                                                                    //
// CASO DE UN VIAJE SIN PARADA ADICIONAL                                                                              //
//                                                                                                                    //
// IP (Inicio Parada):                                                                                                //
//     descripción: El conductor esta en proceso de llevar al pasajero al la parada addicional                        //
//     criterios: La ruta debe de tener una parada adicional, status_route = 2 y time_stop = null.                    //
//     acción: Se debe graficar la ruta solo desde el punto de inicio al punto de parada adicional.                   //
//                                                                                                                    //
// PF (Parada Final):                                                                                                 //
//     descripción: El conductor esta en proceso de llevar al pasajero al destino.                                    //
//     criterios: La ruta debe de tener una parada adicional, status_route = 2 y time_stop != null.                   //
//     acción: Se debe graficar la ruta solo desde la parada adicional al destino.                                    //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const segments = {
    SEARCH: 0,
    IF: 1, // tramo inicio final
    IP: 2, // tramo inicio parada
    PF: 3  // tramo parada final
};

const timeAnimation: number = 1000; // <-------------------------------------------------------------------------------- duración de la animación

class RouteConfirmed extends Component {

    places = [];

    inFocus: boolean = false;

    state = {
        currentLocation: null,
        segment: null,
        showMap: true,
        driver:{}
    };

    driver: MarkerData = null;

    componentDidMount() {
        this.onFocusListener = this.props.navigation.addListener('didFocus', this.didFocus );
        this.onBlurListener = this.props.navigation.addListener('didBlur', () => this.inFocus = false );
    }

    componentWillUnmount() {
        this.onBlurListener?.remove();
        this.onFocusListener?.remove();
    }

    didFocus = () => {

        this.inFocus = true;

        this.setState(
            {
                showMap: true,
                currentLocation: this.props.currentLocation
            },
            () => this.setSegment( this.getSegmentFromRoute(this.props.route) )
        );

        this.addSocketEvents();

        this.setPlaces();
        this.animation();
        Api.getDriver(this.props.route.driverId)
            .then( driver => {
                if (!driver.success) return;
                this.setState({ driver: driver.driver });
            });

        this.onReadyLocation()
            .then( currentLocation => this.setState( { currentLocation } ) );

    };

    getSegmentFromRoute = route => {

        if (!route.status_route || route.status_route===0) return segments.SEARCH;

        if (route.to_destinies.length===1) return segments.IF;

        if (route.time_stop==null) return segments.IP;

        return segments.PF;

    };

    setSegment = segment => {

        let places = [];
        let markers = [];

        switch (segment) {

            case segments.SEARCH:
                markers = G.isNight(this.props.time)
                    ? [null, require('../../assets/icons/start_marker_night.png')]
                    : [null, require('../../assets/icons/start_marker.png')];
                places = [this.driverLocation, this.places[0]];
                break;

            case segments.IF:
                markers = G.isNight(this.props.time)
                    ? [ require('../../assets/icons/start_marker_night.png'), require('../../assets/icons/goal_marker_night.png') ]
                    : [ require('../../assets/icons/start_marker.png'), require('../../assets/icons/goal_marker.png') ];
                places = [ this.places[0], this.places[ this.places.length-1 ] ];
                break;

            case segments.IP:
                markers = G.isNight(this.props.time)
                    ? [require('../../assets/icons/start_marker_night.png'), require('../../assets/icons/stop_marker_night.png')]
                    : [require('../../assets/icons/start_marker.png'), require('../../assets/icons/stop_marker.png')];
                places = [this.places[0], this.places[1]];
                break;

            case segments.PF:
                markers = G.isNight(this.props.time)
                    ? [require('../../assets/icons/stop_marker_night.png'), require('../../assets/icons/goal_marker_night.png')]
                    : [require('../../assets/icons/stop_marker.png'), require('../../assets/icons/goal_marker.png')];
                places = [this.places[1], this.places[2]];
                break;

        }

        if (this.map) this.map.setPlaces(places, markers);

        this.setState( { segment } );

    };

    addSocketEvents = () => {

        socket.on('carrera-iniciada', data => {
            if (data.id==this.props.route.id) this.setSegment( (this.props.route.to_destinies.length===1) ? segments.IF : segments.IP);
        });

        socket.on('pasajero-abordo', data => {
            if (data.id==this.props.user.id) this.setSegment( (this.props.route.to_destinies.length===1) ? segments.IF : segments.IP );
        });

        socket.on('cancelada-conductor', data => {
            if (data.route === this.props.route.id) {
                this.clear();
            }
        });

        socket.on('iniciar-contador', data => {
            if (data.id==this.props.route.id) {
                try {
                    this.header.start();
                } catch (e) {
                    console.log(">>: RouteConfirmed > iniciar-contador > error: ", e);
                }
            }
        });

        socket.on('detener-contador', data => {
            if (data.id==this.props.route.id) {
                try {
                    this.setSegment( segments.PF );
                    this.header.stop();
                } catch (e) {
                    console.log(">>: RouteConfirmed > detener-contador > error: ", e);
                }

            }
        });

        SocketEvents.onDriverPosition( data => {
            // console.log(">>: RouteConfirmed > onDriverPosition");
            if (data.user.id==this.props.route.driverId) {
                if ( (this.state.segment===segments.SEARCH) && !this.driverLocation) {
                    this.driverLocation = {
                        latitude: data.latitude,
                        longitude: data.longitude
                    };
                    this.setSegment(segments.SEARCH);
                }
                if (this.driver) {
                    this.driver.setParams(data.latitude, data.longitude, data.rotation ? data.rotation : 0, new Date().getTime());
                } else {
                    this.driver = new MarkerData(data.user_id, data.latitude, data.longitude);
                }
            }
        } );

        SocketEvents.onFinishedCareer(data => {
            if ( (data.id != this.props.user.id) || (data.ruta_id != this.props.route.id) ) return;
            this.props?.onRouteFinish();
            this.clear();
        });

    };

    animation = () => {

        const interval = () => {

            if (!this.inFocus) return;

            if (!this.props.user) {
                clearInterval( this.animationInterval );
                return false;
            }

            try {
                if (this.map) {
                    this.map.anim(this.oldDriver, this.driver);
                    this.oldDriver = this.driver.clone();
                    if (this.driver) this.map.location( this.driver.getLocation() );
                }
            } catch (e) {
                // console.log(">>: RouteConfirmed > animation > interval > error: ", e);
            }

        };

        Task.add("animationRoute", timeAnimation, interval);

    };

    setPlaces = () => this.places = G.getPlacesFromRoute(this.props.route).locations;

    cancel = () => {

        Modal.alert.onPositiveButton('Si', async () => {

            if (this.state.segment>segments.SEARCH) {
                Modal.showAlert("Usted ya abordo el Bogui, no puede cancelar la solicitud");
            } else {

                const params = {
                    id: this.props.route.id,
                    motivo: "Cancelación por el Cliente",
                    driver_id: this.props.user.id
                };

                const r = await Api.cancelRoute(params);
                if (r.result) {
                    this.clear();
                    socket.emit('cancelada-cliente', { route: this.props.route.id });
                }

            }

        });
        Modal.alert.onCancelButton('No');
        Modal.alert.show({ message: '¿Desea cancelar la solicitud?' });
    };

    clear = () => {
        SocketEvents.onDriverPosition(null);
        SocketEvents.onFinishedCareer(null);
        this.props.dispatch( { type: 'SET_REQUEST', payload: { request: null } } );
        this.props.dispatch( { type: 'SET_ROUTE',   payload: { route: null } } );
        this.props.dispatch( { type: 'SET_PLACES',  payload: { places: null } } );
        Task.remove("animationEvent");
        this.props?.back();
    };

    call = () => {
        if (this.props.call) this.props.call();
    };

    share = () => {
        const data = {
            title: 'Compartir Viaje',
            message: "¡Me encuentro en un viaje con Bogui!, voy destino a " + G.getPlacesFromRoute(this.props.route).address.pop(),
            url: env.BASE_PUBLIC + 'routes/' + this.props.route.id + '?id=' + this.props.user.id
        };
        Share.open(data);
    };

    /**
     * Función que simplifica la llamada de la ubicacion actual
     */
    onReadyLocation = () : Promise => new Promise( (resolve, reject) => {

        // Si ya hay una ruta en memoria se usa
        if (Geolocation.getCurrentLocation()) {
            resolve(Geolocation.getLatLng(  Geolocation.getCurrentLocation() ));
            return;
        }

        // Si no hay una ruta en memoria es probable que se requeiran los permisos.
        Permissions.request('location')
            .then( granted => {

                // Si el permiso de ubicación no esta dado se lanza una excepción.
                if (!granted) {
                    reject('Permissions location no granted');
                    return;
                }

                // Si hay una ruta no se muestra el home si no el componente de Home
                if (!this.props.route) {
                    reject('route is null');
                    return;
                }

                Geolocation.onReadyLocation(location => {
                    if (!this.props.user) {
                        reject('User null');
                    } else {
                        resolve(Geolocation.getLatLng(location));
                    }
                });
                Geolocation.onError( e => {
                    if (this.props.user) Geolocation.start();
                    reject(e);
                });
                Geolocation.stop();
                Geolocation.start();
            })
            .catch(reject);
    } );

    render() {
        return (
            !!this.props.route && (
                <Background boBackground>

                    <Text
                        style={ { height: 40, textAlignVertical: 'center', fontFamily: "Poppins-SemiBold", marginLeft: 50, fontSize: 20, color: Color.primary } }>
                        Bogui
                    </Text>

                    {
                        this.state.showMap
                            ?
                            <MapRoute
                                ref={ map => this.map = map }
                                center={ this.state.currentLocation }
                                markerIcon={ this.props.route.vehicle_type_id==4 ? require('../../assets/icons/moto_marker.png') : require('../../assets/icons/car_marker.png')  }
                                timeAnimation={ timeAnimation }
                                header={
                                    <Header
                                        ref={ header => this.header = header }
                                        showBand={ this.state.segment>segments.SEARCH }/>
                                }
                                nightMode={ G.isNight(this.props.time) }
                                styleFooter={ { padding: 5 } }
                                footer={
                                    <View style={{
                                        width: '100%',
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>

                                        <View style={{ width: '10%' }}>

                                            <EmergencyCall night={ G.isNight(this.props.time) } style={ { marginBottom: 5 } }/>

                                            {
                                                this.state.segment===segments.SEARCH && (
                                                    <TouchableWithoutFeedback onPress={ this.call }>
                                                        <View style={ { padding: 5, width: 40, height: 40, borderRadius: 20, backgroundColor: G.isNight(this.props.time) ? Color.white : Color.primary, justifyContent: 'center', alignItems: 'center' } }>
                                                            <Image
                                                                style={ {width: 20, height: 20, tintColor: G.isNight(this.props.time) ? Color.primary : Color.white} }
                                                                source={ require("../../assets/icons/phone.png") }
                                                                tintColor={ G.isNight(this.props.time) ? Color.primary : Color.white }
                                                                resizeMode="contain" />
                                                        </View>
                                                    </TouchableWithoutFeedback>
                                                )
                                            }
                                        </View>
                                        <View style={{
                                            width: '80%',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}>
                                            {
                                                !!this.state.driver && (
                                                    <View style={{
                                                        backgroundColor: '#fff',
                                                        borderColor: 'black',
                                                        borderWidth: 1,
                                                        paddingHorizontal: '5%',
                                                        paddingVertical: '2%',
                                                        maxWidth: '60%',
                                                        borderRadius: 5, }}>
                                                        <Text style={{ fontFamily: 'Poppins-SemiBold' }}>
                                                            Ptt: { this.state.driver.plate }
                                                        </Text>
                                                        <Text style={{ fontFamily: 'Poppins-SemiBold' }}>
                                                            Modelo: { this.state.driver.brand }
                                                        </Text>
                                                    </View>
                                                )
                                            }
                                        </View>
                                    </View>

                                }/>
                            :
                            <View style={ { width: '100%', flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: Color.white } }>
                                <ActivityIndicator/>
                            </View>
                    }

                    <Footer
                        tariff={ this.props.route.cost }
                        payment={ this.props.route.payment }
                        onCancel={ this.cancel }
                        disabledCancel={ this.state.segment>segments.SEARCH }
                        onShare={ this.share }
                        onEdit={ () => {
                            this.setState({ showMap: false });
                            this.props?.toEdit();
                        } }
                        showRoute={ this.props?.toMyTravel }/>

                </Background>
            )
        );
    }

}

const styles = StyleSheet.create({

});

const redux = state => ({
    user: state.auth.user,
    route: state.route.route,
    time: state.night.night
});

export default connect( redux )(RouteConfirmed);