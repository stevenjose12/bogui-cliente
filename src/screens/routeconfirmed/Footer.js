import React from 'react';
import {StyleSheet, View, Text, TouchableWithoutFeedback, Image} from "react-native";
import Color from "../../assets/Color";
import Hbox from "../../widgets/Hbox";
import Button from "../../widgets/Button";
import G from "../../utils/G";

const Footer = props => (
    <View style={ styles.container }>

        <Hbox style={ styles.hbox }>

            <View style={ styles.col }>

                <Text style={ styles.text }>
                    { "CLP "+ G.formatMoney(props.tariff) }
                </Text>

            </View>

            <View style={ styles.col }>

                <Text style={ styles.text }>
                    { props.payment===1 ? "Tarjeta" : "Efectivo" }
                </Text>

            </View>

            <View style={ styles.col }>

                <TouchableWithoutFeedback onPress={ () => props.onShare ? props.onShare() : null }>

                    <Image
                        style={ styles.share }
                        source={ require('../../assets/icons/menu_share.png') }
                        resizeMode="contain" />

                </TouchableWithoutFeedback>

            </View>

        </Hbox>

        <Hbox style={ styles.hbox }>

            <View style={ styles.col }>
                <TouchableWithoutFeedback onPress={ () => props.showRoute ? props.showRoute() : null }>

                    <Image
                        style={ styles.share }
                        source={ require('../../assets/icons/route.png') }
                        resizeMode="contain" />

                </TouchableWithoutFeedback>
            </View>

            <View style={ { flex: 2 } }>

                <Button
                    title="Cancelar viaje"
                    onPress={ () => props.onCancel ? props.onCancel() : null }
                    titleStyle={ { color: Color.primary } }
                    buttonStyle={ styles.btn }
                    disabled={ props.disabledCancel }/>

            </View>

            <View style={ styles.col }>
                <TouchableWithoutFeedback onPress={ () => props.onEdit ? props.onEdit() : null }>

                    <Image
                        style={ styles.share }
                        source={ require('../../assets/icons/edit_route.png') }
                        resizeMode="contain" />

                </TouchableWithoutFeedback>
            </View>

        </Hbox>

    </View>
);

const styles = StyleSheet.create({
    container: {
        backgroundColor: Color.primary,
        width: "100%"
    },
    hbox: {
        paddingVertical: 10
    },
    col: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: Color.white,
        textAlign: 'center',
        fontSize: 18
    },
    share: {
        width: 30,
        height: 30
    },
    btn: {
        backgroundColor: Color.white,
        borderRadius: 20,
        width: "100%"
    }
});

export default Footer;