import React, { Component } from "react";
import {StyleSheet, View, Text} from "react-native";
import Chronometer from "../../widgets/Chronometer";
import Color from "../../assets/Color";
import G from "../../utils/G";

class Header extends Component {

    componentDidMount() {

    }

    start = () => {
        this.chronometer.start();
    };

    stop = () => {
        this.chronometer.stop();
    };

    render () {
        return (
            <View>
                {
                    this.props.showBand
                        ? <Text style={ styles.band }> Comenzó viaje </Text>
                        : null
                }
                <Chronometer ref={chronometer => this.chronometer = chronometer} />
            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderColor: 'red',
        borderWidth: 1
    },
    band: {
        backgroundColor: Color.blue,
        color: Color.white,
        width: '100%',
        paddingVertical: 2,
        textAlign: 'center'
    }
});

export default Header;