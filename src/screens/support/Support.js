import React, { Component } from "react"
import { connect } from "react-redux"
import {
    FlatList,
    Platform,
    StyleSheet,
    Text,
    View,
} from "react-native"
import { CheckBox } from "react-native-elements"

import Background from "../../widgets/Background"
import Button from "../../widgets/Button"
import Banner from "../../widgets/BannerAuth"
import Api from "../../utils/Api"
import Color from "../../assets/Color"
import Modal from "../../utils/Modal"

class Support extends Component {
    static navigationOptions = {
        title: "Soporte",
    }

    state = {
        issues: [],
        selected: -1,
    }

    componentDidMount() {
        this.setState(state => state.issues = this.props.issue)
        Api.getChatIssues()
            .then(resp => {
                this.setState(state => state.issues = resp.asuntos)
                this.props.dispatch({
                    type: "SET_ISSUE",
                    payload: {
                        issue: resp.asuntos,
                    },
                })
            })
    }

    render() {
        return (
            <Background header noBackground>
                <View style={styles.bannerContainer}>
                    <Banner iconBanner={
                        require("../../assets/img/bogui_banner.png")
                    }
                    />
                    <Text style={styles.subtitle}>
                        Seleccione el tema del que desea hablar en el chat
                    </Text>
                </View>

                {!!this.state.issues &&
                    <FlatList
                        data={this.state.issues}
                        renderItem={({ item }) => (
                            <Issue
                                id={item.id}
                                description={item.description}
                                onSelect={this.onSelect}
                                selected={this.state.selected === item.id}
                            />
                        )}
                        extraData={this.state.selected}
                        keyExtractor={item => String(item.id)}
                        ItemSeparatorComponent={IssueSeparator}
                        ListFooterComponent={() => (
                            <StartChatFooter
                                onPressStartChat={this.onPressStartChat}
                                disabled={this.state.selected === -1}
                            />
                        )}
                    />
                }
            </Background>
        )
    }

    onSelect = id => { this.setState({ selected: id }) }

    onPressStartChat = () => {
        Api.createSupportChat(this.state.selected, this.props.user.id, undefined)
            .then(resp => {
                if (resp.result) {
                    this.props.navigation.navigate('Messages', {
                        chat_id: resp.chat
                    })
                }
            })
            .catch(err => {
                Modal.showGenericError(err)
            })
    }
}

const Issue = ({ id, description, selected, onSelect }) => (
    <CheckBox
        iconRight
        textStyle={styles.issueDescription}
        containerStyle={styles.issueContainer}
        checked={selected}
        title={description}
        checkedIcon={"dot-circle-o"}
        uncheckedIcon={"circle-o"}
        checkedColor={Color.accent}
        onPress={() => onSelect(id)}
        onIconPress={() => onSelect(id)} />
)

const IssueSeparator = () => (
    <View style={styles.issueSeparator} />
)

const StartChatFooter = ({ onPressStartChat, disabled }) => (
    <>
        <IssueSeparator />
        <View style={styles.startChatButtonContainer}>
            <Button
                title={"Iniciar chat"}
                buttonStyle={styles.startChatButton}
                titleStyle={styles.startChatButtonTitle}
                onPress={onPressStartChat}
                disabled={disabled}
            />
        </View>
    </>
)

const styles = StyleSheet.create({
    bannerContainer: {
        marginVertical: 48,
        alignItems: "center",
    },
    subtitle: {
        paddingTop: 16,
        paddingHorizontal: 72,
        fontSize: 18,
        textAlign: "center",
    },
    issueContainer: {
        backgroundColor: 'transparent',
        borderColor: 'transparent',
        marginTop: 4,
        marginBottom: 0,
        marginLeft: 4,
    },
    issueDescription: {
        flex: 1,
        fontWeight: "normal",
        color: Color.primary,
        fontFamily: "Poppins-SemiBold",
        fontSize: 18,
    },
    issueSeparator: {
        backgroundColor: Color.accent,
        height: 2,
        width: "100%",
    },
    startChatButton: {
        backgroundColor: 'black',
        paddingVertical: 12,
        height: undefined,
        marginHorizontal: "25%"
    },
    startChatButtonTitle: {
        fontSize: 20,
    },
    startChatButtonContainer: {
        marginVertical: 36,
        width: "100%",
    },
})

const redux = state => {
    return {
        issue: state.issue.issue,
        user: state.auth.user,
    }
}

export default connect(redux)(Support)
