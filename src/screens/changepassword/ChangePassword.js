import React, { Component } from 'react';
import { ImageBackground, StyleSheet, View} from "react-native";
import Button from "../../widgets/Button";
import Banner from "../../widgets/BannerAuth";
import Input from "../../widgets/InputBogui";
import { passwordConfirmValidator } from '../../utils/Validators';
import Api from "../../utils/Api";
import Modal from '../../utils/Modal';

class ChangePassword extends Component {

    state = {
        password: '',
        passwordConfirmation: '',
        errPassword: ""
    };

    componentDidMount() {
        passwordConfirmValidator.notPass = errPassword => this.setState( { errPassword } );
        this.setState({
            email: this.props.navigation.getParam("email"),
            code: this.props.navigation.getParam("code")
        });
    }

    submit = async () => {

        if (!passwordConfirmValidator.compare(this.state.password, this.state.passwordConfirmation)) return;

        const form = {
            email: this.props.navigation.getParam("email"),
            codigo: this.props.navigation.getParam("code"),
            password: this.state.password
        };

        try {

            const response = await Api.changePassword( form );

            if (response?.result) {
                Modal.showAlert("Cambio de contraseña exitoso.");
                this.props.navigation.navigate("Login");
            } else {
                throw ">>: ChangePassword > submit > response.result: null o false";
            }

        } catch (e) {
            Modal.alert.showGenericError(e);
        }

    };

    render() {
        return (
            <ImageBackground
                source={ require('../../assets/img/background.jpg') }
                style={{width: '100%', height: '100%'}} >

                <Banner
                    iconBanner={ require('../../assets/img/bogui_banner.png') }
                    backIcon={ require('../../assets/icons/back.png') }
                    onBackPress={ () => this.props.navigation.navigate("RememberPassword")  }/>

                <View style={ styles.container }>

                    {/* PASSWORD */}
                    <Input
                        leftIcon={ require('../../assets/img/pass.png') }
                        label="Contraseña Nueva"
                        hint="Contraseña"
                        onChangeText={ password => this.setState( { password } ) }
                        errorMessage={ this.state.errPassword }
                        password
                        onFocus={ () => this.setState( { errPassword: '' } ) } />

                    {/* PASSWORD CONFIRMATION */}
                    <Input
                        leftIcon={ require('../../assets/img/pass.png') }
                        label="Repetir Contraseña nueva"
                        hint="Contraseña"
                        onChangeText={ passwordConfirmation => this.setState( { passwordConfirmation } ) }
                        password
                        onFocus={ () => this.setState( { errPassword: '' } ) }
                        onBlur={ () => passwordConfirmValidator.compare(this.state.password, this.state.passwordConfirmation) }/>

                    <Button
                        title="Continuar"
                        type="outline"
                        titleStyle={{fontSize: 20, color: 'black', fontFamily: 'Poppins-Bold'}}
                        containerStyle={{width: "50%", marginTop: 20}}
                        buttonStyle={{borderColor: 'black', paddingVertical: 5, alignItems: 'center', borderWidth: 1, borderRadius: 20}}
                        onPress={ this.submit } />

                </View>
            </ImageBackground>

        );
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: '4%'
    },
    body: {
        width: "100%",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    submit: {
        marginVertical: 20
    }
});

export default ChangePassword;