import React, { Component } from "react";
import {View, Text, StyleSheet, Platform, Image } from "react-native";
import Hbox from "../../widgets/Hbox";
import Color from "../../assets/Color";
import moment from "moment";
import G from "../../utils/G";

class RouteDetails extends Component {

    render() {

        return (
            this.props.item
                ?
                <View style={ styles.container }>

                    <Hbox style={ styles.hbox }>
                        <Text style={ [styles.text, styles.flex] }> { "ID: "+this.props.item.id } </Text>
                        <Text style={ styles.text }> { this.props.item.distance+" KM" } </Text>
                    </Hbox>

                    <Hbox style={ styles.hbox }>
                        <Text style={ [styles.text, styles.flex] }> { moment(this.props.item.created_at).format("DD-MM-YYYY HH:mm") } </Text>
                        <Text style={ [styles.text, styles.bold] }> { "CLP "+ G.formatMoney(this.props.item.cost) } </Text>
                    </Hbox>

                    <Hbox style={ styles.hbox }>
                        <Text style={ [styles.text, styles.flex] }> { this.props.item.driver.person.name+" "+this.props.item.driver.person.lastname } </Text>
                        <Text style={ [styles.text, styles.bold] }>{ this.props.item.payment_id===1 ? "Tarjeta" : "Efectivo" }</Text>
                    </Hbox>

                    {

                        this.props.places.map( (item, index) => {
                            if (item)
                            return(
                                <Hbox key={ index.toString() } style={ styles.hbox }>
                                    <View style={styles.markerContainer}>
                                        <Image style={ { width: 30, height: 30 } }  source={ G.markers[index] } resizeMode="contain"/>
                                    </View>
                                    <Text style={ { ...styles.text, ...styles.flex, textAlignVertical: 'center' } }> { item } </Text>
                                </Hbox>
                            ) }
                        )

                    }

                </View>
                : null
        )
    }

}

const styles = StyleSheet.create({
    container: {
        padding: 5
    },
    hbox: {
        paddingVertical: 2
    },
    text: {
        color: Color.primary
    },
    bold: {
        fontFamily: "Poppins-SemiBold"
    },
    flex: {
        flex: 1
    },
    markerContainer: {
        paddingRight: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
});

export default RouteDetails;