import React, { Component } from "react";
import {View, Text, FlatList, TouchableWithoutFeedback, StyleSheet } from "react-native";
import Api from "../../utils/Api";
import Modal from "../../utils/Modal";
import Color from "../../assets/Color";
import Hbox from "../../widgets/Hbox";
import Icon from 'react-native-vector-icons/FontAwesome5';
import {connect} from "react-redux";

class Help extends Component {

    state = {
        issues: []
    };

    componentDidMount() {

        this.setState( { issues: this.props.issue } );

        Api.getChatIssues()
            .then( r => {
                this.setState( { issues: r.asuntos } );
                this.props.dispatch( { type: 'SET_ISSUE', payload: { issue: r.asuntos } } );
            } );

    }

    onPress = issue => {
        Modal.alert.onPositiveButton('Aceptar', () => { if (this.props.onPressIssue) this.props.onPressIssue(issue) });
        Modal.alert.onCancelButton('Cancelar');
        Modal.alert.show({ message: '¿Deseas habilitar un chat de ayuda con el asunto "'+issue.description+'" ?' });
    };

    render() {

        return (

            !!this.state.issues?.length>0 && (
                <View style={ styles.container }>

                    <Text style={ { ...styles.text, ...styles.title } }>
                        Ayuda
                    </Text>

                    <FlatList
                        keyExtractor={ item => item.id }
                        data={ this.state.issues }
                        renderItem={ ({item}) =>
                            <TouchableWithoutFeedback onPress={ () => this.onPress(item) }>
                                <Hbox style={ styles.hbox }>
                                    <Text style={ styles.text }>
                                        {item.description}
                                    </Text>
                                    <View style={ { width: 40, height: 40, justifyContent: 'center', alignItems: 'center' } }>
                                        <Icon
                                            color={ Color.accent }
                                            size={ 30 }
                                            name="chevron-right" />
                                    </View>

                                </Hbox>

                            </TouchableWithoutFeedback>
                        } />

                </View>
            )

        )

    }

}

const styles = StyleSheet.create({
    container: {
        padding: 5
    },
    title: {
        fontSize: 25,
        fontFamily: "Poppins-SemiBold"
    },
    hbox: {
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomColor: Color.accent,
        borderBottomWidth: 0.5
    },
    text: {
        color: Color.primary,
        fontSize: 17,
        flex: 1
    }
});

const redux = state => ({ issue: state.issue.issue });

export default connect(redux)(Help);
