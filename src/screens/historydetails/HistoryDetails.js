import React, { Component } from "react";
import { View, ScrollView, Dimensions, StyleSheet } from "react-native";
import Background from "../../widgets/Background";
import RouteDetails from "./RouteDetails";
import G from "../../utils/G";
import MapPlotRoute from "../../widgets/MapPlotRoute";
import {connect} from "react-redux";
import Help from "./Help";
import Api from "../../utils/Api";
import Modal from "../../utils/Modal";

class HistoryDetails extends Component {

    static navigationOptions = { title: "Viajes" };

    state = {
        item: null,
        places: []
    };

    componentDidMount() {

        const item = this.props.navigation.getParam("item");
        const response = G.getPlacesFromRoute(item);
        this.map.setPlaces( response.places );

        this.setState( state => state.item = item );
        this.setState( state => state.places = response.address );

    }

    onPressIssue = async issue => {
        try {
            const r = await Api.createSupportChat(issue.id, this.props.user.id, this.state.item.id);
            if (r.result) this.props.navigation.navigate('Messages', { chat_id: r.chat })
        } catch (e) {
            Modal.showGenericError(e);
        }
    };

    render() {

        return (
            <Background noBackground header>

                <ScrollView>

                    <View style={ styles.mapContainer }>
                        <MapPlotRoute
                            ref={ map => this.map = map }
                            markers={ G.isNight(this.props.night) ? G.markersNight : G.markers }/>
                    </View>

                    <RouteDetails
                        item={ this.state.item }
                        places={ this.state.places } />

                    <Help onPressIssue={ this.onPressIssue }/>

                </ScrollView>

            </Background>
        );
    }

}

const styles = StyleSheet.create({
    mapContainer: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').width
    }
});

const redux = state => {
    return {
        user: state.auth.user,
        time: state.night.night
    }
};

export default connect(redux)(HistoryDetails);