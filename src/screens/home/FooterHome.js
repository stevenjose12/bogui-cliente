import React, { Component } from "react";
import {
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  Text,
  Platform
} from "react-native";
import Button from "../../widgets/Button";
import Color from "../../assets/Color";
import Hbox from "../../widgets/Hbox";
import EmergencyCall from "../../widgets/EmergencyCall";

class FooterHome extends Component {
  state = {
    backButtonVisible: false,
    textButton: "Comencemos",
    showTuto: true
  };

  showTuto(show: boolean) {
    this.setState(state => (state.showTuto = show));
  }

  showBackButton(show: boolean) {
    this.setState(state => (state.backButtonVisible = show));
  }

  setTextButton(text: string) {
    this.setState(state => (state.textButton = text));
  }

  render() {
    return (
      <View style={[styles.container, this.props.containerStyle]}>
        <View style={styles.viewEmergencyCall}>
          <EmergencyCall night={this.props.night} />
        </View>
        {this.state.showTuto && this.props.step !== 0 ? (
          <Text style={styles.tuto}>{this.props.footerText}</Text>
        ) : null}
        {/* <Image
                    source={require('../../assets/icons/current.png')}
                    style={{width: '10%', height: '76%', top: '-210%', left: '85.4%', position: 'absolute'}}
                /> */}

        <Hbox>
          <View style={{ width: 45, height: 40 }}>
            {this.state.backButtonVisible ? (
              <TouchableWithoutFeedback
                onPress={() =>
                  this.props.onBackPress ? this.props.onBackPress() : null
                }
              >
                <View style={styles.backButton}>
                  <Image
                    style={styles.icon}
                    source={require("../../assets/icons/back_circle.png")}
                    resizeMode="contain"
                  />
                </View>
              </TouchableWithoutFeedback>
            ) : null}
          </View>

          <View style={styles.buttonContainer}>
            <Button
              title={this.state.textButton}
              titleStyle={{ width: "100%" }}
              buttonStyle={styles.btnInit}
              onPress={() => (this.props.onPress ? this.props.onPress() : null)}
            />
          </View>

          <View style={{ width: 45, height: 40 }} />
        </Hbox>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    paddingBottom: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  backButton: {
    width: 40,
    height: 40,
    marginLeft: 5
  },
  buttonContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  btnInit: {
    backgroundColor: Color.primary,
    width: Platform.OS == "ios" ? "90%" : "70%",
    height: 40,
    borderRadius: 20,
    alignSelf: Platform.OS == "ios" ? "center" : null
  },
  icon: {
    width: "100%",
    height: "100%",
    position: "absolute",
    left: 0
  },
  tuto: {
    backgroundColor: Color.blue,
    color: Color.white,
    padding: 5,
    fontSize: 10,
    borderRadius: 5,
    width: "90%",
    textAlign: "center",
    marginBottom: 10
  },
  viewEmergencyCall: {
    alignSelf: "flex-start",
    marginStart: 5,
    marginBottom: 10
  }
});

export default FooterHome;
