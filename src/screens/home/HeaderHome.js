import React, { Component } from 'react';
import {View, Text, StyleSheet, Image, TextInput, TouchableWithoutFeedback} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../../assets/Color';
import Input from '../../widgets/InputBogui';
import {Button} from "react-native-elements";
import API from '../../utils/Api';
const items = [
    { label: '¿Donde te Recogemos?', icon: require('../../assets/icons/start_marker.png') },
    { label: '¿Donde harás una parada?', icon: require('../../assets/icons/stop_marker.png') },
    { label: '¿Donde es tu Destino?', icon: require('../../assets/icons/goal_marker.png') }
];
class HeaderHome extends Component {
    state={
        lastInput: '',
        flagPlaces: [false,false,false]
    };
    places = [];
    search = [];
    flagPlaces = [false,false,false];
    flagFocus = false;
    lastID = '';

    setPlaces = places => {
        this.places = places;
        this.forceUpdate();
    };

    _clearInput(i){
        this.places[i]['address'] = '';
        this.forceUpdate()
    }

    async _searchGoogle(text){
        try {
            const response = await API.autoComplete(text);
            this.search = response;
            this.forceUpdate()
        } catch (e) {
            console.log('>>: Error catch', e)
        }
    }
    async _getCoord(id){
        try {
            const response = await API.placeID(id);
            this.props.onChangeText(this.lastID, response.results[0]['formatted_address'])
            data = {
                latitude: response.results[0]['geometry']['location']['lat'],
                longitude: response.results[0]['geometry']['location']['lng']
            };
            this.props.setPlaces(data);
            this.search = [];
            this.forceUpdate()
        } catch (error) {
            console.log('>>: Catch > getCoord', error)
        }
    }

    setInputValueByDragEnd(){
        this.search = [];
    }

    render() {
        return(
                <View style={{zIndex: 3}}>
                    {
                        this.places ? 
                            this.places.map((element, i)=>{
                                if (typeof element.address === 'string' && this.props.step !== 0){
                                    this.lastID = i;
                                        return(
                                            <View key={i} style={styles.container}>
                                                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                                                    <View style={{width: '10%',alignItems: 'center', top: '3%', left: '5%'}}>
                                                        <Image
                                                            source={items[i]['icon']}
                                                            style={{height: 24, bottom: '15%'}}
                                                            resizeMode="contain"
                                                        />
                                                    </View>
                                                    <View style={{width: '75%', paddingHorizontal: 10}}>
                                                        <Text style={[styles.text, {fontWeight: 'bold', color: 'black', padding: 0}]}>{items[i]['label']}</Text>
                                                        {
                                                            this.props.step === i+1
                                                            ?
                                                                <TextInput
                                                                    style={{height: '58%',fontSize: 13, padding: 0, margin: 0}}
                                                                    value={element.address}
                                                                    onFocus={() => this.flagFocus = true}
                                                                    onBlur={() => this.flagFocus = false}
                                                                    onChangeText={(text) => {
                                                                            this.search = [];
                                                                            this._searchGoogle(text);
                                                                            this.props.onChangeText ? this.props.onChangeText(i, text) : null
                                                                        }
                                                                    }
                                                                />
                                                            :
                                                                <Text style={styles.text} numberOfLines={1}>{element.address}</Text>
                                                        }
                                                    </View>
                                                    <View style={{width: '15%'}}>
                                                        {
                                                            this.props.step === i+1
                                                            ? 
                                                                <Button
                                                                    buttonStyle={ styles.closeBtn }
                                                                    type="clear"
                                                                    icon={
                                                                        <Icon name="ios-backspace" size={30} color="#F70000" />
                                                                    }
                                                                    onPress={ () => this._clearInput(i) } />
                                                            : null
                                                        }
                                                    </View>
                                                </View>
                                            </View>
                                        );
                                    }
                                })
                        : null
                    }
                    <View>
                        {
                            this.search.length > 0 && this.props.step !== 0
                            ?
                                this.search.map((element, i)=> {
                                    return(
                                        <TouchableWithoutFeedback
                                            onPress={() => this._getCoord(element.place_id)}
                                        >
                                            <View key={i} style={styles.container}>
                                                <Text style={{padding: 5, color: '#333', textAlign: 'left'}} numberOfLines={1}> {element.description} </Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    )
                                })
                            :
                            null
                        }
                    </View>
                </View>
                )
            }
        }
const styles = StyleSheet.create({
    container:{
        width: '90%',
        flex: 1,
        top: 0,
        right: 0,
        left: '5%',
        bottom: 0,
        backgroundColor: 'white',
        borderColor: Colors.gray,
        borderWidth: .35,
    },
    text:{
        letterSpacing: 0,
        padding: 0,
    }
});
export default HeaderHome;