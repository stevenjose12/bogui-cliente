import React from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Text, Dimensions, ScrollView, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Color from '../../assets/Color';
import G from '../../utils/G';
import { connect } from 'react-redux';
import ENV from '../../utils/env';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { Button } from 'react-native-elements';
import DownloadFile from '../../utils/DownloadFile';
import FileViewer from 'react-native-file-viewer';
import Permissions from '../../utils/Permissions';
import Modal from '../../utils/Modal';
import SocketEvents from '../../utils/SocketEvents';
import Api from "../../utils/Api";

class AdminNotifications extends React.Component {

    state = {
        notifications: [],
        activePage: 0,
    };

    componentDidMount() {
        this.load()
    }

    shouldComponentUpdate(nextProps) {
        if (nextProps.visible) {
            SocketEvents.onAdminNotification(notification => {
                if (notification &&
                    notification.users &&
                    notification.users.includes(this.props.user.id)) {
                    this.load();
                }
            })
        }
        return true
    }

    load = async () => {

        if (!this.props.user) return false;

        try {
            const r = await Api.getAdminNotifications(this.props.user.id);
            if (r.result) this.setState({ notifications: r.notificaciones })
        } catch (e) {
            Modal.showGenericError(e);
        }

    };

    download = async file => {

        const granted = await Permissions.request('storage');
        if (!granted) return false;

        try {
            Modal.loading.show();
            const res = await DownloadFile.download(file);
            Modal.loading.dismiss();
            FileViewer.open(res.path(), { showOpenWithDialog: true });
        } catch (e) {
            Modal.showGenericError(e);
        }
    };

    checkImage = file => {
        let extension = file.split('.');
        extension = extension[extension.length - 1];
        return ['jpg', 'jpeg', 'png', 'gif'].indexOf(extension) !== -1;
    };

    render() {
        return (
            <>
                <TouchableWithoutFeedback onPress={this.props.onClose}>
                    <View style={styles.topContainer}>
                        <TouchableWithoutFeedback onPress={this.props.onClose}>
                            <View style={[styles.tab, G.isNight() ? styles.lightTab : null]}>
                                <Icon size={40} color={G.isNight() ? "#000" : "#fff"} name="md-arrow-round-down" />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
                <View
                    style={[
                        styles.container,
                        styles.bottomContainer,
                        G.isNight() ? { borderTopColor: '#fff' } : null
                    ]} >
                    {
                        this.state.notifications.length === 0 &&
                            <View style={styles.empty}>
                                <Text style={styles.emptyText}>No hay nuevas notificaciones</Text>
                            </View>
                    }

                    <Carousel
                        data={ this.state.notifications }
                        renderItem={({ item }) => (
                            <View style={styles.notification}>
                                <View style={{ flexDirection: 'row', width: '100%' }}>
                                    <View style={{ flex: 2 }} />
                                    <Text style={[styles.title, { flex: 8 }]}>{item.title}</Text>
                                    <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }} >
                                        {item.file &&
                                            <Button
                                                // buttonStyle={{}}
                                                onPress={() => this.download(ENV.BASE_PUBLIC + item.file)}
                                                type="clear"
                                                icon={
                                                    <Icon
                                                        size={28}
                                                        color={Color.accent}
                                                        name="md-download" />
                                                }
                                            />
                                        }
                                    </View>
                                </View>
                                <ScrollView style={styles.scroll}>
                                    <Text style={styles.description}>{item.description}</Text>
                                    {item.file && this.checkImage(item.file) && (
                                        <TouchableWithoutFeedback onPress={() => this.download(ENV.BASE_PUBLIC + item.file)}>
                                            <View style={styles.attachmentContainer}>
                                                <Text style={styles.attachment}>Se adjuntó archivo</Text>
                                                <Image style={styles.attachmentImage} source={{ uri: ENV.BASE_PUBLIC + item.file }} />
                                            </View>
                                        </TouchableWithoutFeedback>
                                    )}
                                </ScrollView>
                            </View>
                        )}
                        sliderWidth={Dimensions.get('window').width}
                        itemWidth={Dimensions.get('window').width}
                        onSnapToItem={i => this.setState({ activePage: i })} />

                    <Pagination
                        dotsLength={this.state.notifications.length}
                        activeDotIndex={this.state.activePage} />

                </View>
            </>
        )
    }
}

const styles = StyleSheet.create({
    topContainer: {
        backgroundColor: "#00000044",
        width: '100%',
        flex: 1,
    },
    container: {
        backgroundColor: "#00000044",
        width: '100%',
    },
    bottomContainer: {
        backgroundColor: '#fff',
        borderColor: '#000',
        borderTopWidth: 10,
        minHeight: '50%',
    },
    tab: {
        backgroundColor: '#000',
        width: 100,
        height: 100,
        paddingTop: 10,
        alignItems: 'center',
        borderRadius: 50,
        alignSelf: 'center',
        position: 'absolute',
        bottom: -50,
    },
    lightTab: {
        backgroundColor: '#fff'
    },
    empty: {
        width: '100%',
        flex: 1,
        justifyContent: 'center'
    },
    emptyText: {
        textAlign: 'center',
        color: '#000'
    },
    notification: {
    },
    title: {
        fontFamily: 'Poppins-Bold',
        marginTop: 15,
        marginBottom: 15,
        fontSize: 18,
        textAlign: 'center',
        color: '#000'
    },
    description: {
        textAlign: 'center',
        fontSize: 12,
        color: '#000',
        marginBottom: 10
    },
    scroll: {
        
    },
    attachment: {
        color: Color.accent,
        textAlign: 'center',
    },
    attachmentImage: {
        marginTop: 16,
        width: 100,
        height: 100,
        borderRadius: 50,
        resizeMode: 'cover'
    },
    attachmentContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default connect(state => ({
    user: state.auth.user
}))(AdminNotifications);