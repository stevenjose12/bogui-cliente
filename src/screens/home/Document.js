import React from "react";
import {View, Text, Image, StyleSheet, TouchableWithoutFeedback } from "react-native";
import Hbox from "../../widgets/Hbox";
import Color from "../../assets/Color";

const Document = props => (

    <TouchableWithoutFeedback onPress={ () => props.toPendingDocuments ? props.toPendingDocuments() : null }>
        <View style={ styles.container }>

            <Text style={ styles.black }> Su cuenta requiere atención </Text>

            <Hbox style={ styles.hbox }>

                <View style={ styles.imageContainer }>
                    <Image style={ styles.image }  source={ require('../../assets/icons/notification6.png') } resizeMode="contain"/>
                </View>

                <Text style={ styles.text }> Actualizar Documentos </Text>

                <View style={ styles.imageContainer }>
                    <Image style={ styles.image } source={ require('../../assets/icons/arrow_right_circle.png') } resizeMode="contain"/>
                </View>

            </Hbox>

        </View>
    </TouchableWithoutFeedback>

);

const styles = StyleSheet.create({
    container: {
        width: "100%",
    },
    black: {
        width: "100%",
        backgroundColor: Color.primary,
        color: Color.white,
        textAlign: 'center',
        paddingVertical: 2
    },
    hbox: {
        width: '100%',
        height: 50,
        backgroundColor: 'white'
    },
    text: {
        flex: 1,
        height: 50,
        textAlignVertical: 'center',
        fontSize: 18,
        color: Color.accent,
        fontFamily: 'Poppins-SemiBold'
    },
    imageContainer: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: 30,
        height: 30,
    }
});

export default Document;