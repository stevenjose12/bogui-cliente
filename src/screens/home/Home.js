import React, { Component } from "react";
import {
  Modal,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  Platform,
  AppState
} from "react-native";
import Permissions from "../../utils/Permissions";
import Geolocation from "../../utils/Geolocation";
import { MapAnimationSmart, MarkerData } from "../../widgets/MapAnimationSmart";
import { socket } from "../../utils/Socket";
import { connect } from "react-redux";
import VerificationModal from "../../widgets/VerificationModal";
import Geo from "../../utils/Geo";
import G from "../../utils/G";
import Color from "../../assets/Color";
import { NavigationActions, StackActions } from "react-navigation";
import ModalCategoryBogui from "../../widgets/ModalCategoryBogui";
import ModalBoguisHere from "../../widgets/ModalBoguisHere";
import HeaderHome from "./HeaderHome";
import WelcomeModal from "../../widgets/WelcomeModal";
import FooterHome from "./FooterHome";
import StopModal from "../../widgets/StopModal";
import CustomModal from "../../utils/Modal";
import RouteConfirmed from "../routeconfirmed/RouteConfirmed";
import Api from "../../utils/Api";
import SocketEvents from "../../utils/SocketEvents";
import ModalDriver from "../../widgets/ModalDriver";
import Bill from "../../widgets/Bill";
import QualifyModal from "../../widgets/QualifyModal";
import Document from "./Document";
import AdminNotifications from "./AdminNotifications";
import Icon from "react-native-vector-icons/Ionicons";
import Task from "../../utils/Task";
import RNUnlockDevice from "react-native-unlock-device";
import LaunchForeground from "../../plugins/LaunchForeground";
import IncomingCall from "../opentok/IncomingCall";
import Call from "../opentok/Call";
import EmergencyCall from "../../widgets/EmergencyCall";
import DeviceInfo from "react-native-device-info";
import FirebasePushNotification from "../../plugins/FirebasePushNotification";
import Notification from "../../plugins/Notifications";
import CoreNative from "../../plugins/CoreNative";
import moment from "moment";
import Toast from "../../widgets/Toast";
import { on } from "../../utils/EventEmit";
import Background from "../../widgets/Background";

const timeAnimation: number = 1000; // <------------------------------------------------------------------------------- duración de la animación
const timeAlive: number = 20000; // <---------------------------------------------------------------------------------- tiempo de espera por socket del conductor, al pasar este tiempo sin recibir socket del conductor se elimina su marker del mapa
const steps = {
  INIT: 0,
  COLLECT: 1,
  STOP: 2,
  GOAL: 3
};
const vehicleType = {
  SEDAN: 1,
  SUV: 2,
  VAN: 3
};

class Home extends Component {
  constructor(props) {
    super(props);
    this.props.navigation.setParams({
      containerDefault: true,
      transparent: true,
      iconColor: Color.accent
    });
  }

  static idMarker: number = 0; // id de prueba (borrar)
  static displayRadius: number = 20000;

  state = {
    intoChat: false,
    currentLocation: null, // <------------------------------------------------------------------------------------- coordenadas de la posición actual del usuario
    unverified: false,
    showWelcomeModal: false,
    outVehicles: false,
    footerText: "",
    documents: null,
    showAdminNotifications: false,
    showTab: false,
    showMap: true,
    shouldShowVerificationModal: true
  };

  // MARKERS CONDUCTORES
  markers = []; // <------------------------------------------------------------------------------------------------- array de markers con los datos mas recientes de los conductores
  oldMarkers = []; // <---------------------------------------------------------------------------------------------- array de markers con el ultimo estado antes de la animación
  placesMarkers = [];
  markersCenters = [
    require("../../assets/icons/marker_center_init.png"),
    require("../../assets/icons/marker_center_stop.png"),
    require("../../assets/icons/marker_center_goal.png")
  ];
  markersCentersNight = [
    require("../../assets/icons/marker_center_init_night.png"),
    require("../../assets/icons/marker_center_stop_night.png"),
    require("../../assets/icons/marker_center_goal_night.png")
  ];
  step = steps.INIT;
  places = [];
  modalBoguisHere = null;
  inFocus: boolean = false; // <------------------------------------------------------------------------------------- bandera que indica si la página esta en foco

  componentDidMount() {
    this.onFocusListener = this.props.navigation.addListener(
      "didFocus",
      this.defaultValues
    );
    this.onBlurListener = this.props.navigation.addListener(
      "didBlur",
      () => (this.inFocus = false)
    );

    on("INTO_CHAT", intoChat => this.setState({ intoChat }));

    FirebasePushNotification.setLogin(true);

    FirebasePushNotification.getPayload(payload => {
      if (!payload) return;
      this.props.navigation.navigate(payload.page);
      FirebasePushNotification.clearPayload();
    });

    G.getDisplayRadius().then(r => (Home.displayRadius = r));

    this.addNotificationEvents();

    // cada vez que se actualize el token, se envia al servidor.
    FirebasePushNotification.onTokenRefresh(this.sendToken);

    this.intervalsStart();

    AppState.addEventListener("change", status => {
      if (status !== "active")
        SocketEvents.onAdminNotification(this.checkNotifications);
    });

    this.checkPendingNotifications();
  }

  componentWillUnmount(): void {
    this.onFocusListener?.remove();
    this.onBlurListener?.remove();
  }

  defaultValues = async () => {
    this.inFocus = true;

    this.onReadyLocation()
      .then(currentLocation => this.setState({ currentLocation }))
      .catch(e => console.log(">>: Home > onReadyLocationError: ", e));

    // refrescar el token en el servidor
    FirebasePushNotification.getToken(token => this.sendToken(token));

    if (
      this.props.panic &&
      this.props.panic.inPanic &&
      Platform.OS === "android"
    ) {
      if (
        moment().format("YYYY-MM-DD HH:mm:ss") >=
        moment(this.props.panic.date.date, "YYYY-MM-DD HH:mm:ss")
          .add({ days: 1 })
          .format("YYYY-MM-DD HH:mm:ss")
      ) {
        this.props.dispatch({ type: "SET_PANIC", payload: { panic: false } });
        CoreNative.stop();
      }
    }
    socket.connect();

    this.addSocketsEvents();

    if (this.props.user == null) this.logout();
    if (!this.props.user.validate && Platform.OS === "ios") {
      this.props.navigation.navigate("Verification");
    }

    this.setState({
      showMap: true,
      shouldShowVerificationModal: true
    });

    Api.isNight()
      .then(r =>
        this.props.dispatch({ type: "SET_NIGHT", payload: { night: r.night } })
      )
      .catch(e => console.log(">>: Home > isNight > e: ", e));

    this.checkPendingDocuments();

    try {
      this.addSocketsEvents();

      this.checkNotifications();

      this.showBill();

      this.placesMarkers = [
        {
          icon: G.isNight(this.props.time)
            ? require("../../assets/icons/start_marker_night.png")
            : require("../../assets/icons/start_marker.png")
        }, // INICIO
        {
          icon: G.isNight(this.props.time)
            ? require("../../assets/icons/stop_marker_night.png")
            : require("../../assets/icons/stop_marker.png")
        }, // PARADA
        {
          icon: G.isNight(this.props.time)
            ? require("../../assets/icons/goal_marker_night.png")
            : require("../../assets/icons/goal_marker.png")
        } // DESTINO
      ];

      this.setPlaces();
      this.setStep(steps.INIT);

      if (this.props.isShowModalDriver && !!this.props.route) {
        setTimeout(() => {
          this.modalDriver.show(true, this.props.route.driverId);
          this.props.dispatch({
            type: "SET_SHOW_MODAL_DRIVER",
            payload: { showModalDriver: false }
          });
        }, 500);
      }

      const isRouteValid = await Api.routeValid(this.props.user.id);

      if (isRouteValid.result) {
        const route = await Api.routeAccepted(isRouteValid.id);
        route.driverId = route.driver_id;
        this.setRoute(route);

        this.places[0] = {
          location: {
            latitude: parseFloat(this.props.route.latitud_origin),
            longitude: parseFloat(this.props.route.longitud_origin)
          },
          address: this.props.route.description
        };
        if (this.props.route.to_destinies.length === 1) {
          this.places[2] = {
            location: {
              latitude: parseFloat(this.props.route.to_destinies[0].latitud),
              longitude: parseFloat(this.props.route.to_destinies[0].longitud)
            },
            address: this.props.route.to_destinies[0].description
          };
        } else {
          for (let i = 0; i < this.props.route.to_destinies.length; i++) {
            this.places[i + 1] = {
              location: {
                latitude: parseFloat(this.props.route.to_destinies[i].latitud),
                longitude: parseFloat(this.props.route.to_destinies[i].longitud)
              },
              address: this.props.route.to_destinies[i].description
            };
          }
        }

        await this.props.dispatch({
          type: "SET_PLACES",
          payload: { places: this.places }
        });

        if (route.driver_id) {
          this.forceUpdate();
        } else {
          this.setState({ showMap: false }, () =>
            this.props.navigation.navigate("CancelRoute")
          );
        }
      } else {
        this.clearRequest();
      }
    } catch (e) {
      console.log(">>: Home > defaultValues > error: ", e);
    }
  };

  intervalsStart = () => {
    // interval que manda la ubicación del pasajero cada 7 seg
    Task.add("sendLocation", 7000, () => {
      if (!this.props.user || !Geolocation.getCurrentLocation()) return;
      const location = Geolocation.getLatLng(Geolocation.getCurrentLocation());
      const params = {
        id: this.props.user.id,
        latitud: location.latitude,
        longitud: location.longitude,
        in_route: false
      };
      socket.emit("ubicacion-pasajero", params);
    });

    this.animation();
  };

  /**
   * Función que simplifica la llamada de la ubicacion actual
   */
  onReadyLocation = (): Promise => {
    return new Promise((resolve, reject) => {
      // if (Geolocation.getCurrentLocation()) {
      //   resolve(Geolocation.getLatLng(Geolocation.getCurrentLocation()));
      //   return;
      // }

      Permissions.request("location")
        .then(granted => {
          // Si el permiso de ubicación no esta dado se lanza una excepción.
          if (!granted) {
            reject("Permissions location no granted");
            return;
          }

          // Si hay una ruta no se muestra el home si no el componente de RouteConfirmed
          if (this.props.route) {
            reject("route not null");
            return;
          }

          Geolocation.onReadyLocation(location => {
            if (!this.props.user) {
              reject("User null");
            } else {
              resolve(Geolocation.getLatLng(location));
            }
          });
          Geolocation.onError(e => {
            if (this.props.user) Geolocation.start();
            reject(e);
          });
          Geolocation.stop();
          Geolocation.start();
        })
        .catch(reject);
    });
  };

  sendToken = token => {
    if (!this.props.user) return;
    const params = {
      token,
      user_id: this.props.user.id,
      device_id: DeviceInfo.getUniqueID()
    };
    socket.emit("refresh-token", params);
  };

  logout = () => {
    SocketEvents.clear();
    socket.disconnect();
    FirebasePushNotification.setLogin(false);
    Geolocation.stop();
    Task.stop();
    this.clearRequest();
    this.props.dispatch({ type: "SET_USER", payload: { user: null } });
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "Login" })]
      })
    );
  };

  addSocketsEvents = () => {
    SocketEvents.onDisableUser(data => {
      if (!this.props.user || this.props.user.id !== data.id) return;
      Toast.show("Cuenta desactivada");
      this.logout();
    });

    SocketEvents.onChangeNetwork(() => {
      if (this.props.user) socket.connect();
    });

    SocketEvents.onSocketInCall(async data => {
      if (!(this.props.user.id == data.id && !G.inCallPage)) return;
      try {
        if (Platform.OS !== "android") return;
        await RNUnlockDevice.unlock();
        LaunchForeground.launch();
      } catch (e) {}
      this.inCallModal.show(data);
    });

    if (!this.props.route) {
      SocketEvents.onDriverPosition(data => {
        if (!this.filterDriver(data)) return;
        const index = this.markers.findIndex(
          marker => marker.id === data.user.id
        );
        if (index >= 0) {
          this.markers[index].setParams(
            data.latitude,
            data.longitude,
            data.rotation ? data.rotation : 0,
            new Date().getTime()
          );
        } else {
          const marker: MarkerData = new MarkerData(
            data.user.id,
            data.latitude,
            data.longitude,
            data.user.vehicle.vehicle_categories[0].vehicle_type_id
          );
          this.markers.push(marker);
        }
      });
    }

    SocketEvents.onSocketDisconnect(data => {
      if (this.props.user) socket.connect();
    });

    SocketEvents.onAdminNotification(this.checkNotifications);

    SocketEvents.onNotification(notification => {
      if (this.props.user.id !== notification.user_id) return;
      const r = this.getDescription(notification.type, notification.driver_id);
      if (Platform.OS === "ios")
        Notification.showPayload("Bogui", r.message, { page: r.page });
    });

    SocketEvents.onSendMessage(data =>
      G.showChatNotification(this.props.user.id, data)
    );

    SocketEvents.onDriverDeleted(data => this.removeMarker(data.id));
  };

  checkNotifications = async data => {
    console.log("Admin notifications", data, this.props.user.id);
    if (!(data && data.users && data.users.includes(this.props.user.id)))
      return false;
    if (Platform.OS === "ios")
      Notification.showPayload(
        "Bogui",
        "Nueva notificación del administrador",
        { page: "Home" }
      );
    this.checkPendingNotifications();
  };

  checkPendingNotifications = async (): void => {
    try {
      const r = await Api.checkAdminNotifications(this.props.user.id);
      if (r.result) this.setState({ showTab: r.notificaciones > 0 });
    } catch (e) {
      console.log(">>: Home: checkNotifications: err:", e);
    }
  };

  getDescription = (type, driver_id = null) => {
    switch (type) {
      case 1: // Carrera Aceptada
        return {
          message: "El viaje ha sido aceptado",
          page: "Home"
        };

      case 2: // Destino Modificado
        return {
          message: "El viaje ha sido finalizado",
          page: "Home"
        };

      case 3: // Carrera Cancelada
        return {
          message: "El viaje ha sido cancelado",
          page: "Home"
        };

      case 4: // Boggui ha llegado al punoto de encuentro
        if (driver_id)
          this.modalDriver.dismiss(() => this.modalBoguisHere.show(true));
        return {
          message: "tu Bogui ya ha llegado",
          page: "Home"
        };

      case 6: // Documentos Requeridos
        this.checkPendingDocuments();
        return {
          message: "Su cuenta requiere atención",
          page: "PendingDocuments"
        };

      case 7: // Documentos Aceptados
        this.setState({ documents: null });
        return {
          message: "Sus documentos fueron aceptados",
          page: "PendingDocuments"
        };

      case 8: // Documentos Rechazados
        this.checkPendingDocuments();
        return {
          message: "Uno o mas de sus documentos fueron rechazados",
          page: "PendingDocuments"
        };
    }
  };

  checkPendingDocuments = () =>
    Api.getPendingDocuments(this.props.user.id).then(r => {
      if (r.result <= 0) return;
      const documents = {
        documentos: r.documentos,
        documentos_pendientes: r.documentos_pendientes
      };
      this.setState({ documents });
    });

  getAddress = async (location, index) => {
    try {
      this.placesMarkers[index].address = await Geo.getAddress(location);
      this.setPlaces();
    } catch (e) {
      setTimeout(() => this.getAddress(location, index), 3000);
    }
  };

  setPlaces = (): void => {
    for (let i = 0; i < this.placesMarkers.length; i++) {
      this.places[i] = {
        location: this.placesMarkers[i].location,
        address: this.placesMarkers[i].address
      };
    }
    this.props.dispatch({
      type: "SET_PLACES",
      payload: { places: this.places }
    });
    this.header?.setPlaces(this.places);
  };

  filterDriver = (data): boolean => {
    if (!this.vehicleType) return false; // <- Si el usuario ya ha establecido una categoria de bogui
    if (!data.user.vehicle) return false; // <- Que el dato tenga vehiculos

    const filterByVehicle = vehicle => {
      for (let i = 0; i < vehicle.vehicle_categories.length; i++) {
        const item = vehicle.vehicle_categories[i];
        if (item.vehicle_type_id === this.vehicleType) return true;
        if (
          this.vehicleType !== vehicleType.VAN &&
          this.vehicleType + 1 === item.vehicle_type_id
        )
          return true;
      }
      return false;
    };

    const distance = this.getDistance(data.latitude, data.longitude);
    const isRouteActive = !data.ruta_activa;
    const out =
      filterByVehicle(data.user.vehicle) && distance <= Home.displayRadius;
    return out && isRouteActive;
  };

  removeMarker = id => {
    let index = this.markers.findIndex((m: MarkerData) => m.id === id);
    if (index >= 0) this.markers.splice(index, 1);
    index = this.oldMarkers.findIndex((m: MarkerData) => m.id === id);
    if (index >= 0) this.oldMarkers.splice(index, 1);
  };

  removeAllMarkers = () => {
    this.markers = [];
    this.oldMarkers = [];
  };

  animation = () => {
    const interval = () => {
      if (!this.inFocus) return;

      if (!this.props.user) {
        Task.remove("animationHome");
        return false;
      }

      const currentTime = new Date().getTime();
      this.markers.forEach(m => {
        if (currentTime - m.time >= timeAlive) this.removeMarker(m.id);
      });

      const markers = [];
      try {
        if (this.mapAnimation) {
          this.markers.forEach((m: MarkerData, i) => (markers[i] = m.clone()));
          this.mapAnimation.anim(this.oldMarkers, markers);
          this.oldMarkers = markers;
        }
      } catch (e) {
        console.log(">>: Home > animation > interval > error: ", e);
      }
    };

    Task.add("animationHome", timeAnimation, interval);
  };

  getDistance = (lat, lng): number => {
    try {
      const cameraCenter = this.mapAnimation.getCameraCenter();
      if (cameraCenter)
        return (
          Geo.getDistanceBetweenCoordinates(
            cameraCenter.latitude,
            cameraCenter.longitude,
            lat,
            lng
          ) * 1000
        );
      return NaN;
    } catch (e) {
      return NaN;
    }
  };

  onFooterButtonPress = () => {
    this.setState({ showWelcomeModal: !this.props.shownWelcomeModal });

    switch (this.step) {
      case steps.INIT:
        // ABRE LA MODAL PARA SELECCIONAR LA CATEGORIA DEL BOGUI
        this.modalCategory.show(true);
        break;

      case steps.COLLECT:
        this.placesMarkers[
          this.step - 1
        ].location = this.mapAnimation.getCameraCenter();
        this.modalStop.onPositiveButton(() => this.setStep(steps.STOP));
        this.modalStop.onNegativeButton(() => this.setStep(steps.GOAL));
        this.modalStop.show(true);
        break;

      case steps.STOP:
        this.placesMarkers[
          this.step - 1
        ].location = this.mapAnimation.getCameraCenter();
        this.setStep(steps.GOAL);
        break;

      case steps.GOAL:
        this.placesMarkers[
          this.step - 1
        ].location = this.mapAnimation.getCameraCenter();
        this.setPlaces();
        this.setState({ showMap: false });
        this.props.navigation.navigate("ConfirmRoute");
        break;
    }
  };

  setStep = (step: number, onBackPress = false) => {
    this.step = step;

    try {
      const setAddress = (index: number): void =>
        this.mapAnimation
          .getCamera()
          .then(camera => this.getAddress(camera.center, index));

      switch (this.step) {
        case steps.INIT:
          this.vehicleType = null;
          this.removeAllMarkers();
          this.footerHome?.setTextButton("Comencemos");
          this.footerHome?.showBackButton(false);
          if (onBackPress) this.onFooterButtonPress();
          break;

        case steps.COLLECT:
          this.footerHome?.showBackButton(true);
          this.setState({
            footerText: "Mueva el mapa para seleccionar tu punto de partida"
          });
          this.footerHome?.setTextButton("Recoger aquí");
          this.placesMarkers[this.step - 1].location = null;
          this.placesMarkers[1].address = null;
          if (
            !this.placesMarkers[1].location &&
            this.placesMarkers[2].address
          ) {
            this.placesMarkers[2].address = null;
            this.placesMarkers[2].location = null;
          }
          setAddress(0);
          break;

        case steps.STOP:
          this.setState({
            footerText: "Mueva el mapa para seleccionar tu parada"
          });
          this.footerHome?.setTextButton("Parada");
          this.placesMarkers[this.step - 1].location = null;
          this.placesMarkers[this.step - 1].address = null;
          this.placesMarkers[this.step].address = null;
          setAddress(1);
          break;

        case steps.GOAL:
          this.setState({
            footerText: "Mueva el mapa para seleccionar tu destino"
          });
          this.footerHome?.setTextButton("Dejar aquí");
          this.placesMarkers[this.step - 1].location = null;
          this.placesMarkers[this.step - 1].address = null;
          setAddress(2);
          break;
      }
      this.setPlaces();
      this.forceUpdate();
    } catch (e) {
      console.log(">>: Home > setStep > error: ", e);
    }
  };

  clearRequest = () => {
    this.props.dispatch({ type: "SET_REQUEST", payload: { request: null } });
    this.props.dispatch({ type: "SET_PLACES", payload: { places: null } });
    this.setRoute();
  };

  outVehicles = data => {
    if (data.condition) {
      this.props.dispatch({
        type: "SET_VEHICLES",
        payload: { vehicles: data.vehicles }
      });
    } else {
      if (this.props.isStorageVehicles) {
        return this.props.vehicles;
      } else {
        CustomModal.showGenericError();
      }
    }
  };

  onDragEnd = async () => {
    const camera = await this.mapAnimation.getCamera();
    const center = camera.center;
    this.header.setInputValueByDragEnd();
    if (this.footerHome) this.footerHome.showTuto(true);

    switch (this.step) {
      case steps.INIT:
        this.getAddress(center, 0);
        break;

      case steps.COLLECT:
        this.getAddress(center, this.step - 1);
        break;

      case steps.STOP:
        this.getAddress(center, this.step - 1);
        break;

      case steps.GOAL:
        this.getAddress(center, this.step - 1);
        break;
    }
  };

  showBill = async () => {
    const r = await Api.routePayable(this.props.user.id);
    if (!r.success) return;
    // this.billModal.show(r.ruta);
    this.props.navigation.navigate("Bill", { route: r.ruta });
    this.setRoute();
  };

  addNotificationEvents = () =>
    FirebasePushNotification.onPayloadReceiver(params =>
      this.props.navigation.navigate(params.page)
    );

  setRoute = (route = null) => {
    route = route || null;
    this.props.dispatch({ type: "SET_ROUTE", payload: { route } });
  };

  render() {
    return (
      <Background noBackground style={styles.container}>
        {!!this.props.route &&
        !!this.props.route &&
        !!this.props.route.driverId ? (
          <RouteConfirmed
            currentLocation={this.state.currentLocation}
            onRouteFinish={this.showBill}
            toMyTravel={() => this.props.navigation.navigate("MyTravelDrawer")}
            toEdit={() => this.props.navigation.navigate("EditRouteDrawer")}
            back={this.defaultValues}
            navigation={this.props.navigation}
            call={() => {
              if (this.callModal) this.callModal.show(true);
            }}
          />
        ) : (
          this.state.showMap && (
            <MapAnimationSmart
              ref={map => (this.mapAnimation = map)}
              center={this.state.currentLocation}
              timeAnimation={timeAnimation}
              markerIcon={
                this.vehicleType === 4
                  ? require("../../assets/icons/moto_marker.png")
                  : require("../../assets/icons/car_marker.png")
              }
              nightMode={this.props.time}
              header={
                <HeaderHome
                  step={this.step}
                  ref={header => (this.header = header)}
                  onChangeText={(i, text) => {
                    this.placesMarkers[i].address = text;
                    this.setPlaces();
                  }}
                  setPlaces={currentLocation =>
                    this.setState({ currentLocation })
                  }
                />
              }
              footer={
                this.state.documents &&
                (this.state.documents.documentos.length > 0 ||
                  this.state.documents.documentos_pendientes.length > 0) ? (
                  <Document
                    toPendingDocuments={() =>
                      this.props.navigation.navigate("PendingDocuments")
                    }
                  />
                ) : (
                  <View>
                    <FooterHome
                      night={G.isNight(this.props.time)}
                      containerStyle={{
                        marginBottom: this.state.showTab ? 60 : undefined
                      }}
                      ref={footerHome => (this.footerHome = footerHome)}
                      onPress={this.onFooterButtonPress}
                      step={this.step}
                      footerText={this.state.footerText}
                      onBackPress={() =>
                        this.setStep(
                          !this.placesMarkers[1].location &&
                            this.placesMarkers[2].address
                            ? this.step - 2
                            : this.step - 1,
                          true
                        )
                      }
                    />
                    {!!this.state.showTab && (
                      <TouchableWithoutFeedback
                        onPress={() =>
                          this.setState({ showAdminNotifications: true })
                        }
                      >
                        <View
                          style={[
                            styles.tab,
                            G.isNight() ? styles.lightTab : null,
                            this.props.route ? { bottom: 10 } : null
                          ]}
                        >
                          <Icon
                            size={40}
                            color={G.isNight() ? "#000" : "#fff"}
                            name="md-arrow-round-up"
                          />
                        </View>
                      </TouchableWithoutFeedback>
                    )}
                  </View>
                )
              }
              marker={
                G.isNight(this.props.time)
                  ? this.markersCentersNight[
                      this.step === 0 ? 0 : this.step - 1
                    ]
                  : this.markersCenters[this.step === 0 ? 0 : this.step - 1]
              }
              placeMarkers={this.placesMarkers}
              onDrag={() => this?.footerHome?.showTuto(false)}
              onDragEnd={this.onDragEnd}
            >
              {/* <EmergencyCall style={ { position: 'absolute', zIndex: 2, bottom: 90, left: 5 } } night={ G.isNight(this.props.time) }/> */}
            </MapAnimationSmart>
          )
        )}

        <ModalCategoryBogui
          ref={modal => (this.modalCategory = modal)}
          onOutVehicles={data => this.outVehicles(data)}
          outInternet={this.state.outVehicles}
          onSelect={item => {
            this.setStep(steps.COLLECT);
            this.vehicleType = item.id;
            this.props.dispatch({
              type: "SET_REQUEST",
              payload: { request: { vehicleType: this.vehicleType } }
            });
            this.removeAllMarkers();
          }}
        />

        <VerificationModal
          visible={
            this.props.user &&
            !this.state.intoChat &&
            this.props.user.validate == 0 &&
            this.state.shouldShowVerificationModal
          }
          onSupportPress={() =>
            this.setState({ shouldShowVerificationModal: false }, () =>
              this.props.navigation.navigate("Support")
            )
          }
          logout={this.logout}
        />

        <StopModal ref={modal => (this.modalStop = modal)} />

        {/* <Bill
          ref={modal => (this.billModal = modal)}
          user={this.props.user}
          navigation={this.props.navigation}
          success={route => this.qualifyModal.show(true, route)}
        /> */}

        <QualifyModal
          ref={modal => (this.qualifyModal = modal)}
          type={1}
          user={this.props.user}
          onCloseModal={() => this.qualifyModal.show(true)}
        />

        {/* MODAL QUE MUESTRA LOS DATOS DEL CONDUCTOR AL MOMENTO DE QUE ESTE ACEPTA LA SOLICITUD */}
        <ModalDriver
          ref={modal => (this.modalDriver = modal)}
          places={this.props.places}
        />

        {/* MODAL QUE SE MUESTRA DE FORMA AUTOMATICA AL LLEGAR EL CONDUCTOR */}
        <ModalBoguisHere ref={modal => (this.modalBoguisHere = modal)} />

        <Modal
          transparent
          useNativeDriver
          animationType={"slide"}
          visible={this.state.showAdminNotifications}
        >
          <AdminNotifications
            visible={this.state.showAdminNotifications}
            onClose={() => {
              SocketEvents.onAdminNotification(this.checkNotifications);
              this.checkPendingNotifications();
              this.setState({ showAdminNotifications: false });
            }}
          />
        </Modal>

        <WelcomeModal
          visible={this.state.showWelcomeModal}
          close={() => {
            this.props.dispatch({
              type: "SET_SHOWN_WELCOME_MODAL",
              payload: { shownWelcomeModal: true }
            });
            this.setState({ showWelcomeModal: false });
          }}
        />

        <IncomingCall
          ref={modal => (this.inCallModal = modal)}
          reply={() => this.callModal.show(false)}
        />

        <Call ref={modal => (this.callModal = modal)} />
      </Background>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%"
  },
  btn: {
    backgroundColor: Color.primary,
    width: "50%",
    height: 40,
    borderRadius: 20,
    marginBottom: 15
  },
  tab: {
    backgroundColor: "#000",
    width: 100,
    height: 100,
    paddingTop: 10,
    alignItems: "center",
    borderRadius: 50,
    alignSelf: "center",
    position: "absolute",
    bottom: -50
  },
  lightTab: {
    backgroundColor: "#fff"
  }
});

const redux = state => ({
  user: state.auth.user,
  isStorageVehicles: state.vehicles.isStorageVehicles,
  vehicles: state.vehicles.vehicles,
  route: state.route.route,
  places: state.places.places,
  isShowModalDriver: state.showModalDriver.showModalDriver,
  time: state.night.night,
  shownWelcomeModal: state.shownWelcomeModal,
  panic: state.panic,
  lastRoute: state.lastRoute
});

export default connect(redux)(Home);
