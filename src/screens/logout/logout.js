import React from 'react';
import { connect } from 'react-redux';
import { StackActions, NavigationActions } from 'react-navigation';

class Logout extends React.Component {

	static navigationOptions = {
		header: null
	};
	
	componentDidMount() {
		setTimeout(() => {
			this.props.navigation.dispatch(
				StackActions.reset({
					index: 0,
					actions: [
						NavigationActions.navigate({ routeName: 'Login' })
					]
				})
			);
		},1500);
	}

	render() {
		return null;
	}

}

export default connect(null)(Logout);