import React, { Component } from "react";
import { StyleSheet, View, Text, ScrollView, Image } from "react-native";
import Background from "../../widgets/Background";
import Button from "../../widgets/Button";
import Hbox from "../../widgets/Hbox";
import Input from "../../widgets/InputBogui";
import Colors from '../../assets/Color';
import Modal from '../../utils/Modal';
import { connect } from 'react-redux';
import Select from './Select';
import Api from "../../utils/Api";
import {Button as ButtonElements} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import CustomHeader from "../../widgets/CustomHeader";

class Contacts extends Component {

    state = {
        form: {
            id: '',
            user_id: this.props.user.id,
            name: '',
            phone: '',
            phoneCode: ''
        },
        formTranslate:{
            name: 'Nombre',
            phone: 'Telefono',
            phoneCode: 'Código de Área'
        },
        phoneAreaCodes:{
            select: '',
            phoneAreaCodesResponse:[],
            listPhoneAreaCodes:[],
        },
        contacts: [],
        edit: false
    };

    static navigationOptions = { header: null };

    componentDidMount() {
        this.phoneAreaCodes();
        this.load();
    }

    phoneAreaCodes = async () => {
        try {
            if (this.props.isStoragePhoneCodes && this.props.phoneCodes && this.props.phoneCodes.length > 0) {
                this.setState({
                    phoneAreaCodes: {
                        listPhoneAreaCodes: this.props.phoneCodes
                    }
                });
            } else {
                const phoneAreaCodesResponse = await Api.phoneAreaCode(true);
                this.setState({
                    phoneAreaCodes: { phoneAreaCodesResponse }
                });
                this.listRenderAreaCodes();
            }
        } catch (e) {
            Modal.showAlert("Ha ocurrido un error al cargar los codigos de Área");
        }
    };

    listRenderAreaCodes = () => {
        let list = [];
        for (let index = 0; index < this.state.phoneAreaCodes.phoneAreaCodesResponse.length; index++) {
            const element = this.state.phoneAreaCodes.phoneAreaCodesResponse[index];
            list[index] =  {
                label: '(+'+element.phonecode+') '+element.name,
                value: element.phonecode
            };
        }

        this.setState({
            phoneAreaCodes: {
                ... this.state.phoneAreaCodes,
                listPhoneAreaCodes: list
            }
        },
            () => this.props.dispatch( { type: 'SET_PHONECODE', payload: {
                    phoneCodes: this.state.phoneAreaCodes.listPhoneAreaCodes
                }
            })
        )

    };

    setSelect = value => {
        this.setState({
            ...this.state,
            phoneAreaCodes:{
                ...this.state.phoneAreaCodes,
                select: value
            }
        });
        this.setState(state => state.form.phoneCode = value);
    };

    isValidForm = () => {
        for (const propName in this.state.form) {
            if (propName !== 'user_id' && propName !== 'id' && !this.state.form[propName]){
                const name = this.state.formTranslate[propName];
                Modal.showAlert("El Campo: "+name+" Es Obligatorio");
                return false
            }
        }
        return true
    };

    load = async () => {
        try {
            const r = await Api.contactsGet({user_id: this.props.user.id});
            if (r.result) {
                this.setState({ contacts: r.contacts });
            } else {
                throw "response false";
            }
        } catch (e) {
            Modal.showGenericError(e);
        }
    };

    submit = async () => {
        try {
            if (!this.isValidForm()) return false;
            const r = await Api.contactsCreate(this.state.form);
            if (r.result) {
                Modal.alert.show({ message: r.msg, time: 3000});
                this.setState({
                    form: {
                        ... this.state.form,
                        id: '',
                        phoneCode: '',
                        name: '',
                        phone: '',
                    },
                    edit: false
                });
                this.load();
            } else {
                throw "response false";
            }
        } catch (e) {
            Modal.showGenericError(e);
        }
    };

    saveEdit = async () => {
        try {
            const r = await Api.contactsEdit(this.state.form);
            if (r.result) {
                Modal.alert.show({ message: r.msg, time: 3000});
                this.setState({
                    form: {
                        ... this.state.form,
                        id: '',
                        phoneCode: '',
                        name: '',
                        phone: '',
                    },
                    edit: false
                });
                this.load();
            } else {
                throw "response false";
            }
        } catch (e) {
            Modal.showGenericError(e);
        }
    };

    edit = element => {
        if (this.scroll) this.scroll.scrollTo({x: 0, y: 0, animated: true});
        this.setState({
            form:{
                ... this.state.form,
                name: element.name,
                phone: element.phone,
                phoneCode: element.phoneCode,
                id: element.id
            },
            edit: true
        });
    };

    delete = element => {
        Modal.alert.onPositiveButton('Si', async () => {
			try {
                const r = await Api.contactsDelete({ id: element.id, user_id: this.props.user.id });
				if (r.result) {
					Modal.alert.show({ message: r.msg, time: 3000 });
					this.load();
				} else {
                    throw "response false";
				}
			} catch (e) {
				Modal.showGenericError(e);
			}
		});
		Modal.alert.onCancelButton('No');
		Modal.alert.show({ message: '¿Desea eliminar el Contacto?' });
    };

    render() {
        return (
            <Background>

                <CustomHeader
                    title="Contactos"
                    onPress={ () => this.props.navigation.navigate("HomeDrawer") }
                    backType />

                <ScrollView ref={ scroll => this.scroll = scroll }>

                    <View style={ styles.container }>
                        <Hbox>
                            <Image
                                source={require('../../assets/icons/address-book-2.png')}
                                style={{
                                    width: 85,
                                    height: 85
                                }} />
                            <View style={{flex: 1}}>
                                <Input
                                    label="Nombre"
                                    text={ this.state.form.name }
                                    onChangeText={ text => this.setState({
                                        form:{
                                            ... this.state.form,
                                            name: text
                                        }
                                    }) } />
                                <Hbox>
                                    {
                                        this.state.phoneAreaCodes &&
                                        this.state.phoneAreaCodes.listPhoneAreaCodes &&
                                        this.state.phoneAreaCodes.listPhoneAreaCodes.length>0 && (
                                            <View style={{
                                                width: '40%',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}>
                                                <Text style={{ color: Colors.primary }}>
                                                    Cod. Área
                                                </Text>
                                                <Select
                                                    showValue
                                                    items={ this.state.phoneAreaCodes.listPhoneAreaCodes }
                                                    onValueChange={value => this.setSelect(value)}
                                                    value={this.state.form.phoneCode}
                                                    placeholder="Código" />
                                            </View>
                                        )
                                    }
                                    <Input
                                        label="Teléfono"
                                        style={{flex: 1}}
                                        text={ this.state.form.phone }
                                        onChangeText={ phone => this.setState({
                                            form: {
                                                ... this.state.form,
                                                phone
                                            }
                                        }) }
                                        keyboardType="numeric"
                                    />
                                </Hbox>
                            </View>
                        </Hbox>

                        <Button
                            title={ this.state.edit ? "Actualizar" : "Guardar" }
                            type="outline"
                            titleStyle={{
                                fontSize: 15,
                                color: Colors.primary,
                                fontFamily: 'Poppins-Bold'
                            }}
                            containerStyle={{
                                width: "50%",
                                marginTop: 20
                            }}
                            buttonStyle={{
                                borderColor: Colors.accent,
                                paddingVertical: 5,
                                alignItems: 'center',
                                borderWidth: 2,
                                borderRadius: 20,
                            }}
                            onPress={ this.state.edit ? this.saveEdit : this.submit }
                        />
                            {
                                this.state.contacts && this.state.contacts.length > 0 && (
                                    <View style={{
                                        paddingVertical: 10,
                                        marginVertical: 10,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderTopColor: Colors.primary,
                                        borderTopWidth: 1.4,
                                    }}>
                                        <Hbox>
                                            <View style={styles.tableItem}>
                                                <Text style={styles.header}>Nombre</Text>
                                            </View>
                                            <View style={styles.tableItem}>
                                                <Text style={styles.header}>Teléfono</Text>
                                            </View>
                                            <View style={styles.tableItem}>
                                                <Text style={styles.header}>Acciones</Text>
                                            </View>
                                        </Hbox>
                                        {
                                            this.state.contacts.map((element, index) => {
                                                return (
                                                    <View
                                                        key={index}
                                                        style={[{ flexDirection: 'row', paddingVertical: 4, }, this.state.contacts.length !== (index+1) ? {
                                                            borderBottomColor: Colors.secondaryGray,
                                                            borderBottomWidth: 1
                                                        } : '']}>
                                                        <Hbox>
                                                            <View style={styles.tableItem}>
                                                                <Text numberOfLines={1}>{element.name}</Text>
                                                            </View>
                                                            <View style={styles.tableItem}>
                                                                <Text numberOfLines={1}>{'+'+element.phoneCode+'-'+element.phone}</Text>
                                                            </View>
                                                            <View style={styles.tableItem}>
                                                                <View style={{flexDirection: 'row'}}>

                                                                    <ButtonElements
                                                                        onPress={ () => this.edit(element) }
                                                                        icon={
                                                                            <Icon
                                                                                name="edit"
                                                                                size={14}
                                                                                color={Colors.white} />
                                                                        }
                                                                        buttonStyle={[styles.buttonTable, {
                                                                            backgroundColor: Colors.blue
                                                                        }]}
                                                                        containerStyle={{
                                                                            paddingHorizontal: 5,
                                                                        }} />

                                                                    <ButtonElements
                                                                        onPress={ () => this.delete(element) }
                                                                        icon={
                                                                            <Icon
                                                                                name="trash"
                                                                                size={14}
                                                                                color={Colors.white} />
                                                                        }
                                                                        buttonStyle={[styles.buttonTable, {
                                                                            backgroundColor: Colors.accent
                                                                        }]} />

                                                                </View>
                                                            </View>
                                                        </Hbox>
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>
                                )
                            }
                    </View>
                </ScrollView>
            </Background>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        alignItems: "center",
        padding: 10
    },
    avatar: {
        marginBottom: 10,
        backgroundColor: 'transparent'

    },
    tableItem: {
        flex: .333,
        justifyContent: 'center',
        alignItems: 'center'
    },
    header:{
        color: Colors.primary,
        fontFamily: 'Poppins-Bold'
    },
    buttonTable: {
        width: 30,
        height: 30,
    }
});

export default connect( state => ({
    user: state.auth.user,
    isStoragePhoneCodes: state.phonecodes.isStoragePhoneCodes,
    phoneCodes: state.phonecodes.phoneCodes
}))(Contacts);