import React, { Component } from 'react';
import {View, TouchableWithoutFeedback, Text, StyleSheet, Image} from 'react-native';
import Hbox from "../../widgets/Hbox";
import Color from "../../assets/Color";
import DatePicker from 'react-native-datepicker';
import G from '../../utils/G';
import ImagePicker from 'react-native-image-picker';
import Rotation from "../../widgets/Rotation";
import moment from "moment";

class Item extends Component {

    state = {
        source: this.props.item.file,
        date: G.minDate
    };

    getText = () => {
        switch (this.props.item.type) {
            case 1: return "Foto de Perfil";
            case 6: return "Documento (RUT o Pasaporte)";
        }
    };

    getStatus = () => this.state.source ? 'Actualizado': 'Actualizar';

    getIcon = () => this.state.source ? require('../../assets/icons/check.png') : require('../../assets/icons/uncheck.png');

    getColor = () => this.state.source ? Color.green : Color.accent;

    showDate = () => [2,4,5,6].indexOf(this.props.item.type) !== -1;

    getTextStyle = () => this.state.source ? styles.green : styles.red;

    chooseImage = () => {

        const options = {
            title: 'Opciones',
            cancelButtonTitle: 'Cancelar',
            takePhotoButtonTitle: 'Usar la Cámara',
            chooseFromLibraryButtonTitle: 'Usar la Galería',
            quality: 0.8,
            maxWidth: 1000,
            maxHeight: 1000,
            noData: true,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            }
        };

        const resizeImageSuccess = r => this.setState(
            { source: r.uri },
            () => this.modalRotation.show(this.state.source)
        );

        ImagePicker.showImagePicker(options, resizeImageSuccess);
    };

    onDateChange = date => {
        this.setState( { date } );
        this.props.item.date = moment(date, 'DD/MM/YYYY').format("YYYY-MM-DD");
    };

    onSave = degrees => {
        this.props.item.file = this.state.source.toString();
        this.props.item.dg = degrees.toString();
    };

    render() {

        return (
            <View style={ styles.container }>
                <TouchableWithoutFeedback onPress={ this.chooseImage }>
                    <View>
                        <Hbox style={styles.hbox}>

                            <View style={ styles.imageContainer } >

                                <Image
                                    style={ styles.image }
                                    source={ this.getIcon() }
                                    tintColor={ this.getColor() }
                                    resizeMode="contain"/>

                            </View>

                            <View>
                                <Text style={ { ...styles.bold, ...styles.row } }>{ this.getText() }</Text>
                                <Text style={ { ...this.getTextStyle(), ...styles.row } }> { this.getStatus() } </Text>
                            </View>

                        </Hbox>
                    </View>
                </TouchableWithoutFeedback>
                {
                    !!this.showDate() && (
                        <View>
                            <Text style={ styles.bold }> Fecha de Expiración </Text>

                            <DatePicker
                                minDate={ G.minDate }
                                maxDate={ G.maxDate }
                                date={ this.state.date }
                                confirmBtnText="Aceptar"
                                cancelBtnText="Cancelar"
                                format="DD/MM/YYYY"
                                showIcon={ false }
                                style={ { width: '100%', height: 40 } }
                                onDateChange={ this.onDateChange } />

                        </View>
                    )
                }

                <Rotation
                    ref={ rotation => this.modalRotation = rotation }
                    onSave={ this.onSave } />

            </View>
        );

    };

}

const styles = StyleSheet.create({
    container: {
        padding: 5,
        borderBottomWidth: 0.5,
        borderTopWidth: 0.5,
        borderColor: Color.chat
    },
    textHeader: {
        width: "100%",
        textAlign: 'center',
        paddingVertical: 2,
        fontSize: 17,
        color: Color.primary
    },
    hbox: {
        width: '100%',
        height: 50
    },
    imageContainer: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: 25,
        height: 25
    },
    row: {
        flex: 1,
        height: 25,
        textAlignVertical: 'center'
    },
    bold: {
        fontFamily: 'Poppins-SemiBold',
        color: Color.primary
    },
    red: {
        color: Color.accent
    },
    green: {
        color: Color.green
    }
});

export default Item;