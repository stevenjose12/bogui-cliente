import React, { Component } from "react";
import { StyleSheet, Text, FlatList, Platform, View } from "react-native";
import Background from "../../widgets/Background";
import Color from "../../assets/Color";
import Modal from "../../utils/Modal";
import Api from "../../utils/Api";
import {connect} from "react-redux";
import Button from '../../widgets/Button';
import Item from "./Item";
import Toast from "../../widgets/Toast";
import { socket } from "../../utils/Socket";

class PendingDocuments extends Component {

    static navigationOptions = { title: "Actualizar Documentos" };

    state = {
        documents: [],
        pendingDocuments: []
    };

    componentDidMount() {
        this.props.navigation.addListener('didFocus', this.onFocus);
    }

    onFocus = async () => {
        try {
            const r = await Api.getPendingDocuments(this.props.user.id, true);
            this.setState( {
                documents: r.documentos,
                pendingDocuments: r.documentos_pendientes
            });
        } catch (e) {
            Modal.showGenericError(e);
        }

    };

    isValidate = () => {
        for (let i=0; i<this.state.documents.lenght; i++) {
            const item = this.state.documents[i];
            if (!!item.file) {
                Modal.showAlert("Por favor, ingrese todos los documentos");
                return false;
            }
        }
        return true;
    };

    upload = item => new Promise( (resolve, reject) => {
        const params = {
            dg: item.dg || 0,
            id: item.id,
            date: item.date
        };
        Api.uploadDocuments(item.file, params, resolve, reject);
    });

    submit = async () => {

        if (!this.isValidate()) return;

        try {
            for (let i=0; i<this.state.documents.length; i++) {
                await this.upload( this.state.documents[i]);
            }
            Toast.show("Se han enviado a revisión los documentos");

            const data = {
                id: this.props.user.id,
                email: this.props.user.email,
                phone: this.props.user.phone
            };
            const response = await Api.updateProfile( data );
            if (response.result) await this.props.dispatch( { type: 'SET_USER', payload: { user: response.user } } );

            socket.emit('notification', { type: 9, driver_id: this.props.user.id });
            this.props.navigation.navigate('Home');
        } catch (e) {
            Modal.showGenericError(e);
        }

    };

    render() {
        return (
            <Background noBackground header>

                {
                    this.state.documents.length>0
                        ?
                        <View>
                            <Text style={ { ...styles.textHeader, ...styles.bold } }> Su cuenta requiere Atención </Text>
                            <Text style={ styles.textHeader }> Algunos documentos necesitan ser actualizados </Text>

                            <FlatList
                                data={ this.state.documents }
                                keyExtractor={ item => item.id.toString() }
                                renderItem={ ({item}) => <Item key={ item.id.toString() } item={ item } /> }
                                ListFooterComponent={ () =>

                                    <View style={ { width: '100%', justifyContent: 'center', alignItems: 'center', padding: 10 } }>
                                        <Button
                                            title='Enviar Documentos'
                                            buttonStyle={ styles.btn }
                                            onPress={ this.submit } />
                                    </View>

                                }/>

                        </View>
                        :
                        <Text style={ styles.text }>
                            {
                                !!this.state.pendingDocuments.length
                                    ? "Actualmente sus documentos se encuentran en revisión."
                                    : "Actualmente no posee documentos pendientes"
                            }
                        </Text>
                }

            </Background>
        );
    }

}

const styles = StyleSheet.create({
    textHeader: {
        width: "100%",
        textAlign: 'center',
        paddingVertical: 2,
        fontSize: 17,
        color: Color.primary
    },
    bold: {
        fontFamily: 'Poppins-SemiBold'
    },
    btn: {
        backgroundColor: Color.primary,
        width: '50%',
    },
    text: {
        padding: 10,
        textAlign: 'center',
        fontSize: 17,
        color: Color.primary
    }
});

const redux = state => ({
    user: state.auth.user
});

export default connect(redux)(PendingDocuments);