import React, { Component } from 'react';
import { Text, View, ImageBackground, StyleSheet, TouchableWithoutFeedback, Platform, ScrollView, Dimensions } from 'react-native';
import Button from '../../widgets/Button';
import Api from "../../utils/Api";
import G from "../../utils/G";
import Color from "../../assets/Color";
import { connect } from "react-redux";
import Input from "../../widgets/InputBogui"
import Banner from "../../widgets/BannerAuth";
import DeviceInfo from 'react-native-device-info';
import Modal from "../../utils/Modal";
import { socket } from '../../utils/Socket';
import { emailValidator, passwordValidator } from "../../utils/Validators";

class Login extends Component {

    state = {
        email: "",
        password: "",
        errEmail: "",
        errPsw: ""
    };

    componentDidMount() {
        if (this.props.user) {
            this.props.navigation.replace("Drawer");
        } else {
            emailValidator.notPass = errEmail => this.setState({ errEmail });
            passwordValidator.notPass = errPsw => this.setState({ errPsw });
        }
    }

    submit = async () => {

        if (
            !passwordValidator.validate(this.state.password) ||
            !emailValidator.validate(this.state.email)
        ) return;

        try {
            const data = {
                email: this.state.email,
                password: this.state.password
            };
            const r = await Api.login(data);
            if (r.success) {
                await this.props.dispatch({ type: 'SET_USER', payload: { user: r.user } });
                socket.connect();
                G.init();
                socket.emit('login', { id: r.user.id, uuid: DeviceInfo.getUniqueID() });
                setTimeout(() => this.props.navigation.replace("Drawer"), 500);
            } else {
                Modal.showAlert(r.message);
            }
        } catch (e) {
            cosole.log(">>: ", e);
            Modal.showGenericError(e);
        }
    };

    render() {
        return (
            <ScrollView>
                <ImageBackground
                    source={require('../../assets/img/background_login.jpg')}
                    style={{ width: '100%', height: Dimensions.get("window").height }} >

                    {
                        !this.props.user && (
                            <View style={styles.container} >

                                {/* BANNER */}
                                <Banner iconBanner={require('../../assets/img/bogui_banner.png')} />

                                <View style={{ flex: 2 }} />

                                {/* FORM CONTAINER */}
                                <View style={styles.formContainer}>

                                    {/* INPUT EMAIL */}
                                    <Input
                                        hint='Correo Electrónico'
                                        onChangeText={email => this.setState({ email: email.trim() })}
                                        text={this.state.email}
                                        onBlur={() => emailValidator.validate(this.state.email)}
                                        onFocus={() => this.setState({ errEmail: '' })}
                                        leftIcon={require('../../assets/img/email.png')}
                                        errorMessage={this.state.errEmail} />

                                    {/*  INPUT PASSWORD */}
                                    <Input
                                        hint='Contraseña'
                                        password
                                        onChangeText={password => this.setState({ password })}
                                        text={this.state.password}
                                        onBlur={() => passwordValidator.validate(this.state.password)}
                                        onFocus={() => this.setState({ errPsw: '' })}
                                        leftIcon={require('../../assets/img/pass.png')}
                                        errorMessage={this.state.errPsw} />

                                    {/* REMEMBER PASSWORD */}
                                    <TouchableWithoutFeedback
                                        onPress={() => this.props.navigation.navigate("RememberPassword")}>
                                        <Text style={styles.rememberPassword}>
                                            ¿Olvidó Su Contraseña?
                                    </Text>
                                    </TouchableWithoutFeedback>

                                    {/* SUBMIT */}
                                    <Button
                                        onPress={this.submit}
                                        title="Iniciar"
                                        titleStyle={{ fontSize: 20 }}
                                        containerStyle={{ backgroundColor: 'black', width: 160 }}
                                        buttonStyle={{ backgroundColor: 'black', borderRadius: 20 }} />

                                </View>

                                <View style={{ flex: 1 }} />

                                {/* REGISTER */}
                                <View style={styles.register}>

                                    <Text style={{ color: 'black', fontSize: 14, marginVertical: 14 }}>
                                        ¿No tienes cuenta?
                                </Text>

                                    <Button
                                        title="Regístrate"
                                        type="outline"
                                        titleStyle={{ fontSize: 20, color: 'black' }}
                                        containerStyle={{ width: 160 }}
                                        buttonStyle={{ borderColor: 'black', alignItems: 'center', borderWidth: 1, borderRadius: 7 }}
                                        onPress={() => this.props.navigation.navigate("Register")} />

                                </View>

                                <Text>
                                    Bogui®
                            </Text>

                            </View>
                        )
                    }

                </ImageBackground>
            </ScrollView>

        );
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
        paddingTop: Platform.OS == 'ios' ? 20 : null
    },
    banner: {
        width: "100%",
        height: 80,
        justifyContent: "center",
        alignItems: "center",
        padding: 15,
    },
    iconBanner: {
        width: "100%",
        height: "100%",
    },
    formContainer: {
        width: "100%",
        padding: 7,
        justifyContent: "center",
        alignItems: "center",
    },
    inputs: {
        borderBottomWidth: 0,
        backgroundColor: Color.white,
        height: 40,
        marginVertical: 5
    },
    inputsLeftIcon: {
        height: 40,
        alignItems: 'center',
        paddingRight: 20
    },
    inputIconSize: {
        width: 15,
        height: 15,
    },
    rememberPassword: {
        width: "100%",
        marginVertical: 15,
        color: Color.primary,
        fontSize: 14,
        textAlign: "center"
    },
    register: {
        width: "90%",
        height: 100,
        alignItems: "center",
        borderTopWidth: 0.5,
        borderColor: Color.white
    }
});

const redux = state => ({ user: state.auth.user });

export default connect(redux)(Login);
