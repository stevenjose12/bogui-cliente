import React, { Component } from "react";
import {StyleSheet, View, Text} from "react-native";
import Color from "../../assets/Color";
import Hbox from "../../widgets/Hbox";
import LabelForm from "../../widgets/LabelForm";
import {Avatar} from "react-native-elements";
import Ranting from "../../widgets/Ranting";
import env from "../../utils/env";
import G from "../../utils/G";
import Geo from "../../utils/Geo";

class FooterDriver extends Component {

    state = {
        address: "",
        vehicle: ''
    };

    componentDidMount(): void {
        this.getAddress( {
            latitude: parseFloat( this.props.driver.latitud ),
            longitude: parseFloat( this.props.driver.longitud )
        } );
        this.setVehicle();
    }

    setVehicle = async () => {
        const vehicle = await G.getVehicleNameByCategory( this.props.route.vehicle_type_id );
        this.setState( { vehicle } );
    };

    async getAddress(location) {
        try {
            const address = await Geo.getAddress(location);
            this.setState( { address: address } );
        } catch (e) {
            setTimeout( () => this.getAddress(location), 500 );
        }
    }

    render() {
        return (

            !!this.props.driver && (
                <View style={ styles.container }>

                    <Hbox >
                        <Text style={ { fontFamily: "Poppins-SemiBold", flex: 1, color: Color.primary } }> Conductor </Text>
                        <Text style={ { fontFamily: "Poppins-SemiBold", color: Color.primary } }> Calificación:  </Text>
                        <Text style={ { color: Color.primary } }> { this.props.driver.ranting } </Text>
                    </Hbox>

                    <Avatar
                        rounded
                        source={ !this.props.driver.person.photo ? require('../../assets/img/avatar.png') : { uri: env.BASE_PUBLIC+this.props.driver.person.photo } }
                        size={ 150 }
                        containerStyle={ styles.avatar } />

                    <Ranting  wrapContent style={ { marginVertical: 20}  } ranting={ parseFloat( this.props.driver.ranting ) } />

                    <LabelForm fontSize={14} label="Nombre" value={ this.props.driver.person.name+" "+this.props.driver.person.lastname }/>
                    <LabelForm fontSize={14} label="Teléfono" value={ G.hideLastFourDigits(this.props.driver.person.phone) }/>
                    <LabelForm fontSize={14} label="Vehículo" value={ this.state.vehicle }/>
                    <LabelForm fontSize={14} label="Patente" value={ this.props.driver.vehicle.plate }/>
                    <LabelForm fontSize={14} label="Ubicación" value={ this.state.address } />

                </View>
            )

        );
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.white,
    },
    textBold: {
        fontFamily: "Poppins-SemiBold"
    }
});

export default FooterDriver;