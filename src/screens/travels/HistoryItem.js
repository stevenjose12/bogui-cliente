import React, { Component } from "react";
import { View, Image, Text, TouchableHighlight } from "react-native";
import {StyleSheet} from "react-native";
import Color from "../../assets/Color";
import Hbox from "../../widgets/Hbox";
import G from "../../utils/G";
import moment from 'moment';
import Api from "../../utils/Api";

class HistoryItem extends Component {

    state = {
        imageUri: null,
        vehicle: ''
    };

    componentDidMount() {
        this.getImage();
        this.setVehicle();
    }

    getImage = async () => {

        //<editor-fold desc="Obteniendo el punto de origen">
        const origin = {
            latitude: this.props.item.latitud_origin,
            longitude: this.props.item.longitud_origin
        };
        //</editor-fold>

        //<editor-fold desc="Obteniendo los waypoints">
        const waypoints = [];
        let indexWaypoint = 0;
        for (let i=1; i<this.props.item.to_destinies.length-1; i++) {
            const item = this.props.item.to_destinies[i];
            waypoints[indexWaypoint] = {
                latitude: item.latitud,
                longitude: item.longitud
            };
            indexWaypoint++;
        }
        //</editor-fold>

        //<editor-fold desc="Obteniendo el punto de destino">
        const lastIndex = this.props.item.to_destinies.length-1;
        const destination = {
            latitude: this.props.item.to_destinies[lastIndex].latitud,
            longitude: this.props.item.to_destinies[lastIndex].longitud
        };
        //</editor-fold>

        // const destination = this.props.places[this.props.places.length-1].location;
        try {
            const directions = await Api.getDirections(origin, destination, waypoints);
            const url = G.getStaticMap(this.props.item, directions.routes);
            this.setState( state => state.imageUri = url );
        } catch (e) {
            console.log(">>: HistoryItem > getImage > directions > error: ", e);
        }

    };

    setVehicle = async () => {
        const vehicle = await G.getVehicleNameByCategory(this.props.item.vehicle_type_id);
        this.setState( { vehicle } );
    };

    render() {
        return (

            (this.props.item.driver && this.props.item.driver.person) && (
                <TouchableHighlight onPress={ () => this.props.onPress?.(this.props.item) }>

                    <Hbox style={ styles.container }>

                        {
                            this.state.imageUri && (
                                <Image
                                    source={ { uri: this.state.imageUri } }
                                    style={ { width: 100, height: '100%' } }/>
                            )
                        }

                        <View style={ styles.infoContainer }>

                            <Hbox>

                                <Text style={ { flex: 1, color: 'black' } } >
                                    { this.state.vehicle }
                                </Text>

                                <Text style={ { color: 'black' } }>
                                    { moment(this.props.item.created_at).format("DD-MM-YYYY HH:mm") }
                                </Text>

                            </Hbox>

                            <Text style={ { color: 'black' } } >
                                { this.props.item.driver.person.name+" "+this.props.item.driver.person.lastname }
                            </Text>

                            <Text style={ { fontFamily: "Poppins-SemiBold",  color: 'black' } }>
                                { "CLP "+ G.formatMoney(this.props.item.cost) }
                            </Text>

                        </View>

                    </Hbox>

                </TouchableHighlight>
            )

        );
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        borderBottomWidth: 1,
        borderBottomColor: Color.accent,
        backgroundColor: Color.white
    },
    infoContainer: {
        padding: 10,
        flex: 1
    }
});

export default HistoryItem;