import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    ActivityIndicator,
    Platform,
    // ScrollView
} from "react-native";
import Color from "../../assets/Color";
import FooterRoute from "./FooterRoute";
import FooterDriver from "./FooterDriver";
import Button from "../../widgets/Button";
import Modal from "../../utils/Modal";
import Api from "../../utils/Api";
import {socket} from "../../utils/Socket";
import Toast from "../../widgets/Toast";
import MapPlotRoute from "../../widgets/MapPlotRoute";
import G from "../../utils/G";
import { ScrollView } from 'react-native-gesture-handler';
// import { ScrollView } from "react-navigation";

const WIDTH = Dimensions.get('window').width;
const HEIGHT = WIDTH/Math.sqrt(2);

class CurrentTravel extends Component {

    places = [];

    state = {
        showMap: true
    };

    componentDidMount() {
        if (this.props.route && this.props.route.driver_id) this.update(this.props.route);
    }

    update = route => {
        this.places = G.getPlacesFromRoute(route).places;
        if (this.map) this.map.setPlaces(this.places);
        this.forceUpdate();
    };

    cancelRoute = () => {

        const onPositiveButton = async () => {

            try {
                await Api.cancelRouteByRouteId(this.props.route.id);
                Toast.show("Se ha cancelado el viaje");
                socket.emit('cancelada-cliente', { route: this.props.route.id });
                if (this.props.navigation) {
                    this.props.navigation.navigate("Home", { test: true });
                } else {
                    this.props.toHome ? this.props.toHome() : null;
                }
            } catch (e) {
                Modal.showGenericError(e);
            }

        };

        Modal.alert.onPositiveButton("Si", onPositiveButton );
        Modal.alert.onCancelButton("No");
        Modal.alert.show({ message: '¿Desea cancelar la solicitud?', time: 0});

    };

    render() {
        return (
            this.props.route && this.props.route.driver_id
                ?
                <ScrollView contentContainerStyle={ {width: "100%", alignItems: 'center', zIndex: 1000} }>

                    <View style={ { width: WIDTH, height: HEIGHT } }>
                        {
                            this.state.showMap
                                ?
                                <MapPlotRoute
                                    ref={ map => this.map = map }
                                    markers={ G.markersPoint }/>
                                :
                                <View style={ styles.activityIndicator }>
                                    <ActivityIndicator/>
                                </View>
                        }
                    </View>

                    <FooterRoute
                        route={ this.props.route }
                        toEdit={ () => this.props.toEdit ? this.props.toEdit() : null }/>

                    <FooterDriver
                        driver={ this.props.route.driver }
                        route={this.props.route} />

                    <Button
                        onPress={ this.cancelRoute }
                        disabled={ this.props.route.status_route===2 }
                        title="Cancelar Viaje"
                        buttonStyle={ styles.buttonStyle }
                        titleStyle={ styles.titleStyle }/>

                </ScrollView>
                :
                <View style={ { width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' } }>
                    <Text style={ { color: Color.primary } }> No tienes un viaje actual </Text>
                </View>

        );

    }

}

const styles = StyleSheet.create({
    buttonStyle:{
        borderColor: Color.primary,
        borderWidth: 2,
        borderRadius: 20,
        width: Platform.OS === 'ios' ? "70%" : "50%",
        backgroundColor: 'transparent',
        alignSelf: Platform.OS === 'ios' ? 'center' : null
    },
    titleStyle: {
        color: Color.primary
    },
    activityIndicator: {
        width: '100%',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Color.white
    }
});

export default CurrentTravel;