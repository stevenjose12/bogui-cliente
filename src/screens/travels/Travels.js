import React, { Component } from 'react';
import {ActivityIndicator, View} from 'react-native';
import TabsContainer from "../../widgets/TabsContainer";
import CurrentTravel from "./Current";
import TravelsHistory from "./History";
import Background from '../../widgets/Background'
import Api from "../../utils/Api";
import Modal from "../../utils/Modal";
import {connect} from "react-redux";

const tabs = {
    CURRENT_TRAVEL: 0,
    TRAVELS_HISTORY: 1
};

class Travels extends Component {
    
    static navigationOptions = { title: "Viajes" };

    state = {
        tab: tabs.CURRENT_TRAVEL,
        travels: null
    };

    componentDidMount() {
        this.props.navigation.addListener('didFocus', this.didFocus);
    }

    didFocus = async () => {

        try {
            const travels = await Api.getRoutesByUser(this.props.user.id);
            if (travels.result) {
                this.setState({ travels });
            } else {
                throw "result false";
            }
        } catch (e) {
            Modal.showGenericError(e);
        }

    };

    render() {
        return (
            <Background header noBackground>

                {
                    this.state.travels==null
                        ?
                        <View style={ { width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' } }>
                            <ActivityIndicator />
                        </View>
                        :
                            <>
                                <TabsContainer
                                    tabs={ ['Mi Viaje Actual', 'Mi Historial de Viajes'] }
                                    onChangeTab={ tab => this.setState( state => state.tab = tab ) }
                                    tabSelected={ this.state.tab } />
                                <>
                                {
                                    this.state.tab === tabs.CURRENT_TRAVEL
                                        ?
                                        <CurrentTravel
                                            route={ this.state.travels.ruta }
                                            toHome={ () => this.props.navigation.navigate("Home", { test: true }) }
                                            toEdit={ () => this.props.navigation.navigate("EditRouteDrawer") } />
                                        :
                                        <TravelsHistory
                                            history={ this.state.travels.historial }
                                            toHistoryDetails={ item => this.props.navigation.navigate("HistoryDetails", { item }) }/>
                                }
                                </>
                            </>

                }

            </Background>
        )
    }

}

const redux = state => ({
    user: state.auth.user,
});

export default connect(redux)(Travels);