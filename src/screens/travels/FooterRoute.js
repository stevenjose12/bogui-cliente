import React, { Component } from "react";
import {StyleSheet, View, Text, Image, TouchableWithoutFeedback} from "react-native";
import Hbox from "../../widgets/Hbox";
import moment from 'moment';
import Color from "../../assets/Color";
import {connect} from "react-redux";
import G from "../../utils/G";

class FooterRoute extends Component {

    places = [];

    componentDidMount() {

        this.places[0] = {
            location: {
                latitude: parseFloat(this.props.route.latitud_origin),
                longitude: parseFloat(this.props.route.longitud_origin)
            },
            address: this.props.route.description
        };
        if (this.props.route.to_destinies.length==1) {
            this.places[2] = {
                location: {
                    latitude: parseFloat(this.props.route.to_destinies[0].latitud),
                    longitude: parseFloat(this.props.route.to_destinies[0].longitud)
                },
                address: this.props.route.to_destinies[0].description
            };
        } else {
            for (let i=0; i<this.props.route.to_destinies.length; i++) {
                this.places[i+1] = {
                    location: {
                        latitude: parseFloat(this.props.route.to_destinies[i].latitud),
                        longitude: parseFloat(this.props.route.to_destinies[i].longitud)
                    },
                    address: this.props.route.to_destinies[i].description
                }
            }
        }
        this.forceUpdate();

    }

    render() {
        return (
            <View style={ styles.container }>

                <Hbox>

                    <Text style={ { ...styles.col } }>
                        { moment(this.props.route.updated_at).format("DD-MM-YYYY HH:mm") }
                    </Text>

                    <Text style={ { ...styles.col, textAlign: 'center' } }>
                        { "ID: "+this.props.route.id }
                    </Text>

                    <Text style={ { ...styles.col, ...styles.textBold, textAlign: 'right' } }>
                        { "CLP: "+this.props.route.cost }
                    </Text>

                </Hbox>

                <Hbox>

                    <Text style={ { ...styles.col } }>
                        { this.props.user.name+" "+this.props.user.lastname }
                    </Text>

                    <Text style={ { ...styles.col, ...styles.textBold, textAlign: 'right' } }>
                        { ( this.props.route.payment===1 ? "Tarjeta" : "Efectivo" ) }
                    </Text>

                </Hbox>

                <Hbox>

                    <View style={ { flex: 1 } }>

                        {
                            this.places.map( (item, i) => {

                                if (item) return (
                                    <Hbox style={ { marginRight: 5, paddingVertical: 2 } } key={ i.toString() }>

                                        <View style={ { paddingRight: 10, justifyContent: 'center' } }>
                                            <Image source={ G.markersPoint[i] }/>
                                        </View>

                                        <Text style={ { flex: 1 } }>
                                            { item.address }
                                        </Text>

                                    </Hbox>
                                    )
                            } )
                        }

                    </View>

                    <View style={ { alignItems: 'flex-end', justifyContent: 'flex-end', paddingLeft: 5, paddingBottom: 15 } }>

                        <TouchableWithoutFeedback onPress={ () => this.props.toEdit ? this.props.toEdit() : null }>

                            <Image
                                source={ require('../../assets/icons/edit_route.png') }
                                tintColor={ Color.accent }
                                resizeMode="contain"
                                style={ { width: 20, height: 20 } } />

                        </TouchableWithoutFeedback>
                    </View>

                </Hbox>

                {
                    !!this.props.route.driver==null && (
                        <Text style={ { fontFamily: "Poppins-Italic", color: Color.primary } }> Esperando confirmacíon </Text>
                    )
                }

            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: Color.accent,
        backgroundColor: Color.white,
    },
    col: {
        flex: 1,
        color: Color.primary
    },
    textBold: {
        fontFamily: "Poppins-SemiBold"
    }
});

const redux = state => {
    return {
        user: state.auth.user,
    }
};

export default connect(redux, null, null, { forwardRef: true })(FooterRoute);