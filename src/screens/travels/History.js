import React from 'react';
import { FlatList } from "react-native";
import HistoryItem from "./HistoryItem";

const TravelsHistory = props => (
    <FlatList
        keyExtractor={ item => item.id.toString() }
        data={ props.history }
        renderItem={ ({item}) =>
            <HistoryItem
                item={ item }
                onPress={ item => props.toHistoryDetails?.(item) } />
        } />

);

export default TravelsHistory;