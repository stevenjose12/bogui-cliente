import React from "react";
import {StyleSheet, View, Text, Platform} from "react-native";
import Hbox from "../../widgets/Hbox";
import Button from "../../widgets/Button";
import Color from "../../assets/Color";
import G from "../../utils/G";

const Footer = props => {

    return (
        <View style={styles.container}>

            <Hbox>

                <Text style={ { ...styles.text, ...styles.col } }>
                    { "CLP "+G.formatMoney( props.cost ) }
                </Text>

                <Text style={ { ...styles.text, ...styles.col } }>
                    { props.payment==1 ? "Tarjeta" : "Efectivo" }
                </Text>

            </Hbox>

            <Button
                onPress={ () => props.submit ? props.submit() : null }
                buttonStyle={{backgroundColor: Color.white, borderRadius: 20, marginVertical: 5, width: Platform.OS == 'ios' ? '70%' : '50%',
                    alignSelf: Platform.OS == 'ios' ? 'center' : null}}
                titleStyle={{color: Color.primary}}
                title="Cambiar Destino" />

        </View>
    )

};

const styles = StyleSheet.create({
    container:{
        padding: 5,
        width: '100%',
        backgroundColor: Color.primary,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: Color.white,
        fontFamily: "Poppins-SemiBold",
        marginVertical: 5,
        fontSize: 16
    },
    col: {
        flex: 1,
        textAlign: 'center'
    }
});

export default Footer;