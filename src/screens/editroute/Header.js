import React, { Component } from "react";
import {StyleSheet, View, Image, Text, TextInput, FlatList, TouchableWithoutFeedback} from "react-native";
import Color from "../../assets/Color";
import Hbox from "../../widgets/Hbox";
import {Button} from "react-native-elements";
import Icon from 'react-native-vector-icons/Ionicons';
import API from "../../utils/Api";

class Header extends Component {

    state = {
        address: []
    };

    searchGoogle = async text => {
        this.props.onWrite ? this.props.onWrite(text) : null;
        await this.setState( state => state.address = [] );
        try {
            const search = await API.autoComplete(text);
            search.forEach( item =>
                this.setState(state => state.address.push({ description: item.description, id: item.place_id }) )
            );
        } catch (e) {
            console.log('>>: Error catch', e);
        }
    };

    onAddressSelected = async id => {
        this.clear();
        try {
            const response = await API.placeID(id);
            const data = {
                latitude: response.results[0].geometry.location.lat,
                longitude: response.results[0].geometry.location.lng
            };
            this.props.onAddressSelected ? this.props.onAddressSelected(data) : null;
        } catch (e) {
            console.log('>>: HeaderEdit > getCoord > error: ', e);
        }
    };

    clear = () => {
        this.setState( state => state.address = [] );
    };

    render() {

        return (
            <View style={styles.container}>

                <Hbox style={styles.item}>

                    <View style={styles.marker}>
                        <Image
                            style={{height: 40}}
                            source={require("../../assets/icons/goal_marker.png")}
                            resizeMode="contain"/>
                    </View>

                    <View style={styles.inputs}>

                        <Text style={styles.title}>¿Dónde es tu destino?</Text>

                        <TextInput
                            style={{height: 25, padding: 0, fontFamily: "Poppins"}}
                            value={ this.props.address }
                            onChangeText={ this.searchGoogle } />

                    </View>

                    <Button
                        type="clear"
                        icon={<Icon name="ios-backspace" size={30} color={Color.accent}/>}
                        onPress={ () => this.props.onClearText ? this.props.onClearText() : null }/>

                </Hbox>

                {
                    this.state.address.length>0
                        ?
                        this.state.address.map( (item, i) => {
                            return(
                                <TouchableWithoutFeedback
                                    key={ i }
                                    onPress={ () => this.onAddressSelected(item.id) }>
                                    <Text style={ { backgroundColor: Color.white, padding: 7, borderColor: 'black', borderWidth: 0.4 } } > { item.description } </Text>
                                </TouchableWithoutFeedback>
                            )
                        })
                        : null
                }

            </View>
        )

    };

}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 5,
        paddingTop: 5,
    },
    item: {
        width: '100%',
        height: 50,
        backgroundColor: Color.white,
        borderColor: 'black',
        borderWidth: 0.4
    },
    marker: {
        height: 50,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontFamily: "Poppins-SemiBold",
        color: Color.primary,
        flex: 1,
        textAlignVertical: 'center'
    },
    inputs: {
        flex: 1
    }
});

export default Header;