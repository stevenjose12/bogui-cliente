import React, { Component } from "react";
import { View, StyleSheet, Text, ActivityIndicator } from "react-native";
import Background from "../../widgets/Background";
import Color from "../../assets/Color";
import Footer from "./Footer";
import MapEdit from "../../widgets/MapEdit";
import Geolocation from "../../utils/Geolocation";
import {connect} from "react-redux";
import Api from "../../utils/Api";
import G from "../../utils/G";
import Geo from "../../utils/Geo";
import Modal from "../../utils/Modal";
import Toast from "../../widgets/Toast";
import { socket} from "../../utils/Socket";
import Header from "./Header";
import CustomHeader from "../../widgets/CustomHeader";

class EditRoute extends Component {

    blockSubmit = false;

    static navigationOptions = { header: null };

    state = {
        cost: 0,
        address: "",
        center: null
    };

    componentDidMount() {
        this.onFocusListener = this.props.navigation.addListener('didFocus', this.onFocus);
    }

    componentWillUnmount() {
        this.onFocusListener.remove();
    }

    onFocus = () => {
        this.blockSubmit = false;
        this.setState( {
            cost: this.props.route.cost,
            center: Geolocation.getLatLng( Geolocation.getCurrentLocation() )
        });
    };

    submit = async () => {

        if (!this.blockSubmit) {
            this.blockSubmit = true;
        } else {
            return;
        }

        const params = {
            route_id: this.props.route.id,
            lat_destiny: this.destination.latitude,
            lng_destiny: this.destination.longitude,
            cost: this.state.cost,
            time: this.time,
            distance: this.km,
            descripcion: this.state.address
        };

        try {
            await Api.editRoute(params);
            const route = await Api.routeAccepted(this.props.route.id);
            route.driverId = route.driver_id;
            this.setRoute(route);
            Toast.show("Tu destino ha sido actualizado");
            socket.emit('current-route-change', { id: this.props.route.id });
            this.props.navigation.navigate("Home", { test: true });
        } catch (e) {
            Modal.showGenericError(e);
        }

    };

    onMapDragEnd = async data => {

        this.header.clear();

        this.destination = {
            latitude: data.latitude,
            longitude: data.longitude
        };

        this.getAddress(this.destination);

        //<editor-fold desc="Obteniendo el punto de origen">
        const origin = {
            latitude: this.props.route.latitud_origin,
            longitude: this.props.route.longitud_origin
        };
        //</editor-fold>

        //<editor-fold desc="Obteniendo los waypoints">
        const waypoints = [];
        for (let i=1; i<this.props.route.to_destinies.length-1; i++) {
            const item = this.props.route.to_destinies[i];
            waypoints.push({ latitude: item.latitud, longitude: item.longitud } );
        }
        //</editor-fold>

        // const destination = this.props.places[this.props.places.length-1].location;
        try {

            //<editor-fold desc="obteniendo las tarifas">
            const tariffsData = { vehicle: this.props.route.vehicle_type_id, latitude: origin.latitude, longitude: origin.longitude };
            const tariffs = await Api.getTariffs( tariffsData );
            this.tariffs = tariffs.tarifas;
            //</editor-fold>

            //<editor-fold desc="obteniendo si la ruta pasa por un aereopuerto">
            const params = {};
            params.partida = {
                lat: origin.latitude,
                lng: origin.longitude,
            };
            params.llegada = {
                lat: this.destination.latitude,
                lng: this.destination.longitude,
            };
            if (waypoints.length>0) {
                params.parada = {
                    lat: waypoints[0].latitude,
                    lng: waypoints[0].longitude,
                }
            }
            const airport = await Api.isAirport(params);
            //</editor-fold>

            const directions = await Api.getDirections(origin, this.destination, waypoints);
            this.km = directions.distance/1000;
            this.time = directions.duration;

            //<editor-fold desc="Calcular tarifa">
            if ( (this.km>this.tariffs.value_km_less) && (this.km<this.tariffs.value_km_greater) ) {
                this.tariff = G.calcTariff(this.tariffs.base_tariff, this.km, this.tariffs.medium_tariff, airport.cost);
            } else if (this.km > this.tariffs.value_km_greater) {
                this.tariff = G.calcTariff(this.tariffs.base_tariff, this.km, this.tariffs.maximum_tariff, airport.cost);
            } else {
                this.tariff = G.calcTariff(0, 1, this.tariffs.minimum_tariff, airport.cost);
            }
            this.setState( { cost: this.tariff });
            //</editor-fold>

        } catch (e) {
            console.log(">>: EditRoute > onMapDragEnd > directions > error: ", e);
        }

    };

    getAddress = async location => {
        try {
            const address = await Geo.getAddress(location);
            this.setState( { address: address } );
        } catch (e) {
            setTimeout( () => this.getAddress(location), 500 );
        }
    };

    setRoute = (route=null) => {
        route = route || null;
        this.props.dispatch({type: 'SET_ROUTE', payload: { route } });
    };

    render() {
        return (

            <Background noBackground>

                {
                    this.props.route
                        ?
                        <View style={ { width: '100%', height: '100%'} }>

                            <CustomHeader
                                title="Editar destino"
                                onPress={ () => this.props.navigation.navigate("HomeDrawer") }
                                backType />

                            <Text style={ styles.banner }> Cambia el destino </Text>

                            <MapEdit
                                center={ this.state.center }
                                marker={ G.isNight(this.props.time) ? require("../../assets/icons/marker_center_goal_night.png") : require("../../assets/icons/marker_center_goal.png")  }
                                onDragEnd={ this.onMapDragEnd }
                                onPress={ () => this.header.clear() }
                                header={
                                    <Header
                                        ref={ header => this.header = header }
                                        address={ this.state.address }
                                        onClearText={ () => this.setState( { address: "" } ) }
                                        onWrite={ text => this.setState( { address: text } ) }
                                        onAddressSelected={ location => this.setState( { center: location } ) } />
                                }/>

                            <Footer
                                cost={ this.state.cost }
                                payment={ this.props.route.payment }
                                submit={ this.submit } />

                        </View>
                        :
                        <View style={ { width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'} }>
                            <ActivityIndicator/>
                        </View>
                }

            </Background>
        );
    }

}

const styles = StyleSheet.create({
    banner: {
        width: "100%",
        paddingVertical: 5,
        textAlign: 'center',
        backgroundColor: Color.gray,
        color: Color.white,
        fontFamily: "Poppins-Italic"
    }
});

const redux = state => ({
    user: state.auth.user,
    route: state.route.route,
    time: state.night.night
});

export default connect( redux )( EditRoute );