import React, { Component } from "react";
import { View, FlatList } from "react-native";
import { connect } from "react-redux";
import G from "../../utils/G";
import Api from "../../utils/Api";
import Background from "../../widgets/Background";
import ElementList from "./ElementList";
import Modal from "../../utils/Modal";
import SocketEvents from "../../utils/SocketEvents";
import Color from "../../assets/Color";
import Button from "../../widgets/Button";
import { emit } from "../../utils/EventEmit";

class Chat extends Component {
  static navigationOptions = { title: "Chat" };

  state = {
    chats: [],
    currentPage: 1,
    lastPage: null
  };

  componentDidMount() {
    this.focusListerer = this.props.navigation.addListener(
      "didFocus",
      this.askChats
    );

    this.blurListener = this.props.navigation.addListener(
      "didBlur",
      this.onBlur
    );

    SocketEvents.onSendMessage(data => {
      G.showChatNotification(this.props.user.id, data);
      if (
        data.chat.users.map(i => i.user_id).indexOf(this.props.user.id) !==
          -1 &&
        data.user_id != this.props.user.id
      ) {
        const _data = this.state.chats;
        if (_data.map(i => i.id).indexOf(data.chat_id) !== -1) {
          const item = _data.splice(
            _data.map(i => i.id).indexOf(data.chat_id),
            1
          );
          item[0].message.push(data);
          _data.unshift(item[0]);
          this.setState({ chats: _data });
          this.forceUpdate();
        } else {
          this.newChat(data);
        }
      }
    });
  }

  componentWillUnmount() {
    this.onBlur();
    this.focusListerer?.remove();
    this.blurListener?.remove();
  }

  onBlur = () => emit("INTO_CHAT", false);

  newChat = async (data): void => {
    try {
      const r = await Api.getOneChat({
        id: this.props.user.id,
        chat_id: data.chat_id
      });
      if (!r.data.result) return;
      r.data.chat.receiver = r.data.chat.chat_user[0].user;
      if (r.data.chat.receiver.person != null) {
        r.data.chat.receiver.photo =
          ENV.BASE_URL + r.data.chat.receiver.person.photo;
      } else {
        r.data.chat.receiver.photo = null;
      }
      const data = this.state.data;
      data.unshift(r.data.chat);
      this.setState({ chats: data });
    } catch (e) {
      Modal.loading.dismiss();
    }
  };

  askChats = async () => {
    emit("INTO_CHAT", true);
    G.intoChatPage = true;
    try {
      const response = await Api.getChats({ id: this.props.user.id });
      if (!response.result) return;
      this.setState({
        currentPage: response.chats.current_page,
        chats: response.chats.data,
        lastPage: response.chats.last_page
      });
    } catch (e) {
      Modal.loading.dismiss();
    }
  };

  loadMoreItems = async () => {
    try {
      const newPage = ++this.state.currentPage;
      const r = await Api.getChats(this.props.user.id, newPage);
      if (!r.result) return;
      const chats = [...r.chats.data, ...this.state.chats];
      this.setState({ chats: chats, currentPage: newPage });
    } catch (e) {
      Modal.showGenericError(e);
    }
  };

  viewMessages = chat_id => {
    try {
      this.props.navigation.navigate("Messages", chat_id);
    } catch (e) {
      console.log(">>: error > navigation > chat", e);
    }
  };

  render() {
    return (
      <Background header>
        <View
          style={{
            width: "100%",
            height: "100%",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <FlatList
            data={this.state.chats}
            keyExtractor={item => item.id.toString()}
            ListFooterComponent={() =>
              this.state.currentPage < this.state.lastPage && (
                <LoadMoreFooter loadMoreItems={this.loadMoreItems} />
              )
            }
            renderItem={({ item }) => (
              <ElementList
                key={item.id}
                data={item}
                logUser={this.props.user?.id}
                viewMessages={chat_id => this.viewMessages(chat_id)}
              />
            )}
          />
        </View>
      </Background>
    );
  }
}

const LoadMoreFooter = ({ loadMoreItems }) => (
  <View style={{ justifyContent: "center", alignItems: "center", padding: 7 }}>
    <Button
      buttonStyle={{
        width: "50%",
        borderColor: Color.primary,
        borderWidth: 2,
        backgroundColor: "transparent",
        borderRadius: 20
      }}
      titleStyle={{ color: Color.primary }}
      title="Ver mas"
      onPress={loadMoreItems}
    />
  </View>
);

const redux = state => ({ user: state.auth.user });
export default connect(redux)(Chat);
