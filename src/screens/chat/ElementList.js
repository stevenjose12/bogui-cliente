import React, { Component } from 'react';
import {Modal, StyleSheet, Text, View, Image, TouchableWithoutFeedback} from "react-native";
import ENV from '../../utils/env';
import { Avatar } from 'react-native-elements';
import Colors from '../../assets/Color';
import Hbox from '../../widgets/Hbox';
import moment from 'moment';

class ElementListChats extends Component{

    noReadMessageCount = 0;

    noReadMessages(messages){
        // if(messages.length >= 1){
            // this.messages.result = false
            let messagesQuantity = 0;
            for (let index = 0; index < messages.length; index++) {
                const element = messages[index];
                if(element.seen == '0' && element.user_id !== this.props.logUser){
                    // this.messages.result = true
                    ++messagesQuantity;
                }
            }
            this.noReadMessageCount = messagesQuantity;
            return messagesQuantity
        // }else return 0
    }

    render() {
        const name = this.props.data.support == '1' ?
            "Soporte" :
            this.props.data.chat_user[0].user.person.name + ' ' +
            this.props.data.chat_user[0].user.person.lastname;

        let content = ""
        if (this.props.data.message.length >= 1) {
            const index =this.props.data.message.length - 1;
            const text = this.props.data.message[index].text;
            const file = this.props.data.message[index].file;
            if (!!text) {
                content = text;
            } else if (!!file) {
                content = "Te ha enviado una imagen"
            }
        } else {
            content = "Comienza a escribirle a " + name
        }

        return(
            <React.Fragment>
                {
                    this.props.data.chat_user[0].user.person && this.props.data.chat_user[0].user.person.photo
                        ?
                        <TouchableWithoutFeedback
                            onPress={() => this.props.viewMessages({chat_id: this.props.data.id, user_id: this.props.logUser})}
                            >
                            <View style={styles.container}>
                                <View style={[styles.element, this.noReadMessages(this.props.data.message) >= 1 ? {borderColor: Colors.blue, borderWidth: 1} : '']}>
                                    <View style={styles.elementContainer}>
                                        <View style={styles.avatar}>
                                            <Avatar
                                                source={{uri: ENV.BASE_PUBLIC+this.props.data.chat_user[0].user.person.photo ? ENV.BASE_PUBLIC+this.props.data.chat_user[0].user.person.photo : ENV.BASE_PUBLIC+'default.png'}}
                                                size={45}
                                                rounded
                                            />
                                        </View>
                                        <View style={styles.data}>
                                            <Hbox>
                                                <Text style={styles.name}>
                                                    {name}
                                                </Text>
                                                {
                                                    this.noReadMessages(this.props.data.message) >= 1
                                                    ?
                                                        <View style={styles.viewNumberCircle}>
                                                            <Text style={styles.numberCircle}> {this.noReadMessageCount} </Text>
                                                        </View>
                                                    :
                                                    null
                                                }
                                            </Hbox>
                                            <Text style={styles.message} numberOfLines={1}>
                                                {content}
                                            </Text>
                                            <Text style={styles.date} numberOfLines={1}>
                                                {moment(this.props.data.created_at, 'YYYY-MM-DD HH:mm:ii').format('DD-MM-YYYY HH:mm')}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                        :
                    null
                }
            </React.Fragment>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    numberCircle:{
        color: Colors.white,
        textAlign: 'center'
    },
    viewNumberCircle:{
        backgroundColor: Colors.blue,
        width: 23,
        borderRadius: 23,
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatar:{
        width: '10%'
    },
    date:{
        fontSize: 10,
        textAlign: 'right',
        color: Colors.secondaryGray
    },
    message:{
        fontSize: 11
    },
    name:{
        flex: 1,
        fontFamily: 'Poppins-Italic',
        textTransform: 'capitalize',
        color: Colors.primary
    },
    data:{
        width: '90%',
        paddingLeft: '12%',
        flexDirection: 'column'
    },
    element:{
        width: '100%',
        backgroundColor: 'white',
        padding: 5
    },
    elementContainer:{
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
export default ElementListChats;