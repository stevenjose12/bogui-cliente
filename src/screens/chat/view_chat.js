import moment from "moment";
import React from "react";
import {
    Dimensions,
    FlatList,
    Image,
    Keyboard,
    StyleSheet,
    Text,
    TextInput,
    View
} from "react-native";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/Ionicons";
import { connect } from "react-redux";
import Colors from "../../assets/Color";
import triangleLeft from "../../assets/icons/triangle-left.png";
import triangleRight from "../../assets/icons/triangle-right.png";
import user from "../../assets/icons/user.png";
import FirebasePushNotification from "../../plugins/FirebasePushNotification";
import API from "../../utils/Api";
import Environment from "../../utils/env";
import Modal from "../../utils/Modal";
import { socket } from "../../utils/Socket";
import SocketEvents from "../../utils/SocketEvents";
import Background from "../../widgets/Background";
import { emit } from "../../utils/EventEmit";

class ViewChat extends React.Component {

    static navigationOptions = { title: "Chat" };

    state = {
        messageText: "",
        data: {},
        img_default: "_img_default",
        visible: false,
        photos: [],
        rotationVisible: false,
        image: null,
        keyboardSpace: 0
    };

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this.keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this.keyboardDidHide);
        on(["carrera-iniciada", "pasajero-abordo"], this.load);

        this.focusListener = this.props.navigation.addListener("didFocus", this.load);
        this.blur = this.props.navigation.addListener("didBlur", this.onBlur);
        this.props.navigation.addListener("willBlur", this.onBlur);

        this.load();
    }

    onFocus = () => {
        FirebasePushNotification.setIntoChats(true);
        emit('INTO_CHAT', true);
    };

    onBlur = () => {
        FirebasePushNotification.setIntoChats(false);
        emit('INTO_CHAT', false);
    };

    componentWillUnmount() {
        this.onBlur();
        this.focusListener?.remove();
        this.blur?.remove();
    }

    keyboardDidShow = event => this.setState({ keyboardSpace: event.endCoordinates.height });

    keyboardDidHide = () => this.setState({ keyboardSpace: 0 });

    load = async () => {

        this.onFocus();

        try {
            const res = await API.chatView({
                id: this.props.user.id,
                chat_id: this.props.navigation.getParam("chat_id")
            });
            if (res.result) {
                this.route = res.chat.route;
                res.chat.message.forEach(item => {
                    item.position = "right";
                    if (item.user_id != this.props.user.id) item.position = "left";
                });
                if (res.chat.chat_user.length > 0) {
                    res.chat.receiver = res.chat.chat_user[0].user;
                    if (res.chat.receiver.person != null)
                        res.chat.receiver.photo =
                            Environment.BASE_PUBLIC + res.chat.receiver.person.photo;
                    else res.chat.receiver.photo = null;
                }
                this.setState(state => (state.data = res.chat));

                SocketEvents.onSendMessage(data => {
                    if (data.chat_id == this.state.data.id) {
                        data.position =
                            data.user_id == this.props.user.id ? "right" : "left";
                        const _data = this.state.data;
                        _data.message.push(data);
                        this.setState({ data: _data });
                        try {
                            API.chatSeen({ id: data.id });
                        } catch (e) {
                            Modal.showGenericError(e);
                        }
                    }
                });
            } else {
                Modal.showGenericError();
            }
        } catch (e) {
            this.props.navigation.navigate("Chat");
            Modal.showGenericError(e);
        }
    };

    send = () => {
        if (this.state.messageText === "") return;
        socket.emit("send-message", {
            chat_id: this.state.data.id,
            user_id: this.props.user.id,
            text: this.state.messageText,
            receiver_id: this.state.data.receiver.id
        });
        this.setState({ messageText: "" });
    };

    deleteChat = async () => {
        Modal.alert.onPositiveButton("Si", async () => {
            try {
                const params = {
                    chat_id: this.state.data.id,
                    user_id: this.props.navigation.getParam("user_id") || this.props.user.id
                };
                const res = await API.chatDelete(params);
                if (!res.result) throw "Result false";
                this.props.navigation.goBack(null);
                Modal.alert.show({
                    message: "El chat ha sido eliminado exitosamente",
                    time: 3000
                });
            } catch (e) {
                Modal.showGenericError(e.response.data);
            }
        });
        Modal.alert.onCancelButton("No");
        Modal.alert.show({ message: "¿Desea eliminar el Chat?" });
    };

    render() {
        const isSupport = !!this.state.data && this.state.data.support == "1";

        const currentRoute = this.props.route;

        const isCurrentRouteDriver =
            !!currentRoute &&
            !!this.state.data &&
            this.state.data.route_id === currentRoute.id;

        const isCurrentRouteElegible =
            !!currentRoute &&
            currentRoute.status === 1 &&
            currentRoute.status_route === 0;

        const isInputEditable =
            isSupport || (isCurrentRouteDriver && isCurrentRouteElegible);

        const isButtonEnabled =
            isInputEditable &&
            !!this.state.messageText &&
            this.state.messageText.length > 0;

        return (
            <React.Fragment>
                <Background header>
                    <View
                        style={{
                            width: "100%",
                            height: "100%",
                            backgroundColor: Colors.white
                        }}>
                        <View style={styles.header}>
                            <React.Fragment>
                                {this.state.data.support == "0" && (
                                    <Image
                                        style={styles.image}
                                        onError={() => {
                                            let data = this.state.data;
                                            data.receiver.photo = null;
                                            this.setState({
                                                data: data
                                            });
                                        }}
                                        source={
                                            this.state.data.support == "0"
                                                ? this.state.data.receiver.photo == null
                                                    ? user
                                                    : { uri: this.state.data.receiver.photo }
                                                : user
                                        }
                                    />
                                )}
                                <View
                                    style={
                                        this.state.data.support == "0"
                                            ? styles.containerDescription
                                            : styles.containerDescriptionMotivo
                                    }
                                >
                                    <Text style={styles.text}>
                                        {this.state.data.support == "0"
                                            ? this.state.data.receiver.person.name +
                                            " " +
                                            this.state.data.receiver.person.lastname
                                            : "Soporte"}
                                    </Text>
                                    {this.state.data.support == "1" && (
                                        <Text style={styles.description}>
                                            {this.state.data.support_type.description}
                                        </Text>
                                    )}
                                </View>
                            </React.Fragment>
                            <View style={styles.trash}>
                                <Button
                                    onPress={this.deleteChat}
                                    type="clear"
                                    icon={
                                        <Image
                                            source={require("../../assets/icons/trash.png")}
                                            style={{ width: 25, height: 25 }}
                                            tintColor="#fff"
                                            resizeMode="contain"
                                        />
                                    }
                                />
                            </View>
                        </View>
                        <View style={{ flex: 1 }}>

                            {this.state.data &&
                                this.state.data.message &&
                                this.state.data.message.length > 0 ? (
                                    <FlatList
                                        contentContainerStyle={styles.scroll}
                                        ref={ref => (this.scrollView = ref)}
                                        style={styles.flat}
                                        data={this.state.data.message}
                                        keyExtractor={item => item.id.toString()}
                                        onContentSizeChange={() => this.scrollView.scrollToEnd()}
                                        renderItem={({ item }) => {
                                            return (
                                                <View style={[styles[item.position], styles.itemChat]}>
                                                    {!!item.file && (
                                                        <Image
                                                            source={{
                                                                uri: Environment.BASE_PUBLIC + item.file
                                                            }}
                                                            style={{
                                                                marginVertical: 5,
                                                                width: 100,
                                                                height: 100,
                                                                resizeMode: "cover",
                                                                borderRadius: 5
                                                            }}
                                                        />
                                                    )}
                                                    {!!item.text && (
                                                        <Text
                                                            style={[
                                                                styles.message,
                                                                styles["message" + item.position]
                                                            ]}
                                                        >
                                                            {item.text}
                                                        </Text>
                                                    )}
                                                    <Text
                                                        style={[
                                                            styles.date,
                                                            styles["date" + item.position]
                                                        ]}
                                                    >
                                                        {moment(item.created_at).format("DD/MM/YYYY HH:mm")}
                                                    </Text>
                                                    <Image
                                                        source={
                                                            item.position == "right"
                                                                ? triangleRight
                                                                : triangleLeft
                                                        }
                                                        style={
                                                            item.position == "right"
                                                                ? styles.triangleRight
                                                                : styles.triangleLeft
                                                        }
                                                    />
                                                </View>
                                            );
                                        }}
                                    />
                                ) : null}

                        </View>
                        <View style={styles.footer}>
                            <View>
                                <TextInput
                                    multiline={true}
                                    editable={isInputEditable}
                                    onChangeText={text => this.setState({ messageText: text })}
                                    value={this.state.messageText}
                                    style={styles.input}
                                />
                            </View>
                            <View>
                                <Button
                                    disabled={!isButtonEnabled}
                                    onPress={this.send}
                                    buttonStyle={styles.button}
                                    type="clear"
                                    icon={<Icon size={25} color="#fff" name="md-send" />}
                                />
                            </View>
                        </View>
                    </View>
                </Background>
            </React.Fragment>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        paddingTop: 45,
        minHeight: "100%"
    },
    footer: {
        backgroundColor: "#000",
        width: "100%",
        flexDirection: "row",
        padding: 10,
        bottom: 0
    },
    input: {
        backgroundColor: "#fff",
        minHeight: 30,
        maxHeight: 150,
        borderRadius: 30,
        minWidth: "90%",
        maxWidth: "90%",
        paddingHorizontal: 20,
        paddingVertical: 5
    },
    button: {
        paddingVertical: 0,
        paddingTop: 6,
        paddingHorizontal: 0
    },
    header: {
        backgroundColor: Colors.blue,
        flexDirection: "row",
        padding: 10,
        paddingVertical: 12,
        height: 55
    },
    trash: {
        position: "absolute",
        right: 8,
        top: 8
    },
    text: {
        color: "#fff",
        fontSize: 13
    },
    image: {
        resizeMode: "cover",
        width: 32,
        height: 32,
        borderRadius: 16,
        marginRight: 10
    },
    scroll: {
        padding: 20,
        paddingTop: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,

    },
    right: {
        alignSelf: "flex-end",
        borderWidth: 1,
        borderColor: Colors.chat
    },
    left: {
        backgroundColor: Colors.chat
    },
    itemChat: {
        width: "80%",
        marginBottom: 15,
        padding: 10,
        position: "relative"
    },
    date: {
        fontSize: 12
    },
    dateright: {
        textAlign: "right"
    },
    message: {},
    messageright: {
        textAlign: "right"
    },
    file: {
        width: 100,
        height: 100,
        borderRadius: 5,
        resizeMode: "cover"
    },
    description: {
        color: "#fff",
        fontSize: 11,
        fontFamily: "Poppins-Light"
    },
    containerDescription: {
        marginTop: 7
    },
    containerDescriptionMotivo: {
        marginTop: -5
    },
    triangleRight: {
        position: "absolute",
        bottom: -12,
        width: 14,
        height: 14,
        right: -1,
        resizeMode: "contain"
    },
    triangleLeft: {
        position: "absolute",
        bottom: -12,
        width: 14,
        height: 14,
        resizeMode: "contain"
    },
    flat: {
        paddingBottom: 20
    }
});

function on(events, fn) {
    for (event in events) {
        socket.on(event, fn);
    }
}

const redux = state => ({
    user: state.auth.user,
    route: state.route.route
});

export default connect(redux)(ViewChat);