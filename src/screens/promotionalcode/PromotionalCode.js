import React, { Component } from "react"
import {
    FlatList,
    Image,
    Platform,
    StyleSheet,
    Text,
    TouchableWithoutFeedback,
    View,
} from "react-native"
import { Button } from "react-native-elements"
import { connect } from "react-redux"

import Api from "../../utils/Api"
import Modal from "../../utils/Modal"
import Color from "../../assets/Color"
import Background from "../../widgets/Background"
import G from "../../utils/G"
import RedeemCodeModal from "./RedeemCodeModal"

class PromotionalCode extends Component {
    static navigationOptions = {
        title: "Código Promocional",
    }

    state = {
        promotions: [],
        expanded: new Map(),
        isRedeemingCode: false,
    }

    componentDidMount() {
        this.props.navigation.addListener("didFocus", () => {
            this.updatePromotions()
        })
    }

    updatePromotions() {
        Api.getPromotions({ id: this.props.user.id })
            .then(resp => {
                if (resp.success) {
                    this.setState({ promotions: resp.promociones || [] })
                }
            })
            .catch(err => {
                Modal.showGenericError(err)
            })
    }

    onPress(id) {
        let newExpanded = new Map(this.state.expanded)
        newExpanded.set(id, !this.state.expanded.get(id))
        this.setState({
            expanded: newExpanded,
        })
    }

    onPressRedeem() {
        this.setState({
            isRedeemingCode: true,
        })
    }

    onCloseModal() {
        this.setState({
            isRedeemingCode: false,
        })
    }

    redeem(code) {
        Api.redeemCode(this.props.user.id, code)
            .then(resp => {
                this.onCloseModal()
                Modal.loading.dismiss();
                setTimeout(() => {
                    if (!resp.data) {
                        Modal.showGenericError()
                        return
                    }

                    if (resp.data.success) {                        
                        this.updatePromotions()
                    } else {
                        Modal.showAlert(resp.data.msg)
                    }
                },1000);                
            })
            .catch(err => {
                console.log(err);
                this.onCloseModal()
                Modal.loading.dismiss();
                setTimeout(() => {
                    Modal.showGenericError(err)
                },1000);                
            })
    }

    render() {
        return (
            <Background noBackground header>
                <Header />
                <TableHeading />
                <PromotionList
                    onPress={id => this.onPress(id)}
                    expanded={this.state.expanded}
                    promotions={this.state.promotions}
                />
                <Button
                    buttonStyle={styles.redeemCodeButton}
                    containerStyle={styles.redeemCodeButtonContainer}
                    onPress={() => this.onPressRedeem()}
                    title={"Canjear código promocional"}
                    titleStyle={styles.redeemCodeButtonTitle}
                />
                <RedeemCodeModal
                    visible={this.state.isRedeemingCode}
                    onClose={() => this.onCloseModal()}
                    onRedeem={code => this.redeem(code)} />
            </Background>
        )
    }
}

const Header = React.memo(() => (
    <View style={styles.headerContainer}>
        <Image
            style={styles.header}
            source={require("../../assets/icons/menu_code_promotion.png")}
        />
    </View>
))

const TableHeading = React.memo(() => {
    const textStyle = [styles.tableText, styles.tableHeadingText]

    return (
        <>
            <View style={styles.tableTextContainer}>
                <Text style={textStyle}>Código</Text>
                <Text style={textStyle}>Descuento</Text>
            </View>
            <Separator />
        </>
    )
})

const Separator = React.memo(() => (
    <View style={styles.tableHeadingSeparatorContainer}>
        <View style={styles.tableHeadingSeparator} />
    </View>
))

const PromotionList = React.memo(({ promotions, expanded, onPress }) => (
    <FlatList
        data={promotions}
        renderItem={({ item }) => (
            <Promotion
                id={item.id}
                code={item.code}
                amount={item.amount}
                expireDate={item.expire}
                expanded={!!expanded.get(item.id)}
                onPress={onPress}
            />
        )}
        extraData={expanded}
        keyExtractor={item => String(item.id)}
        ListEmptyComponent={
            <View style={styles.empty}>
                <Text style={styles.emptyText}>No posees códigos promocionales</Text>
            </View>
        }
    />
))

const Promotion = React.memo(({ id, code, amount, expireDate, expanded, onPress }) => (
    <View>
        <TouchableWithoutFeedback
            onPress={() => { onPress(id) }}
        >
            <View style={styles.tableTextContainer}>
                <Text style={styles.tableText}>{code}</Text>
                <Text style={styles.tableText}>CLP {G.formatMoney(amount)}</Text>
            </View>
        </TouchableWithoutFeedback>
        <Separator />
        {!!expanded && (
            <>
                <View style={styles.expireDateContainer}>
                    <Text style={styles.expireDateText}>Este código se vence:
                    <Text style={styles.expireDate}> {expireDate}</Text>
                    </Text>
                </View>
                <Separator />
            </>
        )}
    </View>
))

const styles = StyleSheet.create({
    headerContainer: {
        alignItems: "center",
        padding: 24,
    },
    header: {
        tintColor: Color.accent,
    },
    tableTextContainer: {
        flexDirection: "row",
        paddingVertical: 8,
    },
    tableText: {
        color: Color.primary,
        fontSize: 18,
        flex: 0.5,
        paddingHorizontal: 16,
        textAlign: "center",
    },
    tableHeadingText: {
        color: Color.secondary,
        fontFamily: "Poppins-SemiBold",
    },
    tableHeadingSeparator: {
        flex: 1,
        backgroundColor: Color.secondaryGray,
        height: StyleSheet.hairlineWidth,
        marginHorizontal: 16,
    },
    tableHeadingSeparatorContainer: {
        flexDirection: "row",
    },
    expireDateContainer: {
        alignItems: "center",
        backgroundColor: "#EEE",
        paddingVertical: 8,
        marginHorizontal: 16,
    },
    expireDateText: {
        fontSize: 18,
    },
    expireDate: {
        color: Color.accent,
    },
    redeemCodeButtonContainer: {
        alignItems: "center",
    },
    redeemCodeButton: {
        margin: 16,
        backgroundColor: "black",
        borderRadius: 6,
    },
    redeemCodeButtonTitle: {
        fontFamily: "Poppins-SemiBold",
        fontSize: 18,
        paddingHorizontal: 16,
    },
    empty: {
        justifyContent: "center",
        padding: 16,
    },
    emptyText: {
        textAlign: "center",
    },
})

const mapStateToProps = state => {
    return {
        user: state.auth.user,
    }
}

export default connect(mapStateToProps)(PromotionalCode)
