import React, { Component } from "react"
import {
    Modal,
    Platform,
    StyleSheet,
    Text,
    View,
} from "react-native"
import {
    Button,
    Input,
} from "react-native-elements"
import Icon from "react-native-vector-icons/FontAwesome5"

import Color from "../../assets/Color"

class RedeemCodeModal extends Component {
    state = {
        code: "",
    }

    render() {
        const close = () => this.close()
        const redeem = () => this.redeem()

        return (
            <Modal
                animationType={"slide"}
                visible={this.props.visible}
                onRequestClose={close}
                useNativeDriver
            >
                <View style={styles.closeButtonContainer}>
                    <Button
                        buttonStyle={styles.closeButton}
                        type="clear"
                        icon={
                            <Icon
                                color={Color.secondary}
                                size={30}
                                outline={true}
                                name={"times-circle"}
                            />
                        }
                        onPress={close}
                    />
                </View>
                <View style={styles.content}>
                    <Text style={styles.title}>
                        Ingrese el código promocional
                    </Text>
                    <Input
                        inputContainerStyle={styles.inputContainer}
                        inputStyle={styles.input}
                        value={this.state.code}
                        onChangeText={code => this.changeCode(code)}
                    />
                    <Button
                        buttonStyle={styles.redeemButton}
                        containerStyle={styles.redeemButtonContainer}
                        onPress={redeem}
                        title={"Canjear"}
                        titleStyle={styles.redeemButtonTitle}
                    />
                </View>
            </Modal>
        )
    }

    clear() {
        this.setState({
            code: ""
        })
    }

    close() {
        this.clear()
        this.props.onClose()
    }

    redeem() {
        this.props.onRedeem(this.state.code)
        this.clear()
    }

    changeCode(code) {
        this.setState({
            code: code
        })
    }
}

const styles = StyleSheet.create({
    closeButtonContainer: {
        position: "relative",
        width: "100%",
    },
    closeButton: {
        height: 40,
        margin: 10,
        padding: 0,
        position: "absolute",
        right: 0,
        width: 40,
    },
    content: {
        alignItems: "center",
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 56,
        width: "100%",
    },
    title: {
        color: Color.primary,
        fontFamily: "Poppins-SemiBold",
        fontSize: 20,
        textAlign:'center',
    },
    inputContainer: {
        borderColor: Color.primary,
        borderRadius: 8,
        borderWidth: 1,
        marginTop: 16,
    },
    input: {
        fontFamily: "Poppins",
        fontSize: 16,
        paddingHorizontal: 16,
        paddingVertical: 8,
    },
    redeemButtonContainer: {
        alignItems: "center",
    },
    redeemButton: {
        margin: 16,
        backgroundColor: "black",
        borderRadius: 6,
    },
    redeemButtonTitle: {
        fontFamily: "Poppins-SemiBold",
        fontSize: 20,
        paddingHorizontal: 16,
    },
})

export default RedeemCodeModal