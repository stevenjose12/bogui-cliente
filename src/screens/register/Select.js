import React from 'react';
import RNPickerSelect from 'react-native-picker-select';
import { StyleSheet, View, Text } from 'react-native';
import Colors from '../../assets/Color';
import Icon from 'react-native-vector-icons/FontAwesome';

_listItems = (items) => {
    let list = [];

    if(items.length == 246){
        for (let index = 0; index < items.length; index++) {
            list[index] =  {
                label: '(+'+items[index]['phonecode']+') '+items[index]['name'],
                value: items[index]['phonecode']
            };
        }
        return list;
    }else{
        return items
    }
};

const Select = props => {
    
    const items = props.items ? props.items : [];
    return (
        <React.Fragment>
            { props.label && <Text>{ props.label }</Text> }
            <View style={ styles.container }>
                <Text style={ styles.text }>
                    { 
                        props.value ?
                            '('+items.find(i => i.value == props.value)[props.showValue ? 'value' : 'label']+')'
                            : (props.placeholder ? props.placeholder: (props.placeholder == null ? null : 'Seleccione')) 
                    }
                    {
                        props.valueSelect ?
                        ' ('+items.find(i => i.value == props.valueSelect)[props.showValue ? 'label' : 'value']+')'
                            : null
                    }
                </Text>
                <Icon size={ 13 } style={ styles.icon } name="caret-down" color={ Colors.gray } />
                <RNPickerSelect
                  style={{
                    inputAndroid: styles.input,
                    inputIOS: styles.input
                  }}
                  placeholder={{
                    label: props.placeholder ? props.placeholder: ''
                  }}
                  value={ props.value }
                  onValueChange={ props.onValueChange }
                  items={items}
                  doneText="Aceptar"
                />
            </View>
        </React.Fragment>
    )
};

const styles = StyleSheet.create({
    container: {
        width: '93%',
        backgroundColor: '#fff',
        marginBottom: 10,
        height: 40,
        alignItems: "center",
        justifyContent: 'center',
        top: 5,
        left: '3.5%',
        borderColor: Colors.secondary,
        borderWidth: 1
    },
    icon: {
        position: 'absolute',
        right: 4,
        top: 15
    },
    input: {
        opacity: 0,
    },
    text: {
        position: 'absolute',
        fontSize: 15,
        fontFamily: 'Poppins',
        paddingVertical: 10,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 5,
        color: Colors.secondary
        
    }
});

export default Select;