import React, { Component } from 'react';
import { Text, View, ImageBackground, StyleSheet, ScrollView, TouchableWithoutFeedback } from 'react-native';
import { Avatar  } from 'react-native-elements';
import Input from "../../widgets/InputBogui";
import Button from '../../widgets/Button';
import Banner from "../../widgets/BannerAuth";
import Api from "../../utils/Api";
import Colors from "../../assets/Color";
import Permissions from "../../utils/Permissions";
import Select from './Select';
import DatePicker from '../../widgets/DatePicker';
import { connect } from "react-redux";
import ImagePicker from 'react-native-image-picker';
import Modal from '../../utils/Modal';
import Rotation from '../../widgets/Rotation';
import moment from 'moment';
import ENV from '../../utils/env';
import G from "../../utils/G";
import {
    phoneCodeValidator,
    avatarValidator,
    nameValidator,
    lastNameValidator,
    phoneValidator,
    passwordConfirmValidator,
    emailValidator,
    documentValidator,
    expirationDate
} from "../../utils/Validators";

const DOCUMENT_TYPE = {
    RUT: '1',
    PASSPORT: '2',
    OTHER: '3'
};

const ITEMS_DOCUMENT_TYPE_SELECT = [
    { label: 'RUT', value: DOCUMENT_TYPE.RUT },
    { label: 'Pasaporte', value: DOCUMENT_TYPE.PASSPORT },
    { label: 'Otro', value: DOCUMENT_TYPE.OTHER }
];

const ITEMS_RUT_TERMINATION = [
    { label: '0', value: '0' },
    { label: '1', value: '1' },
    { label: '2', value: '2' },
    { label: '3', value: '3' },
    { label: '4', value: '4' },
    { label: '5', value: '5' },
    { label: '6', value: '6' },
    { label: '7', value: '7' },
    { label: '8', value: '8' },
    { label: '9', value: '9' },
    { label: 'K', value: 'K' }
];

class Register extends Component {

    flagSubmit = true;

    state = {
        checked: false,
        phoneCodes: [],
        permissionsGranted: false,
        filePath: false,
        photo: false,
        form: {
            photo: '',
        },
        formPhoto: '',
        degrees: 0,
        rotation: {
            visible: false,
            source: null,
            name: ''
        },
        // FORMULARIO DE REGISTRO
        name: '',
        lastName: '',
        email: '',
        password: '',
        passwordConfirmation: '',
        documentType: '',
        document: '',
        phone: '',
        phoneCode: '',
        documentExpiration: '',
        rut: '',
        rutTermination: '',
        // ERROR INPUTS
        errName: '',
        errLastName: '',
        errEmail: '',
        errPassword: '',
        errPhone: '',
        errDocument: '',
        errAvatar: '',
        errExpirationDate: ''
    };

    componentDidMount() {
        this._askPermissions();
        this.getPhoneCodes();
        expirationDate.notPass = errExpirationDate => this.setState( { errExpirationDate } );
        phoneCodeValidator.notPass = errPhone => this.setState( { errPhone } );
        avatarValidator.notPass = errAvatar => this.setState( { errAvatar } );
        nameValidator.notPass = errName => this.setState( { errName } );
        lastNameValidator.notPass = errLastName => this.setState( { errLastName } );
        phoneValidator.notPass = errPhone => this.setState( { errPhone } );
        emailValidator.notPass = errEmail => this.setState( { errEmail } );
        passwordConfirmValidator.notPass = errPassword => this.setState( { errPassword } );
        documentValidator.notPass = errDocument => this.setState( { errDocument } );
    }

    _formatMiles = value => {
        let num = value.replace(/\./g,'');
        if (!isNaN(num)) {
            num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
            num = num.split('').reverse().join('').replace(/^[\.]/,'');
            return num;
        } else {
            alert('Solo se permiten numeros');
            return value.replace(/[^\d\.]*/g,'');
        }
    };

    getPhoneCodes = async () => {

        try {
            if (this.props.isStoragePhoneCodes) {
                this.setState( { phoneCodes: this.props.phoneCodes } );
            } else {
                const codes = await Api.phoneAreaCode(true);
                this.setPhoneCodes(codes);
            }
        } catch (e) {
            Modal.showGenericError(e);
        }
    };

    setPhoneCodes = codes => {
        const phoneCodes = [];
        codes.forEach( code => phoneCodes.push({
            label: '(+'+code.phonecode+') '+code.name,
            value: code.phonecode
        }));
        this.setState( { phoneCodes } );
        this.props.dispatch( { type: 'SET_PHONECODE', payload: { phoneCodes } } );
    };

    _askPermissions = () => {
        Permissions.register(['camera', 'photo'])
            .then( permissionsGranted => {
                if (permissionsGranted) this.setState({ permissionsGranted });
            });
    };

    chooseFile = () => {

        this.setState( { errAvatar: '' } );

        const options = {
            title: 'Seleccione',
            cancelButtonTitle: 'Cancelar',
            takePhotoButtonTitle: 'Usar la Cámara',
            chooseFromLibraryButtonTitle: 'Usar la Galería',
            quality: 0.8,
            maxWidth: 1000,
            maxHeight: 1000,
            noData: true,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
                alert(response.customButton);
            } else {
                this.setState({
                    filePath: {
                        name: response.fileName,
                        path: response.path,
                        size: response.fileSize
                    },
                    photo: {
                        image: response.uri,
                        grade: '0',
                        visible: true
                    },
                    rotation: {
                        source: response.uri,
                        name: response.fileName
                    }
                });
                this.modalRotation.show(response.uri);
            }
        });
      };

	onSaveAvatar = degrees => this.setState( { degrees: degrees.toString() }, this.uploadAvatar );

    uploadAvatar = () => {

        const params = {
            rotate: this.state.degrees || 0
        };

        Api.uploadAvatar(this.state.photo.image, params)
            .then( r => this.setState(state => {
                state.formPhoto = r.data.foto;
                state.filePath.uri = ENV.BASE_PUBLIC+r.data.foto;
                return state;
            }))
            .catch( e => console.log('>>: Register > uploadAvatar > error: ', e) );

    };

    clearForm = () => this.setState({
        formPhoto: '',
        name: '',
        lastName: '',
        email: '',
        password: '',
        passwordConfirmation: '',
        documentType: '',
        document: '',
        phone: '',
        phoneCode: '',
        documentExpiration: '',
        rut: '',
        rutTermination: '',
        filePath: ''
    });

    capitalize = s => {
        if (typeof s !== 'string') return '';
        return s.charAt(0).toUpperCase() + s.slice(1)
    };

    submit = async () => {

        if (this.state.documentType===DOCUMENT_TYPE.RUT) {
            this.state.document = this.state.rut+'-'+this.state.rutTermination;
            this.setState({ document: this.state.document });
        }

        if (!avatarValidator.validate(this.state.formPhoto)
            || !nameValidator.validate(this.state.name)
            || !lastNameValidator.validate(this.state.lastName)
            || !emailValidator.validate(this.state.email)
            || !passwordConfirmValidator.compare(this.state.password, this.state.passwordConfirmation)
            || !documentValidator.validate(this.state.document)
            || !expirationDate.validate(this.state.documentExpiration)
            || !phoneCodeValidator.validate(this.state.phoneCode)
            || !phoneValidator.validate(this.state.phone)
        ) return false;

        const form = {
            photo: this.state.formPhoto,
            nombre: this.state.name,
            apellido: this.state.lastName,
            email: this.state.email,
            password: this.state.password,
            password_confirmation: this.state.passwordConfirmation,
            documentType: this.state.documentType,
            document: this.state.document,
            telefono: this.state.phone,
            phoneCode: this.state.phoneCode,
            expiration_document: this.state.documentExpiration,
            hash: G.getHash()
        };

        try {

            const response = await Api.register(form);
            if (response.success) {
                this.clearForm();
                G.sendMessage(this.state.phoneCode+this.state.phone, 'Bienvenido a Bogui, su código de verificación es: '+form.hash);
                Modal.alert.show({ message: "Su Registro ha sido exitoso", time: 3000});
                this.props.navigation.navigate("Login");
            } else {
                Modal.alert.show({ message: response.message });
            }
        } catch (e) {
            Modal.loading.dismiss();
            if (e.response && e.response.status === 422) {
                const keys = Object.keys(e.response.data);
                setTimeout(() => Modal.alert.show({ message: this.capitalize(keys[0]) + ': ' + e.response.data[keys[0]] }),1000);
            } else {
                setTimeout(() => Modal.showGenericError(e),1000);
            }
        }

    };

    render() {
        return (

            <ImageBackground
                source={require('../../assets/img/background.jpg')}
                style={{width: '100%', height: '100%'}} >

                <Banner
                    iconBanner={ require('../../assets/img/bogui_banner.png') }
                    backIcon={ require('../../assets/icons/back.png') }
                    onBackPress={ () => this.props.navigation.navigate("Login") }/>

                <ScrollView>

                    <View style={styles.container}>

                        <View style={[{width: '100%'}]}>

                            {/* AVATAR */}
                            <View style={{top: -8}}>
                                <TouchableWithoutFeedback onPress={ this.chooseFile } >
                                    <Avatar
                                        source={ !this.state.filePath.uri ? require('../../assets/img/avatar.png') : {uri: this.state.filePath.uri}}
                                        size={115}
                                        rounded
                                        containerStyle={{left: 15, top: 8, borderColor: Colors.secondary, borderWidth: 1.35}} />
                                </TouchableWithoutFeedback>
                                {
                                    !!this.state.errAvatar && (
                                        <Text style={ { fontSize: 13, color: 'red', padding: 3 } }>
                                            { this.state.errAvatar }
                                        </Text>
                                    )
                                }
                            </View>

                            {/* NOMBRE */}
                            <Input
                                hint='Nombre'
                                onChangeText={ name => this.setState( { name } ) }
                                onFocus={ () => this.setState( { errName: '' } ) }
                                onBlur={ () => nameValidator.validate(this.state.name) }
                                text={ this.state.name }
                                errorMessage={ this.state.errName }
                                inputStyle={ styles.inputBorderStyle } />

                            {/* APELIDO */}
                            <Input
                                hint='Apellido'
                                onChangeText={ lastName => this.setState( { lastName } ) }
                                onFocus={ () => this.setState( { errLastName: '' } ) }
                                onBlur={ () => lastNameValidator.validate(this.state.lastName) }
                                text={ this.state.lastName }
                                errorMessage={ this.state.errLastName }
                                inputStyle={styles.inputBorderStyle} />

                            {/*  EMAIL */}
                            <Input
                                hint='Email'
                                keyboardType="email-address"
                                onChangeText={ email => this.setState( { email } ) }
                                onFocus={ () => this.setState( { errEmail: '' } ) }
                                onBlur={ () => emailValidator.validate(this.state.email) }
                                text={ this.state.email }
                                errorMessage={ this.state.errEmail }
                                inputStyle={styles.inputBorderStyle} />

                            {/* CONTRASEÑA */}
                            <Input
                                password
                                hint='Contraseña'
                                onChangeText={ password => this.setState( { password } ) }
                                onFocus={ () => this.setState( { errPassword: '' } ) }
                                text={ this.state.password }
                                errorMessage={ this.state.errPassword }
                                inputStyle={styles.inputBorderStyle} />

                            {/* REPETIR CONTRASEÑA */}
                            <Input
                                password
                                hint='Repetir Contraseña'
                                onChangeText={ passwordConfirmation => this.setState( { passwordConfirmation } ) }
                                onFocus={ () => this.setState( { errPassword: '' } ) }
                                onBlur={ () => passwordConfirmValidator.compare(this.state.password, this.state.passwordConfirmation) }
                                text={ this.state.passwordConfirmation }
                                inputStyle={ styles.inputBorderStyle } />

                            {/* DOCUMENT TYPE */}
                            <Select
                                showValue
                                items={ ITEMS_DOCUMENT_TYPE_SELECT }
                                onValueChange={ documentType => this.setState({ documentType }) }
                                placeholder="Tipo de Documento"
                                valueSelect={ this.state.documentType } />

                            {
                                this.state.documentType === DOCUMENT_TYPE.RUT
                                    ?
                                    <View style={{flexDirection: 'row'}}>
                                        <View style={{width: '63%'}}>
                                            <Input
                                                hint='Número de RUT'
                                                keyboardType="number-pad"
                                                inputStyle={ styles.inputBorderStyle }
                                                onChangeText={ rut => this.setState(  { rut: this._formatMiles(rut) } ) }
                                                onFocus={ () => this.setState( { errDocument: '' } ) }
                                                onBlur={ () => documentValidator.validate(this.state.rut) }
                                                errorMessage={ this.state.errDocument }
                                                text={ this.state.rut } />
                                        </View>
                                        <Text style={{color: 'red', bottom: '-5%'}}>-</Text>
                                        <View style={{width: '32.3%'}}>
                                            <Select
                                                items={ ITEMS_RUT_TERMINATION }
                                                onValueChange={ rutTermination => this.setState({ rutTermination }) }
                                                placeholder={ null }
                                                valueSelect={ this.state.rutTermination } />
                                        </View>
                                    </View>
                                    :
                                    !!this.state.documentType && (
                                        <Input
                                            hint={ this.state.documentType===DOCUMENT_TYPE.PASSPORT ? 'Ingresa tu Pasaporte' : 'Ingresa su Documento'}
                                            onChangeText={ document => this.setState({ document }) }
                                            onFocus={ () => this.setState( { errDocument: '' } ) }
                                            onBlur={ () => documentValidator.validate(this.state.document) }
                                            errorMessage={ this.state.errDocument }
                                            inputStyle={ styles.inputBorderStyle } />
                                    )

                            }

                            <View style={{paddingVertical: 5}}>

                            {/* EXPIRACIÓN DEL DOCUMENTO */}
                            <DatePicker
                                minDate={ moment().add(1,'day').format('DD/MM/YYYY') }
                                style={{width: '93%', left: 10}}
                                placeholder="Expiración del RUT o Pasaporte"
                                onChange={ documentExpiration => this.setState( { documentExpiration: moment(documentExpiration, 'DD/MM/YYYY').format('YYYY/MM/DD') }) }
                                value={ this.state.documentExpiration ? moment(this.state.documentExpiration, 'YYYY/MM/DD').format('DD/MM/YYYY') : null} />
                            </View>

                            {
                                !!this.state.errExpirationDate && (
                                    <Text style={ { fontSize: 13, color: 'red', padding: 3 } }>
                                        { this.state.errExpirationDate }
                                    </Text>
                                )
                            }

                            {/* TELÉFONO */}
                            <View style={{flexDirection: 'row', width: '100%'}}>
                                <View style={{width: '30%', left: 7}}>
                                <Select
                                    showValue
                                    items={ this.state.phoneCodes }
                                    onValueChange={ phoneCode => this.setState( { phoneCode } ) }
                                    value={ this.state.phoneCode }
                                    placeholder="Código" />
                                </View>
                                <View style={{width: '70%'}}>
                                    <Input
                                        keyboardType="number-pad"
                                        hint='Teléfono'
                                        inputStyle={styles.inputBorderStyle}
                                        onChangeText={ phone => this.setState( { phone } ) }
                                        onFocus={ () => this.setState( { errPhone: '' } ) }
                                        onBlur={ () => phoneValidator.validate(this.state.phone) }
                                        errorMessage={ this.state.errPhone }
                                        text={ this.state.phone } />
                                </View>
                            </View>
                        </View>
                        <View style={[{alignItems: 'center', paddingBottom: 10}]}>
                            <View style={styles.separator}/>
                                <Button
                                    title="Regístrate"
                                    type="outline"
                                    titleStyle={{fontSize: 24, color: 'black'}}
                                    containerStyle={{width: 160}}
                                    buttonStyle={styles.buttonStyle}
                                    onPress={ this.submit } />
                        </View>

                    </View>

                </ScrollView>

                <Rotation
                    ref={ rotation => this.modalRotation = rotation }
					onSave={ this.onSaveAvatar } />

            </ImageBackground>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        height: "100%",
        width: "100%",
        paddingHorizontal: 10,
        paddingVertical: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    inputBorderStyle:{borderColor: Colors.secondary, borderWidth: 1},
    map: {
        flex: 1,
        width: "100%"
    },
    separator:{borderBottomColor: 'white', borderBottomWidth: 2},
    paddingInput:{
        paddingVertical: 0
    },
    inputSignRight:{
        color: 'red', right: 8, top: -12
    },
    inputStyle:{
        backgroundColor: 'white',
        paddingLeft: 24
    },
    rightIconContainerStyle: {paddingRight: 3, alignItems: 'center', left: -3, right: 0, top: 0, bottom: 0},
    inputContainerStyle: {paddingVertical: 5},
    buttonStyle: {borderColor: 'black',paddingVertical: 5, alignItems:'center', borderWidth: 1, top: 5}
});

const redux = state => ({
    isStoragePhoneCodes: state.phonecodes.isStoragePhoneCodes,
    phoneCodes: state.phonecodes.phoneCodes
});

export default connect(redux)(Register);