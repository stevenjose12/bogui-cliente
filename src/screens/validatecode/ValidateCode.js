import React, { Component } from 'react';
import { ImageBackground, StyleSheet, View } from "react-native";
import Button from "../../widgets/Button";
import Modal from "../../utils/Modal";
import Input from "../../widgets/InputBogui";
import Api from "../../utils/Api";
import G from "../../utils/G";
import Banner from "../../widgets/BannerAuth";

class ValidateCode extends Component {

    state = {
        code: "",
        eCode: ""
    };

    componentDidMount() {
        this.codeValidator = G.getCodeValidator();
        this.codeValidator.onError( error => this.setState( state => state.eCode = error )  );
    }

    onBackPress = () => {
        this.props.navigation.navigate("RememberPassword");
    };

    submit = async () => {

        if (!this.codeValidator.isValid(this.state.code)) return false;
        try {

            Modal.loading.show();
            const response = await Api.codeValidator( {codigo: this.state.code, email: this.props.navigation.getParam("email")} );
            Modal.loading.dismiss();

            if (response.result) {
                Modal.alert.show({ message: "Su código fue validado exitosamente.", time: 2000 });
                this.props.navigation.navigate("ChangePassword", { code: this.state.code, email: this.props.navigation.getParam("email") });
            } else {
                Modal.alert.show({ message: response.error });
            }

        } catch (e) {
            Modal.showGenericError(e);
        }

    };

    render() {

        return (

            <ImageBackground
                source={ require('../../assets/img/background.jpg') }
                style={{width: '100%', height: '100%'}} >

                <Banner
                    iconBanner={ require('../../assets/img/bogui_banner.png') }
                    backIcon={ require('../../assets/icons/back.png') }
                    onBackPress={ this.onBackPress }/>

                <View style={ styles.container }>
                    <Input
                        label="Coloque el código que se envió a su correo"
                        labelStyle={{fontWeight: 'bold', fontSize: 15}}
                        hint="Código"
                        onChangeText={ t => {
                            this.setState( state => state.code = t );
                            this.setState( state => state.eCode = "" );
                        } }
                        autoCapitalize="characters"
                        errorMessage={ this.state.eCode } />

                    <Button
                        title="Continuar"
                        type="outline"
                        titleStyle={{fontSize: 20, color: 'black', fontFamily: 'Poppins-Bold'}}
                        containerStyle={{width: "50%", marginTop: 20}}
                        buttonStyle={{borderColor: 'black', paddingVertical: 5, alignItems: 'center', borderWidth: 1, borderRadius: 20}}
                        onPress={ this.submit } />

                </View>

            </ImageBackground>

        );

    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: '4%'
    },
    submit: {
        marginVertical: 20
    }

});


export default ValidateCode;