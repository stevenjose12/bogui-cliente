import React, { Component } from 'react'
import { Text, View, StyleSheet, Image } from "react-native";
import Color from "../../assets/Color";
import Button from "../../widgets/Button";
import G from "../../utils/G";

class Footer extends Component {

    getVehicle = () : string => {
        if (!!this.props.vehicleType) return require('../../assets/icons/bogui1.png');
        switch (this.props.vehicleType) {
            case 1: return require('../../assets/icons/bogui1.png');
            case 2: return require('../../assets/icons/bogui2.png');
            case 3: return require('../../assets/icons/bogui3.png');
            default: return require('../../assets/icons/bogui1.png');
        }
    };

    render() {
        return (
            <View style={styles.container}>

                <Text>
                    Método de pago: <Text style={{fontFamily: 'Poppins-SemiBold'}}> {(this.props.payment === 1 ? "Tarjeta de Crédito" : "Efectivo")} </Text>
                </Text>

                <Image
                    source={ G.getVehicleImageByCategory(this.props.vehicleType) }
                    style={{ height: 30, width: '100%', marginVertical: 15 }}
                    resizeMode="contain"
                    tintColor={ Color.primary }/>

                {
                    !!this.props.distance && (
                        <Text> {"Distancia del viaje " + (this.props.distance.toFixed(2)) + (this.props.distance > 1 ? " kilometros" : "kilometro")} </Text>
                    )
                }

                {
                    !!this.props.driverDuration && (
                        <Text style={ { width: '100%', textAlign: 'center', paddingHorizontal: 5 } }> {"El bogui más cerano se encuentra a " + (G.getTime(this.props.driverDuration / 60))} </Text>
                    )
                }

                <Button
                    onPress={() => {
                        if (!this.props.loading) this.props.onPress ? this.props.onPress() : null
                    }}
                    title="Confirmar viaje"
                    titleStyle={{width: '100%'}}
                    buttonStyle={styles.btn}
                    containerStyle={ { width: '100%', justifyContent: 'center', alignItems: 'center' } }
                    loading={ this.props.loading } />

            </View>
        );

    }

}

const styles = StyleSheet.create( {
    container: {
        paddingTop: 7,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: Color.white,
    },
    textTitle: {
        color: Color.white,
        fontSize: 17,
    },
    textCost: {
        color: Color.white,
        fontSize: 40,
        fontFamily: 'Poppins-SemiBold'
    },
    btn: {
        backgroundColor: Color.primary,
        width: '50%',
        height: 40,
        borderRadius: 20,
        marginVertical: 3,
        justifyContent: 'center',
        alignItems: 'center'
    },
} );

export default Footer;