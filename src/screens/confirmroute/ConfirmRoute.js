import React, { Component } from 'react'
import {Dimensions, View, Platform } from 'react-native'
import {connect} from "react-redux";
import Background from "../../widgets/Background";
import MapPlotRoute from "../../widgets/MapPlotRoute";
import Cost from "./Cost";
import Footer from "./Footer";
import Header from "./Header";
import Api from "../../utils/Api";
import G from "../../utils/G";
import Modal from "../../utils/Modal";
import { socket } from '../../utils/Socket';
import ModalNoBogui from "../../widgets/ModalNoBogui";
import Toast from "../../widgets/Toast";
import { ScrollView } from "react-native-gesture-handler";

const WIDTH = Dimensions.get('window').width;
const HEIGHT = WIDTH/Math.sqrt(2);

class ConfirmRoute extends Component {

    static navigationOptions = { title: "Mi viaje" };

    payPromotion = 0;
    isSubmit = false;

    state = {
        loading: true,
        showMap: true,
        imageVehicle: ''
    };

    constructor(props) {
        super(props);
        if (Platform.OS === 'ios') {
            this.props.navigation.setParams({
                titleStyle: {
                    zIndex: 999,
                    position: 'absolute'
                }
            });
        }  
    }

    componentDidMount() {
        this.onFocusListener = this.props.navigation.addListener('didFocus', this.onFocus);
    }

    componentWillUnmount(): void {
        this.onFocusListener?.remove();
    }

    onFocus = async () => {

        this.isSubmit = false;

        this.setState( {
            imageVehicle: G.getVehicleImageByCategory( this.props.request.vehicleType ),
            showMap: true,
            loading: true
        } );

        this.modalNoBogui.setVehicles( this.props.vehicles );

        //<editor-fold defaultstate="collapsed" desc="obteniendo los conductores cercanos">
        const data = {
            vehicle: this.props?.request?.vehicleType,
            lat: this.props?.places[0]?.location?.latitude,
            lng: this.props?.places[0]?.location?.longitude,
        };

        // if (!data.vehicle || !data.lat || data.lng) throw ">>: ConfirmRoute > onFocus > request param error params: "+ data;

        Api.getAround(data)
           .then( async r => {
                   const driverLocation = {
                       latitude: parseFloat(r.cercano.lat),
                       longitude: parseFloat(r.cercano.lng)
                   };
                   const partida = {
                       latitude: this.props.places[0].location.latitude,
                       longitude: this.props.places[0].location.longitude
                   };
               try {
                   const _directions = await Api.getDirections(partida, driverLocation);
                   this.driverDuration = _directions.duration;
                   this.forceUpdate();
               } catch (e) {
                   console.log(">>: ConfirmRoute > onFocus > error: ", e.response);
               }
           } )
           .catch( e => console.log(">>: ConfirmRoute > initComponent > getAround > error: ", e) );
        //</editor-fold>

        try {

            //<editor-fold defaultstate="collapsed" desc="obteniendo las tarifas">
            const tariffs = await Api.getTariffs( { vehicle: data.vehicle, latitude: data.lat, longitude: data.lng } );
            this.tariffs = tariffs.tarifas;
            // this.fotoVehicle = tariffs.tarifas.photo;
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="obteniendo promociones">
            const promotion = await Api.isPromotion(this.props.user.id);
            if (promotion.promociones.length>0) this.code = promotion.promociones[0];
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="obteniendo si la ruta pasa por un aereopuerto">
            let params = {};
            params.partida = {
                lat: this.props.places[0].location.latitude,
                lng: this.props.places[0].location.longitude,
            };
            params.llegada = {
                lat: this.props.places[2].location.latitude,
                lng: this.props.places[2].location.longitude,
            };
            if (this.props.places[1].location) {
                params.parada = {
                    lat: this.props.places[1].location.latitude,
                    lng: this.props.places[1].location.longitude,
                }
            }
            const airport = await Api.isAirport(params);
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="obteniendo distancia">
            this.origin = this.props.places[0].location;
            this.waypoints = [];
            for (let i=1; i<this.props.places.length-1; i++) {
                const item = this.props.places[i];
                if (item.location) this.waypoints[i-1] = item.location;
            }
            this.destination = this.props.places[this.props.places.length-1].location;
            const directions = await Api.getDirections(this.origin, this.destination, this.waypoints);
            this.duration = directions.duration;
            this.distance = directions.distance/1000;
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Calcular tarifa">
            if ( (this.distance>this.tariffs.value_km_less) && (this.distance<this.tariffs.value_km_greater) ) {
                this.tariff = G.calcTariff(this.tariffs.base_tariff, this.distance, this.tariffs.medium_tariff, airport.cost);
            } else if (this.distance > this.tariffs.value_km_greater) {
                this.tariff = G.calcTariff(this.tariffs.base_tariff, this.distance, this.tariffs.maximum_tariff, airport.cost);
            } else {
                this.tariff = G.calcTariff(0, 1, this.tariffs.minimum_tariff, airport.cost);
            }
            //</editor-fold>

            this.map.setPlaces(this.props.places);

            this.setState( { loading: false } );

        } catch (e) {
            Modal.showGenericError(e);
        }

    };

    finish = () => this.setState({ showMap: false }, () => this.props.navigation.replace("CancelRoute") );

    submit = async () => {

        if (this.isSubmit) return;
        this.isSubmit = true;

        let data = {};
        if (!this.props.user.default_payment) {
            this.props.navigation.navigate('PayMethodsDrawer');
            Toast.show('Debe seleccionar un metodo de pago');
            return false
        }

        if (this.code) this.payPromotion = (this.code.amount > this.tariff) ? this.tariff : this.code.amount;
        this.payUser = (this.payPromotion>this.tariff) ? 0 : this.tariff-this.payPromotion;

        //<editor-fold desc="Porcentaje del código promocional">
        data.p_code = this.payPromotion*0.05; //let p_code = this.payPromotion*this.commission;
        if (data.p_code%50!==0) data.p_code = ( Math.floor(data.p_code/50) )*50;
        //</editor-fold>

        //<editor-fold desc="Porcentaje total">
        let p_total = this.tariff*0.05;//this.tariff*this.comision;
        if (p_total%50!==0) p_total = ( Math.floor(p_total/50) )*50;
        //</editor-fold>

        if (this.code) data.code = this.code.id;
        data.p_user = p_total-data.p_code;
        data.monto = this.tariff;
        data.user_id = this.props.user.id;
        data.pago_codigo = this.payPromotion;
        data.pago_pasajero = this.payUser;
        data.tarifa_id = this.tariffs.id;
        data.distance = this.distance;
        data.tiempo = this.duration;
        data.vehicle_type = this.props.request.vehicleType;
        data.partida = {
            descripcion: this.props.places[0].address,
            lat: this.props.places[0].location.latitude,
            lng: this.props.places[0].location.longitude,
        };
        data.llegada = {
            descripcion: this.props.places[2].address,
            lat: this.props.places[2].location.latitude,
            lng: this.props.places[2].location.longitude,
        };
        if (this.props.places[1].location) {
            data.parada = {
                descripcion: this.props.places[1].address,
                lat: this.props.places[1].location.latitude,
                lng: this.props.places[1].location.longitude,
            }
        }

        try {

            const r = await Api.sendRequest(data);
            if (!r.result) {
                setTimeout(() => this.modalNoBogui.show(true),1000);
                return;
            }

            Toast.show(r.msg);

            this.props.dispatch( { type: 'SET_REQUEST', payload: { request: { vehicleType: this.props.request.vehicleType, data } } } );
            this.props.dispatch( { type: 'SET_ROUTE', payload: { route: r.solicitud } } );

            if (!r.cercano) {
                setTimeout(() => this.modalNoBogui.show(true) ,1000);
                return;
            }

            const info = r.solicitud;
            info.partida_descripcion = data.partida.descripcion;
            info.adicional_descripcion = data.parada ? data.parada.descripcion : 'No definida';
            info.destino_descripcion = data.llegada.descripcion;
            info.carrera = { id: r.cercano.id };
            socket.emit('solicitude', info);
            this.finish();

        } catch (e) {
            Modal.showGenericError(e);
        }

    };

    render() {
        return (
            <Background header noBackground>
                <View onStartShouldSetResponder={() => true} >

                    <ScrollView style={ { zIndex: 1000 } }>

                        <Header />

                        {
                            this.state.showMap
                                ?
                                <View style={ { width: WIDTH, height: HEIGHT } }>
                                    <MapPlotRoute
                                        ref={map => this.map = map}
                                        markers={G.markersPoint}
                                        phone />
                                </View>
                                :
                                <View style={{flex: 1, backgroundColor: 'white'}} />
                        }

                        <Cost tariff={ this.tariff }/>

                        <Footer
                            distance={ this.distance }
                            onPress={ this.submit }
                            driverDuration={ this.driverDuration }
                            loading={ this.state.loading }
                            payment={ this.props.user.default_payment }
                            vehicleType={ this.props.request.vehicleType }
                            imageVehicle={ this.state.imageVehicle }/>

                        <ModalNoBogui
                            ref={ modal => this.modalNoBogui = modal }
                            vehicleCategory={ this.props.request.vehicleType } />

                    </ScrollView>
                </View>
            </Background>
        )
    }

}

const redux = state => ( {
    places: state.places.places,
    request: state.request.request,
    user: state.auth.user,
    vehicles: state.vehicles.vehicles,
    route: state.route.route
});

export default connect(redux)( ConfirmRoute );