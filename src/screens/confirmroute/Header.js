import React, { Component } from 'react'
import {Text, View, StyleSheet, Image } from "react-native";
import Color from "../../assets/Color";
import Hbox from "../../widgets/Hbox";
import {connect} from "react-redux";
import Geo from "../../utils/Geo";

const items = [
    { label: 'Partida', icon: require('../../assets/icons/point_start.png') },
    { label: 'Adicional', icon: require('../../assets/icons/point_stop.png') },
    { label: 'Destino', icon: require('../../assets/icons/point_goal.png') }
];

class Header extends Component {

    places = [];

    componentDidMount() {
        this.places = this.props.places;
        for (let i=0; i<this.places.length; i++) {
            if (this.places[i].location && !this.places[i].address) {
                this.getAddress(this.places[i].location, i);
            }
        }
    }

    async getAddress(location, index) {

        try {
            const address = await Geo.getAddress(location);
            this.places[index].address = address;
            this.forceUpdate();
            this.props.dispatch( { type: 'SET_PLACES', payload: { places: this.places } } );
        } catch (e) {
            setTimeout( () => this.getAddress(location, index, 500) );
        }

    }

    render() {
        return (
            <View style={styles.container}>

                {
                    this.props.places && this.props.places.map( (place, i) => {

                        if (place.address) return (

                            <Hbox key={i} style={styles.hbox}>

                                <Text style={styles.text} >
                                    {items[i].label}
                                </Text>

                                <View style={styles.markerContainer}>
                                    <Image source={items[i].icon}/>
                                </View>

                                <Text style={styles.description} >
                                    {place.address}
                                </Text>

                            </Hbox>

                        )
                    })
                }

            </View>
        )
    };

}

const styles = StyleSheet.create( {
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: Color.white,
    },
    text: {
        width: 75,
        textAlignVertical: 'center',
        textAlign: 'center',
        fontFamily: 'Poppins-Italic',
    },
    markerContainer: {
        paddingRight: 5,
        justifyContent: 'center',
    },
    description: {
        flex: 1,
        color: Color.primary,
        alignItems: 'center',
    },
    hbox: {
        paddingVertical: 2,
    }
} );

const redux = state => ({ places: state.places.places });

export default connect( redux )( Header );