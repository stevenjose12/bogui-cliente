import React from 'react'
import {Text, View, StyleSheet, ActivityIndicator} from "react-native";
import Color from "../../assets/Color";
import G from "../../utils/G";

const Cost = props => (
    <View style={ styles.container } >

        <Text style={ styles.textTitle }>
            Tu viaje tendrá un costo de
        </Text>

        {
            props.tariff
                ?
                <Text style={ styles.textCost }>
                    { "CLP "+ G.formatMoney(props.tariff) }
                </Text>
                :
                <ActivityIndicator />
        }

    </View>
);

const styles = StyleSheet.create( {
    container: {
        paddingTop: 7,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: Color.blue,
    },
    textTitle: {
        color: Color.white,
        fontSize: 15,
    },
    textCost: {
        color: Color.white,
        fontSize: 30,
        fontFamily: 'Poppins-SemiBold'
    }
} );

export default Cost;