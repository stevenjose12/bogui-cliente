import React, { Component } from 'react';
import Input from "../../widgets/InputBogui";
import { View, ImageBackground, StyleSheet } from 'react-native';
import Button from '../../widgets/Button';
import Api from "../../utils/Api";
import G from "../../utils/G";
import Banner from "../../widgets/BannerAuth";
import Modal from "../../utils/Modal";

class RememberPassword extends Component {

    state = {
        email: "",
        errEmail: ""
    };

    componentDidMount() {
        this.emailValidator = G.getEmailValidator();
        this.emailValidator.onError( error => {
            this.setState( state => state.errEmail = error );
        } );
    }

    onBackPress = () => {
        this.props.navigation.navigate("Login");
    };

    submit = async () => {

        if (!this.emailValidator.isValid(this.state.email)) return false;

        try {

            const response = await Api.recoveryPassword({ email: this.state.email, codigo: G.getHash() });

            if (response.result) {
                Modal.alert.show({ message: "El código se envió a tu correo electrónico.", time: 2000 });
                this.props.navigation.navigate("ValidateCode", { email: this.state.email });
            } else {
                Modal.alert.show({ message: response.error });
            }

        } catch (e) {
            Modal.showGenericError(e);
        }

    };

    render() {
        return (
            <ImageBackground
                source={ require('../../assets/img/background.jpg') }
                style={{width: '100%', height: '100%'}}>
                <Banner
                    iconBanner={ require('../../assets/img/bogui_banner.png') }
                    backIcon={ require('../../assets/icons/back.png') }
                    onBackPress={ () => this.props.navigation.navigate("Login")  }/>
                <View style={ styles.container }>
                    <Input
                        label="Ingrese su correo Electrónico"
                        labelStyle={{
                            fontSize: 18,
                            margin: 8,
                        }}
                        leftIcon={ require('../../assets/img/email.png') }
                        hint="Correo Electrónico"
                        onChangeText={ t => {
                            this.setState( state => state.email = t );
                            this.setState( state => state.errEmail = "" );
                        } }
                        onBlur={ () => this.emailValidator.isValid(this.state.email) }
                        errorMessage={ this.state.errEmail } />
                    <Button
                        title="Continuar"
                        type="outline"
                        titleStyle={{fontSize: 20, color: 'black',fontFamily: 'Poppins-Bold'}}
                        containerStyle={{width: "50%", marginTop: 20}}
                        buttonStyle={{borderColor: 'black', paddingVertical: 5, alignItems: 'center', borderWidth: 1, borderRadius: 20}}
                        onPress={ this.submit } />
                </View>
            </ImageBackground>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFill,
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: '4%'
    },
    submit: {
        marginVertical: 20
    }
});

export default RememberPassword;