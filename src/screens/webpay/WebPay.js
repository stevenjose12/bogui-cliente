import React, {Component} from 'react';
import {Modal, StyleSheet, View, Text, ActivityIndicator} from 'react-native';
import WebView from "react-native-webview";
import ModalAlert from "../../utils/Modal";
import Colors from "../../assets/Color";

class WebPay extends Component {

    state = {
        url: null,
    };

    show = url => this.setState({ url });

    dismiss = () => this.setState({ url: null });

    render() {
        return (

            <Modal
                animationType={'fade'}
                transparent={ true }
                visible={ !!this.state.url }
                hideModalContentWhileAnimating={ true }
                useNativeDriver={ true }>

                <View style={ styles.container }>

                    <WebView
                        style={{ width: 0, height: 0 }}
                        source={{ uri: this.state.url }}
                        onLoadStart={ () => ModalAlert.loading.show() }
                        onLoad={ () => ModalAlert.loading.dismiss() } />

                    <View style={ styles.card }>

                        <Text style={ styles.text }> Procesando pago... </Text>

                        <ActivityIndicator />

                    </View>

                </View>

            </Modal>

        );
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.17)',
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    card: {
        backgroundColor: Colors.white,
        width: "80%",
        padding: 10,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
    },
    text: {
        color: Colors.primary,
        fontSize: 15,
        paddingVertical: 10
    }
});

export default WebPay;
