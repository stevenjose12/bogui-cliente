import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import Toast from "react-native-root-toast";
import {
  NavigationActions,
  NavigationEvents,
  StackActions
} from "react-navigation";
import { connect } from "react-redux";
import Colors from "../../assets/Color";
import FirebasePushNotification from "../../plugins/FirebasePushNotification";
import API from "../../utils/Api";
import Globals from "../../utils/G";
import Geolocation from "../../utils/Geolocation";
import { socket } from "../../utils/Socket";
import SocketEvents from "../../utils/SocketEvents";
import Task from "../../utils/Task";
import Button from "../../widgets/Button";
import Input from "../../widgets/InputBogui";
import Modal from "../../utils/Modal";

export const Verification = connect(({ auth: { user } }) => ({
  user
}))(
  class extends Component {
    state = { verificationCode: "" };

    subscribeToVerificationEvent = () =>
      socket.on("usuario-validated", this.updateVerificationStatus);

    unsubscribeFromVerificationEvent = () =>
      socket.removeListener("usuario-validated", this.updateVerificationStatus);

    updateVerificationStatus = ({ user_id }) => {
      const { user, dispatch, navigation } = this.props;
      if (user.id !== user_id) {
        return;
      }

      API.updateProfile(user)
        .then(({ user: newUser }) => {
          dispatch({ type: "SET_USER", payload: { user: newUser } });
          navigation.navigate("Home");
        })
        .catch(err => {
          console.log("Verification: updateVerificationStatus: ", err);
          this.showErrorMessage();
        });
    };

    showErrorMessage = (msg = "Ha ocurrido un error") => Toast.show(msg);

    submitVerificationCode = async () => {
      const { user, dispatch, navigation } = this.props;
      const { verificationCode } = this.state;

      try {
        const { success, message } = await API.verifyUser({
          id: user.id,
          code: verificationCode.toUpperCase()
        });

        if (!success) {
          this.showErrorMessage(message);
          return;
        }

        const { user: newUser } = await API.updateProfile(user);
        dispatch({ type: "SET_USER", payload: { user: newUser } });
        this.showMessage(message);
        navigation.navigate("Home");
      } catch (err) {
        console.log("Verification: submitVerificationCode: ", err);
        this.showErrorMessage();
        Modal.loading.dismiss();
      }
    };

    resendVerificationCode = async () => {
      const { user } = this.props;
      const hash = Globals.getHash();

      try {
        const { success, message } = await API.resendCode({
          id: user.id,
          hash: hash
        });

        if (!success) {
          this.showErrorMessage(message);
          return;
        }

        Globals.sendMessage(
          user.phone,
          "Bienvenido a Bogui, su nuevo código de verificación es: " + hash
        );
        this.showMessage(message);
      } catch (err) {
        console.log("Verification: resendVerificationCode: ", err);
        this.showErrorMessage();
        Modal.loading.dismiss();
      }
    };

    showMessage = msg => Toast.show(msg);

    navigateToSupport = () => this.props.navigation.navigate("Support");

    logout = () => {
      const { dispatch, navigation } = this.props;
      FirebasePushNotification.setLogin(false);
      Geolocation.stop();
      Task.stop();
      socket.disconnect();
      SocketEvents.clear();
      dispatch({ type: "SET_REQUEST", payload: { request: null } });
      dispatch({ type: "SET_ROUTE", payload: { route: null } });
      dispatch({ type: "SET_PLACES", payload: { places: null } });
      dispatch({ type: "SET_USER", payload: { user: null } });
      navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "Login" })]
        })
      );
    };

    render() {
      return (
        <View style={styles.container}>
          <NavigationEvents
            onDidFocus={this.subscribeToVerificationEvent}
            onDidBlur={this.unsubscribeFromVerificationEvent}
          />
          <View style={styles.contentContainer}>
            <Text style={styles.title}>Ingresa el Código</Text>
            <Input
              onChangeText={verificationCode =>
                this.setState({ verificationCode })
              }
              inputStyle={styles.input}
              text={this.state.verificationCode}
              autoCapitalize="characters"
            />
            <Button
              title="Reenviar Código"
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.whiteButton}
              titleStyle={styles.blackButtonTitle}
              onPress={this.resendVerificationCode}
            />
            <Button
              title="Aceptar"
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.blackButton}
              titleStyle={styles.whiteButtonTitle}
              onPress={this.submitVerificationCode}
            />
          </View>
          <View style={styles.bottomButtonContainer}>
            <Button
              containerStyle={styles.bottomButtonButtonContainer}
              buttonStyle={styles.blackButton}
              titleStyle={styles.whiteButtonTitle}
              onPress={this.navigateToSupport}
              title="Soporte"
            />
            <View style={styles.bottomButtonDivider} />
            <Button
              containerStyle={styles.bottomButtonButtonContainer}
              buttonStyle={styles.blackButton}
              titleStyle={styles.whiteButtonTitle}
              onPress={this.logout}
              title="Cerrar Sesión"
            />
          </View>
        </View>
      );
    }
  }
);

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    justifyContent: "center"
  },
  container: {
    height: "100%",
    padding: 16
  },
  input: {
    borderColor: Colors.primary,
    borderRadius: 10,
    borderWidth: 2,
    marginBottom: 24
  },
  whiteButton: {
    alignSelf: "center",
    backgroundColor: "transparent",
    borderColor: Colors.primary,
    borderWidth: 1,
    borderRadius: 7
  },
  whiteButtonTitle: {
    color: Colors.white,
    padding: 10
  },
  blackButton: {
    backgroundColor: Colors.primary,
    borderColor: Colors.primary,
    borderWidth: 1,
    borderRadius: 7
  },
  blackButtonTitle: {
    color: Colors.primary,
    padding: 24
  },
  title: {
    alignSelf: "center",
    color: Colors.primary,
    fontFamily: "Poppins",
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 24
  },
  buttonContainer: {
    alignSelf: "center",
    flexDirection: "row",
    paddingBottom: 16
  },
  bottomButtonDivider: {
    width: 16
  },
  bottomButtonButtonContainer: {
    flex: 1
  },
  bottomButtonContainer: {
    flexDirection: "row",
    width: "100%"
  }
});
