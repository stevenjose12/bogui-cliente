import React, { Component } from "react";
import { StyleSheet, Text, View, Platform, WebView } from "react-native";
import Background from "../../widgets/Background";
import Color from "../../assets/Color";
import Button from "../../widgets/Button";
// import WebView from "react-native-webview";
import env from "../../utils/env";
import { connect } from "react-redux";
import Modal from "../../utils/Modal";
import { socket } from "../../utils/Socket";
import Toast from "../../widgets/Toast";
import Api from "../../utils/Api";
import Hbox from "../../widgets/Hbox";

class AddCard extends Component {
  static navigationOptions = { title: "Tarjeta de Crédito" };

  state = {
    showWebView: false,
    card: null
  };

  componentDidMount() {
    this.init();
    this.addEventsSocket();
  }

  init = async () => {
    await this.setState({ showWebView: false });
    await this.setState({ card: null });
    try {
      const r = await Api.webpayCheckUser(this.props.user.id);
      if (r.result && r.user.card)
        this.setState({ card: "**** **** **** " + r.user.card });
    } catch (e) {
      Modal.showGenericError(e);
    }
  };

  addEventsSocket = () => {
    socket.on("webpay-inscripcion", data => {
      if (data.id != this.props.user.id) return;
      const msg =
        data.status == 1
          ? "Se ha registrado la tarjeta de crédito"
          : "Lo sentimos, no se pudo procesar el registro de la tarjeta";

      if (data.status !== 1) {
        const response = Api.getUserById(this.props.user.id);
        if (response.success) {
          this.props.dispatch({
            type: "SET_USER",
            payload: { user: response.user }
          });
        }
      }
      Toast.show(msg);
      this.init();
    });
  };

  add = () => this.setState({ showWebView: true });

  delete = async () => {
    const onPositiveButton = async () => {
      try {
        const r = await Api.deleteCard(this.props.user.id);
        Toast.show(
          r.result
            ? "Se ha eliminado la tarjeta de crédito"
            : "Lo sentimos no se pudo eliminar su tarjeta"
        );
        this.init();
      } catch (e) {
        Modal.showGenericError(e);
      }
    };
    Modal.alert.onPositiveButton("Si", onPositiveButton);
    Modal.alert.onCancelButton("No");
    Modal.alert.show({ message: "¿Desea eliminar la tarjeta de crédito?" });
  };

  render() {
    return (
      <Background header noBackground noPanicEvent>
        {this.state.showWebView ? (
          <WebView
            source={{
              uri:
                env.BASE_PUBLIC +
                "api/webpay/inscripcion?id=" +
                this.props.user.id
            }}
            onLoadStart={Modal.loading.show}
            onLoadEnd={Modal.loading.dismiss}
            style={{ zIndex: 1000 }}
          />
        ) : (
          <View style={{ padding: 7 }}>
            {this.state.card ? (
              <View>
                <Text style={styles.text}>Tarjeta de Crédito</Text>
                <Text style={styles.text}>{this.state.card}</Text>

                <Hbox>
                  <Button
                    title="Modificar"
                    onPress={this.add}
                    containerStyle={{
                      flex: 1,
                      paddingRight: 2
                    }}
                    buttonStyle={{
                      backgroundColor: Color.primary
                    }}
                  />

                  <Button
                    title="Eliminar"
                    onPress={this.delete}
                    containerStyle={{
                      flex: 1,
                      paddingLeft: 2
                    }}
                    buttonStyle={{
                      backgroundColor: Color.primary
                    }}
                  />
                </Hbox>
              </View>
            ) : (
              <View style={{ width: "100%", alignItems: "center" }}>
                <Text style={styles.text}>
                  No posee una tarjeta de crédito asociada
                </Text>

                <Button
                  title="Agregar"
                  buttonStyle={styles.btn}
                  onPress={this.add}
                />
              </View>
            )}
          </View>
        )}
      </Background>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    color: Color.primary,
    paddingVertical: 10
  },
  btn: {
    backgroundColor: Color.primary,
    color: Color.white,
    borderRadius: 5,
    width: "50%",
    alignSelf: Platform.OS === "ios" ? "center" : null
  }
});

const redux = state => ({ user: state.auth.user });

export default connect(redux)(AddCard);
