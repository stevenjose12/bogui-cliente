import React, { Component } from "react";
import TabsContainer from "../../widgets/TabsContainer";
import CreditCard from "./CreditCard";
import Cash from "./Cash";
import Background from "../../widgets/Background";
import { connect } from "react-redux";
import API from "../../utils/Api";
import Modal from "../../utils/Modal";
import Toast from "../../widgets/Toast";

const tabs = {
  CREDIT_CARD: 0,
  CASH: 1
};

class PayMethods extends Component {
  static navigationOptions = { title: "Métodos de Pago" };

  state = {
    tab: tabs.CASH,
    card: false
  };

  componentDidMount() {
    this.props.navigation.addListener("didFocus", this.didFocus);
  }

  didFocus = async () => {
    try {
      const r = await API.webpayCheck(this.props.user.id);

      this.setState({ card: r.result });
      if (!r.result && this.props.user?.default_payment == 1) {
        Modal.showAlert("Usted no posee tarjeta registrada.");
        this.saveDefaultMethod(2);
      }
    } catch (e) {
      Modal.showGenericError(e);
    }
  };

  saveDefaultMethod = async num => {
    try {
      const r = await API.defaultPayment({ num, id: this.props.user.id });
      if (r.result) {
        // await this.props.dispatch( { type: 'SET_USER', payload: { user: r.user } } );
        // if (r.result) {
        //   await this.props.dispatch({
        //     type: "SET_USER",
        //     payload: { user: r.user }
        //   });
        // }
        console.log(">>: PayMeThods > saveDefault > r ", r);
        const response = await API.getUserById(this.props.user.id);

        if (response.success) {
          console.log(">>: PayMeThods > saveDefault > success", response.user);
          await this.props.dispatch({
            type: "SET_USER",
            payload: { user: response.user }
          });

          Toast.show("Se ha cambiado el método de pago por defecto");
        }

        if (num == 1) {
          const r = await API.webpayCheck(this.props.user.id);
          if (!r.result) this.props.navigation.navigate("AddCard");
        }
      }
    } catch (e) {
      Modal.showGenericError(e);
    }
  };

  editCard = () => this.props.navigation.navigate("AddCard");

  render() {
    return (
      <Background header>
        <TabsContainer
          tabs={["Tarjeta de Crédito", "Efectivo"]}
          onChangeTab={tab => this.setState(state => (state.tab = tab))}
          tabSelected={this.state.tab}
        />

        {this.state.tab === tabs.CREDIT_CARD ? (
          <CreditCard
            onSuccess={this.saveDefaultMethod}
            card={this.state.card}
            edit={this.editCard}
          />
        ) : (
          <Cash onSuccess={this.saveDefaultMethod} />
        )}
      </Background>
    );
  }
}

const redux = state => ({ user: state.auth.user });

export default connect(redux)(PayMethods);
