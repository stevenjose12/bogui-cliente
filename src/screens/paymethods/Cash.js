import React, { Component } from "react";
import { View, Text, StyleSheet, Image, Platform } from "react-native";
import Colors from "../../assets/Color";
import Button from "../../widgets/Button";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/FontAwesome5";

class Cash extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require("../../assets/icons/isotipo.png")}
          style={{ width: "35%", height: "35%" }}
          resizeMode="contain"
        />
        <Text style={styles.title}>
          ¿Deseas establecer métodos de pago por efectivo?
        </Text>
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            width: "100%"
          }}
        >
          {this.props.user?.default_payment == "2" ? (
            <View style={{ flexDirection: "row", width: "100%", padding: 15 }}>
              <Icon
                color={Colors.green}
                size={32}
                style={{ paddingRight: 10 }}
                outline={true}
                name={"check"}
              />
              <Text style={Platform.OS === "ios" ? { width: "80%" } : null}>
                Este es tu método de pago seleccionado
              </Text>
            </View>
          ) : (
            <Button
              title="Activar"
              onPress={() => this.props.onSuccess("2")}
              titleStyle={{ fontSize: 15, color: Colors.primary, padding: 0 }}
              containerStyle={{
                backgroundColor: "transparent",
                width: 160,
                height: 40,
                top: "4%"
              }}
              buttonStyle={{
                backgroundColor: "transparent",
                borderColor: Colors.primary,
                height: 30,
                borderWidth: 1,
                borderRadius: 5
              }}
            />
          )}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "92.1%",
    paddingHorizontal: 30,
    alignItems: "center"
  },
  title: {
    textAlign: "center",
    color: Colors.primary,
    fontFamily: "Poppins",
    fontWeight: "bold"
  }
});
const redux = state => {
  return { user: state.auth.user };
};
export default connect(redux)(Cash);
