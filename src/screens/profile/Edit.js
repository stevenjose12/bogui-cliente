import React, { Component } from "react";
import { StyleSheet, View, Text, ScrollView, Platform } from "react-native";
import { Avatar } from "react-native-elements";
import Item from "./ProfileItem";
import Hbox from "../../widgets/Hbox";
import Input from "../../widgets/InputBogui";
import { connect } from "react-redux";
import env from "../../utils/env";
import G from "../../utils/G";
import Api from "../../utils/Api";
import Modal from "../../utils/Modal";
import Ranting from "../../widgets/Ranting";
import Button from "../../widgets/Button";
import Color from "../../assets/Color";
import ActionSheet from "react-native-actionsheet";
import RNFetchBlob from "rn-fetch-blob";
import Permissions from "../../utils/Permissions";
import Toast from "../../widgets/Toast";

const android = RNFetchBlob.android;

const DownloadActions = {
  OPEN: 0,
  DOWNLOAD: 1
};

class Edit extends Component {
  state = {
    form: {
      id: "",
      email: null,
      phone: null
    },
    error: {
      email: "",
      phone: ""
    },
    rating: 3,
    avatar: "../../assets/icons/user.png"
  };

  componentDidMount() {
    this.emailValidator = G.getEmailValidator();
    this.emailValidator.onError(error =>
      this.setState(state => (state.error.email = error))
    );

    this.setState(state => {
      state.form = {
        id: this.props.user.id,
        email: this.props.user.email,
        phone: this.props.user.phone
      };
      state.avatar = env.BASE_PUBLIC + this.props.user.photo;
      return state;
    });
    // this.forceUpdate();
  }

  submit = async () => {
    if (!this.emailValidator.isValid(this.state.form.email)) return;

    try {
      const response = await Api.updateProfile(this.state.form);
      if (response.result) {
        Toast.show("Se han modificado los datos correctamente");
        this.props.dispatch({
          type: "SET_USER",
          payload: { user: response.user }
        });
      } else {
        Modal.alert.show({ message: response.msg });
      }
    } catch (e) {
      Modal.showGenericError(e);
    }
  };

  openDownloadDocumentOptions = () => this.ActionSheet.show();

  downloadDocumentAction = async action => {
    switch (action) {
      case DownloadActions.OPEN:
        const path = await this.download(this.props.user.document_photo);
        if (path) {
          try {
            android.actionViewIntent(path, "image/png");
          } catch (e) {
            Modal.showGenericError(e);
          }
        }
        break;

      case DownloadActions.DOWNLOAD:
        this.download(this.props.user.document_photo);
        break;
    }
  };

  download = async file => {
    if (Platform.OS === "android") {
      const granted = await Permissions.request("storage");
      if (granted) {
        try {
          return await Api.downloadFile(file);
        } catch (e) {
          Modal.showGenericError(e);
        }
      }
    }
  };

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Avatar
            rounded
            source={{ uri: this.state.avatar }}
            size={110}
            containerStyle={styles.avatar}
          />

          <Ranting
            style={{ marginBottom: 40 }}
            ranting={this.props.user?.average}
          />

          <Item label="Nombre" value={this.props.user?.name} />

          <Item label="Apellido" value={this.props.user?.lastname} />

          <Hbox>
            <Text
              style={{
                color: "black",
                marginRight: 0,
                fontSize: 17,
                textAlignVertical: "center"
              }}
            >
              Email:
            </Text>

            <Input
              style={{ flex: 1 }}
              text={this.state.form.email}
              onChangeText={email =>
                this.setState(state => (state.form.email = email))
              }
              errorMessage={this.state.error.email}
              onBlur={() => this.emailValidator.isValid(this.state.form.email)}
            />
          </Hbox>

          <Item label="RUT ó Pasaporte" value={this.props.user?.document} />

          <Hbox>
            <Text
              style={{
                color: "black",
                marginRight: 0,
                fontSize: 17,
                textAlignVertical: "center"
              }}
            >
              Teléfono:
            </Text>

            <Input
              style={{ flex: 1 }}
              onChangeText={phone =>
                this.setState(state => (state.form.phone = phone))
              }
              text={this.state.form.phone}
              keyboardType="numeric"
            />
          </Hbox>

          {this.props.user?.document_photo && (
            <Button
              title="Descargar imagen de RUT o Pasaporte"
              titleStyle={{
                fontSize: 11,
                color: Color.primary,
                width: "100%"
              }}
              buttonStyle={{
                backgroundColor: Color.white,
                width: "100%",
                borderRadius: 0,
                borderColor: Color.secondary,
                borderWidth: 1,
                marginVertical: 15
              }}
              onPress={this.openDownloadDocumentOptions}
            />
          )}

          <Button
            title="Actualizar"
            type="outline"
            titleStyle={{ fontSize: 20, color: "black" }}
            containerStyle={{ width: "50%", marginTop: 20 }}
            buttonStyle={{
              borderColor: "black",
              paddingVertical: 5,
              alignItems: "center",
              borderWidth: 1,
              borderRadius: 20
            }}
            onPress={this.submit}
          />
        </View>

        <ActionSheet
          ref={o => (this.ActionSheet = o)}
          title="Opciones"
          options={["Descargar y Abrir", "Descargar", "Cancelar"]}
          cancelButtonIndex={2}
          onPress={index => this.downloadDocumentAction(index)}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    padding: 10
  },
  avatar: {
    marginBottom: 10
  }
});

export default connect(state => ({ user: state.auth.user }))(Edit);
