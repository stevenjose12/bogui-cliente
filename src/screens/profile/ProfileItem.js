import React from 'react';
import { View, Text } from "react-native";
import Hbox from "../../widgets/Hbox";

const ProfileItem = props => (
    <Hbox style={ { paddingVertical: 13 } }>

        <Text style={ { color: 'black', marginRight: 5, fontSize: 17 } }>
            { props.label+":" }
        </Text>

        <Text style={ { color: 'black', marginRight: 5, fontSize: 17 } }>
            { props.value }
        </Text>

    </Hbox>
);

export default ProfileItem;