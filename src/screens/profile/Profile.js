import React, { Component } from 'react';
import TabsContainer from "../../widgets/TabsContainer";
import ChangePassword from "./ChangePassword";
import Edit from "./Edit";
import Background from "../../widgets/Background";
import { connect } from "react-redux";
import Api from "../../utils/Api";

const tabs = {
    PROFILE: 0,
    PASSWORD: 1
};

class Profile extends Component {

    static navigationOptions = { title: "Mi Perfil" };

    state = {
        tab: tabs.PROFILE
    };

    componentDidMount() {
        this.props.navigation.addListener('didFocus', this.didFocus );
    }

    didFocus = async () => {
        try {
            const r = await Api.getUserById( this.props.user.id );
            if (r.success) await this.props.dispatch( { type: 'SET_USER', payload: { user: r.user } } );
        } catch (e) {
        }
    };

    render() {
        return (
            <Background header>

                <TabsContainer
                    tabs={ ['Editar Datos', 'Cambiar Contraseña'] }
                    onChangeTab={ tab => this.setState( { tab } ) }
                    tabSelected={ this.state.tab } />

                {
                    this.state.tab === tabs.PROFILE
                        ? <Edit />
                        : <ChangePassword onSuccess={ () => this.setState( { tab: tabs.PROFILE } ) } />
                }

            </Background>
        )
    }

}

export default connect( state => ({ user: state.auth.user }) )(Profile);