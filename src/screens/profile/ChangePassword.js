import React, { Component } from 'react';
import { StyleSheet, View } from "react-native";
import Input from "../../widgets/InputBogui";
import Api from "../../utils/Api";
import { connect } from "react-redux";
import Modal from "../../utils/Modal"
import G from "../../utils/G";
import Button from "../../widgets/Button";

class ChangePassword extends Component {

    state = {
        form: {
            id: "",
            old_password: "",
            password: "",
            password_confirmation: ""
        },
        repeatPsw: "",
        eOldPsw: "",
        eNewPsw: ""
    };

    componentDidMount() {
        this.passwordValidator = G.getPasswordValidator();
        this.passwordValidator.onError( error => this.setState( state => state.eNewPsw = error )  );
        this.setState( state => state.form.id = this.props.user.id );
    }

    submit = async () => {

        if ( !this.passwordValidator.validate(this.state.form.password, this.state.form.password_confirmation) ) return false;

        try {

            Modal.loading.show();
            const response = await Api.changePasswordProfile(this.state.form);
            Modal.loading.dismiss();

            if (response.result) {
                Modal.alert.show({message: "Se ha cambiado la contraseña correctamente", time: 2000});
                if (this.props.onSuccess) this.props.onSuccess();
            } else {
                this.setState(state => state.eOldPsw = response.error);
            }

        } catch (e) {
            Modal.loading.dismiss();
            Modal.alert.show({message: "Disculpe ha ocurrido un error."});
        }

    };

    render() {
        return(

            <View style={ styles.container }>

                {/* CURRENT PASSWORD */}
                <Input
                    leftIcon={ require('../../assets/img/pass.png') }
                    label="Contraseña Actual"
                    hint="Contraseña"
                    onChangeText={ t => {
                        this.setState( state => state.form.old_password = t );
                        this.setState( state => state.eOldPsw = "" );
                    } }
                    errorMessage={ this.state.eOldPsw }
                    password
                    onFocus={ () => this.setState( state => state.eOldPsw = "" ) } />

                {/* NEW PASSWORD */}
                <Input
                    leftIcon={ require('../../assets/img/pass.png') }
                    label="Contraseña Nueva"
                    hint="Contraseña"
                    onChangeText={ t => {
                        this.setState( state => state.form.password = t );
                        this.setState( state => state.eNewPsw = "" );
                    } }
                    errorMessage={ this.state.eNewPsw }
                    password />

                {/* CONFIRM PASSWORD */}
                <Input
                    leftIcon={ require('../../assets/img/pass.png') }
                    label="Repetir Contraseña nueva"
                    hint="Contraseña"
                    onChangeText={ t => {
                        this.setState( state => state.form.password_confirmation = t );
                        this.setState( state => state.eNewPsw = "" );
                    } }
                    password
                    onBlur={ () => this.passwordValidator.validate(this.state.form.password, this.state.form.password_confirmation) }/>

                <Button
                    title="Actualizar"
                    type="outline"
                    titleStyle={{fontSize: 20, color: 'black'}}
                    containerStyle={{width: "50%", marginTop: 20}}
                    buttonStyle={{borderColor: 'black', paddingVertical: 5, alignItems: 'center', borderWidth: 1, borderRadius: 20}}
                    onPress={ this.submit } />

            </View>

        )
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        alignItems: "center",
        paddingVertical: 20
    },
    body: {
        width: "100%",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    submit: {
        marginVertical: 20
    }
});

const redux = state => { return { user: state.auth.user, modal: state.modal.modal } };
export default connect( redux )(ChangePassword);