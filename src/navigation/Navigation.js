import { createStackNavigator, createAppContainer } from 'react-navigation';
import React from 'react';
import Home from "../screens/home/Home";
import Login from "../screens/login/Login";
import Register from "../screens/register/Register";
import RememberPassword from "../screens/rememberpassword/RememberPassword";
import ValidateCode from "../screens/validatecode/ValidateCode";
import ChangePassword from "../screens/changepassword/ChangePassword";
import Drawer from "./drawer/Drawer";

const AppNavigator = createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions: {
            header: null,
        }
    },
    Drawer: {
        screen: Drawer,
        navigationOptions: {
            header: null,
        }
    },
    Register:{
        screen: Register,
        navigationOptions: {
            header: null,
        }
    },
    RememberPassword: {
        screen: RememberPassword,
        navigationOptions: {
            header: null,
        }
    },
    ValidateCode: {
        screen: ValidateCode,
        navigationOptions: {
            header: null,
        }
    },
    ChangePassword: {
        screen: ChangePassword,
        navigationOptions: {
            header: null,
        }
    },
    Home: {screen: Home},
},{
    initialRouteName: "Login"
});

export default createAppContainer(AppNavigator);