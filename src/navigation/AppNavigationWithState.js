import { connect } from 'react-redux';
import { createReduxContainer } from 'react-navigation-redux-helpers';
import AppNavigator from './Navigation';
import { BackHandler, AppState, DeviceEventEmitter } from 'react-native';

const ReduxifyApp = createReduxContainer(AppNavigator, 'root');

class AppNavigationWithState extends ReduxifyApp {

    state = {
        interval: null,
        appState: AppState.currentState,
        accepted: 0,
        enableNotification:  true
    };

}

function mapStateToProps(state) {
    return {
        state: state.navigation,
    }
}

export default connect(mapStateToProps)(AppNavigationWithState);