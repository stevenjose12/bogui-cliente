import React from 'react';
import { createStackNavigator } from 'react-navigation';
import Logout from '../../screens/logout/logout';

const LogoutNavigator = createStackNavigator({
	Logout: Logout,
},{
	initialRouteName: "Logout"
});

export default LogoutNavigator;