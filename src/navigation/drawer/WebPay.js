import React from 'react';
import { createStackNavigator } from 'react-navigation';
import NavigationOptions from './MenuButton';
import WebPay from "../../screens/webpay/WebPay";

const WebPayNav = createStackNavigator(
    {
        WebPay: WebPay,
    },
    {
        initialRouteName: "WebPay",
        defaultNavigationOptions: NavigationOptions
    }
);

export default WebPayNav;