import React from 'react';
import { createStackNavigator } from 'react-navigation';
import PayMethods from '../../screens/paymethods/PayMethods';
import NavigationOptions from './MenuButton';
import AddCard from "../../screens/paymethods/AddCard";
import WebPay from "../../screens/paymethods/WebPay";

const PayMethodsNavigator = createStackNavigator(
    {
        PayMethods: PayMethods,
        AddCard: AddCard,
        WebPay: WebPay
    },
    {
        initialRouteName: "PayMethods",
        defaultNavigationOptions: NavigationOptions
    }
);

export default PayMethodsNavigator;