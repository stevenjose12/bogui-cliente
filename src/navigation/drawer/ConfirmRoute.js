import React from 'react';
import { createStackNavigator } from 'react-navigation';
import ConfirmRoute from '../../screens/confirmroute/ConfirmRoute';
import NavigationOptions from './MenuButton';

const ConfirmRouteNavigator = createStackNavigator(
    {
        ConfirmRoute: ConfirmRoute,
    },
    {
        initialRouteName: "ConfirmRoute",
        defaultNavigationOptions: NavigationOptions
    }
);

export default ConfirmRouteNavigator;