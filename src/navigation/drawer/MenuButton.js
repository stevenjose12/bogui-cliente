import React from 'react';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import Header from '../../widgets/Header';
import Colors from '../../assets/Color';
import BackButton from '../../assets/icons/back_button.png';

const MenuButton = props => {

    if (props.canGoBack)
        return (
            <TouchableOpacity onPress={ props.onPress }>
                <Image source={ BackButton } style={ styles.image } />
            </TouchableOpacity>
        );

    return (
        <Button
            buttonStyle={ styles.button }
            type="clear"
            icon={
                <Icon
                    color={ props.iconColor ? props.iconColor : props.containerDefault ? null : Colors.accent }
                    size={ 20 }
                    name={ 'bars' } />
            }
            onPress={ props.onPress } />
    )

};

const NavigationOptions = ({ navigation }) => {

    let index = navigation.dangerouslyGetParent().state.index;

    return {
        header: props => (
            <Header
                style={ { height: 40 } }
                { ...props }
                { ...navigation.state.params }
                headerLeft={
                    <MenuButton
                        { ...navigation.state.params }
                        canGoBack={ index > 0 }
                        onPress={ () => index > 0 ? navigation.goBack(null) : navigation.toggleDrawer() } />
                } />
        )
    }
};

const styles = StyleSheet.create({
    button: {
        marginLeft: 0,
        width: 40,
        height: 40,
        borderRadius: 0
    },
    image: {
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginLeft: 5
    }
});

export default NavigationOptions;