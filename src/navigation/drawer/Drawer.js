import { createDrawerNavigator } from "react-navigation";
import Home from "./Home";
import Profile from "./Profile";
import Menu from "./Menu";
import Notifications from "./Notifications";
import Chat from "./Chat";
import Travels from "./Travels";
import Logout from "./Logout";
import Support from "./Support";
import PayMethods from "./PayMethods";
import PromotionalCode from "./PromotionalCode";
import WebPay from "./WebPay";
import MyTravel from "./MyTravel";
import EditRouteDrawer from "./EditRoute";
import ContactsDrawer from "./Contacts";
import { Verification } from "../../screens/verification";

const Drawer = createDrawerNavigator(
  {
    ProfileDrawer: Profile,
    HomeDrawer: {
      screen: Home,
      navigationOptions: {
        drawerLockMode: "locked-closed"
      }
    },
    NotificationsDrawer: Notifications,
    ChatDrawer: Chat,
    TravelsDrawer: Travels,
    LogoutDrawer: Logout,
    PayMethodsDrawer: PayMethods,
    PromotionalCodeDrawer: PromotionalCode,
    SupportDrawer: Support,
    WebPay: WebPay,
    MyTravelDrawer: MyTravel,
    EditRouteDrawer: EditRouteDrawer,
    ContactsDrawer: ContactsDrawer,
    Verification
  },
  {
    initialRouteName: "HomeDrawer",
    contentComponent: Menu
  }
);

export default Drawer;
