import React from 'react';
import { createStackNavigator } from 'react-navigation';
import Profile from '../../screens/profile/Profile';
import NavigationOptions from './MenuButton';

const ProfileNavigator = createStackNavigator({
    Profile: Profile,
},{
    initialRouteName: "Profile",
    defaultNavigationOptions: NavigationOptions
});

export default ProfileNavigator;