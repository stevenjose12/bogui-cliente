import React from 'react';
import { createStackNavigator } from 'react-navigation';
import Chat from '../../screens/chat/Chat';
import NavigationOptions from './MenuButton';
import Messages from '../../screens/chat/view_chat';

const ChatNavigator = createStackNavigator(
    {
        Chat: Chat,
        Messages: Messages,
    },
    {
        initialRouteName: "Chat",
        defaultNavigationOptions: NavigationOptions
    }
);

export default ChatNavigator;