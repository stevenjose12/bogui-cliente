import React from 'react';
import { createStackNavigator } from 'react-navigation';
import Travels from '../../screens/travels/Travels';
import NavigationOptions from './MenuButton';
import HistoryDetails from "../../screens/historydetails/HistoryDetails";
import Messages from "../../screens/chat/view_chat";

const TravelsNavigator = createStackNavigator(
    {
        Travels: Travels,
        HistoryDetails: HistoryDetails,
        Messages: Messages,
    },
    {
        initialRouteName: "Travels",
        defaultNavigationOptions: NavigationOptions
    }
);

export default TravelsNavigator;