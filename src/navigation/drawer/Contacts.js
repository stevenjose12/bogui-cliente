import React from 'react';
import { createStackNavigator } from 'react-navigation';
import Contacts from "../../screens/contacts/Contacts";

const ContactsDrawer = createStackNavigator(
    { Contacts },
    { initialRouteName: "Contacts" }
);

export default ContactsDrawer;