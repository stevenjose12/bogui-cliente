import React from 'react';
import { createStackNavigator } from 'react-navigation';
import Notifications from '../../screens/notifications/Notifications';
import NavigationOptions from './MenuButton';
import PendingDocuments from "../../screens/pendingdocuments/PendingDocuments";

const NotificationsNavigator = createStackNavigator(
    {
        Notifications: Notifications,
        PendingDocuments: PendingDocuments
    },
    {
        initialRouteName: "Notifications",
        defaultNavigationOptions: NavigationOptions
    }
);

export default NotificationsNavigator;