import React from 'react';
import { createStackNavigator } from 'react-navigation';
import RouteDetails from "../../screens/routedetails/RouteDetails";
import NavigationOptions from './MenuButton';

const RouteDetailsNavigation = createStackNavigator(
    {
        RouteDetails: RouteDetails,
    },
    {
        initialRouteName: "RouteDetails",
        defaultNavigationOptions: NavigationOptions
    }
);

export default RouteDetailsNavigation;