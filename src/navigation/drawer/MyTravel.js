import React from 'react';
import { createStackNavigator } from 'react-navigation';
import NavigationOptions from './MenuButton';
import MyTravel from "../../screens/routeconfirmed/MyTravel";

const MyTravelNavigation = createStackNavigator(
    {
        MyTravel: MyTravel,
    },
    {
        initialRouteName: "MyTravel",
        defaultNavigationOptions: NavigationOptions
    }
);

export default MyTravelNavigation;