import React from "react";
import { createStackNavigator } from "react-navigation";
import Home from "../../screens/home/Home";
import NavigationOptions from "./MenuButton";
import ConfirmRoute from "../../screens/confirmroute/ConfirmRoute";
import CancelRoute from "../../screens/routedetails/RouteDetails";
import RouteConfirmed from "../../screens/routeconfirmed/RouteConfirmed";
import Current from "../../screens/travels/Current";
import PendingDocuments from "../../screens/pendingdocuments/PendingDocuments";
import AddCard from "../../screens/paymethods/AddCard";
import Bill from "../../widgets/Bill";

const HomeNavigator = createStackNavigator(
  {
    Home: Home,
    CancelRoute: CancelRoute,
    ConfirmRoute: ConfirmRoute,
    RouteConfirmed: RouteConfirmed,
    Current: Current,
    PendingDocuments: PendingDocuments,
    AddCard: AddCard,
    Bill: Bill
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: NavigationOptions
  }
);

export default HomeNavigator;
