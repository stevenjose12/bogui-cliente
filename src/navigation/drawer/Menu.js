import React, { Component } from "react";
import {
  Platform,
  ScrollView,
  StyleSheet,
  Image,
  Text,
  View,
  TouchableOpacity,
  PushNotificationIOS
} from "react-native";
import { connect } from "react-redux";
import ItemDrawer from "../../widgets/ItemDrawer";
import Geolocation from "../../utils/Geolocation";
import { socket } from "../../utils/Socket";
import DeviceInfo from "react-native-device-info";
import Api from "../../utils/Api";
import Share from "react-native-share";
import Toast from "../../widgets/Toast";
import Task from "../../utils/Task";
import Logo from "./Logo";
import SocketEvents from "../../utils/SocketEvents";
import FirebasePushNotification from "../../plugins/FirebasePushNotification";
import PushNotification from "react-native-push-notification";

const icons = {
  home: require("../../assets/icons/menu_home.png"),
  profile: require("../../assets/icons/menu_profile.png"),
  notifications: require("../../assets/icons/menu_notifications.png"),
  chat: require("../../assets/icons/menu_chat.png"),
  miViaje: require("../../assets/icons/menu_mi_viaje.png"),
  paymentMethod: require("../../assets/icons/menu_payment_method.png"),
  codePromotion: require("../../assets/icons/menu_code_promotion.png"),
  share: require("../../assets/icons/menu_share.png"),
  support: require("../../assets/icons/menu_support.png"),
  contacts: require("../../assets/icons/address-book.png")
};

function Section(title, route, icon) {
  return {
    title,
    route,
    icon
  };
}

class Menu extends Component {
  constructor(props) {
    super(props);

    if (Platform.OS === "ios") {
      PushNotification.configure({
        onNotification: notification => {
          this.props.navigation.navigate(notification.data.page);
          notification.finish(PushNotificationIOS.FetchResult.NoData);
        }
      });
    }
  }

  disabledSections =
    Platform.OS === "ios"
      ? [Section("Inicio", "Verification", icons.home)]
      : [Section("Inicio", "HomeDrawer", icons.home)];

  enabledSections = [
    Section("Inicio", "HomeDrawer", icons.home),
    Section("Mi Perfil", "ProfileDrawer", icons.profile),
    Section("Notificaciones", "NotificationsDrawer", icons.notifications),
    Section("Chat", "ChatDrawer", icons.chat),
    Section("Mi Viaje", "TravelsDrawer", icons.miViaje),
    Section("Método de pago", "PayMethodsDrawer", icons.paymentMethod),
    Section("Código Promocional", "PromotionalCodeDrawer", icons.codePromotion),
    Section("Compartir Bogui", () => this.share(), icons.share),
    Section("Contactar Soporte", "SupportDrawer", icons.support),
    Section("Contactos", "ContactsDrawer", icons.contacts)
  ];

  componentDidMount() {
    socket.on("login", data => {
      if (
        this.props.user &&
        data.id == this.props.user.id &&
        DeviceInfo.getUniqueID() != data.uuid
      ) {
        Toast.show("Se ha iniciado sesión en otro dispositivo");
        this.logout();
      }
    });

    Api.getStore()
      .then(r => {
        this.googlePlay = r.store.google;
        this.appStore = r.store.apple;
      })
      .catch(e =>
        console.log(">>: Menu > componentDidMount > getStore > e: ", e)
      );
  }

  share = () => {
    Share.open({
      title: "Compartir Bogui",
      subject: "Descarga Bogui",
      message:
        "Descarga Bogui en " +
        (Platform.OS === "android" ? "Google Play" : "App Store") +
        "\n" +
        (Platform.OS === "android" ? this.googlePlay : this.appStore),
      url: Logo
    });
  };

  logout = () => {
    try {
      FirebasePushNotification.setLogin(false);
      SocketEvents.clear();
      socket.disconnect();
      Geolocation.stop();
      Task.stop();
      this.props.dispatch({ type: "SET_REQUEST", payload: { request: null } });
      this.props.dispatch({ type: "SET_ROUTE", payload: { route: null } });
      this.props.dispatch({ type: "SET_PLACES", payload: { places: null } });
      this.props.dispatch({ type: "SET_USER", payload: { user: null } });
      this.props.navigation.replace("Login");
    } catch (e) {
      console.log(">>: Menu > logout > error: ", e);
    }
  };

  render() {
    const sections =
      this.props.user && this.props.user.validate
        ? this.enabledSections
        : this.disabledSections;

    return (
      <ScrollView style={styles.container}>
        <View style={styles.containerImage}>
          <Image
            style={styles.logo}
            source={require("../../assets/icons/banner_bogui.png")}
          />
        </View>

        {sections.map(s => (
          <ItemDrawer
            key={s.title}
            text={s.title}
            icon={s.icon}
            onPress={() => {
              if (typeof s.route == "function") {
                s.route();
              } else {
                this.props.navigation.navigate(s.route);
              }
            }}
          />
        ))}

        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            paddingBottom: 10,
            paddingTop: 30
          }}
        >
          <TouchableOpacity
            style={{ alignItems: "center", justifyContent: "center" }}
            onPress={this.logout}
          >
            <Image
              source={require("../../assets/icons/logout.png")}
              resizeMode="contain"
              style={{ width: 30, height: 30 }}
            />

            <Text style={{ color: "white" }}>Cerrar Sesión</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "black"
  },
  logo: {
    width: "70%",
    height: 100,
    resizeMode: "contain"
  },
  containerImage: {
    width: "100%",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#f4f4f4",
    paddingBottom: 20
  }
});

const mapStateToProps = state => ({
  user: state.auth.user
});

export default connect(mapStateToProps)(Menu);
