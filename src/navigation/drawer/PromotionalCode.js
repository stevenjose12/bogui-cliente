import React from 'react';
import { createStackNavigator } from 'react-navigation';
import PromotionalCode from '../../screens/promotionalcode/PromotionalCode';
import NavigationOptions from './MenuButton';

const PromotionalCodeNavigator = createStackNavigator(
    {
        PromotionalCode: PromotionalCode,
    },
    {
        initialRouteName: "PromotionalCode",
        defaultNavigationOptions: NavigationOptions
    }
);

export default PromotionalCodeNavigator;