import React from 'react';
import { createStackNavigator } from 'react-navigation';
import EditRoute from "../../screens/editroute/EditRoute";

const EditRouteDrawer = createStackNavigator(
    { EditRoute },
    { initialRouteName: "EditRoute" }
);

export default EditRouteDrawer;