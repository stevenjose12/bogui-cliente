import { createStackNavigator } from "react-navigation";
import Chat from "../../screens/chat/Chat";
import Messages from "../../screens/chat/view_chat";
import Support from "../../screens/support/Support";
import MenuButton from "./MenuButton";

const SupportNavigator = createStackNavigator(
  {
    Support,
    Chat,
    Messages
  },
  {
    initialRouteName: "Support",
    defaultNavigationOptions: MenuButton
  }
);

export default SupportNavigator;
