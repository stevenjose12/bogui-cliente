const Color = {
    primary: '#000',
    secondary: '#5b5b5f',
    accent: '#F70000',
    white: '#fff',
    gray: '#5B5B5F',
    chat: '#89b0ce',
    green: '#32db64',
    blue: '#00135B',
    secondaryGray: '#a3a5a7',
};

export default Color;