const Sound = require('react-native-sound');

Sound.setCategory('Playback');

const createSound = sound => new Sound(sound, Sound.MAIN_BUNDLE,e => {
	console.log(e);
});

const Button = createSound('boton.mp3');
const Accepted = createSound('aceptada.mp3');
const Finished = createSound('finalizada.mp3');
const Call = createSound('llamada.mp3');
const Notification = createSound('notification.mp3');
const InCall = createSound('llamadaentrante.mp3');

export default {
    InCall,
    Button,
    Accepted,
    Finished,
    Call,
    Notification
}