import BackgroundGeolocation from "@mauron85/react-native-background-geolocation";
import { socket } from "../utils/Socket";

class Geolocation {
  static location;
  static watchId;
  static onUpdateCallback;
  static onErrorCallback;
  static isStart = false;
  static onReadyLocationCallback;

  static start() {
    console.log(">>: Goolocalitations > start");
    BackgroundGeolocation.configure({
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      stationaryRadius: 50,
      distanceFilter: 50,
      notificationTitle: "Bogui",
      notificationText: "Monitoreando",
      debug: false,
      startOnBoot: true,
      stopOnTerminate: false,
      locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
      interval: 1000,
      fastestInterval: 1000,
      activitiesInterval: 1000,
      stopOnStillActivity: false
    });

    if (!this.isStart) {
      BackgroundGeolocation.on("location", location => {
        this.setLocation(location);
        if (!socket.connected) socket.connect();
      });

      BackgroundGeolocation.on("error", error => {
        this.stop();
        if (this.onErrorCallback) this.onErrorCallback(error);
      });
    }

    BackgroundGeolocation.start();
    this.isStart = true;
  }

  static stop() {
    this.isStart = false;
    BackgroundGeolocation.removeAllListeners();
  }

  static getCurrentLocation() {
    return this.location;
  }

  static onUpdate(onUpdateCallback) {
    this.onUpdateCallback = onUpdateCallback;
  }

  static onError(onErrorCallback) {
    this.onErrorCallback = onErrorCallback;
  }

  static setLocation(location) {
    if (!this.location && this.onReadyLocationCallback)
      this.onReadyLocationCallback(location);
    this.location = location;
    if (this.onUpdateCallback) this.onUpdateCallback(this.location);
  }

  static getLatLng(location = null) {
    return {
      latitude: location?.latitude || this.location.latitude,
      longitude: location?.longitude || this.location.longitude
    };
  }

  static onReadyLocation(onReadyLocationCallback) {
    this.onReadyLocationCallback = onReadyLocationCallback;
    if (this.location) this.onReadyLocationCallback(this.location);
  }
}

export default Geolocation;
