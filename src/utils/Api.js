import axios from "./Axios";
import Modal from "./Modal"
import DownloadFile from "./DownloadFile";
import env from "./env";
import { Platform } from "react-native";

class Api {

    static async getDisplayRadius() {
        const r = await axios.post("routes/get-radius");
        return r.data.radius*1000;
    }

    static async login(data) {
        Modal.loading.show();
        const r = await axios.post("login", data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async getUserById(id: number) {
        Modal.loading.show();
        const r = await axios.post("user/get-user/"+id);
        Modal.loading.dismiss();
        return r.data;
    }

    static async recoveryPassword(data) {
        Modal.loading.show();
        const r = await axios.post("recuperar", data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async codeValidator(data) {
        const r = await axios.post("validar", data);
        return r.data;
    }

    static async changePassword(data) {
        Modal.loading.show();
        const r = await axios.post("reset", data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async changePasswordProfile(data) {
        const r = await axios.post("profile/password", data);
        return r.data;
    }

    static async phoneAreaCode(data) {
        Modal.loading.show();
        const r = await axios.post("countries/get-phone-code", data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async register(data) {
        Modal.loading.show();
        const r = await axios.post("register/form", data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async updateProfile(data) {
        Modal.loading.show();
        const r = await axios.post("profile/post", data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async verifyUser(data) {
        Modal.loading.show();
        const r = await axios.post("register/validarMail", data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async resendCode(data) {
        Modal.loading.show();
        const r = await axios.post("regiseter/resendmail", data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async welcomeModal() {
        // Modal.loading.show();
        const r = await axios.post("information");
        // Modal.loading.dismiss();
        return r.data;
    }

    static async vehiclesAvailable() {
        const r = await axios.get('vehicles/type');
        return r.data;
    }

    static async defaultPayment(data) {
        Modal.loading.show();
        const r = await axios.post('default-payment', data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async webpayCheckUser(userId) {
        Modal.loading.show();
        const r = await axios.post('api-driver/auth/check-user', { id: userId });
        Modal.loading.dismiss();
        return r.data;
    }

    static async deleteCard(userId) {
        Modal.loading.show();
        const r = await axios.post('api/webpay/delete', { id: userId }, { baseURL: env.BASE_PUBLIC });
        Modal.loading.dismiss();
        return r.data;
    }
    
    static async downloadFile(file) {
        Modal.loading.show();
        const r = await DownloadFile.download(env.BASE_PUBLIC+file);
        Modal.loading.dismiss();
        return r.data;
    }

    static async getTariffs(data) {
        const r = await axios.post("tarifas/all", data);
        return r.data;
    }

    static async getAround(data) {
        const r = await axios.post("user/getCercano", data);
        return r.data;
    }

    static async isAirport(data) {
        const r = await axios.post("route/is-airport", data);
        return r.data;
    }

    static async isPromotion(userId) {
        const r = await axios.post("promocion/get", { id: userId });
        return r.data;
    }

    static async sendRequest(data) {
        Modal.loading.show();
        const r = await axios.post("routes", data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async reSendRequest(data) {
        Modal.loading.show();
        const r = await axios.post("routes/clone", data);
        Modal.loading.dismiss();
        return r.data;
    }
    
    static async autoComplete(input) {
        const r = await axios.post(env.SOCKET + '/google/autocomplete', {
            input: input
        });
        return r.data;
    }

    static async placeID(id) {
        const r = await axios.post(env.SOCKET + '/google/place-id', { id: id});
        return r.data;
    }

    static async getDriver(id) {
        const r = await axios.post("user/get-driver", { id: id });
        return r.data;
    }

    static async cancelRoute(data) {
        Modal.loading.show();
        const r = await axios.post("api-driver/routes/cancelar", data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async cancelRouteByRouteId(routeId) {
        Modal.loading.show();
        const r = await axios.post("route/cancelar", {id: routeId});
        Modal.loading.dismiss();
        return r.data;
    }

    static async cancelRouteByModal(data) {
        Modal.loading.show();
        const r = await axios.post("route/cancel/reject", data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async routeAccepted(routeId) {
        const r = await axios.post("routes/accepted", { route_id: routeId });
        return r.data;
    }

    static async chatExist(routeId) {
        const r = await axios.post("chat/exist", { id: routeId });
        return r.data;
    }

    static async chatCreate(data) {
        Modal.loading.show();
        const r = await axios.post('chat/create-chat', data);
        Modal.loading.dismiss();
        return r.data;
    }

    static async routeValid(userId) {
        const r = await axios.post("route/valid", { id: userId});
        return r.data;
    }

    static async routePayable(userId) {
        const r = await axios.post("route/por-pagar", { id: userId});
        return r.data;
    }

    static async promotionByRoute(routeId) {
        const r = await axios.post("promocion/get-by-route", { id: routeId});
        return r.data;
    }

    static async payRoute(params) {
        Modal.loading.show();
        const r = await axios.post("routes/pagar", params);
        Modal.loading.dismiss();
        return r.data;
    }

    static async qualify(params) {
        // Modal.loading.show();
        const r = await axios.post("routes/calificar", params);
        // Modal.loading.dismiss();
        return r.data;
    }

    static async getStore() {
        const r = await axios.post("store/get", {nivel: 1});
        return r.data;
    }

    static async paymentUpdate(params) {
        Modal.loading.show();
        const r = await axios.post("payment/update", params);
        Modal.loading.dismiss();
        return r.data;
    }

    static async getChats(params, page = 1) {
        Modal.loading.show();
        const r = await axios.post(`chat/get?page=${page}`, params);
        Modal.loading.dismiss();
        return r.data;
    }

    static async getOneChat(params) {
        Modal.loading.show();
        const r = await axios.post("chat/load-one", params);
        Modal.loading.dismiss();
        return r.data;
    }

    static async chatView(params) {
        Modal.loading.show();
        const r = await axios.post('chat/view', params);
        Modal.loading.dismiss();
        return r.data;
    }
    
    static async chatSeen(params) {
        const r = await axios.post('chat/seen', params);
        return r.data;
    }

    static async chatDelete(params) {
        Modal.loading.show();
        const r = await axios.post('chat/delete', params);
        Modal.loading.dismiss();
        return r.data;
    }

    static async getRoutesByUser(userId) {
        const r = await axios.post("routes/get", {id: userId});
        return r.data;
    }

    static async editRoute(params) {
        Modal.loading.show();
        const r = await axios.post('route/get-current/edit', params);
        Modal.loading.dismiss();
        return r.data;
    }

    static async isNight() {
        const r = await axios.post('api-driver/night/get');
        return r.data;
    }

    static async getChatIssues() {
        const r = await axios.post('chat/asuntos');
        return r.data;
    }

    static async createSupportChat(issueId, userId, routeId) {
        const r = await axios.post('support/create', { type: issueId, id: userId, route_id: routeId });
        return r.data;
    }

    static async getDirections(origin, destination, waypoints = null) {

        const data = (waypoints && waypoints.length>0)
            ? { origin, destination, waypoints }
            : { origin, destination };

        const route = env.SOCKET+'/google/directions';
        const r = await axios.post(route, data );

        let duration = 0;
        let distance = 0;

        if (!!r?.data?.routes[0]?.legs?.length) {
            for (let i=0; i<r.data.routes[0].legs.length; i++) {
                const leg = r.data.routes[0].legs[i];
                duration += leg.duration.value;
                distance += leg.distance.value;
            }
        }

        return {
            duration,
            distance,
            routes: r?.data?.routes[0]?.overview_polyline?.points
        };

    }

    static async getNotifications(page, userId) {
        Modal.loading.show();
        const r = await axios.post('api-driver/notifications/get?page='+page, { id: userId });
        Modal.loading.dismiss();
        return r.data;
    }

    static async getRouteById(routeId) {
        Modal.loading.show();
        const r = await axios.post('route/get-route-specific', { id: routeId });
        Modal.loading.dismiss();
        return r.data;
    }

    static async getPendingDocuments(userId, showLoading = false) {
        if (showLoading) Modal.loading.show();
        const r = await axios.post('documents/get', { id: userId });
        if (showLoading) Modal.loading.dismiss();
        return r.data;
    }

    static async getPromotions(userId) {
        Modal.loading.show();
        const r = await axios.post('promocion/get', { id: userId });
        Modal.loading.dismiss();
        return r.data;
    }

    static async webpayCheck(userId) {
        Modal.loading.show();
        const r = await axios.post('webpay/check', { id: userId });
        Modal.loading.dismiss();
        return r.data;
    }

    static async redeemCode(userId, code) {
        Modal.loading.show();
        const r = await axios.post('promocion/accepted', {
             id: userId, code: code 
        });
        Modal.loading.dismiss();
        return r;
    }

    static async checkAdminNotifications(userId) {
        const r = await axios.post('api-driver/admin-notifications/check', { id: userId });
        return r.data;
    }

    static uploadDocuments(file, params, success, error=null) {
        this.upload('api-driver/documents/upload', 'file', file, params, success, error);
    }

    static uploadAvatar(file, params) {
        return new Promise( (resolve, reject) => {
            this.upload('register/verificar', 'foto', file, params, resolve, reject);
        } );
    }

    static upload(uri, nameField, file, params, success, error=null) {

        file = Platform.OS === 'ios'
            ? file.replace('file://', '')
            : file;

        const formData = new FormData();
        for (let key in params) {
            const value = params[key];
            if (value==null) continue;
            formData.append(key, value.toString());
        }

        formData.append(nameField, {
            uri: file,
            name: 'photo.jpg',
            type: 'image/jpg',
        });

        const options = {
            headers: {
                "Accept": "application/json",
                "Content-Type": "multipart/form-data"
            }
        };

        Modal.loading.show();
        axios.post(uri, formData, options)
            .then( data => { if (success) success(data) })
            .catch( e => { if (error) error(e) })
            .finally( () => Modal.loading.dismiss() );

    }

    static async getOpentok(userId, mode) {
        const r = await axios.post('api-driver/opentok/get', {id: userId, mode: mode});
        return r.data;
    }

    static async getAdminNotifications(userId) {
        Modal.loading.show();
        const r = await axios.post('api-driver/admin-notifications/get', { id: userId });
        Modal.loading.dismiss();
        return r.data;
    }

    static async contactsCreate(params) {
        Modal.loading.show();
        const r = await axios.post('contacts/create', params);
        Modal.loading.dismiss();
        return r.data;
    }

    static async contactsEdit(params) {
        Modal.loading.show();
            const r = await axios.post('contacts/edit', params);
        Modal.loading.dismiss();
        return r.data;
    }

    static async contactsDelete(params) {
        Modal.loading.show();
            const r = await axios.post('contacts/delete', params);
        Modal.loading.dismiss();
        return r.data;
    }

    static async contactsGet(params) {
        Modal.loading.show();
        const r = await axios.post('contacts/get', params);
        Modal.loading.dismiss();
        return r.data;
    }

    static async getAllContacts(params) {
        Modal.loading.show();
        const r = await axios.post('contacts/get-all', params);
        Modal.loading.dismiss();
        return r.data;
    }

    static async createPanic(params) {
        const r = await axios.post('panic/create', params);
        return r.data;
    }

}

export default Api;