import { emit } from "./EventEmit";

export const Panic = {
  DELAY: 5000,
  emit() {
    emit("PANIC");
  }
};
