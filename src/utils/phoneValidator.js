const ER = /[0-9]{7}/;

class PhoneValidator {
    constructor(errorMessage: string, errorMessageChile: string) {
        this.errorMessage = errorMessage;
        this.errorMessageChile = errorMessageChile;
    }
    isValid(phone: string, codArea = null): boolean {
        const result = !!ER.exec(phone);
        if (!result && this.onError) this.onError(codArea != 56 ? this.errorMessage : this.errorMessageChile);
        return result;
    }
    onError(onError) {
        this.onError = onError;
    }
}
export default PhoneValidator;