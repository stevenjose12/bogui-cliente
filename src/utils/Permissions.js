import Permissions from 'react-native-permissions';

class PermissionController {
	
	static async check(permission) {
		const response = await Permissions.check(permission);
		return response==='authorized';
	}

	static async request(permission) {
		const response = await Permissions.request(permission);
		return response==='authorized';
	}

	static async register(permission) {
		const response = await Permissions.request(permission[0]);
		if(response == 'authorized'){
			const response2 = await Permissions.request(permission[1]);
			return response==='authorized';
		}
		return false;
	}

}

export default PermissionController;