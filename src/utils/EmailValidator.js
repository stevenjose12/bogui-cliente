class EmailValidator {

    constructor(errorMessage: string) {
        this.errorMessage = errorMessage;
    }

    isValid(email: string): boolean {
        const RE = new RegExp("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
        const result = RE.test(email);
        if (!result && this.onError) this.onError(this.errorMessage);
        return result;
    }

    onError(onError) {
        this.onError = onError;
    }

}

export default EmailValidator;