import { NativeModules, Platform } from 'react-native';

const AudioToggle = NativeModules.OTSessionManager;

const earpiece = () => {
	if (Platform.OS == 'android') {
		AudioToggle.earpiece();
	}
}

const speaker = () => {
	if (Platform.OS == 'android') {
		AudioToggle.speaker();
	}
}

export default {
    Earpiece: earpiece,
    Speaker: speaker
}