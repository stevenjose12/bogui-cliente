import ENV from "./env";
import axios from "axios";

export default axios.create({
    baseURL: ENV.BASE_API,
    timeout: 5000
});