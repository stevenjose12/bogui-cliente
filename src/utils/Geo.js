import Geocoder from 'react-native-geocoder';
import ENV from './env';

Geocoder.fallbackToGoogle(ENV.apiKey);

class Geo {

    static getRotation(lat1, lng1, lat2, lng2) {
        const dlat = lat1-lat2;
        const dlng = lng1-lng2;
        const m = dlng/dlat;
        let degrees = (Math.atan(m) * 180) / Math.PI;
        if (dlat>=0 && dlng>=0) degrees -= 180;
        if (dlat >= 0 && dlng < 0) degrees += 180;
        return degrees<0 ? 360+degrees : degrees;
    }

    static getDistanceBetweenCoordinates(latOrigin, lngOrigin, latDestiny, lngDestiny): number {
        const rad = x => x*Math.PI/180;
        const R = 6378.137; // radio de la tierra en km
        const dLat = rad(latOrigin-latDestiny);
        const dLng = rad(lngOrigin-lngDestiny);
        const a = Math.pow(Math.sin(dLat/2), 2)+Math.cos(latDestiny)*Math.cos(latOrigin)*Math.pow(Math.sin(dLng/2), 2);
        const c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return R*c;
    }

    static async getAddress(location): string {
        const latLng = { lat: location.latitude, lng: location.longitude };
        const address = await Geocoder.geocodePosition(latLng);
        return address[0].formattedAddress;
    }

}

export default Geo;