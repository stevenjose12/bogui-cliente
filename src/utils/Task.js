class Task {

    static period; // <------------------------------------------------------------------------------------------------ Periodo entre intervalos en ms, por defecto es de 250 ms
    static interval = null;
    static update = null;
    static events = [];

    static start(period = 125) {
        if (Task.interval!=null) return;
        Task.period = period;
        Task.update = () => {
            Task.events.forEach( (event: Event) => {
                event.updateTime(Task.period);
                if (event.time >= event.period) {
                    event.callback();
                    event.time = 0;
                }
            });
        };
        Task.interval = setInterval(Task.update, Task.period);
    }

    static stop() {
        clearInterval( Task.interval );
        Task.interval = null;
        Task.events = [];
    }

    static add(eventName: string, period: number, callback) {
        Task.events.push( new Event(eventName, period, callback) );
    }

    static remove(eventName: string) {
        const index = Task.events.findIndex( (event: Event) => event.eventName===eventName);
        if (index>=0) Task.events.splice(index, 1);
    }

}

class Event {

    eventName: string;
    period: number;
    time: number = 0;
    callback;

    constructor(eventName: string, period: number, callback) {
        this.eventName = eventName;
        this.period = period;
        this.callback = callback;
    }

    updateTime(period: number) {
        this.time += period;
    }

}

export default Task;