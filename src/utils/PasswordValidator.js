class PasswordValidator {

    minChar : number = 6;

    constructor(invalidPasswordErrorMessage: string, notMatchPasswordErrorMessage: string, notMinCharPasswordErrorMessage: string) {
        this.invalidPasswordErrorMessage = invalidPasswordErrorMessage;
        this.notMatchPasswordErrorMessage = notMatchPasswordErrorMessage;
        this.notMinCharPasswordErrorMessage = notMinCharPasswordErrorMessage;
    }

    validate(password: string, repeat: string) {

        //<editor-fold desc="Validando que se halla escrito algo">
        if (!password) {
            if (this.onError) this.onError(this.invalidPasswordErrorMessage);
            return false;
        }
        //</editor-fold>1

        //<editor-fold desc="Validando que ambas contraseñas coincidan">
        if (repeat) {
            if (password!==repeat) {
                if (this.onError) this.onError(this.notMatchPasswordErrorMessage);
                return false;
            }
        }
        //</editor-fold>

        //<editor-fold desc="Validando el número de caracteres">
        if (password.length<this.minChar) {
            if (this.onError) this.onError( this.notMinCharPasswordErrorMessage.replace("%d", this.minChar) );
            return false;
        }
        //</editor-fold>

        return true;

    }

    onError(onError) {
        this.onError = onError;
    }

}

export default PasswordValidator;