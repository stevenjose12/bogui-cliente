import Api from "./Api";
import EmailValidator from "./EmailValidator";
import PasswordValidator from "./PasswordValidator";
import PhoneValidator from './phoneValidator';
import uniqueHash from "unique-hash"
import CodeValidator from "./CodeValidator";
import Task from "./Task";
import numeral from "numeral";
import moment from 'moment';
import env from "./env";
import { Platform } from 'react-native';
import Notification from '../plugins/Notifications';

class G {

    static intoChatPage: false;

    static inCallPage = false;
    static minDate = moment().add(1,'month').startOf('month').format('DD/MM/YYYY');
    static maxDate = moment().add(10,'years').add(1,'month').startOf('month').format('DD/MM/YYYY');
    static task: Task;
    static key_infobip = "bGltb25ieXRlOjEyMzQ1Njc4OUFhLg==";
    static alert;
    static loading;
    static markersPoint = [
        require('../assets/icons/point_start.png'),
        require('../assets/icons/point_stop.png'),
        require('../assets/icons/point_goal.png')
    ];
    static markers = [
        require('../assets/icons/start_marker.png'),
        require('../assets/icons/stop_marker.png'),
        require('../assets/icons/goal_marker.png')
    ];
    static markersNight = [
        require('../assets/icons/start_marker_night.png'),
        require('../assets/icons/stop_marker_night.png'),
        require('../assets/icons/goal_marker_night.png')
    ];
    static night = null;

    static init() {
        Task.start();
    }

    static displayRadius: number = null;

    static async getDisplayRadius() {
        if (!this.displayRadius) G.displayRadius = await Api.getDisplayRadius();
        return G.displayRadius;
    }

    static getEmailValidator(): EmailValidator {
        return new EmailValidator("Email invalido");
    }

    static getPhoneValidator(): PhoneValidator {
        return new PhoneValidator(
            "El numero de teléfono debe contener solo números de 7-11 Carácteres",
            "El teléfono de teléfono debe contener 9 números"
        );
    }

    static getPasswordValidator(): PasswordValidator {
        return new PasswordValidator(
            "Contraseña invalida",
            "Contraseñas no coinciden",
            "debe contener al menos %d caracteres"
        );
    }

    static getCodeValidator(): CodeValidator {
        return new CodeValidator("Debes ingresar el código que te llegó al correo.");
    }

    static getHash(): string {
        const ERROR = 1564494299;
        const DELTA = new Date().getTime()-ERROR;
        return uniqueHash(DELTA, { format: "string" }).toUpperCase();
    }

    static sendMessage(phone, message) {

        const data = JSON.stringify({
          "from": "Bogui",
          "to": Array.isArray(phone) ? phone : '+'+phone,
          "text": message
        });

        const xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open("POST", "https://1knld.api.infobip.com/sms/2/text/single");
        xhr.setRequestHeader("authorization", "Basic " + this.key_infobip);
        xhr.setRequestHeader("content-type", "application/json");
        xhr.setRequestHeader("accept", "application/json");
        xhr.send(data);

    }

    static calcTariff(baseTariff, km, additional, airport = 0, multiple = 50) {
        let tariff = parseInt(baseTariff + (km * additional));
        let residue = tariff % multiple;

        if (residue !== 0) {
            do {
                tariff = tariff - 1;
                residue = tariff % multiple;
            } while (residue !== 0);
        }

        tariff += airport;
        return tariff;
    }

    static getTime(time: number): string {
        if (time > 60) {
            let h = Math.floor(time / 60);
            let m = Math.floor(time - (h * 60));
            let hor = h > 1 ? "Horas" : "Hora";
            return h + " " + hor + " y " + m + ( m>1 ? " minutos" : " minuto" );
        } else {
            let m = Math.floor(time);
            return m + ( m>1 ? " minutos" : " minuto" );
        }
    }

    static async getTimeBetweenCoords(origin, destination) {
        try {
            const directions = await Api.getDirections(origin, destination);
            return this.getTime(directions.duration / 60);
        } catch (e) {
            console.log(">>: G > getTimeBetweenCoords > error: ", e);
        }
    }

    static formatMoney(number) {
        return numeral(number).format("0,0.00");
    }

    static getVehicleTypeName(vehicle) {
        switch (vehicle) {
            case 1: return "Sedan";
            case 2: return "SUV";
            case 3: return "VAN";
        }
    }

    static getStaticMap(data: any = {}, address) {
        let markers = "";
        markers += '&markers=icon:' + env.BASE_URL_EXTERNAL + 'markers/red.png' + '|' + data.latitud_origin + ',' + data.longitud_origin;
        if (data.to_destinies.find(i => i.type == 1) != null)
            markers += '&markers=icon:' + env.BASE_URL_EXTERNAL + 'markers/green.png' + '|' + data.to_destinies.find(i => i.type == 1).latitud + ',' + data.to_destinies.find(i => i.type == 1).longitud;
        markers += '&markers=icon:' + env.BASE_URL_EXTERNAL + 'markers/blue.png' + '|' + data.to_destinies.find(i => i.type == 2).latitud + ',' + data.to_destinies.find(i => i.type == 2).longitud;
        return "https://maps.googleapis.com/maps/api/staticmap?size=150x120&path=enc:" + address + markers + "&key=" + env.apiKey
    }

    static isNight(night) {

        if (night) {
            const now = moment();
            const start = moment(night.start,'HH:mm:ss');
            const end = moment(night.end,'HH:mm:ss');
            return ( now>=start && now<=end || end<start && ( now>=start || now<=end ) );
        }
        return false;
    }

    static getPlacesFromRoute(route) {

        const locations = [];
        const address = [];
        const places = [];

        try {

            locations[0] = {
                latitude: parseFloat(route.latitud_origin),
                longitude: parseFloat(route.longitud_origin)
            };
            address[0] = route.description;

            if (route.to_destinies.length == 1) {
                locations[2] = {
                    latitude: parseFloat(route.to_destinies[0].latitud),
                    longitude: parseFloat(route.to_destinies[0].longitud)
                };
                address[2] = route.to_destinies[0].description;
            } else {
                for (let i = 0; i < route.to_destinies.length; i++) {
                    locations[i + 1] = {
                        latitude: parseFloat(route.to_destinies[i].latitud),
                        longitude: parseFloat(route.to_destinies[i].longitud)
                    };
                    address[i + 1] = route.to_destinies[i].description;
                }
            }

            for (let i=0; i<3; i++) places[i] = { location: locations[i], address: address[i] };

        } catch (e) {
            console.log(">>: G > getPlacesFromRoute > input: ", route, ", out: ", locations);
        }

        return { locations, address, places };

    }

    static hideLastFourDigits(s) {
        if (typeof s !== "string") return "";
        const len = s.length;
        if (len < 4) return "X".repeat(len);
        return s.slice(0, len - 4) + "X".repeat(4);
    }

    static showChatNotification(userId, data) {
        if (data.chat.users.map( i => i.user_id).indexOf(userId) !== -1 && data.user_id !== userId) {
            if (Platform.OS === 'android') {
                // Notification.showPayload("Bogui", "Tienes un nuevo mensaje", { page: "ChatDrawer" });
            } else {
                Notification.showPayload("Bogui", "Tienes un nuevo mensaje", { page: "ChatDrawer" });
            }
        }
    }

    static getVehicleImageByCategory(category: number) {
        return { uri: env.BASE_PUBLIC+'vehicle_type/bogui'+category+'.png' };

        // try {
        //     const r = await Api.vehiclesAvailable();
        //     if ( r.length>0 ) {
        //         const item = r.find( e => e.id ===category );
        //         out = { uri: env.BASE_PUBLIC+item.photo }
        //     }
        // } catch (e) {
        //     console.log(">>: G > getVehicleImageByCategory > error: ", e);
        // }
        // console.log(">>: G > getVehicleImageByCategory > out: ", out);
        // return out;

        // switch (this.props.vehicleType) {
        //     case 1: return require('../../assets/icons/bogui1.png');
        //     case 2: return require('../../assets/icons/bogui2.png');
        //     case 3: return require('../../assets/icons/bogui3.png');
        // }
        // return require('../../assets/icons/bogui1.png');

    }

    static async getVehicleNameByCategory(category: number) {
        try {
            const r = await Api.vehiclesAvailable();
            if ( r.length>0 ) {
                const item = r.find( e => e.id ===category );
                return item.name;
            }
        } catch (e) {
            console.log(">>: G > getVehicleImageByCategory > error: ", e);
            return '';
        }
    }

}

export default G;