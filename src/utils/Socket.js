import io from 'socket.io-client';
import ENV from './env';

export const socket = io(ENV.SOCKET, {
    reconnection: true,
    reconnectionDelay: 1000,
    reconnectionDelayMax: 5000,
    reconnectionAttempts: Infinity,
    forceNew: false,
    transports: ['websocket'],
    upgrade: false,
});