class CodeValidator {

    constructor(errorMessage: string) {
        this.errorMessage = errorMessage;
    }

    isValid(code: string): boolean {
        const result = !!code;
        if (!result && this.onError) this.onError(this.errorMessage);
        return result;
    }

    onError(onError) {
        this.onError = onError;
    }

}

export default CodeValidator;