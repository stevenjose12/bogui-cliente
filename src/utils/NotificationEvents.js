import {DeviceEventEmitter} from "react-native";

class NotificationEvents {

    constructor() {

        DeviceEventEmitter.addListener("payload", payload => {
            const getParams = payload => {
                const json = eval("("+ payload +")");
                return json.NativeMap;
            };
            const params = getParams(payload);
            if (this.onNotificationEventCallback) this.onNotificationEventCallback(params);
        } );

    }

    onNotificationEvent(callback) {
        this.onNotificationEventCallback = callback;
    }

}

export default new NotificationEvents();