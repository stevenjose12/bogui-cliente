import RNFetchBlob from 'rn-fetch-blob';
import { Platform } from 'react-native';

class DownloadFile {

    static download (urlFile, fileName = null, description = 'Se esta descargando un archivo', type = 'GET') {

        const splitExtension = urlFile.split('.');
        const extension = splitExtension[splitExtension.length - 1];
        const splitName = urlFile.split('/');
        const originalName = splitName[splitName.length - 1];

        const fileNameDownload = fileName
            ? `Bogui_${fileName}.${extension}`
            : originalName;

        const addAndroidDownloads = {
            useDownloadManager : true,
            notification: true,
            description,
            path: `${RNFetchBlob.fs.dirs.DownloadDir}/${fileNameDownload}`,
        };

        let config = {};

        if (Platform.OS === 'android') {
            config = {
                addAndroidDownloads
            }
        }

        return RNFetchBlob.config(config).fetch(type, urlFile);
    }

}

export default DownloadFile;