class Modal {

    static alert;
    static loading;

    static showGenericError(error=null) {
        this.loading.dismiss();
        this.alert.dismiss();
        this.alert.show( { message: "Disculpe ha ocurrido un error" } );
        if (error) console.log(">>: Modal > showGenericError > error: ", error);
    }
    
    static showAlert(message: string, callback = null) {
        this.loading.dismiss();
        this.alert.dismiss();
        this.alert.show( { message: message } );
        this.alert.onPositiveButton("Aceptar", callback)
    }

}

export default Modal;