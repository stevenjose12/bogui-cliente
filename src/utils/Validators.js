import Validator from "./Validator";

const REQUIRE_EMAIL_MESSAGE = 'EL email es requerido.';
const REQUIRE_PASSWORD_MESSAGE = 'La contraseña es requerida.';
const EMAIL_MESSAGE = 'Debe introducir un email valido';
const PASSWORD_MIN = 'La contraseña debe contener al menos %x caracteres';
const PASSWORD_NOT_MATCH = 'Contraseñas no coinciden';
const REQUIRE_FIELD = 'Campo requerido';
const ONLY_NUMBERS = 'Solo se admiten números en este campo';
const PHONE_MESSAGE = 'Debe intruducir un número telefonico valido.';
const REQUIRE_PHOTO = 'Foto es requerida';

const expirationDate = new Validator()
    .notEmpty(REQUIRE_FIELD);

const phoneCodeValidator = new Validator()
    .notEmpty(PHONE_MESSAGE);

const avatarValidator = new Validator()
    .notEmpty(REQUIRE_PHOTO);

const phoneValidator = new Validator()
    .notEmpty(REQUIRE_FIELD)
    .onlyNumber(ONLY_NUMBERS)
    .minLength(9, PHONE_MESSAGE);

const nameValidator = new Validator()
    .notEmpty(REQUIRE_FIELD);

const lastNameValidator = new Validator()
    .notEmpty(REQUIRE_FIELD);

const passwordConfirmValidator = new Validator()
    .match(PASSWORD_NOT_MATCH)
    .notEmpty(REQUIRE_FIELD)
    .minLength(6, PASSWORD_MIN);

const documentValidator = new Validator()
    .notEmpty(REQUIRE_FIELD);

const passwordValidator = new Validator()
    .notEmpty(REQUIRE_PASSWORD_MESSAGE)
    .minLength(6, PASSWORD_MIN);

const emailValidator = new Validator()
    .notEmpty(REQUIRE_EMAIL_MESSAGE)
    .isEmail(EMAIL_MESSAGE);

export {
    expirationDate,
    phoneCodeValidator,
    avatarValidator,
    documentValidator,
    phoneValidator,
    nameValidator,
    lastNameValidator,
    passwordConfirmValidator,
    passwordValidator,
    emailValidator
}