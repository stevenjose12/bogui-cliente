import { socket } from "./Socket";
import NetInfo from "@react-native-community/netinfo";

class SocketEvents {
  netInfo = null;

  constructor() {
    this.netInfo = NetInfo.addEventListener(state => {
      if (this.onChangeNetworkCallback) this.onChangeNetworkCallback(state);
    });

    socket.on("driver-position", data => {
      if (this.onDriverPositionCallback) this.onDriverPositionCallback(data);
    });

    socket.on("send-message", data => {
      if (this.onSendMessageCallback) this.onSendMessageCallback(data);
    });

    socket.on("admin-notification", data => {
      if (this.onAdminNotificationCallback)
        this.onAdminNotificationCallback(data);
    });

    socket.on("notification", data => {
      if (this.onNotificationCallback) this.onNotificationCallback(data);
    });

    socket.on("disconnect", data => {
      if (this.onSocketDisconnectCallback)
        this.onSocketDisconnectCallback(data);
    });

    socket.on("llamada-colgada", data => {
      if (this.onSocketEndCallCallback) this.onSocketEndCallCallback(data);
    });

    socket.on("llamada", data => {
      if (this.onSocketInCallCallback) this.onSocketInCallCallback(data);
    });

    socket.on("driver-deleted", data => {
      if (this.onDriverDeletedCallback) this.onDriverDeletedCallback(data);
    });

    socket.on("usuario-desactivado", data => {
      if (this.disableUserCallback) this.disableUserCallback(data);
    });

    socket.on("carrera-finalizada", data => {
      if (this.finishedCareerCallback) this.finishedCareerCallback(data);
    });

    socket.on("carrera-aceptada", data => {
      if (this.onCareerAcceptedCallback) this.onCareerAcceptedCallback(data);
    });
  }

  onCareerAccepted(callback) {
    this.onCareerAcceptedCallback = callback;
  }

  onFinishedCareer(callback) {
    this.finishedCareerCallback = callback;
  }

  onDisableUser(callback) {
    this.disableUserCallback = callback;
  }

  onChangeNetwork(callback) {
    this.onChangeNetworkCallback = callback;
  }

  onDriverPosition(callback) {
    this.onDriverPositionCallback = callback;
  }

  onSendMessage(callback) {
    this.onSendMessageCallback = callback;
  }

  onAdminNotification(callback) {
    this.onAdminNotificationCallback = callback;
  }

  onNotification(callback) {
    this.onNotificationCallback = callback;
  }

  onSocketDisconnect(callback) {
    this.onSocketDisconnectCallback = callback;
  }

  onSocketEndCall(callback) {
    this.onSocketEndCallCallback = callback;
  }

  onSocketInCall(callback) {
    this.onSocketInCallCallback = callback;
  }

  onDriverDeleted(callback) {
    this.onDriverDeletedCallback = callback;
  }

  clear() {
    // socket.removeAllListeners();
    if (!this.netInfo) return;
    this.netInfo();
    this.netInfo = null;
  }
}

export default new SocketEvents();
