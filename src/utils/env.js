let data = {};

const Mode = {
	local: 1,
	production: 2,
	localServer: 3,
	kelvin: 4,
	localhost: 5,
	steven: 6,
	scorpion: 7,
	productionReact: 8,
	localnode: 9
};

const mode = Mode.productionReact;

switch(mode) {

	case Mode.local:
		data = {
			BASE_PUBLIC: 'http://192.168.1.4/Boggui/public/',
			BASE_API: 'http://192.168.1.4/Boggui/public/api/',
			SOCKET: 'http://192.168.1.4:11018'
		};
		break;

	case Mode.localServer:
		data = {
			BASE_PUBLIC: 'http://200.44.165.156/Boggui/public/',
			BASE_API: 'http://200.44.165.156/Boggui/public/api/',
			SOCKET: 'http://200.44.165.156:11018'
		};
		break;

	case Mode.production:
		data = {
			BASE_PUBLIC: 'https://bogui.cl/',
			BASE_API: 'https://bogui.cl/api/',
			SOCKET: 'https://bogui.cl:11019'
		};
		break;

	case Mode.productionReact:
		data = {
			BASE_PUBLIC: 'https://bogui.cl/',
			BASE_API: 'https://bogui.cl/api/',
			SOCKET: 'https://bogui.cl:11021'
		};
		break;

	case Mode.kelvin:
		data = {
			BASE_PUBLIC: 'http://192.168.1.103/Bogui/api/public/',
			BASE_API: 'http://192.168.1.103/Bogui/api/public/api/',
			SOCKET: 'http://192.168.1.103:11018'
		};
		break;

    case Mode.localhost:
		data = {
			BASE_PUBLIC: 'http://192.168.1.118/Bogui/public/',
			BASE_API: 'http://192.168.1.118/Bogui/public/api/',
			SOCKET: 'http://192.168.1.118:11018'
		};
		break;

	case Mode.steven:
		data = {
			BASE_PUBLIC: 'http://192.168.1.110/bogui/bogui-api/public/',
			BASE_API: 'http://192.168.1.110/bogui/bogui-api/public/api/',
			SOCKET: 'http://192.168.1.110:11020'
		};
		break;

	case Mode.scorpion:
		data = {
			BASE_PUBLIC: 'http://192.168.1.5/Boggui/public/',
			BASE_API: 'http://192.168.1.5/Boggui/public/api/',
			SOCKET: 'http://192.168.1.5:11020'
		};
		break;

	case Mode.localnode:
		data = {
			BASE_PUBLIC: 'http://192.168.1.118/Boggui/public/',
			BASE_API: 'http://192.168.1.118/Boggui/public/api/',
			SOCKET: 'http://192.168.1.118:8001'
		};
		break;

}

// data.apiKey = "AIzaSyCmfVIcUq5udO2xAwa4lwItPvdPJcFxdz4";
data.apiKey = "AIzaSyB2AYKCUTc1BJJaL_SVBT7TwTPI85lCW5A";
data.BASE_URL_EXTERNAL = 'https://bogui.cl/';
export default { ...data }
