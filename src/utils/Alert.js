import { Alert as AlertAndroid, AlertIOS, Platform } from 'react-native';

class Alert {
	
	static alert(title, message, buttons, options) {
		if (Platform.OS === 'android') {
			AlertAndroid.alert(title, message, buttons, options);
		} else {
			AlertIOS.alert(title, message, buttons, options);
		}
	}

}

export default Alert;