let events = [];

const on = (eventName, callback) => {
    if (typeof eventName !== 'string') return;
    if (typeof callback !== 'function') callback = null;
    const event = events.find( event => event.eventName === eventName);
    if (event) {
        event.callback = callback;
    } else {
        events.push({eventName, callback});
    }
};

const emit = (eventName, data = null) => {
    if (typeof eventName !== 'string') return;
    const event = events.find( event => event.eventName === eventName);
    if (event) event.callback?.(data);
};

const remove = eventName => {
    if (typeof eventName !== 'string') return;
    events = events.filter( event => event.eventName !==  eventName);
};

const clear = () => events = [];

export {
    on,
    emit,
    remove,
    clear
};