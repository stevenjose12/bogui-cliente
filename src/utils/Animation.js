class Animation {

    onUpdateCallback = null;
    onFinishCallback = null;
    interval = null;
    i = 0;
    t = 0;
    newTime = 0;
    alive: boolean = false;

    constructor(duration) {
        this.t = 0;
        this.i = 0;
        this.duration = duration;
        this.fps = 15;
        this.periode = Math.round(1000/this.fps);
        this.steps = Math.round(duration/this.periode);
    }

    start() {
        this.t = 0;
        this.i = 0;
        this.alive = true;
        this.testInterval();
    }

    testInterval() {

        setTimeout( () => {
            if (this.newTime===0) this.newTime = new Date().getTime();
            this.update();
            if (this.onUpdateCallback) this.onUpdateCallback(this);

            if (this.t>=this.duration) {
                this.alive = false;
                this.newTime = 0;
                if (this.onFinishCallback) this.onFinishCallback();
            } else {
                this.testInterval();
            }
            this.i++;
        }, this.periode );

    }

    onUpdate(onUpdateCallback) {
        this.onUpdateCallback = onUpdateCallback;
    }

    update() {
        this.oldTime = this.newTime;
        this.newTime = new Date().getTime();
        this.dt = this.newTime-this.oldTime;
        this.t += this.dt;
        this.normalize();
    }

    delta(size): number {
        const delta: number = this.tn*size;
        return delta;
    }

    fromTo(_from, _to) {
        return _from+this.delta(_to-_from);
    }

    normalize() {
        if ( this.t>=this.duration) this.t = this.duration;
        this.tn = this.t/this.duration;
        return this.tn;
    }

    onFinish(onFinishCallback) {
        this.onFinishCallback = onFinishCallback;
    }

}

export default Animation;