package com.limonbyte.bogui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import com.apamatesoft.corenative.Service;
import com.facebook.react.ReactActivity;
import org.devio.rn.splashscreen.SplashScreen;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactRootView;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;
import static com.apamatesoft.corenative.Service.service;

public class MainActivity extends ReactActivity {

    public static MainActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        activity = this;
        SplashScreen.show(this);
        super.onCreate(savedInstanceState);
        boolean permissionDisableKeyguard = ContextCompat.checkSelfPermission(this, Manifest.permission.DISABLE_KEYGUARD) == PackageManager.PERMISSION_GRANTED;
        if (!permissionDisableKeyguard) ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.DISABLE_KEYGUARD } , 0101);
        startService( new Intent(this, Service.class) );
    }

    @Override
    protected String getMainComponentName() {
        return "bogui";
    }

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new ReactActivityDelegate(this, getMainComponentName()) {
            @Override
            protected ReactRootView createRootView() {
                return new RNGestureHandlerEnabledRootView(MainActivity.this);
            }
        };
    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.invokeDefaultOnBackPressed();
        // moveTaskToBack(true);
    }

    @Override
    public void onBackPressed() {
        super.invokeDefaultOnBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (Build.VERSION.SDK_INT>=23 && Settings.canDrawOverlays( this ) ) {
            if (service==null) startService( new Intent(this, Service.class) );
            service.initLocationService();
        }
    }


}