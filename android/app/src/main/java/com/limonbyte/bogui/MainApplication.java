package com.limonbyte.bogui;

import android.app.Application;
import com.facebook.react.ReactApplication;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;
import com.sieuthai.RNUnlockDevicePackage;
import com.github.wumke.RNLocalNotifications.RNLocalNotificationsPackage;
import com.zmxv.RNSound.RNSoundPackage;
import com.zxcpoiu.incallmanager.InCallManagerPackage;
import com.opentokreactnative.OTPackage;
import com.vinzscam.reactnativefileviewer.RNFileViewerPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.devfd.RNGeocoder.RNGeocoderPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.imagepicker.ImagePickerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.airbnb.android.react.maps.MapsPackage;
import com.marianhello.bgloc.react.BackgroundGeolocationPackage;
import java.util.Arrays;
import java.util.List;

import cl.json.RNSharePackage;


public class MainApplication extends Application implements ReactApplication {

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                new MainReactPackage(),
                new FastImageViewPackage(),
                new NetInfoPackage(),
                new RNUnlockDevicePackage(),
                new RNLocalNotificationsPackage(),
                new RNSoundPackage(),
                new InCallManagerPackage(),
                new OTPackage(),
                new RNFileViewerPackage(),
                new RNCWebViewPackage(),
                new RNGeocoderPackage(),
                new RNDeviceInfo(),
                new RNFetchBlobPackage(),
                new ReanimatedPackage(),
                new ImagePickerPackage(),
                new VectorIconsPackage(),
                new SplashScreenReactPackage(),
                new RNGestureHandlerPackage(),
                new MapsPackage(),
                new RNSharePackage(),
                new com.apamatesoft.notifications.NotificationPackage(),
                new com.apamatesoft.launchforeground.LaunchForegroundPackage(),
                new com.apamatesoft.firebasepushnotification.FirebasePushNotificationPackage(),
                new com.apamatesoft.corenative.Package(),
                new BackgroundGeolocationPackage()
            );
        }

        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }

}