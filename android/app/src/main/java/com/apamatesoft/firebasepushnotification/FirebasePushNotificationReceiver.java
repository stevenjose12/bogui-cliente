package com.apamatesoft.firebasepushnotification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static com.apamatesoft.firebasepushnotification.FirebasePushNotificationMessage.message;

public class FirebasePushNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String payload = intent.getExtras().getString("payload");
        if (payload!=null && message!=null) message.action(payload);
    }

}