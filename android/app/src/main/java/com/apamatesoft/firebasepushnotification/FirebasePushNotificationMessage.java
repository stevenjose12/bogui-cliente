package com.apamatesoft.firebasepushnotification;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.apamatesoft.corenative.Storage;
import com.limonbyte.bogui.BuildConfig;
import com.limonbyte.bogui.MainActivity;
import com.limonbyte.bogui.R;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.firebase.messaging.RemoteMessage;
import org.json.JSONObject;
import java.util.List;
import com.google.firebase.messaging.FirebaseMessagingService;
import static com.apamatesoft.firebasepushnotification.FirebasePushNotificationModule.module;
import static com.apamatesoft.notifications.Notification.CHANNEL;

public class FirebasePushNotificationMessage extends FirebaseMessagingService {

    public Storage storage;
    public static FirebasePushNotificationMessage message;

    private static final String title = "Bogui";

    @Override
    public void onCreate() {
        super.onCreate();
        storage = Storage.getInstance(this);
        message = this;
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        try {

            if (remoteMessage==null) return;

            if (remoteMessage.getData()!=null && remoteMessage.getData().get("payload")!=null) {

                String payload = remoteMessage.getData().get("payload"); // <---------------------- Se obtiene el payload del remoteMessage. esté payload contiene un string del json contenido en la socket.
                JSONObject json = new JSONObject(payload); // <------------------------------------ Se convierte el string con el json en un objeto JSONObject.
                String eventName = json.getString("eventName"); // <------------------------------- Se obtiene el eventName.


                switch (eventName) {

                    case "notification":
                        Notification notification = getNotification( json.getInt("type") );
                        showPayload(title, notification.message, notification.page);
                        break;

                    case "admin-notification":
                        if (json.getInt("type")!=2) showPayload(title, "Nueva notificación del administrador", "Home");
                        break;

                    case "send-message":
                        if (isExecute()) {
                            if (!module.intoChats) showPayload(title, "Tienes un nuevo mensaje", "ChatDrawer");
                        } else {
                            showPayload(title, "Tienes un nuevo mensaje", "ChatDrawer");
                        }
                        break;

                }

            }

        } catch (Exception e) {
            System.out.println(">>: FirebasePushNotificationMessage > onMessageReceived > error: "+e.getMessage());
        }

    }

    private Notification getNotification(int type) {

        switch (type) {

            // Carrera Aceptada
            case 1: return new Notification("El viaje ha sido aceptado", "Home");

            // Destino Modificado
            case 2: return new Notification("El viaje ha sido finalizado", "Home");

            // Carrera Cancelada
            case 3: return new Notification( "El viaje ha sido cancelado", "Home");

            // Boggui ha llegado al punto de encuentro
            case 4: return new Notification("tu Bogui ya ha llegado","Home");

            // Documentos Requeridos
            case 6: return new Notification("Su cuenta requiere atención", "PendingDocuments");

            // Documentos Aceptados
            case 7: return new Notification("Sus documentos fueron aceptados", "PendingDocuments");

            // Documentos Rechazads
            case 8: return new Notification("Uno o mas de sus documentos fueron rechazados", "PendingDocuments");

            default: return null;

        }

    }

    class Notification {

        String message;
        String page;

        Notification(String message, String page) {
            this.message = message;
            this.page = page;
        }

    }

    public void showPayload(String title, String msg, String page) {

        if (!storage.isLogin()) return;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = CHANNEL;
            String description = CHANNEL;
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        Intent intent = new Intent(this, FirebasePushNotificationReceiver.class);
        intent.putExtra("payload", "{ 'page': '"+page+"' }");
        int id = (int) (Math.random()*1000);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL)
            .setSmallIcon(R.drawable.bogui)
            .setContentTitle(title)
            .setContentText(msg)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(id, builder.build());
    }

    public void action(String payload) {

        setPayload(payload);

        if (!isExecute()) {
            final Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity( intent );
            new Thread( () -> {
                try {
                    Thread.sleep(2000);
                    module.getReactContext()
                        .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                        .emit("FirebasePushNotificationPayload", payload);
                } catch (Exception e) { }
            } ).start();
        } else {
            module.getReactContext()
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit("FirebasePushNotificationPayload", payload);
        }
    }

    private boolean isExecute() {
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processInfos = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo info : processInfos) {
            if (BuildConfig.APPLICATION_ID.equalsIgnoreCase(info.processName) && ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND == info.importance)
                return true;
        }
        return false;
    }

    public void setPayload(String payload) {
        if (!isExecute()) storage.setPayload(payload);
    }

}