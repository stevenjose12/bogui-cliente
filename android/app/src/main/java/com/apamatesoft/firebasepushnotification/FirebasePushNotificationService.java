package com.apamatesoft.firebasepushnotification;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static com.apamatesoft.firebasepushnotification.FirebasePushNotificationModule.module;

public class FirebasePushNotificationService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        if (module!=null) module.onRefreshToken(FirebaseInstanceId.getInstance().getToken());
    }

}