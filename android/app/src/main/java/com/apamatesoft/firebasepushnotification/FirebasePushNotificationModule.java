package com.apamatesoft.firebasepushnotification;

import com.apamatesoft.corenative.Storage;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.firebase.iid.FirebaseInstanceId;

public class FirebasePushNotificationModule extends ReactContextBaseJavaModule {

    private Storage storage;
    public static FirebasePushNotificationModule module;
    public boolean intoChats = false;

    public FirebasePushNotificationModule(ReactApplicationContext reactContext) {
        super(reactContext);
        storage = Storage.getInstance(reactContext);
        module = this;
    }

    @Override
    public String getName() {
        return "FirebasePushNotificationModule";
    }

    @ReactMethod
    public void getToken(Callback callback){
        callback.invoke( FirebaseInstanceId.getInstance().getToken() );
    }

    @ReactMethod
    public void setIntoChats(boolean intoChats) {
        this.intoChats = intoChats;
    }

    @ReactMethod
    public void getPayload(Callback callback) {
        callback.invoke( storage.getPayload() );
    }

    @ReactMethod
    public void clearPayload() {
        storage.clearPayload();
    }

    @ReactMethod
    public void setLogin(boolean isLogin) {
        storage.setLogin(isLogin);
    }

    public void onRefreshToken(String token){
        getReactApplicationContext()
            .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
            .emit("firebaseprushnotificacionrefreshtoken", token);
    }

    public ReactApplicationContext getReactContext() {
        return getReactApplicationContext();
    }

}