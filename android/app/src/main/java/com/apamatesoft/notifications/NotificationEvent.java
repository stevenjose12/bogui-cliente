package com.apamatesoft.notifications;

public interface NotificationEvent {

    void action(String action);

    void destroy();

}
