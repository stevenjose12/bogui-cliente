package com.apamatesoft.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationReceiver extends BroadcastReceiver {

    private static NotificationEvent notificationEvent;

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String payload = intent.getExtras().getString("payload");
            if (payload!=null && notificationEvent!=null) notificationEvent.action(payload);
        } catch (Exception e) {
            System.out.println(">>: NotificationReceiver > onReceiver > error: "+e.getMessage());
        }
    }

    public static void setNotificationEvent(NotificationEvent notificationEvent) {
        NotificationReceiver.notificationEvent = notificationEvent;
    }

}