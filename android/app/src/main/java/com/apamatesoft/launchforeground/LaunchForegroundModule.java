package com.apamatesoft.launchforeground;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

import com.limonbyte.bogui.MainActivity;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.limonbyte.bogui.BuildConfig;

import java.util.List;

import javax.annotation.Nonnull;

import static android.content.Context.ACTIVITY_SERVICE;

public class LaunchForegroundModule extends ReactContextBaseJavaModule {

    private Context context;

    public LaunchForegroundModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        context = getReactApplicationContext();
    }

    @Nonnull
    @Override
    public String getName() {
        return "LaunchForegroundModule";
    }

    @ReactMethod
    public void launch() {
        if (!isForeground()) {
            final Intent intent = new Intent(context, MainActivity.class);
            intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
            context.startActivity( intent );
        }
    }

    private boolean isForeground() {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processInfos = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo info : processInfos) {
            if (BuildConfig.APPLICATION_ID.equalsIgnoreCase(info.processName) && ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND == info.importance)
                return true;
        }
        return false;
    }

}
