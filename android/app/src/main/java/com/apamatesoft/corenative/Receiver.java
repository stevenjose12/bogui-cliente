package com.apamatesoft.corenative;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

import com.apamatesoft.corenative.pojos.LatLng;
import com.google.android.gms.location.LocationResult;

public class Receiver extends BroadcastReceiver {

    private static Storage storage;
    private LatLng latLng;

    @Override
    public void onReceive(Context context, Intent intent) {

        storage = Storage.getInstance(context);

        if (latLng==null) latLng = new LatLng();

        try {
            final boolean hasResult = LocationResult.hasResult(intent);
            if (hasResult) {
                final LocationResult locationResult = LocationResult.extractResult(intent);
                final Location location = locationResult.getLastLocation();
                storage.setLatLng( latLng.setLatLng( location ) );
            }
        } catch (Exception e) {
            System.out.println(">>: Receiver > onRceceiver > error: "+e.getMessage());
        }
    }

}
