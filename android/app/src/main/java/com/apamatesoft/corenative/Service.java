package com.apamatesoft.corenative;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;

import com.apamatesoft.corenative.pojos.SocketPosition;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.URISyntaxException;
import static com.limonbyte.bogui.MainActivity.activity;

public class Service extends android.app.Service {

    private static final int
        PERIOD_THREAD = 1000,
        ACCESS_FINE_LOCATION = 1212;

    private static Storage storage;
    private static Thread thread;
    private static Socket socket;
    public  static Service service;
    private FusedLocationProviderClient fusedLocation;
    private PendingIntent pendingIntent;
    private SocketPosition socketPosition;

    @Override
    public void onCreate() {
        super.onCreate();
        service = this;
        storage = Storage.getInstance(this);
    }

    public void start(int userId, String baseUrlSocket) {
        storage.setEnable(true);
        storage.setUserId(userId);
        storage.setBaseUrlSocket( baseUrlSocket );
        if (socketPosition==null) socketPosition = new SocketPosition(userId);
        initSocket();
        initThread();
        initLocationService();
    }

    public void stop() {
        storage.setEnable(false);
    }

    public void initSocket() {
        if (socket!=null) return;
        try {
            socket = IO.socket( storage.getBaseUrlSocket() );
            socket.connect();
        } catch (URISyntaxException e) {
            System.out.println(">>: Service > initSocket > error: "+e.getMessage());
        }
    }

    public void initThread() {

        if (thread==null || !thread.isAlive()) {

            thread = new Thread( () -> {
                while (storage.getEnable()) {
                    try {
                        if (socket!=null) {
                            sendSocketPosition(  );
                        } else {
                            initSocket();
                        }
                        Thread.sleep( PERIOD_THREAD );
                    } catch (Exception e) {
                        System.out.println(">>: Service > initThread > error: "+e.getMessage());
                    }
                }
                dispose();
            } );
            thread.start();
        }

    }

    private void sendSocketPosition() {
        try {
            socketPosition.setLatLng( storage.getLatLng() );
            final JSONObject json = new JSONObject( socketPosition.toString() );
            socket.emit("position_emergency", json );
        } catch (JSONException e) {
            System.out.println(">>: Service > sendSocketPosition > error: "+e.getMessage());
        }
    }

    public void initLocationService() {

        if (fusedLocation!=null) return;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) {

            final String
                PACKAGE_NAME = getPackageName(),
                NAME_APP = getResources().getString( getResources().getIdentifier("app_name", "string", PACKAGE_NAME) ),
                CHANNEL = "channel";

            final int
                ICON_ID = getResources().getIdentifier("taxi", "drawable", PACKAGE_NAME),
                PERIOD = 1000;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

                NotificationChannel channel = new NotificationChannel(CHANNEL, NAME_APP, NotificationManager.IMPORTANCE_NONE);
                channel.setLightColor(Color.BLUE);
                channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
                NotificationManager manager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.createNotificationChannel(channel);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL);
                Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(ICON_ID)
                    .setContentTitle(NAME_APP +" se está ejecutando en segundo plano")
                    .setPriority(NotificationManager.IMPORTANCE_MAX)
                    .setCategory(Notification.CATEGORY_SERVICE)
                    .build();
                startForeground(2, notification);

            }

            fusedLocation = LocationServices.getFusedLocationProviderClient( this );

            final LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setInterval(PERIOD/2);
            locationRequest.setFastestInterval(PERIOD/2);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            try {

                //<editor-fold desc="BACKGROUND">
                Intent backgroundIntent = new Intent(this, Receiver.class);
                pendingIntent = PendingIntent.getBroadcast(
                        getApplicationContext(),
                        14872,
                        backgroundIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );
                fusedLocation.requestLocationUpdates(locationRequest, pendingIntent);
                //</editor-fold>

            } catch (Exception e) {
                System.out.println(">>: Service > initLocationService > error: " + e.getMessage());
            }

        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION);
        }

    }

    private void dispose() {
        if (socket!=null) socket.disconnect();
        if (fusedLocation!=null) fusedLocation.removeLocationUpdates(pendingIntent);
//        if (storage!=null) storage.clearAll();
        socket = null;
        thread = null;
        fusedLocation = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dispose();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}