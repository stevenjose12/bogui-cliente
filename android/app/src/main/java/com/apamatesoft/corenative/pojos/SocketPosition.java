package com.apamatesoft.corenative.pojos;

import com.google.gson.Gson;

import java.io.Serializable;

public class SocketPosition implements Serializable {

    private LatLng latLng;
    private int id;

    public SocketPosition(int id) {
        this.id = id;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}