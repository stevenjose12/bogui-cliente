package com.apamatesoft.corenative;

import android.content.Context;
import android.content.Intent;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import javax.annotation.Nonnull;
import static com.apamatesoft.corenative.Service.service;

public class Module extends ReactContextBaseJavaModule {

    private static Storage storage;
    public static Module module;
    private Context context;

    public Module(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        storage = Storage.getInstance( reactContext.getBaseContext() );
        module = this;
        context = reactContext;
    }

    @Nonnull
    @Override
    public String getName() {
        return "CoreNative";
    }

    @ReactMethod
    public void start(int userId, String baseUrlSocket) {
        if (service==null) getCurrentActivity().startService( new Intent(getCurrentActivity(), Service.class) );
        service.start(userId, baseUrlSocket);
    }

    @ReactMethod
    public void stop() {
        service.stop();
    }

}