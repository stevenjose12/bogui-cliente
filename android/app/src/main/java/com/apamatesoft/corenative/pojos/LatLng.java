package com.apamatesoft.corenative.pojos;

import android.location.Location;

import com.google.gson.Gson;

import java.io.Serializable;

public class LatLng implements Serializable {

    private double lat;
    private double lng;

    public LatLng() { }

    public LatLng(double latitude, double longitude) {
        this.lat = latitude;
        this.lng = longitude;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public LatLng setLatLng(Location location) {
        this.lat = location.getLatitude();
        this.lng = location.getLongitude();
        return this;
    }


    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}