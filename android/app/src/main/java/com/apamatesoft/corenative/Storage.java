package com.apamatesoft.corenative;

import android.content.Context;
import com.apamatesoft.corenative.pojos.LatLng;
import com.apamatesoft.localstorage.LocalStorage;

public class Storage {

    private static final String
        KEY_PAYLOAD = "payload",
        KEY_LOGIN = "login",
        KEY_BASE_URL_SOCKET = "base_url_socket",
        KEY_USER_ID = "user_id",
        KEY_ENABLE = "enable",
        KEY_LAT_LNG = "lat_lng";

    private static final Storage instance = new Storage();

    private LocalStorage storage;

    public static Storage getInstance(Context context) {
        if (instance.storage!=null) return instance;
        instance.storage = LocalStorage.getInstance(context.getFilesDir());
        return instance;
    }

    //<editor-fold desc="PAYLOAD">
    public boolean setPayload(String payload) {
        try {
            return storage.write(payload, KEY_PAYLOAD);
        } catch (Exception e) {
            return false;
        }
    }

    public String getPayload() {
        try {
            return (String) storage.read(KEY_PAYLOAD);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean clearPayload() {
        return storage.clear(KEY_PAYLOAD);
    }
    //</editor-fold>

    //<editor-fold desc="LOGIN">
    public boolean setLogin(Boolean isLogin) {
        try {
            return storage.write(isLogin, KEY_LOGIN);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isLogin() {
        try {
            return (Boolean) storage.read(KEY_LOGIN);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean clearLogin() {
        return storage.clear(KEY_LOGIN);
    }
    //</editor-fold>

    //<editor-fold desc="CURRENT LOCATION">
    public boolean setLatLng(LatLng latLng) {
        try {
            return storage.write(latLng, KEY_LAT_LNG);
        } catch (Exception e) {
            return false;
        }
    }

    public LatLng getLatLng() {
        try {
            return (LatLng) storage.read(KEY_LAT_LNG);
        } catch (Exception e) {
            return new LatLng();
        }
    }

    public boolean clearLatLng(){
        return storage.clear(KEY_LAT_LNG);
    }
    //</editor-fold>

    //<editor-fold desc="ENABLE">
    public boolean setEnable(Boolean socketEmit){
        try {
            return storage.write(socketEmit, KEY_ENABLE);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean getEnable(){
        try {
            return (Boolean) storage.read(KEY_ENABLE);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean clearEnable(){
        return storage.clear(KEY_ENABLE);
    }
    //</editor-fold>

    //<editor-fold desc="BASE URL SOCKET">
    public boolean setBaseUrlSocket(String baseUrlSocket) {
        try {
            return storage.write(baseUrlSocket, KEY_BASE_URL_SOCKET);
        } catch (Exception e) {
            return false;
        }
    }

    public String getBaseUrlSocket() {
        try {
            return (String) instance.storage.read(KEY_BASE_URL_SOCKET);
        } catch (Exception e) {
            return "";
        }
    }

    public boolean clearBaseUrlSocket() {
        return storage.clear(KEY_BASE_URL_SOCKET);
    }
    //</editor-fold>

    //<editor-fold desc="USER ID">
    public boolean setUserId(Integer vehicleType) {
        try {
            return storage.write(vehicleType, KEY_USER_ID);
        } catch (Exception e) {
            return false;
        }
    }

    public Integer getUserId() {
        try {
            return (Integer) storage.read(KEY_USER_ID);
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean clearUserId() {
        try {
            return storage.clear(KEY_USER_ID);
        } catch (Exception e) {
            return false;
        }
    }
    //</editor-fold>

    public boolean clearAll() {
        return storage.clearAll();
    }

}