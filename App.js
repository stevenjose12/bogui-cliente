import React, { Component } from "react";
import {
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    StatusBar,
    View,
    YellowBox
} from "react-native";
import {
    setCustomText,
    setCustomTouchableHighlight,
    setCustomTouchableNativeFeedback,
    setCustomTouchableOpacity,
    setCustomTouchableWithoutFeedback
} from "react-native-global-props";
import SplashScreen from "react-native-splash-screen";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import Navigation from "./src/navigation/Navigation";
import CoreNative from "./src/plugins/CoreNative";
import { persistor, store } from "./src/store/Store";
import API from "./src/utils/Api";
import Environment from "./src/utils/env";
import { on, remove as off } from "./src/utils/EventEmit";
import G from "./src/utils/G";
import Geolocation from "./src/utils/Geolocation";
import Modal from "./src/utils/Modal";
import ModalAlertSmart from "./src/widgets/ModalAlertSmart";
import Permissions from "./src/utils/Permissions";
import { Panic } from "./src/utils/Panic";

const touchableProps = {
    onLongPress: Panic.emit,
    delayLongPress: Panic.DELAY
};

setCustomTouchableWithoutFeedback({
    ...touchableProps,
    touchSoundDisabled: true
});
setCustomTouchableHighlight(touchableProps);
setCustomTouchableOpacity(touchableProps);
setCustomTouchableNativeFeedback(touchableProps);

setCustomText({
    style: {
        fontFamily: "Poppins"
    }
});

export default class App extends Component {

    state = {
        modalVisible: true
    };

    constructor(props) {
        super(props);
        YellowBox.ignoreWarnings([
            "Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?"
        ]);
    }

    componentDidMount() {
        SplashScreen.hide();
        G.init();
        if (Platform.OS === "ios") StatusBar.setHidden(true);
        on("PANIC", this.panic);
    }

    componentWillUnmount() {
        off("PANIC");
    }

    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <View style={{ width: "100%", height: "100%" }}>
                        <React.Fragment>
                            <SafeAreaView style={{ height: "100%", width: "100%" }}>
                                {
                                    Platform.OS === "ios"
                                        ?
                                        <KeyboardAvoidingView
                                            behavior="padding"
                                            enabled
                                            style={{ height: "100%", width: "100%" }} >
                                            <Navigation />
                                        </KeyboardAvoidingView>
                                        :
                                        <Navigation />
                                }
                            </SafeAreaView>
                            <ModalAlertSmart
                                loading="Cargando ..."
                                ref={ modal => Modal.loading = modal } />
                            <ModalAlertSmart ref={ modal => Modal.alert = modal } />
                        </React.Fragment>
                    </View>
                </PersistGate>
            </Provider>
        );
    }

    panic = async () => {

        const dispatch = action => store.dispatch(action);

        const { auth: { user } } = store.getState();

        if (!user) return;

        try {

            let message = "Soy " + user.name + " " + user.lastname + " necesito ayuda";
            let newPermission = false;

            const response = await API.getAllContacts({ user_id: user.id });
            if (!response?.result) throw ">>: Background > panicEvent > resposen.result: false";

            const contacts = response.contacts.map(i => "+" + i.phoneCode + i.phone);
            if (!contacts?.length) throw ">>: Background > panicEvent > contacts: no esta definido o esta vacío";

            let location = Geolocation.getLatLng( Geolocation.getCurrentLocation() );

            const params = {
                user_id: user.id,
                latitude: location?.latitude,
                longitude: location?.longitude
            };

            const panic = await API.createPanic(params);

            if (!panic.result) throw ">>: Background > panicEvent > panic.result: false";

            dispatch({
                type: "SET_PANIC",
                payload: { inPanic: true, date: panic.date }
            });

            const granted = await Permissions.check("location");

            if (granted) {

                if (Platform.OS === "android") CoreNative.start(user.id, Environment.SOCKET);

                location = Geolocation.getCurrentLocation();
                if (!location) {
                    Geolocation.start();
                    newPermission = true;
                } else {
                    message = "Soy " + user.name + " " + user.lastname + " necesito ayuda. Ve mi ubicación: " + panic.url;
                    G.sendMessage(contacts, message);
                }
            } else if (!location && !granted) {
                newPermission = true;
                Permissions.request("location")
                    .then( granted => {
                        if (granted) Geolocation.start();
                    });
            }

            if (newPermission) {
                setTimeout(() => {
                    if (Platform.OS === "android")
                        CoreNative.start(user.id, Environment.SOCKET);
                    if (!location) {
                        location = Geolocation.getCurrentLocation();
                    } else {
                        message = "Soy " + user.name + " " + user.lastname + " necesito ayuda. Ve mi ubicación: " + panic.url;
                    }
                    G.sendMessage(contacts, message);
                }, 3500);
            }
            Modal.showAlert("La Alerta fue enviada exitosamente");
        } catch (e) {
            Modal.showGenericError(e);
        }

    };

}
